#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/media/eco/IECOService.h>

namespace android {

namespace media {

namespace eco {

class BpECOService : public ::android::BpInterface<IECOService> {
public:
  explicit BpECOService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpECOService() = default;
  ::android::binder::Status obtainSession(int32_t width, int32_t height, bool isCameraRecording, ::android::sp<::android::media::eco::IECOSession>* _aidl_return) override;
  ::android::binder::Status getNumOfSessions(int32_t* _aidl_return) override;
  ::android::binder::Status getSessions(::std::vector<::android::sp<::android::IBinder>>* _aidl_return) override;
};  // class BpECOService

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_H_
