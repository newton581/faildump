#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_INFO_LISTENER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_INFO_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/media/eco/IECOServiceInfoListener.h>

namespace android {

namespace media {

namespace eco {

class BpECOServiceInfoListener : public ::android::BpInterface<IECOServiceInfoListener> {
public:
  explicit BpECOServiceInfoListener(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpECOServiceInfoListener() = default;
  ::android::binder::Status getType(int32_t* _aidl_return) override;
  ::android::binder::Status getName(::android::String16* _aidl_return) override;
  ::android::binder::Status getECOSession(::android::sp<::android::IBinder>* _aidl_return) override;
  ::android::binder::Status onNewInfo(const ::android::media::eco::ECOData& newInfo) override;
};  // class BpECOServiceInfoListener

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_INFO_LISTENER_H_
