#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SESSION_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SESSION_H_

#include <android/media/eco/IECOServiceInfoListener.h>
#include <android/media/eco/IECOServiceStatsProvider.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <eco/ECOData.h>
#include <utils/StrongPointer.h>

namespace android {

namespace media {

namespace eco {

class IECOSession : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(ECOSession)
  enum  : int32_t {
    ERROR_PERMISSION_DENIED = 1,
    ERROR_ALREADY_EXISTS = 2,
    ERROR_ILLEGAL_ARGUMENT = 3,
    ERROR_DISCONNECTED = 4,
    ERROR_INVALID_OPERATION = 5,
    ERROR_UNSUPPORTED = 6,
  };
  virtual ::android::binder::Status addStatsProvider(const ::android::sp<::android::media::eco::IECOServiceStatsProvider>& provider, const ::android::media::eco::ECOData& config, bool* _aidl_return) = 0;
  virtual ::android::binder::Status removeStatsProvider(const ::android::sp<::android::media::eco::IECOServiceStatsProvider>& provider, bool* _aidl_return) = 0;
  virtual ::android::binder::Status addInfoListener(const ::android::sp<::android::media::eco::IECOServiceInfoListener>& listener, const ::android::media::eco::ECOData& config, bool* _aidl_return) = 0;
  virtual ::android::binder::Status removeInfoListener(const ::android::sp<::android::media::eco::IECOServiceInfoListener>& listener, bool* _aidl_return) = 0;
  virtual ::android::binder::Status pushNewStats(const ::android::media::eco::ECOData& newStats, bool* _aidl_return) = 0;
  virtual ::android::binder::Status getWidth(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getHeight(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getIsCameraRecording(bool* _aidl_return) = 0;
  virtual ::android::binder::Status getNumOfListeners(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getNumOfProviders(int32_t* _aidl_return) = 0;
};  // class IECOSession

class IECOSessionDefault : public IECOSession {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status addStatsProvider(const ::android::sp<::android::media::eco::IECOServiceStatsProvider>& provider, const ::android::media::eco::ECOData& config, bool* _aidl_return) override;
  ::android::binder::Status removeStatsProvider(const ::android::sp<::android::media::eco::IECOServiceStatsProvider>& provider, bool* _aidl_return) override;
  ::android::binder::Status addInfoListener(const ::android::sp<::android::media::eco::IECOServiceInfoListener>& listener, const ::android::media::eco::ECOData& config, bool* _aidl_return) override;
  ::android::binder::Status removeInfoListener(const ::android::sp<::android::media::eco::IECOServiceInfoListener>& listener, bool* _aidl_return) override;
  ::android::binder::Status pushNewStats(const ::android::media::eco::ECOData& newStats, bool* _aidl_return) override;
  ::android::binder::Status getWidth(int32_t* _aidl_return) override;
  ::android::binder::Status getHeight(int32_t* _aidl_return) override;
  ::android::binder::Status getIsCameraRecording(bool* _aidl_return) override;
  ::android::binder::Status getNumOfListeners(int32_t* _aidl_return) override;
  ::android::binder::Status getNumOfProviders(int32_t* _aidl_return) override;

};

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SESSION_H_
