#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_STATS_PROVIDER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_STATS_PROVIDER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/media/eco/IECOServiceStatsProvider.h>

namespace android {

namespace media {

namespace eco {

class BpECOServiceStatsProvider : public ::android::BpInterface<IECOServiceStatsProvider> {
public:
  explicit BpECOServiceStatsProvider(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpECOServiceStatsProvider() = default;
  ::android::binder::Status getType(int32_t* _aidl_return) override;
  ::android::binder::Status getName(::android::String16* _aidl_return) override;
  ::android::binder::Status getECOSession(::android::sp<::android::IBinder>* _aidl_return) override;
};  // class BpECOServiceStatsProvider

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SERVICE_STATS_PROVIDER_H_
