#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_BN_E_C_O_SERVICE_INFO_LISTENER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_BN_E_C_O_SERVICE_INFO_LISTENER_H_

#include <binder/IInterface.h>
#include <android/media/eco/IECOServiceInfoListener.h>

namespace android {

namespace media {

namespace eco {

class BnECOServiceInfoListener : public ::android::BnInterface<IECOServiceInfoListener> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnECOServiceInfoListener

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_BN_E_C_O_SERVICE_INFO_LISTENER_H_
