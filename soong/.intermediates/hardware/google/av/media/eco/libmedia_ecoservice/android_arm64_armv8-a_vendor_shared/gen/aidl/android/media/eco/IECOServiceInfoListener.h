#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_INFO_LISTENER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_INFO_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <eco/ECOData.h>
#include <utils/String16.h>
#include <utils/StrongPointer.h>

namespace android {

namespace media {

namespace eco {

class IECOServiceInfoListener : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(ECOServiceInfoListener)
  enum  : int32_t {
    ERROR_PERMISSION_DENIED = 1,
    ERROR_ILLEGAL_ARGUMENT = 2,
    ERROR_INVALID_OPERATION = 3,
    ERROR_UNSUPPORTED = 4,
    INFO_LISTENER_TYPE_UNKNOWN = 1,
    INFO_LISTENER_TYPE_VIDEO_ENCODER = 2,
    INFO_LISTENER_TYPE_CAMERA = 3,
  };
  virtual ::android::binder::Status getType(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getName(::android::String16* _aidl_return) = 0;
  virtual ::android::binder::Status getECOSession(::android::sp<::android::IBinder>* _aidl_return) = 0;
  virtual ::android::binder::Status onNewInfo(const ::android::media::eco::ECOData& newInfo) = 0;
};  // class IECOServiceInfoListener

class IECOServiceInfoListenerDefault : public IECOServiceInfoListener {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status getType(int32_t* _aidl_return) override;
  ::android::binder::Status getName(::android::String16* _aidl_return) override;
  ::android::binder::Status getECOSession(::android::sp<::android::IBinder>* _aidl_return) override;
  ::android::binder::Status onNewInfo(const ::android::media::eco::ECOData& newInfo) override;

};

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_INFO_LISTENER_H_
