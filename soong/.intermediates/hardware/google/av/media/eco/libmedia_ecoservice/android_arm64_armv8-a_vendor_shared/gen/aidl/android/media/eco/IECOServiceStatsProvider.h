#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_STATS_PROVIDER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_STATS_PROVIDER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/String16.h>
#include <utils/StrongPointer.h>

namespace android {

namespace media {

namespace eco {

class IECOServiceStatsProvider : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(ECOServiceStatsProvider)
  enum  : int32_t {
    ERROR_PERMISSION_DENIED = 1,
    ERROR_ILLEGAL_ARGUMENT = 2,
    ERROR_INVALID_OPERATION = 3,
    ERROR_UNSUPPORTED = 4,
    STATS_PROVIDER_TYPE_UNKNOWN = 1,
    STATS_PROVIDER_TYPE_VIDEO_ENCODER = 2,
    STATS_PROVIDER_TYPE_CAMERA = 3,
  };
  virtual ::android::binder::Status getType(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getName(::android::String16* _aidl_return) = 0;
  virtual ::android::binder::Status getECOSession(::android::sp<::android::IBinder>* _aidl_return) = 0;
};  // class IECOServiceStatsProvider

class IECOServiceStatsProviderDefault : public IECOServiceStatsProvider {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status getType(int32_t* _aidl_return) override;
  ::android::binder::Status getName(::android::String16* _aidl_return) override;
  ::android::binder::Status getECOSession(::android::sp<::android::IBinder>* _aidl_return) override;

};

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_STATS_PROVIDER_H_
