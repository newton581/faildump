#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SESSION_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SESSION_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/media/eco/IECOSession.h>

namespace android {

namespace media {

namespace eco {

class BpECOSession : public ::android::BpInterface<IECOSession> {
public:
  explicit BpECOSession(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpECOSession() = default;
  ::android::binder::Status addStatsProvider(const ::android::sp<::android::media::eco::IECOServiceStatsProvider>& provider, const ::android::media::eco::ECOData& config, bool* _aidl_return) override;
  ::android::binder::Status removeStatsProvider(const ::android::sp<::android::media::eco::IECOServiceStatsProvider>& provider, bool* _aidl_return) override;
  ::android::binder::Status addInfoListener(const ::android::sp<::android::media::eco::IECOServiceInfoListener>& listener, const ::android::media::eco::ECOData& config, bool* _aidl_return) override;
  ::android::binder::Status removeInfoListener(const ::android::sp<::android::media::eco::IECOServiceInfoListener>& listener, bool* _aidl_return) override;
  ::android::binder::Status pushNewStats(const ::android::media::eco::ECOData& newStats, bool* _aidl_return) override;
  ::android::binder::Status getWidth(int32_t* _aidl_return) override;
  ::android::binder::Status getHeight(int32_t* _aidl_return) override;
  ::android::binder::Status getIsCameraRecording(bool* _aidl_return) override;
  ::android::binder::Status getNumOfListeners(int32_t* _aidl_return) override;
  ::android::binder::Status getNumOfProviders(int32_t* _aidl_return) override;
};  // class BpECOSession

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_BP_E_C_O_SESSION_H_
