#ifndef AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_H_
#define AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_H_

#include <android/media/eco/IECOSession.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace media {

namespace eco {

class IECOService : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(ECOService)
  enum  : int32_t {
    ERROR_PERMISSION_DENIED = 1,
    ERROR_ALREADY_EXISTS = 2,
    ERROR_ILLEGAL_ARGUMENT = 3,
    ERROR_DISCONNECTED = 4,
    ERROR_INVALID_OPERATION = 5,
    ERROR_UNSUPPORTED = 6,
  };
  virtual ::android::binder::Status obtainSession(int32_t width, int32_t height, bool isCameraRecording, ::android::sp<::android::media::eco::IECOSession>* _aidl_return) = 0;
  virtual ::android::binder::Status getNumOfSessions(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getSessions(::std::vector<::android::sp<::android::IBinder>>* _aidl_return) = 0;
};  // class IECOService

class IECOServiceDefault : public IECOService {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status obtainSession(int32_t width, int32_t height, bool isCameraRecording, ::android::sp<::android::media::eco::IECOSession>* _aidl_return) override;
  ::android::binder::Status getNumOfSessions(int32_t* _aidl_return) override;
  ::android::binder::Status getSessions(::std::vector<::android::sp<::android::IBinder>>* _aidl_return) override;

};

}  // namespace eco

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_ECO_I_E_C_O_SERVICE_H_
