#ifndef AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BN_PIXEL_POWER_STATS_PROVIDER_H_
#define AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BN_PIXEL_POWER_STATS_PROVIDER_H_

#include <binder/IInterface.h>
#include <android/vendor/powerstats/IPixelPowerStatsProvider.h>

namespace android {

namespace vendor {

namespace powerstats {

class BnPixelPowerStatsProvider : public ::android::BnInterface<IPixelPowerStatsProvider> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnPixelPowerStatsProvider

}  // namespace powerstats

}  // namespace vendor

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BN_PIXEL_POWER_STATS_PROVIDER_H_
