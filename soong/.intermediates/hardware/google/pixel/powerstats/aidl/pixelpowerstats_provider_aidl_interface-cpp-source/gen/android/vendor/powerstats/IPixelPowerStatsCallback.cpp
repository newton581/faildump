#include <android/vendor/powerstats/IPixelPowerStatsCallback.h>
#include <android/vendor/powerstats/BpPixelPowerStatsCallback.h>

namespace android {

namespace vendor {

namespace powerstats {

IMPLEMENT_META_INTERFACE(PixelPowerStatsCallback, "android.vendor.powerstats.IPixelPowerStatsCallback")

::android::IBinder* IPixelPowerStatsCallbackDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status IPixelPowerStatsCallbackDefault::getStats(::std::vector<::android::vendor::powerstats::StateResidencyData>*) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace powerstats

}  // namespace vendor

}  // namespace android
#include <android/vendor/powerstats/BpPixelPowerStatsCallback.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace android {

namespace vendor {

namespace powerstats {

BpPixelPowerStatsCallback::BpPixelPowerStatsCallback(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<IPixelPowerStatsCallback>(_aidl_impl){
}

::android::binder::Status BpPixelPowerStatsCallback::getStats(::std::vector<::android::vendor::powerstats::StateResidencyData>* stats) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeVectorSize(*stats);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* getStats */, _aidl_data, &_aidl_reply);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IPixelPowerStatsCallback::getDefaultImpl())) {
     return IPixelPowerStatsCallback::getDefaultImpl()->getStats(stats);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_status.readFromParcel(_aidl_reply);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  if (!_aidl_status.isOk()) {
    return _aidl_status;
  }
  _aidl_ret_status = _aidl_reply.readParcelableVector(stats);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace powerstats

}  // namespace vendor

}  // namespace android
#include <android/vendor/powerstats/BnPixelPowerStatsCallback.h>
#include <binder/Parcel.h>

namespace android {

namespace vendor {

namespace powerstats {

::android::status_t BnPixelPowerStatsCallback::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* getStats */:
  {
    ::std::vector<::android::vendor::powerstats::StateResidencyData> out_stats;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.resizeOutVector(&out_stats);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(getStats(&out_stats));
    _aidl_ret_status = _aidl_status.writeToParcel(_aidl_reply);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    if (!_aidl_status.isOk()) {
      break;
    }
    _aidl_ret_status = _aidl_reply->writeParcelableVector(out_stats);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace powerstats

}  // namespace vendor

}  // namespace android
