#include <android/vendor/powerstats/IPixelPowerStatsProvider.h>
#include <android/vendor/powerstats/BpPixelPowerStatsProvider.h>

namespace android {

namespace vendor {

namespace powerstats {

IMPLEMENT_META_INTERFACE(PixelPowerStatsProvider, "android.vendor.powerstats.IPixelPowerStatsProvider")

::android::IBinder* IPixelPowerStatsProviderDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status IPixelPowerStatsProviderDefault::registerCallback(const ::std::string&, const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

::android::binder::Status IPixelPowerStatsProviderDefault::unregisterCallback(const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace powerstats

}  // namespace vendor

}  // namespace android
#include <android/vendor/powerstats/BpPixelPowerStatsProvider.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace android {

namespace vendor {

namespace powerstats {

BpPixelPowerStatsProvider::BpPixelPowerStatsProvider(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<IPixelPowerStatsProvider>(_aidl_impl){
}

::android::binder::Status BpPixelPowerStatsProvider::registerCallback(const ::std::string& entityName, const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeUtf8AsUtf16(entityName);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeStrongBinder(::android::vendor::powerstats::IPixelPowerStatsCallback::asBinder(callback));
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* registerCallback */, _aidl_data, &_aidl_reply);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IPixelPowerStatsProvider::getDefaultImpl())) {
     return IPixelPowerStatsProvider::getDefaultImpl()->registerCallback(entityName, callback);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_status.readFromParcel(_aidl_reply);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  if (!_aidl_status.isOk()) {
    return _aidl_status;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

::android::binder::Status BpPixelPowerStatsProvider::unregisterCallback(const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeStrongBinder(::android::vendor::powerstats::IPixelPowerStatsCallback::asBinder(callback));
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 1 /* unregisterCallback */, _aidl_data, &_aidl_reply);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IPixelPowerStatsProvider::getDefaultImpl())) {
     return IPixelPowerStatsProvider::getDefaultImpl()->unregisterCallback(callback);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_status.readFromParcel(_aidl_reply);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  if (!_aidl_status.isOk()) {
    return _aidl_status;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace powerstats

}  // namespace vendor

}  // namespace android
#include <android/vendor/powerstats/BnPixelPowerStatsProvider.h>
#include <binder/Parcel.h>

namespace android {

namespace vendor {

namespace powerstats {

::android::status_t BnPixelPowerStatsProvider::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* registerCallback */:
  {
    ::std::string in_entityName;
    ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback> in_callback;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readUtf8FromUtf16(&in_entityName);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readStrongBinder(&in_callback);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(registerCallback(in_entityName, in_callback));
    _aidl_ret_status = _aidl_status.writeToParcel(_aidl_reply);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    if (!_aidl_status.isOk()) {
      break;
    }
  }
  break;
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 1 /* unregisterCallback */:
  {
    ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback> in_callback;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readStrongBinder(&in_callback);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(unregisterCallback(in_callback));
    _aidl_ret_status = _aidl_status.writeToParcel(_aidl_reply);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    if (!_aidl_status.isOk()) {
      break;
    }
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace powerstats

}  // namespace vendor

}  // namespace android
