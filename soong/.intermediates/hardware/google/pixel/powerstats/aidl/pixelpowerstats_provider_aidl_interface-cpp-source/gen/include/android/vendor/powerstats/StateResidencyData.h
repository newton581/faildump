#ifndef AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_STATE_RESIDENCY_DATA_H_
#define AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_STATE_RESIDENCY_DATA_H_

#include <binder/Parcel.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>

namespace android {

namespace vendor {

namespace powerstats {

class StateResidencyData : public ::android::Parcelable {
public:
  ::std::string state;
  int64_t totalTimeInStateMs;
  int64_t totalStateEntryCount;
  int64_t lastEntryTimestampMs;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class StateResidencyData

}  // namespace powerstats

}  // namespace vendor

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_STATE_RESIDENCY_DATA_H_
