#ifndef AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BP_PIXEL_POWER_STATS_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BP_PIXEL_POWER_STATS_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/vendor/powerstats/IPixelPowerStatsCallback.h>

namespace android {

namespace vendor {

namespace powerstats {

class BpPixelPowerStatsCallback : public ::android::BpInterface<IPixelPowerStatsCallback> {
public:
  explicit BpPixelPowerStatsCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpPixelPowerStatsCallback() = default;
  ::android::binder::Status getStats(::std::vector<::android::vendor::powerstats::StateResidencyData>* stats) override;
};  // class BpPixelPowerStatsCallback

}  // namespace powerstats

}  // namespace vendor

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BP_PIXEL_POWER_STATS_CALLBACK_H_
