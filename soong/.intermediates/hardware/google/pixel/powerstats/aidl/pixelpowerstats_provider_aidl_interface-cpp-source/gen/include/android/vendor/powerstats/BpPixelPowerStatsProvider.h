#ifndef AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BP_PIXEL_POWER_STATS_PROVIDER_H_
#define AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BP_PIXEL_POWER_STATS_PROVIDER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/vendor/powerstats/IPixelPowerStatsProvider.h>

namespace android {

namespace vendor {

namespace powerstats {

class BpPixelPowerStatsProvider : public ::android::BpInterface<IPixelPowerStatsProvider> {
public:
  explicit BpPixelPowerStatsProvider(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpPixelPowerStatsProvider() = default;
  ::android::binder::Status registerCallback(const ::std::string& entityName, const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) override;
  ::android::binder::Status unregisterCallback(const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) override;
};  // class BpPixelPowerStatsProvider

}  // namespace powerstats

}  // namespace vendor

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_BP_PIXEL_POWER_STATS_PROVIDER_H_
