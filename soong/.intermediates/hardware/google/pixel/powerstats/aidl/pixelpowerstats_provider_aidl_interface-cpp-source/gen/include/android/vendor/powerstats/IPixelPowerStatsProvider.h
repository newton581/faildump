#ifndef AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_I_PIXEL_POWER_STATS_PROVIDER_H_
#define AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_I_PIXEL_POWER_STATS_PROVIDER_H_

#include <android/vendor/powerstats/IPixelPowerStatsCallback.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <string>
#include <utils/StrongPointer.h>

namespace android {

namespace vendor {

namespace powerstats {

class IPixelPowerStatsProvider : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(PixelPowerStatsProvider)
  virtual ::android::binder::Status registerCallback(const ::std::string& entityName, const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) = 0;
  virtual ::android::binder::Status unregisterCallback(const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) = 0;
};  // class IPixelPowerStatsProvider

class IPixelPowerStatsProviderDefault : public IPixelPowerStatsProvider {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status registerCallback(const ::std::string& entityName, const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) override;
  ::android::binder::Status unregisterCallback(const ::android::sp<::android::vendor::powerstats::IPixelPowerStatsCallback>& callback) override;

};

}  // namespace powerstats

}  // namespace vendor

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_I_PIXEL_POWER_STATS_PROVIDER_H_
