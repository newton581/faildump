#ifndef AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_I_PIXEL_POWER_STATS_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_I_PIXEL_POWER_STATS_CALLBACK_H_

#include <android/vendor/powerstats/StateResidencyData.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace vendor {

namespace powerstats {

class IPixelPowerStatsCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(PixelPowerStatsCallback)
  virtual ::android::binder::Status getStats(::std::vector<::android::vendor::powerstats::StateResidencyData>* stats) = 0;
};  // class IPixelPowerStatsCallback

class IPixelPowerStatsCallbackDefault : public IPixelPowerStatsCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status getStats(::std::vector<::android::vendor::powerstats::StateResidencyData>* stats) override;

};

}  // namespace powerstats

}  // namespace vendor

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_VENDOR_POWERSTATS_I_PIXEL_POWER_STATS_CALLBACK_H_
