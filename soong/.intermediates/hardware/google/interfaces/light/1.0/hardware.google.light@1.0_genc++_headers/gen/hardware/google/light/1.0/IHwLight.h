#ifndef HIDL_GENERATED_HARDWARE_GOOGLE_LIGHT_V1_0_IHWLIGHT_H
#define HIDL_GENERATED_HARDWARE_GOOGLE_LIGHT_V1_0_IHWLIGHT_H

#include <hardware/google/light/1.0/ILight.h>

#include <android/hardware/light/2.0/BnHwLight.h>
#include <android/hardware/light/2.0/BpHwLight.h>
#include <android/hardware/light/2.0/hwtypes.h>

#include <hidl/Status.h>
#include <hwbinder/IBinder.h>
#include <hwbinder/Parcel.h>

namespace hardware {
namespace google {
namespace light {
namespace V1_0 {
}  // namespace V1_0
}  // namespace light
}  // namespace google
}  // namespace hardware

#endif  // HIDL_GENERATED_HARDWARE_GOOGLE_LIGHT_V1_0_IHWLIGHT_H
