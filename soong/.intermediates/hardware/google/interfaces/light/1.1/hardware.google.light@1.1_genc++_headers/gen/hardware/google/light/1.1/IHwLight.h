#ifndef HIDL_GENERATED_HARDWARE_GOOGLE_LIGHT_V1_1_IHWLIGHT_H
#define HIDL_GENERATED_HARDWARE_GOOGLE_LIGHT_V1_1_IHWLIGHT_H

#include <hardware/google/light/1.1/ILight.h>

#include <android/hardware/light/2.0/hwtypes.h>
#include <hardware/google/light/1.0/BnHwLight.h>
#include <hardware/google/light/1.0/BpHwLight.h>

#include <hidl/Status.h>
#include <hwbinder/IBinder.h>
#include <hwbinder/Parcel.h>

namespace hardware {
namespace google {
namespace light {
namespace V1_1 {
}  // namespace V1_1
}  // namespace light
}  // namespace google
}  // namespace hardware

#endif  // HIDL_GENERATED_HARDWARE_GOOGLE_LIGHT_V1_1_IHWLIGHT_H
