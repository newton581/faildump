#define LOG_TAG "android.hardware.wifi@1.2::Wifi"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/1.2/BpHwWifi.h>
#include <android/hardware/wifi/1.2/BnHwWifi.h>
#include <android/hardware/wifi/1.2/BsWifi.h>
#include <android/hardware/wifi/1.1/BpHwWifi.h>
#include <android/hardware/wifi/1.0/BpHwWifi.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace V1_2 {

const char* IWifi::descriptor("android.hardware.wifi@1.2::IWifi");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IWifi::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwWifi(static_cast<IWifi *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IWifi::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsWifi(static_cast<IWifi *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IWifi::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IWifi::descriptor);
};

// Methods from ::android::hardware::wifi::V1_0::IWifi follow.
// no default implementation for: ::android::hardware::Return<void> IWifi::registerEventCallback(const ::android::sp<::android::hardware::wifi::V1_0::IWifiEventCallback>& callback, registerEventCallback_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<bool> IWifi::isStarted()
// no default implementation for: ::android::hardware::Return<void> IWifi::start(start_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifi::stop(stop_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifi::getChipIds(getChipIds_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifi::getChip(uint32_t chipId, getChip_cb _hidl_cb)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IWifi::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::V1_2::IWifi::descriptor,
        ::android::hardware::wifi::V1_1::IWifi::descriptor,
        ::android::hardware::wifi::V1_0::IWifi::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IWifi::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifi::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::V1_2::IWifi::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifi::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){139,199,90,13,250,193,92,111,135,255,236,149,11,118,199,215,222,48,213,22,181,78,142,11,31,60,15,249,199,198,135,59} /* 8bc75a0dfac15c6f87ffec950b76c7d7de30d516b54e8e0b1f3c0ff9c7c6873b */,
        (uint8_t[32]){194,100,115,226,228,160,10,244,62,40,160,221,249,0,46,80,98,167,208,148,4,41,229,239,182,229,81,58,138,188,183,92} /* c26473e2e4a00af43e28a0ddf9002e5062a7d0940429e5efb6e5513a8abcb75c */,
        (uint8_t[32]){185,190,54,113,154,138,213,52,0,10,81,234,7,190,145,190,148,196,5,191,30,3,138,232,37,172,246,80,135,255,211,120} /* b9be36719a8ad534000a51ea07be91be94c405bf1e038ae825acf65087ffd378 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifi::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IWifi::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IWifi::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifi::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifi::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IWifi::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_2::IWifi>> IWifi::castFrom(const ::android::sp<::android::hardware::wifi::V1_2::IWifi>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_2::IWifi>> IWifi::castFrom(const ::android::sp<::android::hardware::wifi::V1_1::IWifi>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifi, ::android::hardware::wifi::V1_1::IWifi, BpHwWifi>(
            parent, "android.hardware.wifi@1.2::IWifi", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_2::IWifi>> IWifi::castFrom(const ::android::sp<::android::hardware::wifi::V1_0::IWifi>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifi, ::android::hardware::wifi::V1_0::IWifi, BpHwWifi>(
            parent, "android.hardware.wifi@1.2::IWifi", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_2::IWifi>> IWifi::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifi, ::android::hidl::base::V1_0::IBase, BpHwWifi>(
            parent, "android.hardware.wifi@1.2::IWifi", emitError);
}

BpHwWifi::BpHwWifi(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IWifi>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi@1.2", "IWifi") {
}


// Methods from ::android::hardware::wifi::V1_0::IWifi follow.
::android::hardware::Return<void> BpHwWifi::registerEventCallback(const ::android::sp<::android::hardware::wifi::V1_0::IWifiEventCallback>& callback, registerEventCallback_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifi::_hidl_registerEventCallback(this, this, callback, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwWifi::isStarted(){
    ::android::hardware::Return<bool>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifi::_hidl_isStarted(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::start(start_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifi::_hidl_start(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::stop(stop_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifi::_hidl_stop(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::getChipIds(getChipIds_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifi::_hidl_getChipIds(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::getChip(uint32_t chipId, getChip_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifi::_hidl_getChip(this, this, chipId, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwWifi::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwWifi::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwWifi::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifi::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwWifi::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwWifi::BnHwWifi(const ::android::sp<IWifi> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi@1.2", "IWifi") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwWifi::~BnHwWifi() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}


// Methods from ::android::hardware::wifi::V1_0::IWifi follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwWifi::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwWifi::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwWifi::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* registerEventCallback */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifi::_hidl_registerEventCallback(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* isStarted */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifi::_hidl_isStarted(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* start */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifi::_hidl_start(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* stop */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifi::_hidl_stop(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* getChipIds */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifi::_hidl_getChipIds(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* getChip */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifi::_hidl_getChip(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsWifi::BsWifi(const ::android::sp<::android::hardware::wifi::V1_2::IWifi> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi@1.2", "IWifi"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsWifi::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IWifi> IWifi::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwWifi>(serviceName, false, getStub);
}

::android::sp<IWifi> IWifi::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwWifi>(serviceName, true, getStub);
}

::android::status_t IWifi::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IWifi::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi@1.2::IWifi",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_2
}  // namespace wifi
}  // namespace hardware
}  // namespace android
