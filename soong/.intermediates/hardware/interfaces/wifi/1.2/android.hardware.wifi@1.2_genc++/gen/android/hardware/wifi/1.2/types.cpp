#define LOG_TAG "android.hardware.wifi@1.2::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/wifi/1.2/types.h>
#include <android/hardware/wifi/1.2/hwtypes.h>

namespace android {
namespace hardware {
namespace wifi {
namespace V1_2 {

::android::status_t readEmbeddedFromParcel(
        const NanDataPathConfirmInd &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_0::NanDataPathConfirmInd &>(obj.V1_0),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathConfirmInd, V1_0));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_channelInfo_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::wifi::V1_2::NanDataPathChannelInfo> &>(obj.channelInfo),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathConfirmInd, channelInfo), &_hidl_channelInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const NanDataPathConfirmInd &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.V1_0,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathConfirmInd, V1_0));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_channelInfo_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.channelInfo,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathConfirmInd, channelInfo), &_hidl_channelInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const NanDataPathScheduleUpdateInd &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_channelInfo_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::wifi::V1_2::NanDataPathChannelInfo> &>(obj.channelInfo),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathScheduleUpdateInd, channelInfo), &_hidl_channelInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_ndpInstanceIds_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint32_t> &>(obj.ndpInstanceIds),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathScheduleUpdateInd, ndpInstanceIds), &_hidl_ndpInstanceIds_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const NanDataPathScheduleUpdateInd &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_channelInfo_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.channelInfo,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathScheduleUpdateInd, channelInfo), &_hidl_channelInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_ndpInstanceIds_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.ndpInstanceIds,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_2::NanDataPathScheduleUpdateInd, ndpInstanceIds), &_hidl_ndpInstanceIds_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_2
}  // namespace wifi
}  // namespace hardware
}  // namespace android
