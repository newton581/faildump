#define LOG_TAG "android.hardware.wifi@1.3::WifiChip"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/1.3/BpHwWifiChip.h>
#include <android/hardware/wifi/1.3/BnHwWifiChip.h>
#include <android/hardware/wifi/1.3/BsWifiChip.h>
#include <android/hardware/wifi/1.2/BpHwWifiChip.h>
#include <android/hardware/wifi/1.1/BpHwWifiChip.h>
#include <android/hardware/wifi/1.0/BpHwWifiChip.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace V1_3 {

const char* IWifiChip::descriptor("android.hardware.wifi@1.3::IWifiChip");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IWifiChip::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwWifiChip(static_cast<IWifiChip *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IWifiChip::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsWifiChip(static_cast<IWifiChip *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IWifiChip::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IWifiChip::descriptor);
};

// Methods from ::android::hardware::wifi::V1_0::IWifiChip follow.
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getId(getId_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::registerEventCallback(const ::android::sp<::android::hardware::wifi::V1_0::IWifiChipEventCallback>& callback, registerEventCallback_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getCapabilities(getCapabilities_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getAvailableModes(getAvailableModes_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::configureChip(uint32_t modeId, configureChip_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getMode(getMode_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::requestChipDebugInfo(requestChipDebugInfo_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::requestDriverDebugDump(requestDriverDebugDump_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::requestFirmwareDebugDump(requestFirmwareDebugDump_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::createApIface(createApIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getApIfaceNames(getApIfaceNames_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getApIface(const ::android::hardware::hidl_string& ifname, getApIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::removeApIface(const ::android::hardware::hidl_string& ifname, removeApIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::createNanIface(createNanIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getNanIfaceNames(getNanIfaceNames_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getNanIface(const ::android::hardware::hidl_string& ifname, getNanIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::removeNanIface(const ::android::hardware::hidl_string& ifname, removeNanIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::createP2pIface(createP2pIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getP2pIfaceNames(getP2pIfaceNames_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getP2pIface(const ::android::hardware::hidl_string& ifname, getP2pIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::removeP2pIface(const ::android::hardware::hidl_string& ifname, removeP2pIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::createStaIface(createStaIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getStaIfaceNames(getStaIfaceNames_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getStaIface(const ::android::hardware::hidl_string& ifname, getStaIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::removeStaIface(const ::android::hardware::hidl_string& ifname, removeStaIface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::createRttController(const ::android::sp<::android::hardware::wifi::V1_0::IWifiIface>& boundIface, createRttController_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getDebugRingBuffersStatus(getDebugRingBuffersStatus_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::startLoggingToDebugRingBuffer(const ::android::hardware::hidl_string& ringName, ::android::hardware::wifi::V1_0::WifiDebugRingBufferVerboseLevel verboseLevel, uint32_t maxIntervalInSec, uint32_t minDataSizeInBytes, startLoggingToDebugRingBuffer_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::forceDumpToDebugRingBuffer(const ::android::hardware::hidl_string& ringName, forceDumpToDebugRingBuffer_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::stopLoggingToDebugRingBuffer(stopLoggingToDebugRingBuffer_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getDebugHostWakeReasonStats(getDebugHostWakeReasonStats_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::enableDebugErrorAlerts(bool enable, enableDebugErrorAlerts_cb _hidl_cb)

// Methods from ::android::hardware::wifi::V1_1::IWifiChip follow.
// no default implementation for: ::android::hardware::Return<void> IWifiChip::selectTxPowerScenario(::android::hardware::wifi::V1_1::IWifiChip::TxPowerScenario scenario, selectTxPowerScenario_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::resetTxPowerScenario(resetTxPowerScenario_cb _hidl_cb)

// Methods from ::android::hardware::wifi::V1_2::IWifiChip follow.
// no default implementation for: ::android::hardware::Return<void> IWifiChip::selectTxPowerScenario_1_2(::android::hardware::wifi::V1_2::IWifiChip::TxPowerScenario scenario, selectTxPowerScenario_1_2_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::registerEventCallback_1_2(const ::android::sp<::android::hardware::wifi::V1_2::IWifiChipEventCallback>& callback, registerEventCallback_1_2_cb _hidl_cb)

// Methods from ::android::hardware::wifi::V1_3::IWifiChip follow.
// no default implementation for: ::android::hardware::Return<void> IWifiChip::getCapabilities_1_3(getCapabilities_1_3_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::setLatencyMode(::android::hardware::wifi::V1_3::IWifiChip::LatencyMode mode, setLatencyMode_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiChip::flushRingBufferToFile(flushRingBufferToFile_cb _hidl_cb)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IWifiChip::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::V1_3::IWifiChip::descriptor,
        ::android::hardware::wifi::V1_2::IWifiChip::descriptor,
        ::android::hardware::wifi::V1_1::IWifiChip::descriptor,
        ::android::hardware::wifi::V1_0::IWifiChip::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IWifiChip::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiChip::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::V1_3::IWifiChip::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiChip::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){100,190,8,75,110,30,243,48,183,95,169,22,89,61,192,185,75,14,199,161,109,92,250,165,163,30,108,145,67,200,40,141} /* 64be084b6e1ef330b75fa916593dc0b94b0ec7a16d5cfaa5a31e6c9143c8288d */,
        (uint8_t[32]){120,12,22,253,237,161,59,119,157,153,57,83,166,127,124,165,120,201,56,161,114,169,66,76,28,113,90,232,27,196,15,215} /* 780c16fdeda13b779d993953a67f7ca578c938a172a9424c1c715ae81bc40fd7 */,
        (uint8_t[32]){176,86,225,222,250,180,7,21,132,33,69,132,5,125,11,199,58,97,48,129,191,17,82,89,5,73,100,157,69,130,193,60} /* b056e1defab4071584214584057d0bc73a613081bf1152590549649d4582c13c */,
        (uint8_t[32]){243,238,204,72,157,235,76,116,137,47,89,235,122,219,118,144,99,189,92,53,74,193,50,182,38,165,244,43,54,61,54,188} /* f3eecc489deb4c74892f59eb7adb769063bd5c354ac132b626a5f42b363d36bc */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiChip::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IWifiChip::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IWifiChip::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiChip::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiChip::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IWifiChip::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiChip>> IWifiChip::castFrom(const ::android::sp<::android::hardware::wifi::V1_3::IWifiChip>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiChip>> IWifiChip::castFrom(const ::android::sp<::android::hardware::wifi::V1_2::IWifiChip>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiChip, ::android::hardware::wifi::V1_2::IWifiChip, BpHwWifiChip>(
            parent, "android.hardware.wifi@1.3::IWifiChip", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiChip>> IWifiChip::castFrom(const ::android::sp<::android::hardware::wifi::V1_1::IWifiChip>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiChip, ::android::hardware::wifi::V1_1::IWifiChip, BpHwWifiChip>(
            parent, "android.hardware.wifi@1.3::IWifiChip", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiChip>> IWifiChip::castFrom(const ::android::sp<::android::hardware::wifi::V1_0::IWifiChip>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiChip, ::android::hardware::wifi::V1_0::IWifiChip, BpHwWifiChip>(
            parent, "android.hardware.wifi@1.3::IWifiChip", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiChip>> IWifiChip::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiChip, ::android::hidl::base::V1_0::IBase, BpHwWifiChip>(
            parent, "android.hardware.wifi@1.3::IWifiChip", emitError);
}

BpHwWifiChip::BpHwWifiChip(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IWifiChip>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi@1.3", "IWifiChip") {
}

// Methods from ::android::hardware::wifi::V1_3::IWifiChip follow.
::android::hardware::Return<void> BpHwWifiChip::_hidl_getCapabilities_1_3(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, getCapabilities_1_3_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IWifiChip::getCapabilities_1_3::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiChip", "getCapabilities_1_3", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::V1_0::WifiStatus* _hidl_out_status;
    ::android::hardware::hidl_bitfield<::android::hardware::wifi::V1_3::IWifiChip::ChipCapabilityMask> _hidl_out_capabilities;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwWifiChip::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(37 /* getCapabilities_1_3 */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_0::WifiStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_reply.readUint32((uint32_t *)&_hidl_out_capabilities);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status, _hidl_out_capabilities);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        _hidl_args.push_back((void *)&_hidl_out_capabilities);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi", "1.3", "IWifiChip", "getCapabilities_1_3", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwWifiChip::_hidl_setLatencyMode(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, ::android::hardware::wifi::V1_3::IWifiChip::LatencyMode mode, setLatencyMode_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IWifiChip::setLatencyMode::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&mode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiChip", "setLatencyMode", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::V1_0::WifiStatus* _hidl_out_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwWifiChip::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32((uint32_t)mode);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(38 /* setLatencyMode */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_0::WifiStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi", "1.3", "IWifiChip", "setLatencyMode", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwWifiChip::_hidl_flushRingBufferToFile(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, flushRingBufferToFile_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IWifiChip::flushRingBufferToFile::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiChip", "flushRingBufferToFile", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::V1_0::WifiStatus* _hidl_out_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwWifiChip::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(39 /* flushRingBufferToFile */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_0::WifiStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi", "1.3", "IWifiChip", "flushRingBufferToFile", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::wifi::V1_0::IWifiChip follow.
::android::hardware::Return<void> BpHwWifiChip::getId(getId_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getId(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::registerEventCallback(const ::android::sp<::android::hardware::wifi::V1_0::IWifiChipEventCallback>& callback, registerEventCallback_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_registerEventCallback(this, this, callback, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getCapabilities(getCapabilities_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getCapabilities(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getAvailableModes(getAvailableModes_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getAvailableModes(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::configureChip(uint32_t modeId, configureChip_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_configureChip(this, this, modeId, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getMode(getMode_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getMode(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::requestChipDebugInfo(requestChipDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_requestChipDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::requestDriverDebugDump(requestDriverDebugDump_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_requestDriverDebugDump(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::requestFirmwareDebugDump(requestFirmwareDebugDump_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_requestFirmwareDebugDump(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::createApIface(createApIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_createApIface(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getApIfaceNames(getApIfaceNames_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getApIfaceNames(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getApIface(const ::android::hardware::hidl_string& ifname, getApIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getApIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::removeApIface(const ::android::hardware::hidl_string& ifname, removeApIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_removeApIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::createNanIface(createNanIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_createNanIface(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getNanIfaceNames(getNanIfaceNames_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getNanIfaceNames(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getNanIface(const ::android::hardware::hidl_string& ifname, getNanIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getNanIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::removeNanIface(const ::android::hardware::hidl_string& ifname, removeNanIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_removeNanIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::createP2pIface(createP2pIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_createP2pIface(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getP2pIfaceNames(getP2pIfaceNames_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getP2pIfaceNames(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getP2pIface(const ::android::hardware::hidl_string& ifname, getP2pIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getP2pIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::removeP2pIface(const ::android::hardware::hidl_string& ifname, removeP2pIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_removeP2pIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::createStaIface(createStaIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_createStaIface(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getStaIfaceNames(getStaIfaceNames_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getStaIfaceNames(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getStaIface(const ::android::hardware::hidl_string& ifname, getStaIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getStaIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::removeStaIface(const ::android::hardware::hidl_string& ifname, removeStaIface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_removeStaIface(this, this, ifname, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::createRttController(const ::android::sp<::android::hardware::wifi::V1_0::IWifiIface>& boundIface, createRttController_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_createRttController(this, this, boundIface, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getDebugRingBuffersStatus(getDebugRingBuffersStatus_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getDebugRingBuffersStatus(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::startLoggingToDebugRingBuffer(const ::android::hardware::hidl_string& ringName, ::android::hardware::wifi::V1_0::WifiDebugRingBufferVerboseLevel verboseLevel, uint32_t maxIntervalInSec, uint32_t minDataSizeInBytes, startLoggingToDebugRingBuffer_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_startLoggingToDebugRingBuffer(this, this, ringName, verboseLevel, maxIntervalInSec, minDataSizeInBytes, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::forceDumpToDebugRingBuffer(const ::android::hardware::hidl_string& ringName, forceDumpToDebugRingBuffer_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_forceDumpToDebugRingBuffer(this, this, ringName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::stopLoggingToDebugRingBuffer(stopLoggingToDebugRingBuffer_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_stopLoggingToDebugRingBuffer(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getDebugHostWakeReasonStats(getDebugHostWakeReasonStats_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_getDebugHostWakeReasonStats(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::enableDebugErrorAlerts(bool enable, enableDebugErrorAlerts_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiChip::_hidl_enableDebugErrorAlerts(this, this, enable, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::V1_1::IWifiChip follow.
::android::hardware::Return<void> BpHwWifiChip::selectTxPowerScenario(::android::hardware::wifi::V1_1::IWifiChip::TxPowerScenario scenario, selectTxPowerScenario_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_1::BpHwWifiChip::_hidl_selectTxPowerScenario(this, this, scenario, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::resetTxPowerScenario(resetTxPowerScenario_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_1::BpHwWifiChip::_hidl_resetTxPowerScenario(this, this, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::V1_2::IWifiChip follow.
::android::hardware::Return<void> BpHwWifiChip::selectTxPowerScenario_1_2(::android::hardware::wifi::V1_2::IWifiChip::TxPowerScenario scenario, selectTxPowerScenario_1_2_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_2::BpHwWifiChip::_hidl_selectTxPowerScenario_1_2(this, this, scenario, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::registerEventCallback_1_2(const ::android::sp<::android::hardware::wifi::V1_2::IWifiChipEventCallback>& callback, registerEventCallback_1_2_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_2::BpHwWifiChip::_hidl_registerEventCallback_1_2(this, this, callback, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::V1_3::IWifiChip follow.
::android::hardware::Return<void> BpHwWifiChip::getCapabilities_1_3(getCapabilities_1_3_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_3::BpHwWifiChip::_hidl_getCapabilities_1_3(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::setLatencyMode(::android::hardware::wifi::V1_3::IWifiChip::LatencyMode mode, setLatencyMode_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_3::BpHwWifiChip::_hidl_setLatencyMode(this, this, mode, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::flushRingBufferToFile(flushRingBufferToFile_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_3::BpHwWifiChip::_hidl_flushRingBufferToFile(this, this, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwWifiChip::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwWifiChip::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwWifiChip::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiChip::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwWifiChip::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwWifiChip::BnHwWifiChip(const ::android::sp<IWifiChip> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi@1.3", "IWifiChip") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwWifiChip::~BnHwWifiChip() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::wifi::V1_3::IWifiChip follow.
::android::status_t BnHwWifiChip::_hidl_getCapabilities_1_3(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwWifiChip::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IWifiChip::getCapabilities_1_3::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiChip", "getCapabilities_1_3", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IWifiChip*>(_hidl_this->getImpl().get())->getCapabilities_1_3([&](const auto &_hidl_out_status, const auto &_hidl_out_capabilities) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("getCapabilities_1_3: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        _hidl_err = _hidl_reply->writeUint32((uint32_t)_hidl_out_capabilities);
        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            _hidl_args.push_back((void *)&_hidl_out_capabilities);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi", "1.3", "IWifiChip", "getCapabilities_1_3", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("getCapabilities_1_3: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwWifiChip::_hidl_setLatencyMode(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwWifiChip::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    ::android::hardware::wifi::V1_3::IWifiChip::LatencyMode mode;

    _hidl_err = _hidl_data.readUint32((uint32_t *)&mode);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IWifiChip::setLatencyMode::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&mode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiChip", "setLatencyMode", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IWifiChip*>(_hidl_this->getImpl().get())->setLatencyMode(mode, [&](const auto &_hidl_out_status) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("setLatencyMode: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi", "1.3", "IWifiChip", "setLatencyMode", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("setLatencyMode: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwWifiChip::_hidl_flushRingBufferToFile(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwWifiChip::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IWifiChip::flushRingBufferToFile::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiChip", "flushRingBufferToFile", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IWifiChip*>(_hidl_this->getImpl().get())->flushRingBufferToFile([&](const auto &_hidl_out_status) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("flushRingBufferToFile: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi", "1.3", "IWifiChip", "flushRingBufferToFile", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("flushRingBufferToFile: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}


// Methods from ::android::hardware::wifi::V1_0::IWifiChip follow.

// Methods from ::android::hardware::wifi::V1_1::IWifiChip follow.

// Methods from ::android::hardware::wifi::V1_2::IWifiChip follow.

// Methods from ::android::hardware::wifi::V1_3::IWifiChip follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwWifiChip::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwWifiChip::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwWifiChip::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* getId */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getId(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* registerEventCallback */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_registerEventCallback(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* getCapabilities */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getCapabilities(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* getAvailableModes */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getAvailableModes(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* configureChip */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_configureChip(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* getMode */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getMode(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* requestChipDebugInfo */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_requestChipDebugInfo(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* requestDriverDebugDump */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_requestDriverDebugDump(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 9 /* requestFirmwareDebugDump */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_requestFirmwareDebugDump(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 10 /* createApIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_createApIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 11 /* getApIfaceNames */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getApIfaceNames(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 12 /* getApIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getApIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 13 /* removeApIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_removeApIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 14 /* createNanIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_createNanIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 15 /* getNanIfaceNames */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getNanIfaceNames(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 16 /* getNanIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getNanIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 17 /* removeNanIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_removeNanIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 18 /* createP2pIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_createP2pIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 19 /* getP2pIfaceNames */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getP2pIfaceNames(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 20 /* getP2pIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getP2pIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 21 /* removeP2pIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_removeP2pIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 22 /* createStaIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_createStaIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 23 /* getStaIfaceNames */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getStaIfaceNames(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 24 /* getStaIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getStaIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 25 /* removeStaIface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_removeStaIface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 26 /* createRttController */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_createRttController(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 27 /* getDebugRingBuffersStatus */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getDebugRingBuffersStatus(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 28 /* startLoggingToDebugRingBuffer */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_startLoggingToDebugRingBuffer(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 29 /* forceDumpToDebugRingBuffer */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_forceDumpToDebugRingBuffer(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 30 /* stopLoggingToDebugRingBuffer */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_stopLoggingToDebugRingBuffer(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 31 /* getDebugHostWakeReasonStats */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_getDebugHostWakeReasonStats(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 32 /* enableDebugErrorAlerts */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiChip::_hidl_enableDebugErrorAlerts(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 33 /* selectTxPowerScenario */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_1::BnHwWifiChip::_hidl_selectTxPowerScenario(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 34 /* resetTxPowerScenario */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_1::BnHwWifiChip::_hidl_resetTxPowerScenario(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 35 /* selectTxPowerScenario_1_2 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_2::BnHwWifiChip::_hidl_selectTxPowerScenario_1_2(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 36 /* registerEventCallback_1_2 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_2::BnHwWifiChip::_hidl_registerEventCallback_1_2(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 37 /* getCapabilities_1_3 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_3::BnHwWifiChip::_hidl_getCapabilities_1_3(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 38 /* setLatencyMode */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_3::BnHwWifiChip::_hidl_setLatencyMode(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 39 /* flushRingBufferToFile */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_3::BnHwWifiChip::_hidl_flushRingBufferToFile(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsWifiChip::BsWifiChip(const ::android::sp<::android::hardware::wifi::V1_3::IWifiChip> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi@1.3", "IWifiChip"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsWifiChip::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IWifiChip> IWifiChip::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwWifiChip>(serviceName, false, getStub);
}

::android::sp<IWifiChip> IWifiChip::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwWifiChip>(serviceName, true, getStub);
}

::android::status_t IWifiChip::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IWifiChip::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi@1.3::IWifiChip",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_3
}  // namespace wifi
}  // namespace hardware
}  // namespace android
