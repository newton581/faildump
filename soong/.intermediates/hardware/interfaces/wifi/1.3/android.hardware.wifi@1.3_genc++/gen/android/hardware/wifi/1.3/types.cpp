#define LOG_TAG "android.hardware.wifi@1.3::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/wifi/1.3/types.h>
#include <android/hardware/wifi/1.3/hwtypes.h>

namespace android {
namespace hardware {
namespace wifi {
namespace V1_3 {

::android::status_t readEmbeddedFromParcel(
        const StaLinkLayerRadioStats &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_0::StaLinkLayerRadioStats &>(obj.V1_0),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_3::StaLinkLayerRadioStats, V1_0));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_channelStats_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::wifi::V1_3::WifiChannelStats> &>(obj.channelStats),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_3::StaLinkLayerRadioStats, channelStats), &_hidl_channelStats_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const StaLinkLayerRadioStats &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.V1_0,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_3::StaLinkLayerRadioStats, V1_0));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_channelStats_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.channelStats,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_3::StaLinkLayerRadioStats, channelStats), &_hidl_channelStats_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const StaLinkLayerStats &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_radios_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::wifi::V1_3::StaLinkLayerRadioStats> &>(obj.radios),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_3::StaLinkLayerStats, radios), &_hidl_radios_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.radios.size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::wifi::V1_3::StaLinkLayerRadioStats &>(obj.radios[_hidl_index_0]),
                parcel,
                _hidl_radios_child,
                _hidl_index_0 * sizeof(::android::hardware::wifi::V1_3::StaLinkLayerRadioStats));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const StaLinkLayerStats &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_radios_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.radios,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::V1_3::StaLinkLayerStats, radios), &_hidl_radios_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.radios.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                obj.radios[_hidl_index_0],
                parcel,
                _hidl_radios_child,
                _hidl_index_0 * sizeof(::android::hardware::wifi::V1_3::StaLinkLayerRadioStats));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_3
}  // namespace wifi
}  // namespace hardware
}  // namespace android
