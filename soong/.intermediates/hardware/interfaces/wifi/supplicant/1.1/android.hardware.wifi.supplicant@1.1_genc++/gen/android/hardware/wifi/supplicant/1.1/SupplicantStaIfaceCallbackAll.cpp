#define LOG_TAG "android.hardware.wifi.supplicant@1.1::SupplicantStaIfaceCallback"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/supplicant/1.1/BpHwSupplicantStaIfaceCallback.h>
#include <android/hardware/wifi/supplicant/1.1/BnHwSupplicantStaIfaceCallback.h>
#include <android/hardware/wifi/supplicant/1.1/BsSupplicantStaIfaceCallback.h>
#include <android/hardware/wifi/supplicant/1.0/BpHwSupplicantStaIfaceCallback.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {
namespace V1_1 {

const char* ISupplicantStaIfaceCallback::descriptor("android.hardware.wifi.supplicant@1.1::ISupplicantStaIfaceCallback");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(ISupplicantStaIfaceCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwSupplicantStaIfaceCallback(static_cast<ISupplicantStaIfaceCallback *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(ISupplicantStaIfaceCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsSupplicantStaIfaceCallback(static_cast<ISupplicantStaIfaceCallback *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(ISupplicantStaIfaceCallback::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(ISupplicantStaIfaceCallback::descriptor);
};

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onNetworkAdded(uint32_t id)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onNetworkRemoved(uint32_t id)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onStateChanged(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::State newState, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, uint32_t id, const ::android::hardware::hidl_vec<uint8_t>& ssid)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onAnqpQueryDone(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, const ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::AnqpData& data, const ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::Hs20AnqpData& hs20Data)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onHs20IconQueryDone(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, const ::android::hardware::hidl_string& fileName, const ::android::hardware::hidl_vec<uint8_t>& data)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onHs20SubscriptionRemediation(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::OsuMethod osuMethod, const ::android::hardware::hidl_string& url)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onHs20DeauthImminentNotice(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, uint32_t reasonCode, uint32_t reAuthDelayInSec, const ::android::hardware::hidl_string& url)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onDisconnected(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, bool locallyGenerated, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::ReasonCode reasonCode)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onAssociationRejected(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::StatusCode statusCode, bool timedOut)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onAuthenticationTimeout(const ::android::hardware::hidl_array<uint8_t, 6>& bssid)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onEapFailure()
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onBssidChanged(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::BssidChangeReason reason, const ::android::hardware::hidl_array<uint8_t, 6>& bssid)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onWpsEventSuccess()
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onWpsEventFail(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::WpsConfigError configError, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::WpsErrorIndication errorInd)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onWpsEventPbcOverlap()
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onExtRadioWorkStart(uint32_t id)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onExtRadioWorkTimeout(uint32_t id)

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIfaceCallback::onEapFailure_1_1(::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback::EapErrorCode errorCode)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> ISupplicantStaIfaceCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback::descriptor,
        ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> ISupplicantStaIfaceCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIfaceCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIfaceCallback::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){205,67,48,195,25,107,218,29,100,42,50,171,254,35,167,214,78,191,189,167,33,148,6,67,175,104,103,175,59,63,10,169} /* cd4330c3196bda1d642a32abfe23a7d64ebfbda721940643af6867af3b3f0aa9 */,
        (uint8_t[32]){215,129,200,215,231,179,254,92,202,140,246,225,216,128,110,119,9,130,174,83,88,199,129,110,213,27,15,14,194,114,231,13} /* d781c8d7e7b3fe5cca8cf6e1d8806e770982ae5358c7816ed51b0f0ec272e70d */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIfaceCallback::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantStaIfaceCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> ISupplicantStaIfaceCallback::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIfaceCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIfaceCallback::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantStaIfaceCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback>> ISupplicantStaIfaceCallback::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback>> ISupplicantStaIfaceCallback::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantStaIfaceCallback, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback, BpHwSupplicantStaIfaceCallback>(
            parent, "android.hardware.wifi.supplicant@1.1::ISupplicantStaIfaceCallback", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback>> ISupplicantStaIfaceCallback::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantStaIfaceCallback, ::android::hidl::base::V1_0::IBase, BpHwSupplicantStaIfaceCallback>(
            parent, "android.hardware.wifi.supplicant@1.1::ISupplicantStaIfaceCallback", emitError);
}

BpHwSupplicantStaIfaceCallback::BpHwSupplicantStaIfaceCallback(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<ISupplicantStaIfaceCallback>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.1", "ISupplicantStaIfaceCallback") {
}

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback follow.
::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::_hidl_onEapFailure_1_1(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback::EapErrorCode errorCode) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantStaIfaceCallback::onEapFailure_1_1::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&errorCode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIfaceCallback", "onEapFailure_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantStaIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32((uint32_t)errorCode);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(18 /* onEapFailure_1_1 */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIfaceCallback", "onEapFailure_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback follow.
::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onNetworkAdded(uint32_t id){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onNetworkAdded(this, this, id);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onNetworkRemoved(uint32_t id){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onNetworkRemoved(this, this, id);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onStateChanged(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::State newState, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, uint32_t id, const ::android::hardware::hidl_vec<uint8_t>& ssid){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onStateChanged(this, this, newState, bssid, id, ssid);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onAnqpQueryDone(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, const ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::AnqpData& data, const ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::Hs20AnqpData& hs20Data){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onAnqpQueryDone(this, this, bssid, data, hs20Data);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onHs20IconQueryDone(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, const ::android::hardware::hidl_string& fileName, const ::android::hardware::hidl_vec<uint8_t>& data){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onHs20IconQueryDone(this, this, bssid, fileName, data);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onHs20SubscriptionRemediation(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::OsuMethod osuMethod, const ::android::hardware::hidl_string& url){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onHs20SubscriptionRemediation(this, this, bssid, osuMethod, url);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onHs20DeauthImminentNotice(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, uint32_t reasonCode, uint32_t reAuthDelayInSec, const ::android::hardware::hidl_string& url){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onHs20DeauthImminentNotice(this, this, bssid, reasonCode, reAuthDelayInSec, url);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onDisconnected(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, bool locallyGenerated, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::ReasonCode reasonCode){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onDisconnected(this, this, bssid, locallyGenerated, reasonCode);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onAssociationRejected(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::StatusCode statusCode, bool timedOut){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onAssociationRejected(this, this, bssid, statusCode, timedOut);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onAuthenticationTimeout(const ::android::hardware::hidl_array<uint8_t, 6>& bssid){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onAuthenticationTimeout(this, this, bssid);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onEapFailure(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onEapFailure(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onBssidChanged(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::BssidChangeReason reason, const ::android::hardware::hidl_array<uint8_t, 6>& bssid){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onBssidChanged(this, this, reason, bssid);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onWpsEventSuccess(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onWpsEventSuccess(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onWpsEventFail(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::WpsConfigError configError, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback::WpsErrorIndication errorInd){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onWpsEventFail(this, this, bssid, configError, errorInd);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onWpsEventPbcOverlap(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onWpsEventPbcOverlap(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onExtRadioWorkStart(uint32_t id){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onExtRadioWorkStart(this, this, id);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onExtRadioWorkTimeout(uint32_t id){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIfaceCallback::_hidl_onExtRadioWorkTimeout(this, this, id);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback follow.
::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::onEapFailure_1_1(::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback::EapErrorCode errorCode){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_1::BpHwSupplicantStaIfaceCallback::_hidl_onEapFailure_1_1(this, this, errorCode);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantStaIfaceCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIfaceCallback::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantStaIfaceCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwSupplicantStaIfaceCallback::BnHwSupplicantStaIfaceCallback(const ::android::sp<ISupplicantStaIfaceCallback> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi.supplicant@1.1", "ISupplicantStaIfaceCallback") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwSupplicantStaIfaceCallback::~BnHwSupplicantStaIfaceCallback() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback follow.
::android::status_t BnHwSupplicantStaIfaceCallback::_hidl_onEapFailure_1_1(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantStaIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback::EapErrorCode errorCode;

    _hidl_err = _hidl_data.readUint32((uint32_t *)&errorCode);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantStaIfaceCallback::onEapFailure_1_1::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&errorCode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIfaceCallback", "onEapFailure_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantStaIfaceCallback*>(_hidl_this->getImpl().get())->onEapFailure_1_1(errorCode);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIfaceCallback", "onEapFailure_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback follow.

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwSupplicantStaIfaceCallback::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwSupplicantStaIfaceCallback::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwSupplicantStaIfaceCallback::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* onNetworkAdded */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onNetworkAdded(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* onNetworkRemoved */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onNetworkRemoved(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* onStateChanged */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onStateChanged(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* onAnqpQueryDone */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onAnqpQueryDone(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* onHs20IconQueryDone */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onHs20IconQueryDone(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* onHs20SubscriptionRemediation */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onHs20SubscriptionRemediation(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* onHs20DeauthImminentNotice */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onHs20DeauthImminentNotice(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* onDisconnected */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onDisconnected(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 9 /* onAssociationRejected */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onAssociationRejected(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 10 /* onAuthenticationTimeout */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onAuthenticationTimeout(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 11 /* onEapFailure */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onEapFailure(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 12 /* onBssidChanged */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onBssidChanged(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 13 /* onWpsEventSuccess */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onWpsEventSuccess(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 14 /* onWpsEventFail */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onWpsEventFail(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 15 /* onWpsEventPbcOverlap */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onWpsEventPbcOverlap(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 16 /* onExtRadioWorkStart */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onExtRadioWorkStart(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 17 /* onExtRadioWorkTimeout */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIfaceCallback::_hidl_onExtRadioWorkTimeout(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 18 /* onEapFailure_1_1 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_1::BnHwSupplicantStaIfaceCallback::_hidl_onEapFailure_1_1(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsSupplicantStaIfaceCallback::BsSupplicantStaIfaceCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.1", "ISupplicantStaIfaceCallback"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsSupplicantStaIfaceCallback::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<ISupplicantStaIfaceCallback> ISupplicantStaIfaceCallback::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantStaIfaceCallback>(serviceName, false, getStub);
}

::android::sp<ISupplicantStaIfaceCallback> ISupplicantStaIfaceCallback::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantStaIfaceCallback>(serviceName, true, getStub);
}

::android::status_t ISupplicantStaIfaceCallback::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool ISupplicantStaIfaceCallback::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi.supplicant@1.1::ISupplicantStaIfaceCallback",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_1
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
