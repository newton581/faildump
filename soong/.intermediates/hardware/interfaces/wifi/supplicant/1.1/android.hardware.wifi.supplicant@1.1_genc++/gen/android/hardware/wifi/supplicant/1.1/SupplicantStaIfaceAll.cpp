#define LOG_TAG "android.hardware.wifi.supplicant@1.1::SupplicantStaIface"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/supplicant/1.1/BpHwSupplicantStaIface.h>
#include <android/hardware/wifi/supplicant/1.1/BnHwSupplicantStaIface.h>
#include <android/hardware/wifi/supplicant/1.1/BsSupplicantStaIface.h>
#include <android/hardware/wifi/supplicant/1.0/BpHwSupplicantStaIface.h>
#include <android/hardware/wifi/supplicant/1.0/BpHwSupplicantIface.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {
namespace V1_1 {

const char* ISupplicantStaIface::descriptor("android.hardware.wifi.supplicant@1.1::ISupplicantStaIface");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(ISupplicantStaIface::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwSupplicantStaIface(static_cast<ISupplicantStaIface *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(ISupplicantStaIface::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsSupplicantStaIface(static_cast<ISupplicantStaIface *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(ISupplicantStaIface::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(ISupplicantStaIface::descriptor);
};

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::getName(getName_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::getType(getType_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::addNetwork(addNetwork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::removeNetwork(uint32_t id, removeNetwork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::getNetwork(uint32_t id, getNetwork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::listNetworks(listNetworks_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setWpsDeviceName(const ::android::hardware::hidl_string& name, setWpsDeviceName_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setWpsDeviceType(const ::android::hardware::hidl_array<uint8_t, 8>& type, setWpsDeviceType_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setWpsManufacturer(const ::android::hardware::hidl_string& manufacturer, setWpsManufacturer_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setWpsModelName(const ::android::hardware::hidl_string& modelName, setWpsModelName_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setWpsModelNumber(const ::android::hardware::hidl_string& modelNumber, setWpsModelNumber_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setWpsSerialNumber(const ::android::hardware::hidl_string& serialNumber, setWpsSerialNumber_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setWpsConfigMethods(::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, setWpsConfigMethods_cb _hidl_cb)

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::registerCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback>& callback, registerCallback_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::reassociate(reassociate_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::reconnect(reconnect_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::disconnect(disconnect_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setPowerSave(bool enable, setPowerSave_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::initiateTdlsDiscover(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, initiateTdlsDiscover_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::initiateTdlsSetup(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, initiateTdlsSetup_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::initiateTdlsTeardown(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, initiateTdlsTeardown_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::initiateAnqpQuery(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, const ::android::hardware::hidl_vec<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::AnqpInfoId>& infoElements, const ::android::hardware::hidl_vec<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::Hs20AnqpSubtypes>& subTypes, initiateAnqpQuery_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::initiateHs20IconQuery(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, const ::android::hardware::hidl_string& fileName, initiateHs20IconQuery_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::getMacAddress(getMacAddress_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::startRxFilter(startRxFilter_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::stopRxFilter(stopRxFilter_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::addRxFilter(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::RxFilterType type, addRxFilter_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::removeRxFilter(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::RxFilterType type, removeRxFilter_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setBtCoexistenceMode(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::BtCoexistenceMode mode, setBtCoexistenceMode_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setBtCoexistenceScanModeEnabled(bool enable, setBtCoexistenceScanModeEnabled_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setSuspendModeEnabled(bool enable, setSuspendModeEnabled_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setCountryCode(const ::android::hardware::hidl_array<int8_t, 2>& code, setCountryCode_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::startWpsRegistrar(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, const ::android::hardware::hidl_string& pin, startWpsRegistrar_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::startWpsPbc(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPbc_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::startWpsPinKeypad(const ::android::hardware::hidl_string& pin, startWpsPinKeypad_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::startWpsPinDisplay(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPinDisplay_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::cancelWps(cancelWps_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::setExternalSim(bool useExternalSim, setExternalSim_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::addExtRadioWork(const ::android::hardware::hidl_string& name, uint32_t freqInMhz, uint32_t timeoutInSec, addExtRadioWork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::removeExtRadioWork(uint32_t id, removeExtRadioWork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::enableAutoReconnect(bool enable, enableAutoReconnect_cb _hidl_cb)

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantStaIface::registerCallback_1_1(const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback>& callback, registerCallback_1_1_cb _hidl_cb)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> ISupplicantStaIface::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface::descriptor,
        ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::descriptor,
        ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> ISupplicantStaIface::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIface::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIface::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){33,117,125,14,93,212,183,228,189,152,26,74,32,83,27,202,60,50,39,26,217,119,123,23,183,78,181,161,234,80,131,132} /* 21757d0e5dd4b7e4bd981a4a20531bca3c32271ad9777b17b74eb5a1ea508384 */,
        (uint8_t[32]){119,82,225,222,147,170,245,254,211,112,17,194,25,172,36,112,105,246,175,50,11,8,16,218,169,133,16,88,74,16,231,180} /* 7752e1de93aaf5fed37011c219ac247069f6af320b0810daa98510584a10e7b4 */,
        (uint8_t[32]){53,186,123,205,241,143,36,168,102,167,229,66,149,72,240,103,104,187,32,162,87,247,91,16,163,151,196,216,37,239,132,56} /* 35ba7bcdf18f24a866a7e5429548f06768bb20a257f75b10a397c4d825ef8438 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIface::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantStaIface::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> ISupplicantStaIface::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIface::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantStaIface::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantStaIface::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface>> ISupplicantStaIface::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface>> ISupplicantStaIface::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantStaIface, ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface, BpHwSupplicantStaIface>(
            parent, "android.hardware.wifi.supplicant@1.1::ISupplicantStaIface", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface>> ISupplicantStaIface::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantIface>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantStaIface, ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface, BpHwSupplicantStaIface>(
            parent, "android.hardware.wifi.supplicant@1.1::ISupplicantStaIface", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface>> ISupplicantStaIface::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantStaIface, ::android::hidl::base::V1_0::IBase, BpHwSupplicantStaIface>(
            parent, "android.hardware.wifi.supplicant@1.1::ISupplicantStaIface", emitError);
}

BpHwSupplicantStaIface::BpHwSupplicantStaIface(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<ISupplicantStaIface>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.1", "ISupplicantStaIface") {
}

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface follow.
::android::hardware::Return<void> BpHwSupplicantStaIface::_hidl_registerCallback_1_1(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback>& callback, registerCallback_1_1_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantStaIface::registerCallback_1_1::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&callback);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIface", "registerCallback_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::supplicant::V1_0::SupplicantStatus* _hidl_out_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantStaIface::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (callback == nullptr) {
        _hidl_err = _hidl_data.writeStrongBinder(nullptr);
    } else {
        ::android::sp<::android::hardware::IBinder> _hidl_binder = ::android::hardware::getOrCreateCachedBinder(callback.get());
        if (_hidl_binder.get() != nullptr) {
            _hidl_err = _hidl_data.writeStrongBinder(_hidl_binder);
        } else {
            _hidl_err = ::android::UNKNOWN_ERROR;
        }
    }
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    ::android::hardware::ProcessState::self()->startThreadPool();
    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(42 /* registerCallback_1_1 */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::supplicant::V1_0::SupplicantStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIface", "registerCallback_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface follow.
::android::hardware::Return<void> BpHwSupplicantStaIface::getName(getName_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_getName(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::getType(getType_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_getType(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::addNetwork(addNetwork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_addNetwork(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::removeNetwork(uint32_t id, removeNetwork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_removeNetwork(this, this, id, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::getNetwork(uint32_t id, getNetwork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_getNetwork(this, this, id, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::listNetworks(listNetworks_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_listNetworks(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setWpsDeviceName(const ::android::hardware::hidl_string& name, setWpsDeviceName_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsDeviceName(this, this, name, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setWpsDeviceType(const ::android::hardware::hidl_array<uint8_t, 8>& type, setWpsDeviceType_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsDeviceType(this, this, type, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setWpsManufacturer(const ::android::hardware::hidl_string& manufacturer, setWpsManufacturer_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsManufacturer(this, this, manufacturer, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setWpsModelName(const ::android::hardware::hidl_string& modelName, setWpsModelName_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsModelName(this, this, modelName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setWpsModelNumber(const ::android::hardware::hidl_string& modelNumber, setWpsModelNumber_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsModelNumber(this, this, modelNumber, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setWpsSerialNumber(const ::android::hardware::hidl_string& serialNumber, setWpsSerialNumber_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsSerialNumber(this, this, serialNumber, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setWpsConfigMethods(::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, setWpsConfigMethods_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsConfigMethods(this, this, configMethods, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface follow.
::android::hardware::Return<void> BpHwSupplicantStaIface::registerCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIfaceCallback>& callback, registerCallback_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_registerCallback(this, this, callback, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::reassociate(reassociate_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_reassociate(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::reconnect(reconnect_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_reconnect(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::disconnect(disconnect_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_disconnect(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setPowerSave(bool enable, setPowerSave_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_setPowerSave(this, this, enable, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::initiateTdlsDiscover(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, initiateTdlsDiscover_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_initiateTdlsDiscover(this, this, macAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::initiateTdlsSetup(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, initiateTdlsSetup_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_initiateTdlsSetup(this, this, macAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::initiateTdlsTeardown(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, initiateTdlsTeardown_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_initiateTdlsTeardown(this, this, macAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::initiateAnqpQuery(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, const ::android::hardware::hidl_vec<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::AnqpInfoId>& infoElements, const ::android::hardware::hidl_vec<::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::Hs20AnqpSubtypes>& subTypes, initiateAnqpQuery_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_initiateAnqpQuery(this, this, macAddress, infoElements, subTypes, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::initiateHs20IconQuery(const ::android::hardware::hidl_array<uint8_t, 6>& macAddress, const ::android::hardware::hidl_string& fileName, initiateHs20IconQuery_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_initiateHs20IconQuery(this, this, macAddress, fileName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::getMacAddress(getMacAddress_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_getMacAddress(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::startRxFilter(startRxFilter_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_startRxFilter(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::stopRxFilter(stopRxFilter_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_stopRxFilter(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::addRxFilter(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::RxFilterType type, addRxFilter_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_addRxFilter(this, this, type, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::removeRxFilter(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::RxFilterType type, removeRxFilter_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_removeRxFilter(this, this, type, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setBtCoexistenceMode(::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface::BtCoexistenceMode mode, setBtCoexistenceMode_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_setBtCoexistenceMode(this, this, mode, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setBtCoexistenceScanModeEnabled(bool enable, setBtCoexistenceScanModeEnabled_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_setBtCoexistenceScanModeEnabled(this, this, enable, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setSuspendModeEnabled(bool enable, setSuspendModeEnabled_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_setSuspendModeEnabled(this, this, enable, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setCountryCode(const ::android::hardware::hidl_array<int8_t, 2>& code, setCountryCode_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_setCountryCode(this, this, code, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::startWpsRegistrar(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, const ::android::hardware::hidl_string& pin, startWpsRegistrar_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_startWpsRegistrar(this, this, bssid, pin, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::startWpsPbc(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPbc_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_startWpsPbc(this, this, bssid, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::startWpsPinKeypad(const ::android::hardware::hidl_string& pin, startWpsPinKeypad_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_startWpsPinKeypad(this, this, pin, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::startWpsPinDisplay(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPinDisplay_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_startWpsPinDisplay(this, this, bssid, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::cancelWps(cancelWps_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_cancelWps(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setExternalSim(bool useExternalSim, setExternalSim_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_setExternalSim(this, this, useExternalSim, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::addExtRadioWork(const ::android::hardware::hidl_string& name, uint32_t freqInMhz, uint32_t timeoutInSec, addExtRadioWork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_addExtRadioWork(this, this, name, freqInMhz, timeoutInSec, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::removeExtRadioWork(uint32_t id, removeExtRadioWork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_removeExtRadioWork(this, this, id, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::enableAutoReconnect(bool enable, enableAutoReconnect_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantStaIface::_hidl_enableAutoReconnect(this, this, enable, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface follow.
::android::hardware::Return<void> BpHwSupplicantStaIface::registerCallback_1_1(const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback>& callback, registerCallback_1_1_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_1::BpHwSupplicantStaIface::_hidl_registerCallback_1_1(this, this, callback, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwSupplicantStaIface::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantStaIface::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwSupplicantStaIface::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantStaIface::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantStaIface::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwSupplicantStaIface::BnHwSupplicantStaIface(const ::android::sp<ISupplicantStaIface> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi.supplicant@1.1", "ISupplicantStaIface") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwSupplicantStaIface::~BnHwSupplicantStaIface() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface follow.
::android::status_t BnHwSupplicantStaIface::_hidl_registerCallback_1_1(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantStaIface::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback> callback;

    {
        ::android::sp<::android::hardware::IBinder> _hidl_binder;
        _hidl_err = _hidl_data.readNullableStrongBinder(&_hidl_binder);
        if (_hidl_err != ::android::OK) { return _hidl_err; }

        callback = ::android::hardware::fromBinder<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIfaceCallback,::android::hardware::wifi::supplicant::V1_1::BpHwSupplicantStaIfaceCallback,::android::hardware::wifi::supplicant::V1_1::BnHwSupplicantStaIfaceCallback>(_hidl_binder);
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantStaIface::registerCallback_1_1::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&callback);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIface", "registerCallback_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantStaIface*>(_hidl_this->getImpl().get())->registerCallback_1_1(callback, [&](const auto &_hidl_out_status) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("registerCallback_1_1: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.1", "ISupplicantStaIface", "registerCallback_1_1", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("registerCallback_1_1: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface follow.

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantStaIface follow.

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwSupplicantStaIface::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwSupplicantStaIface::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwSupplicantStaIface::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* getName */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_getName(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* getType */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_getType(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* addNetwork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_addNetwork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* removeNetwork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_removeNetwork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* getNetwork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_getNetwork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* listNetworks */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_listNetworks(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* setWpsDeviceName */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsDeviceName(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* setWpsDeviceType */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsDeviceType(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 9 /* setWpsManufacturer */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsManufacturer(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 10 /* setWpsModelName */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsModelName(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 11 /* setWpsModelNumber */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsModelNumber(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 12 /* setWpsSerialNumber */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsSerialNumber(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 13 /* setWpsConfigMethods */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsConfigMethods(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 14 /* registerCallback */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_registerCallback(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 15 /* reassociate */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_reassociate(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 16 /* reconnect */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_reconnect(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 17 /* disconnect */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_disconnect(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 18 /* setPowerSave */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_setPowerSave(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 19 /* initiateTdlsDiscover */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_initiateTdlsDiscover(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 20 /* initiateTdlsSetup */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_initiateTdlsSetup(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 21 /* initiateTdlsTeardown */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_initiateTdlsTeardown(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 22 /* initiateAnqpQuery */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_initiateAnqpQuery(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 23 /* initiateHs20IconQuery */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_initiateHs20IconQuery(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 24 /* getMacAddress */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_getMacAddress(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 25 /* startRxFilter */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_startRxFilter(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 26 /* stopRxFilter */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_stopRxFilter(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 27 /* addRxFilter */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_addRxFilter(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 28 /* removeRxFilter */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_removeRxFilter(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 29 /* setBtCoexistenceMode */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_setBtCoexistenceMode(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 30 /* setBtCoexistenceScanModeEnabled */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_setBtCoexistenceScanModeEnabled(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 31 /* setSuspendModeEnabled */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_setSuspendModeEnabled(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 32 /* setCountryCode */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_setCountryCode(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 33 /* startWpsRegistrar */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_startWpsRegistrar(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 34 /* startWpsPbc */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_startWpsPbc(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 35 /* startWpsPinKeypad */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_startWpsPinKeypad(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 36 /* startWpsPinDisplay */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_startWpsPinDisplay(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 37 /* cancelWps */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_cancelWps(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 38 /* setExternalSim */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_setExternalSim(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 39 /* addExtRadioWork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_addExtRadioWork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 40 /* removeExtRadioWork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_removeExtRadioWork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 41 /* enableAutoReconnect */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantStaIface::_hidl_enableAutoReconnect(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 42 /* registerCallback_1_1 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_1::BnHwSupplicantStaIface::_hidl_registerCallback_1_1(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsSupplicantStaIface::BsSupplicantStaIface(const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicantStaIface> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.1", "ISupplicantStaIface"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsSupplicantStaIface::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<ISupplicantStaIface> ISupplicantStaIface::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantStaIface>(serviceName, false, getStub);
}

::android::sp<ISupplicantStaIface> ISupplicantStaIface::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantStaIface>(serviceName, true, getStub);
}

::android::status_t ISupplicantStaIface::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool ISupplicantStaIface::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi.supplicant@1.1::ISupplicantStaIface",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_1
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
