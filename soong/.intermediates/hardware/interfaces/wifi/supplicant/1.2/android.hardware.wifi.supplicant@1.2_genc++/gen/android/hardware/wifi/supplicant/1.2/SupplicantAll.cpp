#define LOG_TAG "android.hardware.wifi.supplicant@1.2::Supplicant"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/supplicant/1.2/BpHwSupplicant.h>
#include <android/hardware/wifi/supplicant/1.2/BnHwSupplicant.h>
#include <android/hardware/wifi/supplicant/1.2/BsSupplicant.h>
#include <android/hardware/wifi/supplicant/1.1/BpHwSupplicant.h>
#include <android/hardware/wifi/supplicant/1.0/BpHwSupplicant.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {
namespace V1_2 {

const char* ISupplicant::descriptor("android.hardware.wifi.supplicant@1.2::ISupplicant");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(ISupplicant::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwSupplicant(static_cast<ISupplicant *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(ISupplicant::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsSupplicant(static_cast<ISupplicant *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(ISupplicant::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(ISupplicant::descriptor);
};

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicant follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicant::getInterface(const ::android::hardware::wifi::supplicant::V1_0::ISupplicant::IfaceInfo& ifaceInfo, getInterface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicant::listInterfaces(listInterfaces_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicant::registerCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantCallback>& callback, registerCallback_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicant::setDebugParams(::android::hardware::wifi::supplicant::V1_0::ISupplicant::DebugLevel level, bool showTimestamp, bool showKeys, setDebugParams_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<::android::hardware::wifi::supplicant::V1_0::ISupplicant::DebugLevel> ISupplicant::getDebugLevel()
// no default implementation for: ::android::hardware::Return<bool> ISupplicant::isDebugShowTimestampEnabled()
// no default implementation for: ::android::hardware::Return<bool> ISupplicant::isDebugShowKeysEnabled()
// no default implementation for: ::android::hardware::Return<void> ISupplicant::setConcurrencyPriority(::android::hardware::wifi::supplicant::V1_0::IfaceType type, setConcurrencyPriority_cb _hidl_cb)

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicant follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicant::addInterface(const ::android::hardware::wifi::supplicant::V1_0::ISupplicant::IfaceInfo& ifaceInfo, addInterface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicant::removeInterface(const ::android::hardware::wifi::supplicant::V1_0::ISupplicant::IfaceInfo& ifaceInfo, removeInterface_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicant::terminate()

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> ISupplicant::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::supplicant::V1_2::ISupplicant::descriptor,
        ::android::hardware::wifi::supplicant::V1_1::ISupplicant::descriptor,
        ::android::hardware::wifi::supplicant::V1_0::ISupplicant::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> ISupplicant::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicant::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::supplicant::V1_2::ISupplicant::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicant::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){6,123,34,239,197,5,41,168,141,101,15,231,64,6,3,66,157,17,100,164,126,233,106,23,71,111,219,10,173,214,180,211} /* 067b22efc50529a88d650fe7400603429d1164a47ee96a17476fdb0aadd6b4d3 */,
        (uint8_t[32]){227,98,32,59,148,31,24,189,76,186,41,166,42,223,160,36,83,237,0,214,190,91,114,205,182,196,215,224,191,57,74,64} /* e362203b941f18bd4cba29a62adfa02453ed00d6be5b72cdb6c4d7e0bf394a40 */,
        (uint8_t[32]){247,229,92,8,24,125,140,133,80,104,161,238,61,12,141,174,238,117,112,41,45,150,80,156,33,168,117,109,79,92,251,155} /* f7e55c08187d8c855068a1ee3d0c8daeee7570292d96509c21a8756d4f5cfb9b */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicant::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicant::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> ISupplicant::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicant::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicant::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicant::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicant>> ISupplicant::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicant>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicant>> ISupplicant::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_1::ISupplicant>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicant, ::android::hardware::wifi::supplicant::V1_1::ISupplicant, BpHwSupplicant>(
            parent, "android.hardware.wifi.supplicant@1.2::ISupplicant", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicant>> ISupplicant::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicant>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicant, ::android::hardware::wifi::supplicant::V1_0::ISupplicant, BpHwSupplicant>(
            parent, "android.hardware.wifi.supplicant@1.2::ISupplicant", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicant>> ISupplicant::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicant, ::android::hidl::base::V1_0::IBase, BpHwSupplicant>(
            parent, "android.hardware.wifi.supplicant@1.2::ISupplicant", emitError);
}

BpHwSupplicant::BpHwSupplicant(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<ISupplicant>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.2", "ISupplicant") {
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicant follow.
::android::hardware::Return<void> BpHwSupplicant::getInterface(const ::android::hardware::wifi::supplicant::V1_0::ISupplicant::IfaceInfo& ifaceInfo, getInterface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_getInterface(this, this, ifaceInfo, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::listInterfaces(listInterfaces_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_listInterfaces(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::registerCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantCallback>& callback, registerCallback_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_registerCallback(this, this, callback, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::setDebugParams(::android::hardware::wifi::supplicant::V1_0::ISupplicant::DebugLevel level, bool showTimestamp, bool showKeys, setDebugParams_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_setDebugParams(this, this, level, showTimestamp, showKeys, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<::android::hardware::wifi::supplicant::V1_0::ISupplicant::DebugLevel> BpHwSupplicant::getDebugLevel(){
    ::android::hardware::Return<::android::hardware::wifi::supplicant::V1_0::ISupplicant::DebugLevel>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_getDebugLevel(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicant::isDebugShowTimestampEnabled(){
    ::android::hardware::Return<bool>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_isDebugShowTimestampEnabled(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicant::isDebugShowKeysEnabled(){
    ::android::hardware::Return<bool>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_isDebugShowKeysEnabled(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::setConcurrencyPriority(::android::hardware::wifi::supplicant::V1_0::IfaceType type, setConcurrencyPriority_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicant::_hidl_setConcurrencyPriority(this, this, type, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicant follow.
::android::hardware::Return<void> BpHwSupplicant::addInterface(const ::android::hardware::wifi::supplicant::V1_0::ISupplicant::IfaceInfo& ifaceInfo, addInterface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_1::BpHwSupplicant::_hidl_addInterface(this, this, ifaceInfo, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::removeInterface(const ::android::hardware::wifi::supplicant::V1_0::ISupplicant::IfaceInfo& ifaceInfo, removeInterface_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_1::BpHwSupplicant::_hidl_removeInterface(this, this, ifaceInfo, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::terminate(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_1::BpHwSupplicant::_hidl_terminate(this, this);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwSupplicant::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicant::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwSupplicant::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicant::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicant::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwSupplicant::BnHwSupplicant(const ::android::sp<ISupplicant> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi.supplicant@1.2", "ISupplicant") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwSupplicant::~BnHwSupplicant() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicant follow.

// Methods from ::android::hardware::wifi::supplicant::V1_1::ISupplicant follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwSupplicant::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwSupplicant::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwSupplicant::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* getInterface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_getInterface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* listInterfaces */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_listInterfaces(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* registerCallback */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_registerCallback(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* setDebugParams */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_setDebugParams(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* getDebugLevel */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_getDebugLevel(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* isDebugShowTimestampEnabled */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_isDebugShowTimestampEnabled(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* isDebugShowKeysEnabled */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_isDebugShowKeysEnabled(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* setConcurrencyPriority */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicant::_hidl_setConcurrencyPriority(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 9 /* addInterface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_1::BnHwSupplicant::_hidl_addInterface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 10 /* removeInterface */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_1::BnHwSupplicant::_hidl_removeInterface(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 11 /* terminate */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_1::BnHwSupplicant::_hidl_terminate(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsSupplicant::BsSupplicant(const ::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicant> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.2", "ISupplicant"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsSupplicant::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<ISupplicant> ISupplicant::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicant>(serviceName, false, getStub);
}

::android::sp<ISupplicant> ISupplicant::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicant>(serviceName, true, getStub);
}

::android::status_t ISupplicant::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool ISupplicant::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi.supplicant@1.2::ISupplicant",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_2
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
