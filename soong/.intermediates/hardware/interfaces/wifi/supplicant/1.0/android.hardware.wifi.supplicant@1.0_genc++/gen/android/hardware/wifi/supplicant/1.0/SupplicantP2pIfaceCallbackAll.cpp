#define LOG_TAG "android.hardware.wifi.supplicant@1.0::SupplicantP2pIfaceCallback"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/supplicant/1.0/BpHwSupplicantP2pIfaceCallback.h>
#include <android/hardware/wifi/supplicant/1.0/BnHwSupplicantP2pIfaceCallback.h>
#include <android/hardware/wifi/supplicant/1.0/BsSupplicantP2pIfaceCallback.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {
namespace V1_0 {

const char* ISupplicantP2pIfaceCallback::descriptor("android.hardware.wifi.supplicant@1.0::ISupplicantP2pIfaceCallback");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(ISupplicantP2pIfaceCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwSupplicantP2pIfaceCallback(static_cast<ISupplicantP2pIfaceCallback *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(ISupplicantP2pIfaceCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsSupplicantP2pIfaceCallback(static_cast<ISupplicantP2pIfaceCallback *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(ISupplicantP2pIfaceCallback::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(ISupplicantP2pIfaceCallback::descriptor);
};

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onNetworkAdded(uint32_t id)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onNetworkRemoved(uint32_t id)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onDeviceFound(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 8>& primaryDeviceType, const ::android::hardware::hidl_string& deviceName, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, uint8_t deviceCapabilities, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::P2pGroupCapabilityMask> groupCapabilities, const ::android::hardware::hidl_array<uint8_t, 6>& wfdDeviceInfo)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onDeviceLost(const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onFindStopped()
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onGoNegotiationRequest(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::WpsDevPasswordId passwordId)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onGoNegotiationCompleted(::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onGroupFormationSuccess()
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onGroupFormationFailure(const ::android::hardware::hidl_string& failureReason)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onGroupStarted(const ::android::hardware::hidl_string& groupIfname, bool isGo, const ::android::hardware::hidl_vec<uint8_t>& ssid, uint32_t frequency, const ::android::hardware::hidl_array<uint8_t, 32>& psk, const ::android::hardware::hidl_string& passphrase, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, bool isPersistent)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onGroupRemoved(const ::android::hardware::hidl_string& groupIfname, bool isGo)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onInvitationReceived(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, uint32_t persistentNetworkId, uint32_t operatingFrequency)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onInvitationResult(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onProvisionDiscoveryCompleted(const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress, bool isRequest, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pProvDiscStatusCode status, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, const ::android::hardware::hidl_string& generatedPin)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onServiceDiscoveryResponse(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, uint16_t updateIndicator, const ::android::hardware::hidl_vec<uint8_t>& tlvs)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onStaAuthorized(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIfaceCallback::onStaDeauthorized(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> ISupplicantP2pIfaceCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> ISupplicantP2pIfaceCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIfaceCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIfaceCallback::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){139,99,245,239,162,227,190,58,124,184,164,40,118,13,130,40,90,74,183,155,203,222,166,239,144,170,84,117,85,229,130,212} /* 8b63f5efa2e3be3a7cb8a428760d82285a4ab79bcbdea6ef90aa547555e582d4 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIfaceCallback::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantP2pIfaceCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> ISupplicantP2pIfaceCallback::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIfaceCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIfaceCallback::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantP2pIfaceCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback>> ISupplicantP2pIfaceCallback::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback>> ISupplicantP2pIfaceCallback::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantP2pIfaceCallback, ::android::hidl::base::V1_0::IBase, BpHwSupplicantP2pIfaceCallback>(
            parent, "android.hardware.wifi.supplicant@1.0::ISupplicantP2pIfaceCallback", emitError);
}

BpHwSupplicantP2pIfaceCallback::BpHwSupplicantP2pIfaceCallback(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<ISupplicantP2pIfaceCallback>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.0", "ISupplicantP2pIfaceCallback") {
}

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback follow.
::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onNetworkAdded(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint32_t id) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onNetworkAdded::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&id);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkAdded", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(id);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(1 /* onNetworkAdded */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkAdded", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onNetworkRemoved(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint32_t id) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onNetworkRemoved::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&id);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(id);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(2 /* onNetworkRemoved */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onDeviceFound(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 8>& primaryDeviceType, const ::android::hardware::hidl_string& deviceName, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, uint8_t deviceCapabilities, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::P2pGroupCapabilityMask> groupCapabilities, const ::android::hardware::hidl_array<uint8_t, 6>& wfdDeviceInfo) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onDeviceFound::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&srcAddress);
        _hidl_args.push_back((void *)&p2pDeviceAddress);
        _hidl_args.push_back((void *)&primaryDeviceType);
        _hidl_args.push_back((void *)&deviceName);
        _hidl_args.push_back((void *)&configMethods);
        _hidl_args.push_back((void *)&deviceCapabilities);
        _hidl_args.push_back((void *)&groupCapabilities);
        _hidl_args.push_back((void *)&wfdDeviceInfo);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceFound", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(srcAddress.data(), 6 * sizeof(uint8_t), &_hidl_srcAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(p2pDeviceAddress.data(), 6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_primaryDeviceType_parent;

    _hidl_err = _hidl_data.writeBuffer(primaryDeviceType.data(), 8 * sizeof(uint8_t), &_hidl_primaryDeviceType_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_deviceName_parent;

    _hidl_err = _hidl_data.writeBuffer(&deviceName, sizeof(deviceName), &_hidl_deviceName_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            deviceName,
            &_hidl_data,
            _hidl_deviceName_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint16((uint16_t)configMethods);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint8(deviceCapabilities);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32((uint32_t)groupCapabilities);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_wfdDeviceInfo_parent;

    _hidl_err = _hidl_data.writeBuffer(wfdDeviceInfo.data(), 6 * sizeof(uint8_t), &_hidl_wfdDeviceInfo_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(3 /* onDeviceFound */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceFound", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onDeviceLost(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onDeviceLost::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&p2pDeviceAddress);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceLost", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(p2pDeviceAddress.data(), 6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(4 /* onDeviceLost */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceLost", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onFindStopped(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onFindStopped::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onFindStopped", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(5 /* onFindStopped */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onFindStopped", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationRequest(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::WpsDevPasswordId passwordId) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGoNegotiationRequest::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&srcAddress);
        _hidl_args.push_back((void *)&passwordId);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationRequest", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(srcAddress.data(), 6 * sizeof(uint8_t), &_hidl_srcAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint16((uint16_t)passwordId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(6 /* onGoNegotiationRequest */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationRequest", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationCompleted(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGoNegotiationCompleted::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32((uint32_t)status);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(7 /* onGoNegotiationCompleted */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationSuccess(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupFormationSuccess::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationSuccess", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(8 /* onGroupFormationSuccess */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationSuccess", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationFailure(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& failureReason) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupFormationFailure::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&failureReason);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationFailure", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_failureReason_parent;

    _hidl_err = _hidl_data.writeBuffer(&failureReason, sizeof(failureReason), &_hidl_failureReason_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            failureReason,
            &_hidl_data,
            _hidl_failureReason_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(9 /* onGroupFormationFailure */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationFailure", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onGroupStarted(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& groupIfname, bool isGo, const ::android::hardware::hidl_vec<uint8_t>& ssid, uint32_t frequency, const ::android::hardware::hidl_array<uint8_t, 32>& psk, const ::android::hardware::hidl_string& passphrase, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, bool isPersistent) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupStarted::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&groupIfname);
        _hidl_args.push_back((void *)&isGo);
        _hidl_args.push_back((void *)&ssid);
        _hidl_args.push_back((void *)&frequency);
        _hidl_args.push_back((void *)&psk);
        _hidl_args.push_back((void *)&passphrase);
        _hidl_args.push_back((void *)&goDeviceAddress);
        _hidl_args.push_back((void *)&isPersistent);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupStarted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_groupIfname_parent;

    _hidl_err = _hidl_data.writeBuffer(&groupIfname, sizeof(groupIfname), &_hidl_groupIfname_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            groupIfname,
            &_hidl_data,
            _hidl_groupIfname_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeBool(isGo);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_ssid_parent;

    _hidl_err = _hidl_data.writeBuffer(&ssid, sizeof(ssid), &_hidl_ssid_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_ssid_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            ssid,
            &_hidl_data,
            _hidl_ssid_parent,
            0 /* parentOffset */, &_hidl_ssid_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(frequency);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_psk_parent;

    _hidl_err = _hidl_data.writeBuffer(psk.data(), 32 * sizeof(uint8_t), &_hidl_psk_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_passphrase_parent;

    _hidl_err = _hidl_data.writeBuffer(&passphrase, sizeof(passphrase), &_hidl_passphrase_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            passphrase,
            &_hidl_data,
            _hidl_passphrase_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_goDeviceAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(goDeviceAddress.data(), 6 * sizeof(uint8_t), &_hidl_goDeviceAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeBool(isPersistent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(10 /* onGroupStarted */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupStarted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onGroupRemoved(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& groupIfname, bool isGo) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupRemoved::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&groupIfname);
        _hidl_args.push_back((void *)&isGo);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_groupIfname_parent;

    _hidl_err = _hidl_data.writeBuffer(&groupIfname, sizeof(groupIfname), &_hidl_groupIfname_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            groupIfname,
            &_hidl_data,
            _hidl_groupIfname_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeBool(isGo);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(11 /* onGroupRemoved */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onInvitationReceived(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, uint32_t persistentNetworkId, uint32_t operatingFrequency) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onInvitationReceived::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&srcAddress);
        _hidl_args.push_back((void *)&goDeviceAddress);
        _hidl_args.push_back((void *)&bssid);
        _hidl_args.push_back((void *)&persistentNetworkId);
        _hidl_args.push_back((void *)&operatingFrequency);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationReceived", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(srcAddress.data(), 6 * sizeof(uint8_t), &_hidl_srcAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_goDeviceAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(goDeviceAddress.data(), 6 * sizeof(uint8_t), &_hidl_goDeviceAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_bssid_parent;

    _hidl_err = _hidl_data.writeBuffer(bssid.data(), 6 * sizeof(uint8_t), &_hidl_bssid_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(persistentNetworkId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(operatingFrequency);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(12 /* onInvitationReceived */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationReceived", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onInvitationResult(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onInvitationResult::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&bssid);
        _hidl_args.push_back((void *)&status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_bssid_parent;

    _hidl_err = _hidl_data.writeBuffer(bssid.data(), 6 * sizeof(uint8_t), &_hidl_bssid_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32((uint32_t)status);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(13 /* onInvitationResult */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onProvisionDiscoveryCompleted(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress, bool isRequest, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pProvDiscStatusCode status, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, const ::android::hardware::hidl_string& generatedPin) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onProvisionDiscoveryCompleted::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&p2pDeviceAddress);
        _hidl_args.push_back((void *)&isRequest);
        _hidl_args.push_back((void *)&status);
        _hidl_args.push_back((void *)&configMethods);
        _hidl_args.push_back((void *)&generatedPin);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onProvisionDiscoveryCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(p2pDeviceAddress.data(), 6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeBool(isRequest);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint8((uint8_t)status);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint16((uint16_t)configMethods);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_generatedPin_parent;

    _hidl_err = _hidl_data.writeBuffer(&generatedPin, sizeof(generatedPin), &_hidl_generatedPin_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            generatedPin,
            &_hidl_data,
            _hidl_generatedPin_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(14 /* onProvisionDiscoveryCompleted */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onProvisionDiscoveryCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onServiceDiscoveryResponse(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, uint16_t updateIndicator, const ::android::hardware::hidl_vec<uint8_t>& tlvs) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onServiceDiscoveryResponse::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&srcAddress);
        _hidl_args.push_back((void *)&updateIndicator);
        _hidl_args.push_back((void *)&tlvs);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onServiceDiscoveryResponse", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(srcAddress.data(), 6 * sizeof(uint8_t), &_hidl_srcAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint16(updateIndicator);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_tlvs_parent;

    _hidl_err = _hidl_data.writeBuffer(&tlvs, sizeof(tlvs), &_hidl_tlvs_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_tlvs_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            tlvs,
            &_hidl_data,
            _hidl_tlvs_parent,
            0 /* parentOffset */, &_hidl_tlvs_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(15 /* onServiceDiscoveryResponse */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onServiceDiscoveryResponse", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onStaAuthorized(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onStaAuthorized::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&srcAddress);
        _hidl_args.push_back((void *)&p2pDeviceAddress);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaAuthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(srcAddress.data(), 6 * sizeof(uint8_t), &_hidl_srcAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(p2pDeviceAddress.data(), 6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(16 /* onStaAuthorized */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaAuthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::_hidl_onStaDeauthorized(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onStaDeauthorized::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&srcAddress);
        _hidl_args.push_back((void *)&p2pDeviceAddress);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaDeauthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIfaceCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(srcAddress.data(), 6 * sizeof(uint8_t), &_hidl_srcAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(p2pDeviceAddress.data(), 6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(17 /* onStaDeauthorized */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaDeauthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback follow.
::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onNetworkAdded(uint32_t id){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onNetworkAdded(this, this, id);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onNetworkRemoved(uint32_t id){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onNetworkRemoved(this, this, id);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onDeviceFound(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 8>& primaryDeviceType, const ::android::hardware::hidl_string& deviceName, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, uint8_t deviceCapabilities, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::P2pGroupCapabilityMask> groupCapabilities, const ::android::hardware::hidl_array<uint8_t, 6>& wfdDeviceInfo){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onDeviceFound(this, this, srcAddress, p2pDeviceAddress, primaryDeviceType, deviceName, configMethods, deviceCapabilities, groupCapabilities, wfdDeviceInfo);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onDeviceLost(const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onDeviceLost(this, this, p2pDeviceAddress);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onFindStopped(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onFindStopped(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onGoNegotiationRequest(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::WpsDevPasswordId passwordId){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationRequest(this, this, srcAddress, passwordId);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onGoNegotiationCompleted(::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationCompleted(this, this, status);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onGroupFormationSuccess(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationSuccess(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onGroupFormationFailure(const ::android::hardware::hidl_string& failureReason){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationFailure(this, this, failureReason);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onGroupStarted(const ::android::hardware::hidl_string& groupIfname, bool isGo, const ::android::hardware::hidl_vec<uint8_t>& ssid, uint32_t frequency, const ::android::hardware::hidl_array<uint8_t, 32>& psk, const ::android::hardware::hidl_string& passphrase, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, bool isPersistent){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onGroupStarted(this, this, groupIfname, isGo, ssid, frequency, psk, passphrase, goDeviceAddress, isPersistent);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onGroupRemoved(const ::android::hardware::hidl_string& groupIfname, bool isGo){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onGroupRemoved(this, this, groupIfname, isGo);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onInvitationReceived(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, uint32_t persistentNetworkId, uint32_t operatingFrequency){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onInvitationReceived(this, this, srcAddress, goDeviceAddress, bssid, persistentNetworkId, operatingFrequency);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onInvitationResult(const ::android::hardware::hidl_array<uint8_t, 6>& bssid, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onInvitationResult(this, this, bssid, status);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onProvisionDiscoveryCompleted(const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress, bool isRequest, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pProvDiscStatusCode status, ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, const ::android::hardware::hidl_string& generatedPin){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onProvisionDiscoveryCompleted(this, this, p2pDeviceAddress, isRequest, status, configMethods, generatedPin);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onServiceDiscoveryResponse(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, uint16_t updateIndicator, const ::android::hardware::hidl_vec<uint8_t>& tlvs){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onServiceDiscoveryResponse(this, this, srcAddress, updateIndicator, tlvs);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onStaAuthorized(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onStaAuthorized(this, this, srcAddress, p2pDeviceAddress);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::onStaDeauthorized(const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& p2pDeviceAddress){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIfaceCallback::_hidl_onStaDeauthorized(this, this, srcAddress, p2pDeviceAddress);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantP2pIfaceCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIfaceCallback::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantP2pIfaceCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwSupplicantP2pIfaceCallback::BnHwSupplicantP2pIfaceCallback(const ::android::sp<ISupplicantP2pIfaceCallback> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi.supplicant@1.0", "ISupplicantP2pIfaceCallback") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwSupplicantP2pIfaceCallback::~BnHwSupplicantP2pIfaceCallback() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback follow.
::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onNetworkAdded(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint32_t id;

    _hidl_err = _hidl_data.readUint32(&id);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onNetworkAdded::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&id);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkAdded", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onNetworkAdded(id);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkAdded", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onNetworkRemoved(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint32_t id;

    _hidl_err = _hidl_data.readUint32(&id);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onNetworkRemoved::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&id);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onNetworkRemoved(id);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onNetworkRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onDeviceFound(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* srcAddress;
    const ::android::hardware::hidl_array<uint8_t, 6>* p2pDeviceAddress;
    const ::android::hardware::hidl_array<uint8_t, 8>* primaryDeviceType;
    const ::android::hardware::hidl_string* deviceName;
    ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods;
    uint8_t deviceCapabilities;
    ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::P2pGroupCapabilityMask> groupCapabilities;
    const ::android::hardware::hidl_array<uint8_t, 6>* wfdDeviceInfo;

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_srcAddress_parent,  reinterpret_cast<const void **>(&srcAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent,  reinterpret_cast<const void **>(&p2pDeviceAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_primaryDeviceType_parent;

    _hidl_err = _hidl_data.readBuffer(8 * sizeof(uint8_t), &_hidl_primaryDeviceType_parent,  reinterpret_cast<const void **>(&primaryDeviceType));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_deviceName_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*deviceName), &_hidl_deviceName_parent,  reinterpret_cast<const void **>(&deviceName));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*deviceName),
            _hidl_data,
            _hidl_deviceName_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint16((uint16_t *)&configMethods);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint8(&deviceCapabilities);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32((uint32_t *)&groupCapabilities);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_wfdDeviceInfo_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_wfdDeviceInfo_parent,  reinterpret_cast<const void **>(&wfdDeviceInfo));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onDeviceFound::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)srcAddress);
        _hidl_args.push_back((void *)p2pDeviceAddress);
        _hidl_args.push_back((void *)primaryDeviceType);
        _hidl_args.push_back((void *)deviceName);
        _hidl_args.push_back((void *)&configMethods);
        _hidl_args.push_back((void *)&deviceCapabilities);
        _hidl_args.push_back((void *)&groupCapabilities);
        _hidl_args.push_back((void *)wfdDeviceInfo);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceFound", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onDeviceFound(*srcAddress, *p2pDeviceAddress, *primaryDeviceType, *deviceName, configMethods, deviceCapabilities, groupCapabilities, *wfdDeviceInfo);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceFound", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onDeviceLost(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* p2pDeviceAddress;

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent,  reinterpret_cast<const void **>(&p2pDeviceAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onDeviceLost::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)p2pDeviceAddress);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceLost", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onDeviceLost(*p2pDeviceAddress);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onDeviceLost", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onFindStopped(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onFindStopped::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onFindStopped", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onFindStopped();

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onFindStopped", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationRequest(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* srcAddress;
    ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::WpsDevPasswordId passwordId;

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_srcAddress_parent,  reinterpret_cast<const void **>(&srcAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint16((uint16_t *)&passwordId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGoNegotiationRequest::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)srcAddress);
        _hidl_args.push_back((void *)&passwordId);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationRequest", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onGoNegotiationRequest(*srcAddress, passwordId);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationRequest", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationCompleted(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status;

    _hidl_err = _hidl_data.readUint32((uint32_t *)&status);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGoNegotiationCompleted::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onGoNegotiationCompleted(status);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGoNegotiationCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationSuccess(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupFormationSuccess::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationSuccess", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onGroupFormationSuccess();

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationSuccess", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationFailure(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* failureReason;

    size_t _hidl_failureReason_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*failureReason), &_hidl_failureReason_parent,  reinterpret_cast<const void **>(&failureReason));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*failureReason),
            _hidl_data,
            _hidl_failureReason_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupFormationFailure::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)failureReason);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationFailure", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onGroupFormationFailure(*failureReason);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupFormationFailure", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onGroupStarted(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* groupIfname;
    bool isGo;
    const ::android::hardware::hidl_vec<uint8_t>* ssid;
    uint32_t frequency;
    const ::android::hardware::hidl_array<uint8_t, 32>* psk;
    const ::android::hardware::hidl_string* passphrase;
    const ::android::hardware::hidl_array<uint8_t, 6>* goDeviceAddress;
    bool isPersistent;

    size_t _hidl_groupIfname_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*groupIfname), &_hidl_groupIfname_parent,  reinterpret_cast<const void **>(&groupIfname));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*groupIfname),
            _hidl_data,
            _hidl_groupIfname_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readBool(&isGo);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_ssid_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*ssid), &_hidl_ssid_parent,  reinterpret_cast<const void **>(&ssid));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_ssid_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint8_t> &>(*ssid),
            _hidl_data,
            _hidl_ssid_parent,
            0 /* parentOffset */, &_hidl_ssid_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32(&frequency);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_psk_parent;

    _hidl_err = _hidl_data.readBuffer(32 * sizeof(uint8_t), &_hidl_psk_parent,  reinterpret_cast<const void **>(&psk));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_passphrase_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*passphrase), &_hidl_passphrase_parent,  reinterpret_cast<const void **>(&passphrase));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*passphrase),
            _hidl_data,
            _hidl_passphrase_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_goDeviceAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_goDeviceAddress_parent,  reinterpret_cast<const void **>(&goDeviceAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readBool(&isPersistent);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupStarted::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)groupIfname);
        _hidl_args.push_back((void *)&isGo);
        _hidl_args.push_back((void *)ssid);
        _hidl_args.push_back((void *)&frequency);
        _hidl_args.push_back((void *)psk);
        _hidl_args.push_back((void *)passphrase);
        _hidl_args.push_back((void *)goDeviceAddress);
        _hidl_args.push_back((void *)&isPersistent);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupStarted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onGroupStarted(*groupIfname, isGo, *ssid, frequency, *psk, *passphrase, *goDeviceAddress, isPersistent);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupStarted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onGroupRemoved(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* groupIfname;
    bool isGo;

    size_t _hidl_groupIfname_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*groupIfname), &_hidl_groupIfname_parent,  reinterpret_cast<const void **>(&groupIfname));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*groupIfname),
            _hidl_data,
            _hidl_groupIfname_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readBool(&isGo);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onGroupRemoved::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)groupIfname);
        _hidl_args.push_back((void *)&isGo);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onGroupRemoved(*groupIfname, isGo);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onGroupRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onInvitationReceived(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* srcAddress;
    const ::android::hardware::hidl_array<uint8_t, 6>* goDeviceAddress;
    const ::android::hardware::hidl_array<uint8_t, 6>* bssid;
    uint32_t persistentNetworkId;
    uint32_t operatingFrequency;

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_srcAddress_parent,  reinterpret_cast<const void **>(&srcAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_goDeviceAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_goDeviceAddress_parent,  reinterpret_cast<const void **>(&goDeviceAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_bssid_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_bssid_parent,  reinterpret_cast<const void **>(&bssid));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32(&persistentNetworkId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32(&operatingFrequency);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onInvitationReceived::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)srcAddress);
        _hidl_args.push_back((void *)goDeviceAddress);
        _hidl_args.push_back((void *)bssid);
        _hidl_args.push_back((void *)&persistentNetworkId);
        _hidl_args.push_back((void *)&operatingFrequency);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationReceived", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onInvitationReceived(*srcAddress, *goDeviceAddress, *bssid, persistentNetworkId, operatingFrequency);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationReceived", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onInvitationResult(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* bssid;
    ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pStatusCode status;

    size_t _hidl_bssid_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_bssid_parent,  reinterpret_cast<const void **>(&bssid));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32((uint32_t *)&status);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onInvitationResult::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)bssid);
        _hidl_args.push_back((void *)&status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onInvitationResult(*bssid, status);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onInvitationResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onProvisionDiscoveryCompleted(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* p2pDeviceAddress;
    bool isRequest;
    ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback::P2pProvDiscStatusCode status;
    ::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods;
    const ::android::hardware::hidl_string* generatedPin;

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent,  reinterpret_cast<const void **>(&p2pDeviceAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readBool(&isRequest);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint8((uint8_t *)&status);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint16((uint16_t *)&configMethods);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_generatedPin_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*generatedPin), &_hidl_generatedPin_parent,  reinterpret_cast<const void **>(&generatedPin));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*generatedPin),
            _hidl_data,
            _hidl_generatedPin_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onProvisionDiscoveryCompleted::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)p2pDeviceAddress);
        _hidl_args.push_back((void *)&isRequest);
        _hidl_args.push_back((void *)&status);
        _hidl_args.push_back((void *)&configMethods);
        _hidl_args.push_back((void *)generatedPin);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onProvisionDiscoveryCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onProvisionDiscoveryCompleted(*p2pDeviceAddress, isRequest, status, configMethods, *generatedPin);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onProvisionDiscoveryCompleted", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onServiceDiscoveryResponse(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* srcAddress;
    uint16_t updateIndicator;
    const ::android::hardware::hidl_vec<uint8_t>* tlvs;

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_srcAddress_parent,  reinterpret_cast<const void **>(&srcAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint16(&updateIndicator);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_tlvs_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*tlvs), &_hidl_tlvs_parent,  reinterpret_cast<const void **>(&tlvs));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_tlvs_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint8_t> &>(*tlvs),
            _hidl_data,
            _hidl_tlvs_parent,
            0 /* parentOffset */, &_hidl_tlvs_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onServiceDiscoveryResponse::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)srcAddress);
        _hidl_args.push_back((void *)&updateIndicator);
        _hidl_args.push_back((void *)tlvs);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onServiceDiscoveryResponse", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onServiceDiscoveryResponse(*srcAddress, updateIndicator, *tlvs);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onServiceDiscoveryResponse", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onStaAuthorized(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* srcAddress;
    const ::android::hardware::hidl_array<uint8_t, 6>* p2pDeviceAddress;

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_srcAddress_parent,  reinterpret_cast<const void **>(&srcAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent,  reinterpret_cast<const void **>(&p2pDeviceAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onStaAuthorized::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)srcAddress);
        _hidl_args.push_back((void *)p2pDeviceAddress);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaAuthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onStaAuthorized(*srcAddress, *p2pDeviceAddress);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaAuthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIfaceCallback::_hidl_onStaDeauthorized(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIfaceCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_array<uint8_t, 6>* srcAddress;
    const ::android::hardware::hidl_array<uint8_t, 6>* p2pDeviceAddress;

    size_t _hidl_srcAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_srcAddress_parent,  reinterpret_cast<const void **>(&srcAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_p2pDeviceAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_p2pDeviceAddress_parent,  reinterpret_cast<const void **>(&p2pDeviceAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIfaceCallback::onStaDeauthorized::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)srcAddress);
        _hidl_args.push_back((void *)p2pDeviceAddress);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaDeauthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIfaceCallback*>(_hidl_this->getImpl().get())->onStaDeauthorized(*srcAddress, *p2pDeviceAddress);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.0", "ISupplicantP2pIfaceCallback", "onStaDeauthorized", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwSupplicantP2pIfaceCallback::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwSupplicantP2pIfaceCallback::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwSupplicantP2pIfaceCallback::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* onNetworkAdded */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onNetworkAdded(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* onNetworkRemoved */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onNetworkRemoved(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* onDeviceFound */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onDeviceFound(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* onDeviceLost */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onDeviceLost(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* onFindStopped */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onFindStopped(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* onGoNegotiationRequest */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationRequest(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* onGoNegotiationCompleted */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onGoNegotiationCompleted(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* onGroupFormationSuccess */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationSuccess(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 9 /* onGroupFormationFailure */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onGroupFormationFailure(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 10 /* onGroupStarted */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onGroupStarted(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 11 /* onGroupRemoved */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onGroupRemoved(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 12 /* onInvitationReceived */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onInvitationReceived(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 13 /* onInvitationResult */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onInvitationResult(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 14 /* onProvisionDiscoveryCompleted */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onProvisionDiscoveryCompleted(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 15 /* onServiceDiscoveryResponse */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onServiceDiscoveryResponse(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 16 /* onStaAuthorized */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onStaAuthorized(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 17 /* onStaDeauthorized */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIfaceCallback::_hidl_onStaDeauthorized(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsSupplicantP2pIfaceCallback::BsSupplicantP2pIfaceCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.0", "ISupplicantP2pIfaceCallback"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsSupplicantP2pIfaceCallback::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<ISupplicantP2pIfaceCallback> ISupplicantP2pIfaceCallback::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantP2pIfaceCallback>(serviceName, false, getStub);
}

::android::sp<ISupplicantP2pIfaceCallback> ISupplicantP2pIfaceCallback::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantP2pIfaceCallback>(serviceName, true, getStub);
}

::android::status_t ISupplicantP2pIfaceCallback::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool ISupplicantP2pIfaceCallback::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi.supplicant@1.0::ISupplicantP2pIfaceCallback",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_0
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
