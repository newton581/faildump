#ifndef HIDL_GENERATED_ANDROID_HARDWARE_WIFI_V1_0_IWIFICHIPEVENTCALLBACK_H
#define HIDL_GENERATED_ANDROID_HARDWARE_WIFI_V1_0_IWIFICHIPEVENTCALLBACK_H

#include <android/hardware/wifi/1.0/types.h>
#include <android/hidl/base/1.0/IBase.h>

#include <android/hidl/manager/1.0/IServiceNotification.h>

#include <hidl/HidlSupport.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/NativeHandle.h>
#include <utils/misc.h>

namespace android {
namespace hardware {
namespace wifi {
namespace V1_0 {

struct IWifiChipEventCallback : public ::android::hidl::base::V1_0::IBase {
    /**
     * Type tag for use in template logic that indicates this is a 'pure' class.
     */
    typedef android::hardware::details::i_tag _hidl_tag;

    /**
     * Fully qualified interface name: "android.hardware.wifi@1.0::IWifiChipEventCallback"
     */
    static const char* descriptor;

    /**
     * Returns whether this object's implementation is outside of the current process.
     */
    virtual bool isRemote() const override { return false; }

    /**
     * Callback indicating that the chip has been reconfigured successfully. At
     * this point the interfaces available in the mode must be able to be
     * configured. When this is called any previous iface objects must be
     * considered invalid.
     * 
     * @param modeId The mode that the chip switched to, corresponding to the id
     *        property of the target ChipMode.
     */
    virtual ::android::hardware::Return<void> onChipReconfigured(uint32_t modeId) = 0;

    /**
     * Callback indicating that a chip reconfiguration failed. This is a fatal
     * error and any iface objects available previously must be considered
     * invalid. The client can attempt to recover by trying to reconfigure the
     * chip again using |IWifiChip.configureChip|.
     * 
     * @param status Failure reason code.
     */
    virtual ::android::hardware::Return<void> onChipReconfigureFailure(const ::android::hardware::wifi::V1_0::WifiStatus& status) = 0;

    /**
     * Callback indicating that a new iface has been added to the chip.
     * 
     * @param type Type of iface added.
     * @param name Name of iface added.
     */
    virtual ::android::hardware::Return<void> onIfaceAdded(::android::hardware::wifi::V1_0::IfaceType type, const ::android::hardware::hidl_string& name) = 0;

    /**
     * Callback indicating that an existing iface has been removed from the chip.
     * 
     * @param type Type of iface removed.
     * @param name Name of iface removed.
     */
    virtual ::android::hardware::Return<void> onIfaceRemoved(::android::hardware::wifi::V1_0::IfaceType type, const ::android::hardware::hidl_string& name) = 0;

    /**
     * Callbacks for reporting debug ring buffer data.
     * 
     * The ring buffer data collection is event based:
     * - Driver calls this callback when new records are available, the
     *   |WifiDebugRingBufferStatus| passed up to framework in the callback
     *   indicates to framework if more data is available in the ring buffer.
     *   It is not expected that driver will necessarily always empty the ring
     *   immediately as data is available, instead driver will report data
     *   every X seconds or if N bytes are available based on the parameters
     *   set via |startLoggingToDebugRingBuffer|.
     * - In the case where a bug report has to be captured, framework will
     *   require driver to upload all data immediately. This is indicated to
     *   driver when framework calls |forceDumpToDebugRingBuffer|.  The driver
     *   will start sending all available data in the indicated ring by repeatedly
     *   invoking this callback.
     * 
     * @return status Status of the corresponding ring buffer. This should
     *         contain the name of the ring buffer on which the data is
     *         available.
     * @return data Raw bytes of data sent by the driver. Must be dumped
     *         out to a bugreport and post processed.
     */
    virtual ::android::hardware::Return<void> onDebugRingBufferDataAvailable(const ::android::hardware::wifi::V1_0::WifiDebugRingBufferStatus& status, const ::android::hardware::hidl_vec<uint8_t>& data) = 0;

    /**
     * Callback indicating that the chip has encountered a fatal error.
     * Client must not attempt to parse either the errorCode or debugData.
     * Must only be captured in a bugreport.
     * 
     * @param errorCode Vendor defined error code.
     * @param debugData Vendor defined data used for debugging.
     */
    virtual ::android::hardware::Return<void> onDebugErrorAlert(int32_t errorCode, const ::android::hardware::hidl_vec<uint8_t>& debugData) = 0;

    /**
     * Return callback for interfaceChain
     */
    using interfaceChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& descriptors)>;
    virtual ::android::hardware::Return<void> interfaceChain(interfaceChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options) override;

    /**
     * Return callback for interfaceDescriptor
     */
    using interfaceDescriptor_cb = std::function<void(const ::android::hardware::hidl_string& descriptor)>;
    virtual ::android::hardware::Return<void> interfaceDescriptor(interfaceDescriptor_cb _hidl_cb) override;

    /**
     * Return callback for getHashChain
     */
    using getHashChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_array<uint8_t, 32>>& hashchain)>;
    virtual ::android::hardware::Return<void> getHashChain(getHashChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> setHALInstrumentation() override;

    virtual ::android::hardware::Return<bool> linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie) override;

    virtual ::android::hardware::Return<void> ping() override;

    /**
     * Return callback for getDebugInfo
     */
    using getDebugInfo_cb = std::function<void(const ::android::hidl::base::V1_0::DebugInfo& info)>;
    virtual ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> notifySyspropsChanged() override;

    virtual ::android::hardware::Return<bool> unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient) override;

    // cast static functions
    /**
     * This performs a checked cast based on what the underlying implementation actually is.
     */
    static ::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_0::IWifiChipEventCallback>> castFrom(const ::android::sp<::android::hardware::wifi::V1_0::IWifiChipEventCallback>& parent, bool emitError = false);
    /**
     * This performs a checked cast based on what the underlying implementation actually is.
     */
    static ::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_0::IWifiChipEventCallback>> castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError = false);

    // helper methods for interactions with the hwservicemanager
    /**
     * This gets the service of this type with the specified instance name. If the
     * service is currently not available or not in the VINTF manifest on a Trebilized
     * device, this will return nullptr. This is useful when you don't want to block
     * during device boot. If getStub is true, this will try to return an unwrapped
     * passthrough implementation in the same process. This is useful when getting an
     * implementation from the same partition/compilation group.
     * 
     * In general, prefer getService(std::string,bool)
     */
    static ::android::sp<IWifiChipEventCallback> tryGetService(const std::string &serviceName="default", bool getStub=false);
    /**
     * Deprecated. See tryGetService(std::string, bool)
     */
    static ::android::sp<IWifiChipEventCallback> tryGetService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return tryGetService(str, getStub); }
    /**
     * Deprecated. See tryGetService(std::string, bool)
     */
    static ::android::sp<IWifiChipEventCallback> tryGetService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return tryGetService(str, getStub); }
    /**
     * Calls tryGetService("default", bool). This is the recommended instance name for singleton services.
     */
    static ::android::sp<IWifiChipEventCallback> tryGetService(bool getStub) { return tryGetService("default", getStub); }
    /**
     * This gets the service of this type with the specified instance name. If the
     * service is not in the VINTF manifest on a Trebilized device, this will return
     * nullptr. If the service is not available, this will wait for the service to
     * become available. If the service is a lazy service, this will start the service
     * and return when it becomes available. If getStub is true, this will try to
     * return an unwrapped passthrough implementation in the same process. This is
     * useful when getting an implementation from the same partition/compilation group.
     */
    static ::android::sp<IWifiChipEventCallback> getService(const std::string &serviceName="default", bool getStub=false);
    /**
     * Deprecated. See getService(std::string, bool)
     */
    static ::android::sp<IWifiChipEventCallback> getService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return getService(str, getStub); }
    /**
     * Deprecated. See getService(std::string, bool)
     */
    static ::android::sp<IWifiChipEventCallback> getService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return getService(str, getStub); }
    /**
     * Calls getService("default", bool). This is the recommended instance name for singleton services.
     */
    static ::android::sp<IWifiChipEventCallback> getService(bool getStub) { return getService("default", getStub); }
    /**
     * Registers a service with the service manager. For Trebilized devices, the service
     * must also be in the VINTF manifest.
     */
    __attribute__ ((warn_unused_result))::android::status_t registerAsService(const std::string &serviceName="default");
    /**
     * Registers for notifications for when a service is registered.
     */
    static bool registerForNotifications(
            const std::string &serviceName,
            const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification);
};

//
// type declarations for package
//

static inline std::string toString(const ::android::sp<::android::hardware::wifi::V1_0::IWifiChipEventCallback>& o);

//
// type header definitions for package
//

static inline std::string toString(const ::android::sp<::android::hardware::wifi::V1_0::IWifiChipEventCallback>& o) {
    std::string os = "[class or subclass of ";
    os += ::android::hardware::wifi::V1_0::IWifiChipEventCallback::descriptor;
    os += "]";
    os += o->isRemote() ? "@remote" : "@local";
    return os;
}


}  // namespace V1_0
}  // namespace wifi
}  // namespace hardware
}  // namespace android

//
// global type declarations for package
//


#endif  // HIDL_GENERATED_ANDROID_HARDWARE_WIFI_V1_0_IWIFICHIPEVENTCALLBACK_H
