#define LOG_TAG "android.hardware.secure_element@1.1::SecureElement"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/secure_element/1.1/BpHwSecureElement.h>
#include <android/hardware/secure_element/1.1/BnHwSecureElement.h>
#include <android/hardware/secure_element/1.1/BsSecureElement.h>
#include <android/hardware/secure_element/1.0/BpHwSecureElement.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace secure_element {
namespace V1_1 {

const char* ISecureElement::descriptor("android.hardware.secure_element@1.1::ISecureElement");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(ISecureElement::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwSecureElement(static_cast<ISecureElement *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(ISecureElement::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsSecureElement(static_cast<ISecureElement *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(ISecureElement::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(ISecureElement::descriptor);
};

// Methods from ::android::hardware::secure_element::V1_0::ISecureElement follow.
// no default implementation for: ::android::hardware::Return<void> ISecureElement::init(const ::android::sp<::android::hardware::secure_element::V1_0::ISecureElementHalCallback>& clientCallback)
// no default implementation for: ::android::hardware::Return<void> ISecureElement::getAtr(getAtr_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<bool> ISecureElement::isCardPresent()
// no default implementation for: ::android::hardware::Return<void> ISecureElement::transmit(const ::android::hardware::hidl_vec<uint8_t>& data, transmit_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISecureElement::openLogicalChannel(const ::android::hardware::hidl_vec<uint8_t>& aid, uint8_t p2, openLogicalChannel_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISecureElement::openBasicChannel(const ::android::hardware::hidl_vec<uint8_t>& aid, uint8_t p2, openBasicChannel_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<::android::hardware::secure_element::V1_0::SecureElementStatus> ISecureElement::closeChannel(uint8_t channelNumber)

// Methods from ::android::hardware::secure_element::V1_1::ISecureElement follow.
// no default implementation for: ::android::hardware::Return<void> ISecureElement::init_1_1(const ::android::sp<::android::hardware::secure_element::V1_1::ISecureElementHalCallback>& clientCallback)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> ISecureElement::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::secure_element::V1_1::ISecureElement::descriptor,
        ::android::hardware::secure_element::V1_0::ISecureElement::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> ISecureElement::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISecureElement::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::secure_element::V1_1::ISecureElement::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISecureElement::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){8,212,57,196,99,228,4,79,167,136,116,3,125,142,131,121,170,60,171,236,222,50,240,138,119,88,151,238,165,165,56,175} /* 08d439c463e4044fa78874037d8e8379aa3cabecde32f08a775897eea5a538af */,
        (uint8_t[32]){189,118,153,240,123,165,57,35,16,254,253,51,234,150,78,1,244,244,166,96,21,20,104,69,200,80,85,0,72,35,204,129} /* bd7699f07ba5392310fefd33ea964e01f4f4a66015146845c85055004823cc81 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISecureElement::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISecureElement::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> ISecureElement::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISecureElement::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISecureElement::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISecureElement::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::secure_element::V1_1::ISecureElement>> ISecureElement::castFrom(const ::android::sp<::android::hardware::secure_element::V1_1::ISecureElement>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::secure_element::V1_1::ISecureElement>> ISecureElement::castFrom(const ::android::sp<::android::hardware::secure_element::V1_0::ISecureElement>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISecureElement, ::android::hardware::secure_element::V1_0::ISecureElement, BpHwSecureElement>(
            parent, "android.hardware.secure_element@1.1::ISecureElement", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::secure_element::V1_1::ISecureElement>> ISecureElement::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISecureElement, ::android::hidl::base::V1_0::IBase, BpHwSecureElement>(
            parent, "android.hardware.secure_element@1.1::ISecureElement", emitError);
}

BpHwSecureElement::BpHwSecureElement(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<ISecureElement>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.secure_element@1.1", "ISecureElement") {
}

// Methods from ::android::hardware::secure_element::V1_1::ISecureElement follow.
::android::hardware::Return<void> BpHwSecureElement::_hidl_init_1_1(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::sp<::android::hardware::secure_element::V1_1::ISecureElementHalCallback>& clientCallback) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISecureElement::init_1_1::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&clientCallback);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.secure_element", "1.1", "ISecureElement", "init_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSecureElement::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (clientCallback == nullptr) {
        _hidl_err = _hidl_data.writeStrongBinder(nullptr);
    } else {
        ::android::sp<::android::hardware::IBinder> _hidl_binder = ::android::hardware::getOrCreateCachedBinder(clientCallback.get());
        if (_hidl_binder.get() != nullptr) {
            _hidl_err = _hidl_data.writeStrongBinder(_hidl_binder);
        } else {
            _hidl_err = ::android::UNKNOWN_ERROR;
        }
    }
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    ::android::hardware::ProcessState::self()->startThreadPool();
    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(8 /* init_1_1 */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.secure_element", "1.1", "ISecureElement", "init_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::secure_element::V1_0::ISecureElement follow.
::android::hardware::Return<void> BpHwSecureElement::init(const ::android::sp<::android::hardware::secure_element::V1_0::ISecureElementHalCallback>& clientCallback){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::secure_element::V1_0::BpHwSecureElement::_hidl_init(this, this, clientCallback);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::getAtr(getAtr_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::secure_element::V1_0::BpHwSecureElement::_hidl_getAtr(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSecureElement::isCardPresent(){
    ::android::hardware::Return<bool>  _hidl_out = ::android::hardware::secure_element::V1_0::BpHwSecureElement::_hidl_isCardPresent(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::transmit(const ::android::hardware::hidl_vec<uint8_t>& data, transmit_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::secure_element::V1_0::BpHwSecureElement::_hidl_transmit(this, this, data, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::openLogicalChannel(const ::android::hardware::hidl_vec<uint8_t>& aid, uint8_t p2, openLogicalChannel_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::secure_element::V1_0::BpHwSecureElement::_hidl_openLogicalChannel(this, this, aid, p2, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::openBasicChannel(const ::android::hardware::hidl_vec<uint8_t>& aid, uint8_t p2, openBasicChannel_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::secure_element::V1_0::BpHwSecureElement::_hidl_openBasicChannel(this, this, aid, p2, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<::android::hardware::secure_element::V1_0::SecureElementStatus> BpHwSecureElement::closeChannel(uint8_t channelNumber){
    ::android::hardware::Return<::android::hardware::secure_element::V1_0::SecureElementStatus>  _hidl_out = ::android::hardware::secure_element::V1_0::BpHwSecureElement::_hidl_closeChannel(this, this, channelNumber);

    return _hidl_out;
}


// Methods from ::android::hardware::secure_element::V1_1::ISecureElement follow.
::android::hardware::Return<void> BpHwSecureElement::init_1_1(const ::android::sp<::android::hardware::secure_element::V1_1::ISecureElementHalCallback>& clientCallback){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::secure_element::V1_1::BpHwSecureElement::_hidl_init_1_1(this, this, clientCallback);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwSecureElement::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSecureElement::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwSecureElement::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSecureElement::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSecureElement::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwSecureElement::BnHwSecureElement(const ::android::sp<ISecureElement> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.secure_element@1.1", "ISecureElement") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwSecureElement::~BnHwSecureElement() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::secure_element::V1_1::ISecureElement follow.
::android::status_t BnHwSecureElement::_hidl_init_1_1(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSecureElement::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    ::android::sp<::android::hardware::secure_element::V1_1::ISecureElementHalCallback> clientCallback;

    {
        ::android::sp<::android::hardware::IBinder> _hidl_binder;
        _hidl_err = _hidl_data.readNullableStrongBinder(&_hidl_binder);
        if (_hidl_err != ::android::OK) { return _hidl_err; }

        clientCallback = ::android::hardware::fromBinder<::android::hardware::secure_element::V1_1::ISecureElementHalCallback,::android::hardware::secure_element::V1_1::BpHwSecureElementHalCallback,::android::hardware::secure_element::V1_1::BnHwSecureElementHalCallback>(_hidl_binder);
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISecureElement::init_1_1::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&clientCallback);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.secure_element", "1.1", "ISecureElement", "init_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISecureElement*>(_hidl_this->getImpl().get())->init_1_1(clientCallback);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.secure_element", "1.1", "ISecureElement", "init_1_1", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}


// Methods from ::android::hardware::secure_element::V1_0::ISecureElement follow.

// Methods from ::android::hardware::secure_element::V1_1::ISecureElement follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwSecureElement::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwSecureElement::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwSecureElement::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* init */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_0::BnHwSecureElement::_hidl_init(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* getAtr */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_0::BnHwSecureElement::_hidl_getAtr(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* isCardPresent */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_0::BnHwSecureElement::_hidl_isCardPresent(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* transmit */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_0::BnHwSecureElement::_hidl_transmit(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* openLogicalChannel */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_0::BnHwSecureElement::_hidl_openLogicalChannel(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* openBasicChannel */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_0::BnHwSecureElement::_hidl_openBasicChannel(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* closeChannel */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_0::BnHwSecureElement::_hidl_closeChannel(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* init_1_1 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::secure_element::V1_1::BnHwSecureElement::_hidl_init_1_1(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsSecureElement::BsSecureElement(const ::android::sp<::android::hardware::secure_element::V1_1::ISecureElement> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.secure_element@1.1", "ISecureElement"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsSecureElement::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<ISecureElement> ISecureElement::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSecureElement>(serviceName, false, getStub);
}

::android::sp<ISecureElement> ISecureElement::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSecureElement>(serviceName, true, getStub);
}

::android::status_t ISecureElement::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool ISecureElement::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.secure_element@1.1::ISecureElement",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_1
}  // namespace secure_element
}  // namespace hardware
}  // namespace android
