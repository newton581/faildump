#define LOG_TAG "android.hardware.tetheroffload.control@1.0::OffloadControl"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/tetheroffload/control/1.0/BpHwOffloadControl.h>
#include <android/hardware/tetheroffload/control/1.0/BnHwOffloadControl.h>
#include <android/hardware/tetheroffload/control/1.0/BsOffloadControl.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace tetheroffload {
namespace control {
namespace V1_0 {

const char* IOffloadControl::descriptor("android.hardware.tetheroffload.control@1.0::IOffloadControl");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IOffloadControl::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwOffloadControl(static_cast<IOffloadControl *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IOffloadControl::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsOffloadControl(static_cast<IOffloadControl *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IOffloadControl::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IOffloadControl::descriptor);
};

// Methods from ::android::hardware::tetheroffload::control::V1_0::IOffloadControl follow.
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::initOffload(const ::android::sp<::android::hardware::tetheroffload::control::V1_0::ITetheringOffloadCallback>& cb, initOffload_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::stopOffload(stopOffload_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::setLocalPrefixes(const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& prefixes, setLocalPrefixes_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::getForwardedStats(const ::android::hardware::hidl_string& upstream, getForwardedStats_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::setDataLimit(const ::android::hardware::hidl_string& upstream, uint64_t limit, setDataLimit_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::setUpstreamParameters(const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& v4Addr, const ::android::hardware::hidl_string& v4Gw, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& v6Gws, setUpstreamParameters_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::addDownstream(const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& prefix, addDownstream_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IOffloadControl::removeDownstream(const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& prefix, removeDownstream_cb _hidl_cb)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IOffloadControl::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::tetheroffload::control::V1_0::IOffloadControl::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IOffloadControl::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IOffloadControl::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::tetheroffload::control::V1_0::IOffloadControl::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IOffloadControl::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){68,123,0,48,107,201,90,122,175,236,29,102,15,111,62,159,118,172,139,192,53,49,147,67,94,85,121,171,131,61,166,25} /* 447b00306bc95a7aafec1d660f6f3e9f76ac8bc0353193435e5579ab833da619 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IOffloadControl::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IOffloadControl::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IOffloadControl::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IOffloadControl::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IOffloadControl::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IOffloadControl::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::tetheroffload::control::V1_0::IOffloadControl>> IOffloadControl::castFrom(const ::android::sp<::android::hardware::tetheroffload::control::V1_0::IOffloadControl>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::tetheroffload::control::V1_0::IOffloadControl>> IOffloadControl::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IOffloadControl, ::android::hidl::base::V1_0::IBase, BpHwOffloadControl>(
            parent, "android.hardware.tetheroffload.control@1.0::IOffloadControl", emitError);
}

BpHwOffloadControl::BpHwOffloadControl(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IOffloadControl>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.tetheroffload.control@1.0", "IOffloadControl") {
}

// Methods from ::android::hardware::tetheroffload::control::V1_0::IOffloadControl follow.
::android::hardware::Return<void> BpHwOffloadControl::_hidl_initOffload(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::sp<::android::hardware::tetheroffload::control::V1_0::ITetheringOffloadCallback>& cb, initOffload_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::initOffload::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&cb);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "initOffload", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    bool _hidl_out_success;
    const ::android::hardware::hidl_string* _hidl_out_errMsg;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (cb == nullptr) {
        _hidl_err = _hidl_data.writeStrongBinder(nullptr);
    } else {
        ::android::sp<::android::hardware::IBinder> _hidl_binder = ::android::hardware::getOrCreateCachedBinder(cb.get());
        if (_hidl_binder.get() != nullptr) {
            _hidl_err = _hidl_data.writeStrongBinder(_hidl_binder);
        } else {
            _hidl_err = ::android::UNKNOWN_ERROR;
        }
    }
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    ::android::hardware::ProcessState::self()->startThreadPool();
    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(1 /* initOffload */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readBool(&_hidl_out_success);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_errMsg_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent,  reinterpret_cast<const void **>(&_hidl_out_errMsg));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*_hidl_out_errMsg),
            _hidl_reply,
            _hidl__hidl_out_errMsg_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_success, *_hidl_out_errMsg);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_success);
        _hidl_args.push_back((void *)_hidl_out_errMsg);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "initOffload", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwOffloadControl::_hidl_stopOffload(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, stopOffload_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::stopOffload::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "stopOffload", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    bool _hidl_out_success;
    const ::android::hardware::hidl_string* _hidl_out_errMsg;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(2 /* stopOffload */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readBool(&_hidl_out_success);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_errMsg_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent,  reinterpret_cast<const void **>(&_hidl_out_errMsg));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*_hidl_out_errMsg),
            _hidl_reply,
            _hidl__hidl_out_errMsg_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_success, *_hidl_out_errMsg);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_success);
        _hidl_args.push_back((void *)_hidl_out_errMsg);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "stopOffload", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwOffloadControl::_hidl_setLocalPrefixes(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& prefixes, setLocalPrefixes_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::setLocalPrefixes::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&prefixes);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setLocalPrefixes", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    bool _hidl_out_success;
    const ::android::hardware::hidl_string* _hidl_out_errMsg;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_prefixes_parent;

    _hidl_err = _hidl_data.writeBuffer(&prefixes, sizeof(prefixes), &_hidl_prefixes_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_prefixes_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            prefixes,
            &_hidl_data,
            _hidl_prefixes_parent,
            0 /* parentOffset */, &_hidl_prefixes_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < prefixes.size(); ++_hidl_index_0) {
        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                prefixes[_hidl_index_0],
                &_hidl_data,
                _hidl_prefixes_child,
                _hidl_index_0 * sizeof(::android::hardware::hidl_string));

        if (_hidl_err != ::android::OK) { goto _hidl_error; }

    }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(3 /* setLocalPrefixes */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readBool(&_hidl_out_success);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_errMsg_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent,  reinterpret_cast<const void **>(&_hidl_out_errMsg));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*_hidl_out_errMsg),
            _hidl_reply,
            _hidl__hidl_out_errMsg_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_success, *_hidl_out_errMsg);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_success);
        _hidl_args.push_back((void *)_hidl_out_errMsg);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setLocalPrefixes", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwOffloadControl::_hidl_getForwardedStats(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& upstream, getForwardedStats_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::getForwardedStats::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&upstream);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "getForwardedStats", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    uint64_t _hidl_out_rxBytes;
    uint64_t _hidl_out_txBytes;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_upstream_parent;

    _hidl_err = _hidl_data.writeBuffer(&upstream, sizeof(upstream), &_hidl_upstream_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            upstream,
            &_hidl_data,
            _hidl_upstream_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(4 /* getForwardedStats */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readUint64(&_hidl_out_rxBytes);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_reply.readUint64(&_hidl_out_txBytes);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_rxBytes, _hidl_out_txBytes);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_rxBytes);
        _hidl_args.push_back((void *)&_hidl_out_txBytes);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "getForwardedStats", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwOffloadControl::_hidl_setDataLimit(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& upstream, uint64_t limit, setDataLimit_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::setDataLimit::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&upstream);
        _hidl_args.push_back((void *)&limit);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setDataLimit", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    bool _hidl_out_success;
    const ::android::hardware::hidl_string* _hidl_out_errMsg;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_upstream_parent;

    _hidl_err = _hidl_data.writeBuffer(&upstream, sizeof(upstream), &_hidl_upstream_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            upstream,
            &_hidl_data,
            _hidl_upstream_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(limit);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(5 /* setDataLimit */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readBool(&_hidl_out_success);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_errMsg_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent,  reinterpret_cast<const void **>(&_hidl_out_errMsg));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*_hidl_out_errMsg),
            _hidl_reply,
            _hidl__hidl_out_errMsg_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_success, *_hidl_out_errMsg);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_success);
        _hidl_args.push_back((void *)_hidl_out_errMsg);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setDataLimit", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwOffloadControl::_hidl_setUpstreamParameters(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& v4Addr, const ::android::hardware::hidl_string& v4Gw, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& v6Gws, setUpstreamParameters_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::setUpstreamParameters::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&iface);
        _hidl_args.push_back((void *)&v4Addr);
        _hidl_args.push_back((void *)&v4Gw);
        _hidl_args.push_back((void *)&v6Gws);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setUpstreamParameters", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    bool _hidl_out_success;
    const ::android::hardware::hidl_string* _hidl_out_errMsg;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_iface_parent;

    _hidl_err = _hidl_data.writeBuffer(&iface, sizeof(iface), &_hidl_iface_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            iface,
            &_hidl_data,
            _hidl_iface_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_v4Addr_parent;

    _hidl_err = _hidl_data.writeBuffer(&v4Addr, sizeof(v4Addr), &_hidl_v4Addr_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            v4Addr,
            &_hidl_data,
            _hidl_v4Addr_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_v4Gw_parent;

    _hidl_err = _hidl_data.writeBuffer(&v4Gw, sizeof(v4Gw), &_hidl_v4Gw_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            v4Gw,
            &_hidl_data,
            _hidl_v4Gw_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_v6Gws_parent;

    _hidl_err = _hidl_data.writeBuffer(&v6Gws, sizeof(v6Gws), &_hidl_v6Gws_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_v6Gws_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            v6Gws,
            &_hidl_data,
            _hidl_v6Gws_parent,
            0 /* parentOffset */, &_hidl_v6Gws_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < v6Gws.size(); ++_hidl_index_0) {
        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                v6Gws[_hidl_index_0],
                &_hidl_data,
                _hidl_v6Gws_child,
                _hidl_index_0 * sizeof(::android::hardware::hidl_string));

        if (_hidl_err != ::android::OK) { goto _hidl_error; }

    }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(6 /* setUpstreamParameters */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readBool(&_hidl_out_success);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_errMsg_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent,  reinterpret_cast<const void **>(&_hidl_out_errMsg));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*_hidl_out_errMsg),
            _hidl_reply,
            _hidl__hidl_out_errMsg_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_success, *_hidl_out_errMsg);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_success);
        _hidl_args.push_back((void *)_hidl_out_errMsg);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setUpstreamParameters", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwOffloadControl::_hidl_addDownstream(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& prefix, addDownstream_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::addDownstream::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&iface);
        _hidl_args.push_back((void *)&prefix);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "addDownstream", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    bool _hidl_out_success;
    const ::android::hardware::hidl_string* _hidl_out_errMsg;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_iface_parent;

    _hidl_err = _hidl_data.writeBuffer(&iface, sizeof(iface), &_hidl_iface_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            iface,
            &_hidl_data,
            _hidl_iface_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_prefix_parent;

    _hidl_err = _hidl_data.writeBuffer(&prefix, sizeof(prefix), &_hidl_prefix_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            prefix,
            &_hidl_data,
            _hidl_prefix_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(7 /* addDownstream */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readBool(&_hidl_out_success);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_errMsg_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent,  reinterpret_cast<const void **>(&_hidl_out_errMsg));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*_hidl_out_errMsg),
            _hidl_reply,
            _hidl__hidl_out_errMsg_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_success, *_hidl_out_errMsg);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_success);
        _hidl_args.push_back((void *)_hidl_out_errMsg);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "addDownstream", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwOffloadControl::_hidl_removeDownstream(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& prefix, removeDownstream_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IOffloadControl::removeDownstream::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&iface);
        _hidl_args.push_back((void *)&prefix);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "removeDownstream", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    bool _hidl_out_success;
    const ::android::hardware::hidl_string* _hidl_out_errMsg;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwOffloadControl::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_iface_parent;

    _hidl_err = _hidl_data.writeBuffer(&iface, sizeof(iface), &_hidl_iface_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            iface,
            &_hidl_data,
            _hidl_iface_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_prefix_parent;

    _hidl_err = _hidl_data.writeBuffer(&prefix, sizeof(prefix), &_hidl_prefix_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            prefix,
            &_hidl_data,
            _hidl_prefix_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(8 /* removeDownstream */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    _hidl_err = _hidl_reply.readBool(&_hidl_out_success);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_errMsg_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent,  reinterpret_cast<const void **>(&_hidl_out_errMsg));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*_hidl_out_errMsg),
            _hidl_reply,
            _hidl__hidl_out_errMsg_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(_hidl_out_success, *_hidl_out_errMsg);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_success);
        _hidl_args.push_back((void *)_hidl_out_errMsg);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "removeDownstream", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::tetheroffload::control::V1_0::IOffloadControl follow.
::android::hardware::Return<void> BpHwOffloadControl::initOffload(const ::android::sp<::android::hardware::tetheroffload::control::V1_0::ITetheringOffloadCallback>& cb, initOffload_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_initOffload(this, this, cb, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::stopOffload(stopOffload_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_stopOffload(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::setLocalPrefixes(const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& prefixes, setLocalPrefixes_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_setLocalPrefixes(this, this, prefixes, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::getForwardedStats(const ::android::hardware::hidl_string& upstream, getForwardedStats_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_getForwardedStats(this, this, upstream, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::setDataLimit(const ::android::hardware::hidl_string& upstream, uint64_t limit, setDataLimit_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_setDataLimit(this, this, upstream, limit, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::setUpstreamParameters(const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& v4Addr, const ::android::hardware::hidl_string& v4Gw, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& v6Gws, setUpstreamParameters_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_setUpstreamParameters(this, this, iface, v4Addr, v4Gw, v6Gws, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::addDownstream(const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& prefix, addDownstream_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_addDownstream(this, this, iface, prefix, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::removeDownstream(const ::android::hardware::hidl_string& iface, const ::android::hardware::hidl_string& prefix, removeDownstream_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::tetheroffload::control::V1_0::BpHwOffloadControl::_hidl_removeDownstream(this, this, iface, prefix, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwOffloadControl::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwOffloadControl::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwOffloadControl::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwOffloadControl::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwOffloadControl::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwOffloadControl::BnHwOffloadControl(const ::android::sp<IOffloadControl> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.tetheroffload.control@1.0", "IOffloadControl") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwOffloadControl::~BnHwOffloadControl() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::tetheroffload::control::V1_0::IOffloadControl follow.
::android::status_t BnHwOffloadControl::_hidl_initOffload(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    ::android::sp<::android::hardware::tetheroffload::control::V1_0::ITetheringOffloadCallback> cb;

    {
        ::android::sp<::android::hardware::IBinder> _hidl_binder;
        _hidl_err = _hidl_data.readNullableStrongBinder(&_hidl_binder);
        if (_hidl_err != ::android::OK) { return _hidl_err; }

        cb = ::android::hardware::fromBinder<::android::hardware::tetheroffload::control::V1_0::ITetheringOffloadCallback,::android::hardware::tetheroffload::control::V1_0::BpHwTetheringOffloadCallback,::android::hardware::tetheroffload::control::V1_0::BnHwTetheringOffloadCallback>(_hidl_binder);
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::initOffload::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&cb);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "initOffload", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->initOffload(cb, [&](const auto &_hidl_out_success, const auto &_hidl_out_errMsg) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("initOffload: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeBool(_hidl_out_success);
        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_errMsg_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_errMsg, sizeof(_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent);
        /* _hidl_err ignored! */

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                _hidl_out_errMsg,
                _hidl_reply,
                _hidl__hidl_out_errMsg_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_success);
            _hidl_args.push_back((void *)&_hidl_out_errMsg);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "initOffload", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("initOffload: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwOffloadControl::_hidl_stopOffload(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::stopOffload::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "stopOffload", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->stopOffload([&](const auto &_hidl_out_success, const auto &_hidl_out_errMsg) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("stopOffload: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeBool(_hidl_out_success);
        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_errMsg_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_errMsg, sizeof(_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent);
        /* _hidl_err ignored! */

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                _hidl_out_errMsg,
                _hidl_reply,
                _hidl__hidl_out_errMsg_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_success);
            _hidl_args.push_back((void *)&_hidl_out_errMsg);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "stopOffload", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("stopOffload: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwOffloadControl::_hidl_setLocalPrefixes(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_vec<::android::hardware::hidl_string>* prefixes;

    size_t _hidl_prefixes_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*prefixes), &_hidl_prefixes_parent,  reinterpret_cast<const void **>(&prefixes));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_prefixes_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::hidl_string> &>(*prefixes),
            _hidl_data,
            _hidl_prefixes_parent,
            0 /* parentOffset */, &_hidl_prefixes_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < prefixes->size(); ++_hidl_index_0) {
        _hidl_err = ::android::hardware::readEmbeddedFromParcel(
                const_cast<::android::hardware::hidl_string &>((*prefixes)[_hidl_index_0]),
                _hidl_data,
                _hidl_prefixes_child,
                _hidl_index_0 * sizeof(::android::hardware::hidl_string));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::setLocalPrefixes::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)prefixes);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setLocalPrefixes", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->setLocalPrefixes(*prefixes, [&](const auto &_hidl_out_success, const auto &_hidl_out_errMsg) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("setLocalPrefixes: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeBool(_hidl_out_success);
        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_errMsg_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_errMsg, sizeof(_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent);
        /* _hidl_err ignored! */

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                _hidl_out_errMsg,
                _hidl_reply,
                _hidl__hidl_out_errMsg_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_success);
            _hidl_args.push_back((void *)&_hidl_out_errMsg);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setLocalPrefixes", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("setLocalPrefixes: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwOffloadControl::_hidl_getForwardedStats(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* upstream;

    size_t _hidl_upstream_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*upstream), &_hidl_upstream_parent,  reinterpret_cast<const void **>(&upstream));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*upstream),
            _hidl_data,
            _hidl_upstream_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::getForwardedStats::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)upstream);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "getForwardedStats", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->getForwardedStats(*upstream, [&](const auto &_hidl_out_rxBytes, const auto &_hidl_out_txBytes) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("getForwardedStats: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeUint64(_hidl_out_rxBytes);
        /* _hidl_err ignored! */

        _hidl_err = _hidl_reply->writeUint64(_hidl_out_txBytes);
        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_rxBytes);
            _hidl_args.push_back((void *)&_hidl_out_txBytes);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "getForwardedStats", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("getForwardedStats: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwOffloadControl::_hidl_setDataLimit(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* upstream;
    uint64_t limit;

    size_t _hidl_upstream_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*upstream), &_hidl_upstream_parent,  reinterpret_cast<const void **>(&upstream));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*upstream),
            _hidl_data,
            _hidl_upstream_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint64(&limit);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::setDataLimit::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)upstream);
        _hidl_args.push_back((void *)&limit);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setDataLimit", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->setDataLimit(*upstream, limit, [&](const auto &_hidl_out_success, const auto &_hidl_out_errMsg) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("setDataLimit: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeBool(_hidl_out_success);
        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_errMsg_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_errMsg, sizeof(_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent);
        /* _hidl_err ignored! */

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                _hidl_out_errMsg,
                _hidl_reply,
                _hidl__hidl_out_errMsg_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_success);
            _hidl_args.push_back((void *)&_hidl_out_errMsg);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setDataLimit", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("setDataLimit: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwOffloadControl::_hidl_setUpstreamParameters(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* iface;
    const ::android::hardware::hidl_string* v4Addr;
    const ::android::hardware::hidl_string* v4Gw;
    const ::android::hardware::hidl_vec<::android::hardware::hidl_string>* v6Gws;

    size_t _hidl_iface_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*iface), &_hidl_iface_parent,  reinterpret_cast<const void **>(&iface));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*iface),
            _hidl_data,
            _hidl_iface_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_v4Addr_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*v4Addr), &_hidl_v4Addr_parent,  reinterpret_cast<const void **>(&v4Addr));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*v4Addr),
            _hidl_data,
            _hidl_v4Addr_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_v4Gw_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*v4Gw), &_hidl_v4Gw_parent,  reinterpret_cast<const void **>(&v4Gw));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*v4Gw),
            _hidl_data,
            _hidl_v4Gw_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_v6Gws_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*v6Gws), &_hidl_v6Gws_parent,  reinterpret_cast<const void **>(&v6Gws));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_v6Gws_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::hidl_string> &>(*v6Gws),
            _hidl_data,
            _hidl_v6Gws_parent,
            0 /* parentOffset */, &_hidl_v6Gws_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < v6Gws->size(); ++_hidl_index_0) {
        _hidl_err = ::android::hardware::readEmbeddedFromParcel(
                const_cast<::android::hardware::hidl_string &>((*v6Gws)[_hidl_index_0]),
                _hidl_data,
                _hidl_v6Gws_child,
                _hidl_index_0 * sizeof(::android::hardware::hidl_string));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::setUpstreamParameters::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)iface);
        _hidl_args.push_back((void *)v4Addr);
        _hidl_args.push_back((void *)v4Gw);
        _hidl_args.push_back((void *)v6Gws);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setUpstreamParameters", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->setUpstreamParameters(*iface, *v4Addr, *v4Gw, *v6Gws, [&](const auto &_hidl_out_success, const auto &_hidl_out_errMsg) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("setUpstreamParameters: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeBool(_hidl_out_success);
        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_errMsg_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_errMsg, sizeof(_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent);
        /* _hidl_err ignored! */

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                _hidl_out_errMsg,
                _hidl_reply,
                _hidl__hidl_out_errMsg_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_success);
            _hidl_args.push_back((void *)&_hidl_out_errMsg);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "setUpstreamParameters", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("setUpstreamParameters: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwOffloadControl::_hidl_addDownstream(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* iface;
    const ::android::hardware::hidl_string* prefix;

    size_t _hidl_iface_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*iface), &_hidl_iface_parent,  reinterpret_cast<const void **>(&iface));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*iface),
            _hidl_data,
            _hidl_iface_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_prefix_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*prefix), &_hidl_prefix_parent,  reinterpret_cast<const void **>(&prefix));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*prefix),
            _hidl_data,
            _hidl_prefix_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::addDownstream::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)iface);
        _hidl_args.push_back((void *)prefix);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "addDownstream", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->addDownstream(*iface, *prefix, [&](const auto &_hidl_out_success, const auto &_hidl_out_errMsg) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("addDownstream: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeBool(_hidl_out_success);
        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_errMsg_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_errMsg, sizeof(_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent);
        /* _hidl_err ignored! */

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                _hidl_out_errMsg,
                _hidl_reply,
                _hidl__hidl_out_errMsg_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_success);
            _hidl_args.push_back((void *)&_hidl_out_errMsg);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "addDownstream", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("addDownstream: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwOffloadControl::_hidl_removeDownstream(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwOffloadControl::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_string* iface;
    const ::android::hardware::hidl_string* prefix;

    size_t _hidl_iface_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*iface), &_hidl_iface_parent,  reinterpret_cast<const void **>(&iface));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*iface),
            _hidl_data,
            _hidl_iface_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_prefix_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*prefix), &_hidl_prefix_parent,  reinterpret_cast<const void **>(&prefix));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*prefix),
            _hidl_data,
            _hidl_prefix_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IOffloadControl::removeDownstream::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)iface);
        _hidl_args.push_back((void *)prefix);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "removeDownstream", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IOffloadControl*>(_hidl_this->getImpl().get())->removeDownstream(*iface, *prefix, [&](const auto &_hidl_out_success, const auto &_hidl_out_errMsg) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("removeDownstream: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        _hidl_err = _hidl_reply->writeBool(_hidl_out_success);
        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_errMsg_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_errMsg, sizeof(_hidl_out_errMsg), &_hidl__hidl_out_errMsg_parent);
        /* _hidl_err ignored! */

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                _hidl_out_errMsg,
                _hidl_reply,
                _hidl__hidl_out_errMsg_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_success);
            _hidl_args.push_back((void *)&_hidl_out_errMsg);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.tetheroffload.control", "1.0", "IOffloadControl", "removeDownstream", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("removeDownstream: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}


// Methods from ::android::hardware::tetheroffload::control::V1_0::IOffloadControl follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwOffloadControl::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwOffloadControl::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwOffloadControl::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* initOffload */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_initOffload(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* stopOffload */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_stopOffload(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* setLocalPrefixes */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_setLocalPrefixes(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* getForwardedStats */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_getForwardedStats(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* setDataLimit */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_setDataLimit(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* setUpstreamParameters */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_setUpstreamParameters(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* addDownstream */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_addDownstream(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* removeDownstream */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::tetheroffload::control::V1_0::BnHwOffloadControl::_hidl_removeDownstream(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsOffloadControl::BsOffloadControl(const ::android::sp<::android::hardware::tetheroffload::control::V1_0::IOffloadControl> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.tetheroffload.control@1.0", "IOffloadControl"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsOffloadControl::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IOffloadControl> IOffloadControl::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwOffloadControl>(serviceName, false, getStub);
}

::android::sp<IOffloadControl> IOffloadControl::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwOffloadControl>(serviceName, true, getStub);
}

::android::status_t IOffloadControl::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IOffloadControl::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.tetheroffload.control@1.0::IOffloadControl",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_0
}  // namespace control
}  // namespace tetheroffload
}  // namespace hardware
}  // namespace android
