#define LOG_TAG "android.hardware.cas@1.1::MediaCasService"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/cas/1.1/BpHwMediaCasService.h>
#include <android/hardware/cas/1.1/BnHwMediaCasService.h>
#include <android/hardware/cas/1.1/BsMediaCasService.h>
#include <android/hardware/cas/1.0/BpHwMediaCasService.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace cas {
namespace V1_1 {

const char* IMediaCasService::descriptor("android.hardware.cas@1.1::IMediaCasService");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IMediaCasService::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwMediaCasService(static_cast<IMediaCasService *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IMediaCasService::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsMediaCasService(static_cast<IMediaCasService *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IMediaCasService::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IMediaCasService::descriptor);
};

// Methods from ::android::hardware::cas::V1_0::IMediaCasService follow.
// no default implementation for: ::android::hardware::Return<void> IMediaCasService::enumeratePlugins(enumeratePlugins_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<bool> IMediaCasService::isSystemIdSupported(int32_t CA_system_id)
// no default implementation for: ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_0::ICas>> IMediaCasService::createPlugin(int32_t CA_system_id, const ::android::sp<::android::hardware::cas::V1_0::ICasListener>& listener)
// no default implementation for: ::android::hardware::Return<bool> IMediaCasService::isDescramblerSupported(int32_t CA_system_id)
// no default implementation for: ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_0::IDescramblerBase>> IMediaCasService::createDescrambler(int32_t CA_system_id)

// Methods from ::android::hardware::cas::V1_1::IMediaCasService follow.
// no default implementation for: ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::ICas>> IMediaCasService::createPluginExt(int32_t caSystemId, const ::android::sp<::android::hardware::cas::V1_1::ICasListener>& listener)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IMediaCasService::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::cas::V1_1::IMediaCasService::descriptor,
        ::android::hardware::cas::V1_0::IMediaCasService::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IMediaCasService::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IMediaCasService::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::cas::V1_1::IMediaCasService::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IMediaCasService::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){223,250,205,190,11,207,132,67,1,61,229,189,197,106,131,71,154,217,121,212,145,158,209,90,85,133,83,159,70,9,31,7} /* dffacdbe0bcf8443013de5bdc56a83479ad979d4919ed15a5585539f46091f07 */,
        (uint8_t[32]){134,186,156,3,151,139,121,167,66,233,144,66,11,197,206,208,103,61,37,169,57,248,37,114,153,107,239,146,98,30,32,20} /* 86ba9c03978b79a742e990420bc5ced0673d25a939f82572996bef92621e2014 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IMediaCasService::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IMediaCasService::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IMediaCasService::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IMediaCasService::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IMediaCasService::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IMediaCasService::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::IMediaCasService>> IMediaCasService::castFrom(const ::android::sp<::android::hardware::cas::V1_1::IMediaCasService>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::IMediaCasService>> IMediaCasService::castFrom(const ::android::sp<::android::hardware::cas::V1_0::IMediaCasService>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IMediaCasService, ::android::hardware::cas::V1_0::IMediaCasService, BpHwMediaCasService>(
            parent, "android.hardware.cas@1.1::IMediaCasService", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::IMediaCasService>> IMediaCasService::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IMediaCasService, ::android::hidl::base::V1_0::IBase, BpHwMediaCasService>(
            parent, "android.hardware.cas@1.1::IMediaCasService", emitError);
}

BpHwMediaCasService::BpHwMediaCasService(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IMediaCasService>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.cas@1.1", "IMediaCasService") {
}

// Methods from ::android::hardware::cas::V1_1::IMediaCasService follow.
::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::ICas>> BpHwMediaCasService::_hidl_createPluginExt(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, int32_t caSystemId, const ::android::sp<::android::hardware::cas::V1_1::ICasListener>& listener) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IMediaCasService::createPluginExt::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&caSystemId);
        _hidl_args.push_back((void *)&listener);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.cas", "1.1", "IMediaCasService", "createPluginExt", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::sp<::android::hardware::cas::V1_1::ICas> _hidl_out_cas;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwMediaCasService::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(caSystemId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (listener == nullptr) {
        _hidl_err = _hidl_data.writeStrongBinder(nullptr);
    } else {
        ::android::sp<::android::hardware::IBinder> _hidl_binder = ::android::hardware::getOrCreateCachedBinder(listener.get());
        if (_hidl_binder.get() != nullptr) {
            _hidl_err = _hidl_data.writeStrongBinder(_hidl_binder);
        } else {
            _hidl_err = ::android::UNKNOWN_ERROR;
        }
    }
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    ::android::hardware::ProcessState::self()->startThreadPool();
    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(6 /* createPluginExt */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    {
        ::android::sp<::android::hardware::IBinder> _hidl_binder;
        _hidl_err = _hidl_reply.readNullableStrongBinder(&_hidl_binder);
        if (_hidl_err != ::android::OK) { goto _hidl_error; }

        _hidl_out_cas = ::android::hardware::fromBinder<::android::hardware::cas::V1_1::ICas,::android::hardware::cas::V1_1::BpHwCas,::android::hardware::cas::V1_1::BnHwCas>(_hidl_binder);
    }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_cas);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.cas", "1.1", "IMediaCasService", "createPluginExt", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::ICas>>(_hidl_out_cas);

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::ICas>>(_hidl_status);
}


// Methods from ::android::hardware::cas::V1_0::IMediaCasService follow.
::android::hardware::Return<void> BpHwMediaCasService::enumeratePlugins(enumeratePlugins_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::cas::V1_0::BpHwMediaCasService::_hidl_enumeratePlugins(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwMediaCasService::isSystemIdSupported(int32_t CA_system_id){
    ::android::hardware::Return<bool>  _hidl_out = ::android::hardware::cas::V1_0::BpHwMediaCasService::_hidl_isSystemIdSupported(this, this, CA_system_id);

    return _hidl_out;
}

::android::hardware::Return<::android::sp<::android::hardware::cas::V1_0::ICas>> BpHwMediaCasService::createPlugin(int32_t CA_system_id, const ::android::sp<::android::hardware::cas::V1_0::ICasListener>& listener){
    ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_0::ICas>>  _hidl_out = ::android::hardware::cas::V1_0::BpHwMediaCasService::_hidl_createPlugin(this, this, CA_system_id, listener);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwMediaCasService::isDescramblerSupported(int32_t CA_system_id){
    ::android::hardware::Return<bool>  _hidl_out = ::android::hardware::cas::V1_0::BpHwMediaCasService::_hidl_isDescramblerSupported(this, this, CA_system_id);

    return _hidl_out;
}

::android::hardware::Return<::android::sp<::android::hardware::cas::V1_0::IDescramblerBase>> BpHwMediaCasService::createDescrambler(int32_t CA_system_id){
    ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_0::IDescramblerBase>>  _hidl_out = ::android::hardware::cas::V1_0::BpHwMediaCasService::_hidl_createDescrambler(this, this, CA_system_id);

    return _hidl_out;
}


// Methods from ::android::hardware::cas::V1_1::IMediaCasService follow.
::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::ICas>> BpHwMediaCasService::createPluginExt(int32_t caSystemId, const ::android::sp<::android::hardware::cas::V1_1::ICasListener>& listener){
    ::android::hardware::Return<::android::sp<::android::hardware::cas::V1_1::ICas>>  _hidl_out = ::android::hardware::cas::V1_1::BpHwMediaCasService::_hidl_createPluginExt(this, this, caSystemId, listener);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwMediaCasService::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwMediaCasService::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwMediaCasService::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwMediaCasService::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwMediaCasService::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwMediaCasService::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwMediaCasService::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwMediaCasService::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwMediaCasService::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwMediaCasService::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwMediaCasService::BnHwMediaCasService(const ::android::sp<IMediaCasService> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.cas@1.1", "IMediaCasService") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwMediaCasService::~BnHwMediaCasService() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::cas::V1_1::IMediaCasService follow.
::android::status_t BnHwMediaCasService::_hidl_createPluginExt(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwMediaCasService::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    int32_t caSystemId;
    ::android::sp<::android::hardware::cas::V1_1::ICasListener> listener;

    _hidl_err = _hidl_data.readInt32(&caSystemId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    {
        ::android::sp<::android::hardware::IBinder> _hidl_binder;
        _hidl_err = _hidl_data.readNullableStrongBinder(&_hidl_binder);
        if (_hidl_err != ::android::OK) { return _hidl_err; }

        listener = ::android::hardware::fromBinder<::android::hardware::cas::V1_1::ICasListener,::android::hardware::cas::V1_1::BpHwCasListener,::android::hardware::cas::V1_1::BnHwCasListener>(_hidl_binder);
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IMediaCasService::createPluginExt::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&caSystemId);
        _hidl_args.push_back((void *)&listener);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.cas", "1.1", "IMediaCasService", "createPluginExt", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::sp<::android::hardware::cas::V1_1::ICas> _hidl_out_cas = static_cast<IMediaCasService*>(_hidl_this->getImpl().get())->createPluginExt(caSystemId, listener);

    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    if (_hidl_out_cas == nullptr) {
        _hidl_err = _hidl_reply->writeStrongBinder(nullptr);
    } else {
        ::android::sp<::android::hardware::IBinder> _hidl_binder = ::android::hardware::getOrCreateCachedBinder(_hidl_out_cas.get());
        if (_hidl_binder.get() != nullptr) {
            _hidl_err = _hidl_reply->writeStrongBinder(_hidl_binder);
        } else {
            _hidl_err = ::android::UNKNOWN_ERROR;
        }
    }
    /* _hidl_err ignored! */

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&_hidl_out_cas);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.cas", "1.1", "IMediaCasService", "createPluginExt", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_cb(*_hidl_reply);
    return _hidl_err;
}


// Methods from ::android::hardware::cas::V1_0::IMediaCasService follow.

// Methods from ::android::hardware::cas::V1_1::IMediaCasService follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwMediaCasService::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwMediaCasService::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwMediaCasService::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* enumeratePlugins */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::cas::V1_0::BnHwMediaCasService::_hidl_enumeratePlugins(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* isSystemIdSupported */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::cas::V1_0::BnHwMediaCasService::_hidl_isSystemIdSupported(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* createPlugin */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::cas::V1_0::BnHwMediaCasService::_hidl_createPlugin(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* isDescramblerSupported */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::cas::V1_0::BnHwMediaCasService::_hidl_isDescramblerSupported(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* createDescrambler */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::cas::V1_0::BnHwMediaCasService::_hidl_createDescrambler(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* createPluginExt */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::cas::V1_1::BnHwMediaCasService::_hidl_createPluginExt(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsMediaCasService::BsMediaCasService(const ::android::sp<::android::hardware::cas::V1_1::IMediaCasService> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.cas@1.1", "IMediaCasService"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsMediaCasService::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IMediaCasService> IMediaCasService::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwMediaCasService>(serviceName, false, getStub);
}

::android::sp<IMediaCasService> IMediaCasService::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwMediaCasService>(serviceName, true, getStub);
}

::android::status_t IMediaCasService::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IMediaCasService::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.cas@1.1::IMediaCasService",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_1
}  // namespace cas
}  // namespace hardware
}  // namespace android
