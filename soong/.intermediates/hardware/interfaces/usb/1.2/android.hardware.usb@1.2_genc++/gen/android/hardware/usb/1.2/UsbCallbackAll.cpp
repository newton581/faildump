#define LOG_TAG "android.hardware.usb@1.2::UsbCallback"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/usb/1.2/BpHwUsbCallback.h>
#include <android/hardware/usb/1.2/BnHwUsbCallback.h>
#include <android/hardware/usb/1.2/BsUsbCallback.h>
#include <android/hardware/usb/1.1/BpHwUsbCallback.h>
#include <android/hardware/usb/1.0/BpHwUsbCallback.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace usb {
namespace V1_2 {

const char* IUsbCallback::descriptor("android.hardware.usb@1.2::IUsbCallback");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IUsbCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwUsbCallback(static_cast<IUsbCallback *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IUsbCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsUsbCallback(static_cast<IUsbCallback *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IUsbCallback::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IUsbCallback::descriptor);
};

// Methods from ::android::hardware::usb::V1_0::IUsbCallback follow.
// no default implementation for: ::android::hardware::Return<void> IUsbCallback::notifyPortStatusChange(const ::android::hardware::hidl_vec<::android::hardware::usb::V1_0::PortStatus>& currentPortStatus, ::android::hardware::usb::V1_0::Status retval)
// no default implementation for: ::android::hardware::Return<void> IUsbCallback::notifyRoleSwitchStatus(const ::android::hardware::hidl_string& portName, const ::android::hardware::usb::V1_0::PortRole& newRole, ::android::hardware::usb::V1_0::Status retval)

// Methods from ::android::hardware::usb::V1_1::IUsbCallback follow.
// no default implementation for: ::android::hardware::Return<void> IUsbCallback::notifyPortStatusChange_1_1(const ::android::hardware::hidl_vec<::android::hardware::usb::V1_1::PortStatus_1_1>& currentPortStatus, ::android::hardware::usb::V1_0::Status retval)

// Methods from ::android::hardware::usb::V1_2::IUsbCallback follow.
// no default implementation for: ::android::hardware::Return<void> IUsbCallback::notifyPortStatusChange_1_2(const ::android::hardware::hidl_vec<::android::hardware::usb::V1_2::PortStatus>& currentPortStatus, ::android::hardware::usb::V1_0::Status retval)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IUsbCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::usb::V1_2::IUsbCallback::descriptor,
        ::android::hardware::usb::V1_1::IUsbCallback::descriptor,
        ::android::hardware::usb::V1_0::IUsbCallback::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IUsbCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsbCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::usb::V1_2::IUsbCallback::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsbCallback::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){70,153,108,210,161,198,98,97,167,90,31,110,202,218,119,238,181,134,30,178,100,250,57,185,150,84,143,224,167,242,45,211} /* 46996cd2a1c66261a75a1f6ecada77eeb5861eb264fa39b996548fe0a7f22dd3 */,
        (uint8_t[32]){19,165,128,227,90,240,18,112,161,233,119,65,119,197,29,181,29,134,114,230,19,155,160,8,81,230,84,230,138,77,125,255} /* 13a580e35af01270a1e9774177c51db51d8672e6139ba00851e654e68a4d7dff */,
        (uint8_t[32]){75,231,136,30,65,27,164,39,132,191,91,115,84,193,74,224,207,22,16,4,211,148,51,170,236,170,176,209,158,169,147,84} /* 4be7881e411ba42784bf5b7354c14ae0cf161004d39433aaecaab0d19ea99354 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsbCallback::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IUsbCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IUsbCallback::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsbCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsbCallback::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IUsbCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::usb::V1_2::IUsbCallback>> IUsbCallback::castFrom(const ::android::sp<::android::hardware::usb::V1_2::IUsbCallback>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::usb::V1_2::IUsbCallback>> IUsbCallback::castFrom(const ::android::sp<::android::hardware::usb::V1_1::IUsbCallback>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IUsbCallback, ::android::hardware::usb::V1_1::IUsbCallback, BpHwUsbCallback>(
            parent, "android.hardware.usb@1.2::IUsbCallback", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::usb::V1_2::IUsbCallback>> IUsbCallback::castFrom(const ::android::sp<::android::hardware::usb::V1_0::IUsbCallback>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IUsbCallback, ::android::hardware::usb::V1_0::IUsbCallback, BpHwUsbCallback>(
            parent, "android.hardware.usb@1.2::IUsbCallback", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::usb::V1_2::IUsbCallback>> IUsbCallback::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IUsbCallback, ::android::hidl::base::V1_0::IBase, BpHwUsbCallback>(
            parent, "android.hardware.usb@1.2::IUsbCallback", emitError);
}

BpHwUsbCallback::BpHwUsbCallback(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IUsbCallback>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.usb@1.2", "IUsbCallback") {
}

// Methods from ::android::hardware::usb::V1_2::IUsbCallback follow.
::android::hardware::Return<void> BpHwUsbCallback::_hidl_notifyPortStatusChange_1_2(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_vec<::android::hardware::usb::V1_2::PortStatus>& currentPortStatus, ::android::hardware::usb::V1_0::Status retval) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IUsbCallback::notifyPortStatusChange_1_2::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&currentPortStatus);
        _hidl_args.push_back((void *)&retval);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.usb", "1.2", "IUsbCallback", "notifyPortStatusChange_1_2", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwUsbCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_currentPortStatus_parent;

    _hidl_err = _hidl_data.writeBuffer(&currentPortStatus, sizeof(currentPortStatus), &_hidl_currentPortStatus_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_currentPortStatus_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            currentPortStatus,
            &_hidl_data,
            _hidl_currentPortStatus_parent,
            0 /* parentOffset */, &_hidl_currentPortStatus_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < currentPortStatus.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                currentPortStatus[_hidl_index_0],
                &_hidl_data,
                _hidl_currentPortStatus_child,
                _hidl_index_0 * sizeof(::android::hardware::usb::V1_2::PortStatus));

        if (_hidl_err != ::android::OK) { goto _hidl_error; }

    }

    _hidl_err = _hidl_data.writeUint32((uint32_t)retval);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(4 /* notifyPortStatusChange_1_2 */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.usb", "1.2", "IUsbCallback", "notifyPortStatusChange_1_2", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::usb::V1_0::IUsbCallback follow.
::android::hardware::Return<void> BpHwUsbCallback::notifyPortStatusChange(const ::android::hardware::hidl_vec<::android::hardware::usb::V1_0::PortStatus>& currentPortStatus, ::android::hardware::usb::V1_0::Status retval){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::usb::V1_0::BpHwUsbCallback::_hidl_notifyPortStatusChange(this, this, currentPortStatus, retval);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsbCallback::notifyRoleSwitchStatus(const ::android::hardware::hidl_string& portName, const ::android::hardware::usb::V1_0::PortRole& newRole, ::android::hardware::usb::V1_0::Status retval){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::usb::V1_0::BpHwUsbCallback::_hidl_notifyRoleSwitchStatus(this, this, portName, newRole, retval);

    return _hidl_out;
}


// Methods from ::android::hardware::usb::V1_1::IUsbCallback follow.
::android::hardware::Return<void> BpHwUsbCallback::notifyPortStatusChange_1_1(const ::android::hardware::hidl_vec<::android::hardware::usb::V1_1::PortStatus_1_1>& currentPortStatus, ::android::hardware::usb::V1_0::Status retval){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::usb::V1_1::BpHwUsbCallback::_hidl_notifyPortStatusChange_1_1(this, this, currentPortStatus, retval);

    return _hidl_out;
}


// Methods from ::android::hardware::usb::V1_2::IUsbCallback follow.
::android::hardware::Return<void> BpHwUsbCallback::notifyPortStatusChange_1_2(const ::android::hardware::hidl_vec<::android::hardware::usb::V1_2::PortStatus>& currentPortStatus, ::android::hardware::usb::V1_0::Status retval){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::usb::V1_2::BpHwUsbCallback::_hidl_notifyPortStatusChange_1_2(this, this, currentPortStatus, retval);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwUsbCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsbCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsbCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsbCallback::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsbCallback::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwUsbCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwUsbCallback::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsbCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsbCallback::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwUsbCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwUsbCallback::BnHwUsbCallback(const ::android::sp<IUsbCallback> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.usb@1.2", "IUsbCallback") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwUsbCallback::~BnHwUsbCallback() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::usb::V1_2::IUsbCallback follow.
::android::status_t BnHwUsbCallback::_hidl_notifyPortStatusChange_1_2(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwUsbCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_vec<::android::hardware::usb::V1_2::PortStatus>* currentPortStatus;
    ::android::hardware::usb::V1_0::Status retval;

    size_t _hidl_currentPortStatus_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*currentPortStatus), &_hidl_currentPortStatus_parent,  reinterpret_cast<const void **>(&currentPortStatus));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_currentPortStatus_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::usb::V1_2::PortStatus> &>(*currentPortStatus),
            _hidl_data,
            _hidl_currentPortStatus_parent,
            0 /* parentOffset */, &_hidl_currentPortStatus_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < currentPortStatus->size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::usb::V1_2::PortStatus &>((*currentPortStatus)[_hidl_index_0]),
                _hidl_data,
                _hidl_currentPortStatus_child,
                _hidl_index_0 * sizeof(::android::hardware::usb::V1_2::PortStatus));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    _hidl_err = _hidl_data.readUint32((uint32_t *)&retval);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IUsbCallback::notifyPortStatusChange_1_2::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)currentPortStatus);
        _hidl_args.push_back((void *)&retval);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.usb", "1.2", "IUsbCallback", "notifyPortStatusChange_1_2", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IUsbCallback*>(_hidl_this->getImpl().get())->notifyPortStatusChange_1_2(*currentPortStatus, retval);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.usb", "1.2", "IUsbCallback", "notifyPortStatusChange_1_2", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}


// Methods from ::android::hardware::usb::V1_0::IUsbCallback follow.

// Methods from ::android::hardware::usb::V1_1::IUsbCallback follow.

// Methods from ::android::hardware::usb::V1_2::IUsbCallback follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwUsbCallback::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwUsbCallback::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwUsbCallback::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* notifyPortStatusChange */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::usb::V1_0::BnHwUsbCallback::_hidl_notifyPortStatusChange(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* notifyRoleSwitchStatus */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::usb::V1_0::BnHwUsbCallback::_hidl_notifyRoleSwitchStatus(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* notifyPortStatusChange_1_1 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::usb::V1_1::BnHwUsbCallback::_hidl_notifyPortStatusChange_1_1(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* notifyPortStatusChange_1_2 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::usb::V1_2::BnHwUsbCallback::_hidl_notifyPortStatusChange_1_2(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsUsbCallback::BsUsbCallback(const ::android::sp<::android::hardware::usb::V1_2::IUsbCallback> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.usb@1.2", "IUsbCallback"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsUsbCallback::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IUsbCallback> IUsbCallback::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwUsbCallback>(serviceName, false, getStub);
}

::android::sp<IUsbCallback> IUsbCallback::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwUsbCallback>(serviceName, true, getStub);
}

::android::status_t IUsbCallback::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IUsbCallback::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.usb@1.2::IUsbCallback",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_2
}  // namespace usb
}  // namespace hardware
}  // namespace android
