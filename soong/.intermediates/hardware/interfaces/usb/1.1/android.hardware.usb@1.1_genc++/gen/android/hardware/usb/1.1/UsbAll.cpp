#define LOG_TAG "android.hardware.usb@1.1::Usb"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/usb/1.1/BpHwUsb.h>
#include <android/hardware/usb/1.1/BnHwUsb.h>
#include <android/hardware/usb/1.1/BsUsb.h>
#include <android/hardware/usb/1.0/BpHwUsb.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace usb {
namespace V1_1 {

const char* IUsb::descriptor("android.hardware.usb@1.1::IUsb");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IUsb::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwUsb(static_cast<IUsb *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IUsb::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsUsb(static_cast<IUsb *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IUsb::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IUsb::descriptor);
};

// Methods from ::android::hardware::usb::V1_0::IUsb follow.
// no default implementation for: ::android::hardware::Return<void> IUsb::switchRole(const ::android::hardware::hidl_string& portName, const ::android::hardware::usb::V1_0::PortRole& role)
// no default implementation for: ::android::hardware::Return<void> IUsb::setCallback(const ::android::sp<::android::hardware::usb::V1_0::IUsbCallback>& callback)
// no default implementation for: ::android::hardware::Return<void> IUsb::queryPortStatus()

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IUsb::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::usb::V1_1::IUsb::descriptor,
        ::android::hardware::usb::V1_0::IUsb::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IUsb::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsb::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::usb::V1_1::IUsb::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsb::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){174,188,217,255,45,160,92,157,76,67,153,22,244,13,253,33,155,167,98,153,25,0,124,185,129,235,241,80,6,75,79,130} /* aebcd9ff2da05c9d4c439916f40dfd219ba7629919007cb981ebf150064b4f82 */,
        (uint8_t[32]){78,245,116,153,39,63,56,189,189,208,193,94,86,238,122,75,197,241,138,86,68,9,33,112,165,49,223,53,65,217,224,21} /* 4ef57499273f38bdbdd0c15e56ee7a4bc5f18a5644092170a531df3541d9e015 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsb::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IUsb::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IUsb::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsb::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IUsb::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IUsb::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::usb::V1_1::IUsb>> IUsb::castFrom(const ::android::sp<::android::hardware::usb::V1_1::IUsb>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::usb::V1_1::IUsb>> IUsb::castFrom(const ::android::sp<::android::hardware::usb::V1_0::IUsb>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IUsb, ::android::hardware::usb::V1_0::IUsb, BpHwUsb>(
            parent, "android.hardware.usb@1.1::IUsb", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::usb::V1_1::IUsb>> IUsb::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IUsb, ::android::hidl::base::V1_0::IBase, BpHwUsb>(
            parent, "android.hardware.usb@1.1::IUsb", emitError);
}

BpHwUsb::BpHwUsb(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IUsb>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.usb@1.1", "IUsb") {
}


// Methods from ::android::hardware::usb::V1_0::IUsb follow.
::android::hardware::Return<void> BpHwUsb::switchRole(const ::android::hardware::hidl_string& portName, const ::android::hardware::usb::V1_0::PortRole& role){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::usb::V1_0::BpHwUsb::_hidl_switchRole(this, this, portName, role);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::setCallback(const ::android::sp<::android::hardware::usb::V1_0::IUsbCallback>& callback){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::usb::V1_0::BpHwUsb::_hidl_setCallback(this, this, callback);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::queryPortStatus(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::usb::V1_0::BpHwUsb::_hidl_queryPortStatus(this, this);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwUsb::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwUsb::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwUsb::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwUsb::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwUsb::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwUsb::BnHwUsb(const ::android::sp<IUsb> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.usb@1.1", "IUsb") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwUsb::~BnHwUsb() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}


// Methods from ::android::hardware::usb::V1_0::IUsb follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwUsb::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwUsb::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwUsb::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* switchRole */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::usb::V1_0::BnHwUsb::_hidl_switchRole(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* setCallback */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::usb::V1_0::BnHwUsb::_hidl_setCallback(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* queryPortStatus */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::usb::V1_0::BnHwUsb::_hidl_queryPortStatus(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsUsb::BsUsb(const ::android::sp<::android::hardware::usb::V1_1::IUsb> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.usb@1.1", "IUsb"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsUsb::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IUsb> IUsb::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwUsb>(serviceName, false, getStub);
}

::android::sp<IUsb> IUsb::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwUsb>(serviceName, true, getStub);
}

::android::status_t IUsb::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IUsb::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.usb@1.1::IUsb",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_1
}  // namespace usb
}  // namespace hardware
}  // namespace android
