#define LOG_TAG "android.hardware.biometrics.face@1.0::BiometricsFaceClientCallback"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/biometrics/face/1.0/BpHwBiometricsFaceClientCallback.h>
#include <android/hardware/biometrics/face/1.0/BnHwBiometricsFaceClientCallback.h>
#include <android/hardware/biometrics/face/1.0/BsBiometricsFaceClientCallback.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace biometrics {
namespace face {
namespace V1_0 {

const char* IBiometricsFaceClientCallback::descriptor("android.hardware.biometrics.face@1.0::IBiometricsFaceClientCallback");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IBiometricsFaceClientCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwBiometricsFaceClientCallback(static_cast<IBiometricsFaceClientCallback *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IBiometricsFaceClientCallback::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsBiometricsFaceClientCallback(static_cast<IBiometricsFaceClientCallback *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IBiometricsFaceClientCallback::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IBiometricsFaceClientCallback::descriptor);
};

// Methods from ::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback follow.
// no default implementation for: ::android::hardware::Return<void> IBiometricsFaceClientCallback::onEnrollResult(uint64_t deviceId, uint32_t faceId, int32_t userId, uint32_t remaining)
// no default implementation for: ::android::hardware::Return<void> IBiometricsFaceClientCallback::onAuthenticated(uint64_t deviceId, uint32_t faceId, int32_t userId, const ::android::hardware::hidl_vec<uint8_t>& token)
// no default implementation for: ::android::hardware::Return<void> IBiometricsFaceClientCallback::onAcquired(uint64_t deviceId, int32_t userId, ::android::hardware::biometrics::face::V1_0::FaceAcquiredInfo acquiredInfo, int32_t vendorCode)
// no default implementation for: ::android::hardware::Return<void> IBiometricsFaceClientCallback::onError(uint64_t deviceId, int32_t userId, ::android::hardware::biometrics::face::V1_0::FaceError error, int32_t vendorCode)
// no default implementation for: ::android::hardware::Return<void> IBiometricsFaceClientCallback::onRemoved(uint64_t deviceId, const ::android::hardware::hidl_vec<uint32_t>& removed, int32_t userId)
// no default implementation for: ::android::hardware::Return<void> IBiometricsFaceClientCallback::onEnumerate(uint64_t deviceId, const ::android::hardware::hidl_vec<uint32_t>& faceIds, int32_t userId)
// no default implementation for: ::android::hardware::Return<void> IBiometricsFaceClientCallback::onLockoutChanged(uint64_t duration)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IBiometricsFaceClientCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IBiometricsFaceClientCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IBiometricsFaceClientCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IBiometricsFaceClientCallback::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){182,229,93,119,149,187,175,208,17,251,149,163,182,211,149,75,246,108,52,158,20,207,16,127,59,114,3,44,227,206,180,72} /* b6e55d7795bbafd011fb95a3b6d3954bf66c349e14cf107f3b72032ce3ceb448 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IBiometricsFaceClientCallback::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IBiometricsFaceClientCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IBiometricsFaceClientCallback::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IBiometricsFaceClientCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IBiometricsFaceClientCallback::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IBiometricsFaceClientCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback>> IBiometricsFaceClientCallback::castFrom(const ::android::sp<::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback>> IBiometricsFaceClientCallback::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IBiometricsFaceClientCallback, ::android::hidl::base::V1_0::IBase, BpHwBiometricsFaceClientCallback>(
            parent, "android.hardware.biometrics.face@1.0::IBiometricsFaceClientCallback", emitError);
}

BpHwBiometricsFaceClientCallback::BpHwBiometricsFaceClientCallback(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IBiometricsFaceClientCallback>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.biometrics.face@1.0", "IBiometricsFaceClientCallback") {
}

// Methods from ::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback follow.
::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::_hidl_onEnrollResult(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint64_t deviceId, uint32_t faceId, int32_t userId, uint32_t remaining) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onEnrollResult::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&faceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)&remaining);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnrollResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwBiometricsFaceClientCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(deviceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(faceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(userId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(remaining);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(1 /* onEnrollResult */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnrollResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::_hidl_onAuthenticated(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint64_t deviceId, uint32_t faceId, int32_t userId, const ::android::hardware::hidl_vec<uint8_t>& token) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onAuthenticated::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&faceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)&token);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAuthenticated", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwBiometricsFaceClientCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(deviceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(faceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(userId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_token_parent;

    _hidl_err = _hidl_data.writeBuffer(&token, sizeof(token), &_hidl_token_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_token_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            token,
            &_hidl_data,
            _hidl_token_parent,
            0 /* parentOffset */, &_hidl_token_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(2 /* onAuthenticated */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAuthenticated", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::_hidl_onAcquired(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint64_t deviceId, int32_t userId, ::android::hardware::biometrics::face::V1_0::FaceAcquiredInfo acquiredInfo, int32_t vendorCode) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onAcquired::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)&acquiredInfo);
        _hidl_args.push_back((void *)&vendorCode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAcquired", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwBiometricsFaceClientCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(deviceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(userId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32((int32_t)acquiredInfo);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(vendorCode);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(3 /* onAcquired */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAcquired", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::_hidl_onError(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint64_t deviceId, int32_t userId, ::android::hardware::biometrics::face::V1_0::FaceError error, int32_t vendorCode) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onError::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)&error);
        _hidl_args.push_back((void *)&vendorCode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onError", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwBiometricsFaceClientCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(deviceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(userId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32((int32_t)error);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(vendorCode);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(4 /* onError */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onError", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::_hidl_onRemoved(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint64_t deviceId, const ::android::hardware::hidl_vec<uint32_t>& removed, int32_t userId) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onRemoved::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&removed);
        _hidl_args.push_back((void *)&userId);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwBiometricsFaceClientCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(deviceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_removed_parent;

    _hidl_err = _hidl_data.writeBuffer(&removed, sizeof(removed), &_hidl_removed_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_removed_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            removed,
            &_hidl_data,
            _hidl_removed_parent,
            0 /* parentOffset */, &_hidl_removed_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(userId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(5 /* onRemoved */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::_hidl_onEnumerate(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint64_t deviceId, const ::android::hardware::hidl_vec<uint32_t>& faceIds, int32_t userId) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onEnumerate::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&faceIds);
        _hidl_args.push_back((void *)&userId);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnumerate", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwBiometricsFaceClientCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(deviceId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_faceIds_parent;

    _hidl_err = _hidl_data.writeBuffer(&faceIds, sizeof(faceIds), &_hidl_faceIds_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_faceIds_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            faceIds,
            &_hidl_data,
            _hidl_faceIds_parent,
            0 /* parentOffset */, &_hidl_faceIds_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeInt32(userId);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(6 /* onEnumerate */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnumerate", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::_hidl_onLockoutChanged(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, uint64_t duration) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onLockoutChanged::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&duration);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onLockoutChanged", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwBiometricsFaceClientCallback::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint64(duration);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(7 /* onLockoutChanged */, _hidl_data, &_hidl_reply, 1u /* oneway */);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onLockoutChanged", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback follow.
::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::onEnrollResult(uint64_t deviceId, uint32_t faceId, int32_t userId, uint32_t remaining){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::biometrics::face::V1_0::BpHwBiometricsFaceClientCallback::_hidl_onEnrollResult(this, this, deviceId, faceId, userId, remaining);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::onAuthenticated(uint64_t deviceId, uint32_t faceId, int32_t userId, const ::android::hardware::hidl_vec<uint8_t>& token){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::biometrics::face::V1_0::BpHwBiometricsFaceClientCallback::_hidl_onAuthenticated(this, this, deviceId, faceId, userId, token);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::onAcquired(uint64_t deviceId, int32_t userId, ::android::hardware::biometrics::face::V1_0::FaceAcquiredInfo acquiredInfo, int32_t vendorCode){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::biometrics::face::V1_0::BpHwBiometricsFaceClientCallback::_hidl_onAcquired(this, this, deviceId, userId, acquiredInfo, vendorCode);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::onError(uint64_t deviceId, int32_t userId, ::android::hardware::biometrics::face::V1_0::FaceError error, int32_t vendorCode){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::biometrics::face::V1_0::BpHwBiometricsFaceClientCallback::_hidl_onError(this, this, deviceId, userId, error, vendorCode);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::onRemoved(uint64_t deviceId, const ::android::hardware::hidl_vec<uint32_t>& removed, int32_t userId){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::biometrics::face::V1_0::BpHwBiometricsFaceClientCallback::_hidl_onRemoved(this, this, deviceId, removed, userId);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::onEnumerate(uint64_t deviceId, const ::android::hardware::hidl_vec<uint32_t>& faceIds, int32_t userId){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::biometrics::face::V1_0::BpHwBiometricsFaceClientCallback::_hidl_onEnumerate(this, this, deviceId, faceIds, userId);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::onLockoutChanged(uint64_t duration){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::biometrics::face::V1_0::BpHwBiometricsFaceClientCallback::_hidl_onLockoutChanged(this, this, duration);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwBiometricsFaceClientCallback::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwBiometricsFaceClientCallback::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwBiometricsFaceClientCallback::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwBiometricsFaceClientCallback::BnHwBiometricsFaceClientCallback(const ::android::sp<IBiometricsFaceClientCallback> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.biometrics.face@1.0", "IBiometricsFaceClientCallback") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwBiometricsFaceClientCallback::~BnHwBiometricsFaceClientCallback() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback follow.
::android::status_t BnHwBiometricsFaceClientCallback::_hidl_onEnrollResult(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwBiometricsFaceClientCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint64_t deviceId;
    uint32_t faceId;
    int32_t userId;
    uint32_t remaining;

    _hidl_err = _hidl_data.readUint64(&deviceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32(&faceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&userId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32(&remaining);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onEnrollResult::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&faceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)&remaining);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnrollResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IBiometricsFaceClientCallback*>(_hidl_this->getImpl().get())->onEnrollResult(deviceId, faceId, userId, remaining);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnrollResult", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwBiometricsFaceClientCallback::_hidl_onAuthenticated(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwBiometricsFaceClientCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint64_t deviceId;
    uint32_t faceId;
    int32_t userId;
    const ::android::hardware::hidl_vec<uint8_t>* token;

    _hidl_err = _hidl_data.readUint64(&deviceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32(&faceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&userId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_token_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*token), &_hidl_token_parent,  reinterpret_cast<const void **>(&token));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_token_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint8_t> &>(*token),
            _hidl_data,
            _hidl_token_parent,
            0 /* parentOffset */, &_hidl_token_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onAuthenticated::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&faceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)token);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAuthenticated", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IBiometricsFaceClientCallback*>(_hidl_this->getImpl().get())->onAuthenticated(deviceId, faceId, userId, *token);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAuthenticated", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwBiometricsFaceClientCallback::_hidl_onAcquired(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwBiometricsFaceClientCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint64_t deviceId;
    int32_t userId;
    ::android::hardware::biometrics::face::V1_0::FaceAcquiredInfo acquiredInfo;
    int32_t vendorCode;

    _hidl_err = _hidl_data.readUint64(&deviceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&userId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32((int32_t *)&acquiredInfo);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&vendorCode);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onAcquired::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)&acquiredInfo);
        _hidl_args.push_back((void *)&vendorCode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAcquired", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IBiometricsFaceClientCallback*>(_hidl_this->getImpl().get())->onAcquired(deviceId, userId, acquiredInfo, vendorCode);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onAcquired", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwBiometricsFaceClientCallback::_hidl_onError(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwBiometricsFaceClientCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint64_t deviceId;
    int32_t userId;
    ::android::hardware::biometrics::face::V1_0::FaceError error;
    int32_t vendorCode;

    _hidl_err = _hidl_data.readUint64(&deviceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&userId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32((int32_t *)&error);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&vendorCode);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onError::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)&userId);
        _hidl_args.push_back((void *)&error);
        _hidl_args.push_back((void *)&vendorCode);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onError", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IBiometricsFaceClientCallback*>(_hidl_this->getImpl().get())->onError(deviceId, userId, error, vendorCode);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onError", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwBiometricsFaceClientCallback::_hidl_onRemoved(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwBiometricsFaceClientCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint64_t deviceId;
    const ::android::hardware::hidl_vec<uint32_t>* removed;
    int32_t userId;

    _hidl_err = _hidl_data.readUint64(&deviceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_removed_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*removed), &_hidl_removed_parent,  reinterpret_cast<const void **>(&removed));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_removed_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint32_t> &>(*removed),
            _hidl_data,
            _hidl_removed_parent,
            0 /* parentOffset */, &_hidl_removed_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&userId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onRemoved::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)removed);
        _hidl_args.push_back((void *)&userId);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IBiometricsFaceClientCallback*>(_hidl_this->getImpl().get())->onRemoved(deviceId, *removed, userId);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onRemoved", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwBiometricsFaceClientCallback::_hidl_onEnumerate(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwBiometricsFaceClientCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint64_t deviceId;
    const ::android::hardware::hidl_vec<uint32_t>* faceIds;
    int32_t userId;

    _hidl_err = _hidl_data.readUint64(&deviceId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_faceIds_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*faceIds), &_hidl_faceIds_parent,  reinterpret_cast<const void **>(&faceIds));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_faceIds_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint32_t> &>(*faceIds),
            _hidl_data,
            _hidl_faceIds_parent,
            0 /* parentOffset */, &_hidl_faceIds_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readInt32(&userId);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onEnumerate::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&deviceId);
        _hidl_args.push_back((void *)faceIds);
        _hidl_args.push_back((void *)&userId);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnumerate", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IBiometricsFaceClientCallback*>(_hidl_this->getImpl().get())->onEnumerate(deviceId, *faceIds, userId);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onEnumerate", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}

::android::status_t BnHwBiometricsFaceClientCallback::_hidl_onLockoutChanged(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwBiometricsFaceClientCallback::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    uint64_t duration;

    _hidl_err = _hidl_data.readUint64(&duration);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IBiometricsFaceClientCallback::onLockoutChanged::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&duration);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onLockoutChanged", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Return<void> _hidl_ret = static_cast<IBiometricsFaceClientCallback*>(_hidl_this->getImpl().get())->onLockoutChanged(duration);

    (void) _hidl_cb;

    atrace_end(ATRACE_TAG_HAL);
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.biometrics.face", "1.0", "IBiometricsFaceClientCallback", "onLockoutChanged", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_ret.assertOk();
    ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

    return _hidl_err;
}


// Methods from ::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwBiometricsFaceClientCallback::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwBiometricsFaceClientCallback::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwBiometricsFaceClientCallback::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* onEnrollResult */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::biometrics::face::V1_0::BnHwBiometricsFaceClientCallback::_hidl_onEnrollResult(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* onAuthenticated */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::biometrics::face::V1_0::BnHwBiometricsFaceClientCallback::_hidl_onAuthenticated(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* onAcquired */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::biometrics::face::V1_0::BnHwBiometricsFaceClientCallback::_hidl_onAcquired(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* onError */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::biometrics::face::V1_0::BnHwBiometricsFaceClientCallback::_hidl_onError(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* onRemoved */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::biometrics::face::V1_0::BnHwBiometricsFaceClientCallback::_hidl_onRemoved(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* onEnumerate */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::biometrics::face::V1_0::BnHwBiometricsFaceClientCallback::_hidl_onEnumerate(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* onLockoutChanged */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != true) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::biometrics::face::V1_0::BnHwBiometricsFaceClientCallback::_hidl_onLockoutChanged(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsBiometricsFaceClientCallback::BsBiometricsFaceClientCallback(const ::android::sp<::android::hardware::biometrics::face::V1_0::IBiometricsFaceClientCallback> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.biometrics.face@1.0", "IBiometricsFaceClientCallback"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsBiometricsFaceClientCallback::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IBiometricsFaceClientCallback> IBiometricsFaceClientCallback::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwBiometricsFaceClientCallback>(serviceName, false, getStub);
}

::android::sp<IBiometricsFaceClientCallback> IBiometricsFaceClientCallback::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwBiometricsFaceClientCallback>(serviceName, true, getStub);
}

::android::status_t IBiometricsFaceClientCallback::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IBiometricsFaceClientCallback::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.biometrics.face@1.0::IBiometricsFaceClientCallback",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_0
}  // namespace face
}  // namespace biometrics
}  // namespace hardware
}  // namespace android
