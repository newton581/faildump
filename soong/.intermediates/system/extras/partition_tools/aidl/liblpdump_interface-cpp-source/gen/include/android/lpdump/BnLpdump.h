#ifndef AIDL_GENERATED_ANDROID_LPDUMP_BN_LPDUMP_H_
#define AIDL_GENERATED_ANDROID_LPDUMP_BN_LPDUMP_H_

#include <binder/IInterface.h>
#include <android/lpdump/ILpdump.h>
#include <chrono>
#include <functional>
#include <json/value.h>

namespace android {

namespace lpdump {

class BnLpdump : public ::android::BnInterface<ILpdump> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
  static std::function<void(const Json::Value&)> logFunc;
};  // class BnLpdump

}  // namespace lpdump

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_LPDUMP_BN_LPDUMP_H_
