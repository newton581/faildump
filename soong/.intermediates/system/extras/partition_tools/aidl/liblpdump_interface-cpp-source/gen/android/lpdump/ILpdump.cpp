#include <android/lpdump/ILpdump.h>
#include <android/lpdump/BpLpdump.h>

namespace android {

namespace lpdump {

IMPLEMENT_META_INTERFACE(Lpdump, "android.lpdump.ILpdump")

::android::IBinder* ILpdumpDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status ILpdumpDefault::run(const ::std::vector<::std::string>&, ::std::string* ) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace lpdump

}  // namespace android
#include <android/lpdump/BpLpdump.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>
#include <chrono>
#include <functional>
#include <json/value.h>

namespace android {

namespace lpdump {

BpLpdump::BpLpdump(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<ILpdump>(_aidl_impl){
}

std::function<void(const Json::Value&)> BpLpdump::logFunc;

::android::binder::Status BpLpdump::run(const ::std::vector<::std::string>& args, ::std::string* _aidl_return) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  Json::Value _log_input_args(Json::arrayValue);
  if (BpLpdump::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "args";
      _log_arg_element["value"] = Json::Value(Json::arrayValue);
      for (const auto& v: args) _log_arg_element["value"].append(Json::Value(v));
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeUtf8VectorAsUtf16Vector(args);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* run */, _aidl_data, &_aidl_reply);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && ILpdump::getDefaultImpl())) {
     return ILpdump::getDefaultImpl()->run(args, _aidl_return);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_status.readFromParcel(_aidl_reply);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  if (!_aidl_status.isOk()) {
    return _aidl_status;
  }
  _aidl_ret_status = _aidl_reply.readUtf8FromUtf16(_aidl_return);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  if (BpLpdump::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.lpdump.ILpdump");
    _log_transaction["method_name"] = Json::Value("run");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(_aidl_status.exceptionCode());
    _log_status["exception_message"] = Json::Value(_aidl_status.exceptionMessage());
    _log_status["transaction_error"] = Json::Value(_aidl_status.transactionError());
    _log_status["service_specific_error_code"] = Json::Value(_aidl_status.serviceSpecificErrorCode());
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    _log_transaction["_aidl_return"] = Json::Value(*_aidl_return);
    BpLpdump::logFunc(_log_transaction);
  }
  return _aidl_status;
}

}  // namespace lpdump

}  // namespace android
#include <android/lpdump/BnLpdump.h>
#include <binder/Parcel.h>
#include <chrono>
#include <functional>
#include <json/value.h>

namespace android {

namespace lpdump {

::android::status_t BnLpdump::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* run */:
  {
    ::std::vector<::std::string> in_args;
    ::std::string _aidl_return;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readUtf8VectorFromUtf16Vector(&in_args);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    Json::Value _log_input_args(Json::arrayValue);
    if (BnLpdump::logFunc != nullptr) {
      {
        Json::Value _log_arg_element(Json::objectValue);
        _log_arg_element["name"] = "in_args";
        _log_arg_element["value"] = Json::Value(Json::arrayValue);
        for (const auto& v: in_args) _log_arg_element["value"].append(Json::Value(v));
        _log_input_args.append(_log_arg_element);
        }
    }
    auto _log_start = std::chrono::steady_clock::now();
    ::android::binder::Status _aidl_status(run(in_args, &_aidl_return));
    if (BnLpdump::logFunc != nullptr) {
      auto _log_end = std::chrono::steady_clock::now();
      Json::Value _log_transaction(Json::objectValue);
      _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
      _log_transaction["interface_name"] = Json::Value("android.lpdump.ILpdump");
      _log_transaction["method_name"] = Json::Value("run");
      _log_transaction["stub_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
      _log_transaction["input_args"] = _log_input_args;
      Json::Value _log_output_args(Json::arrayValue);
      Json::Value _log_status(Json::objectValue);
      _log_status["exception_code"] = Json::Value(_aidl_status.exceptionCode());
      _log_status["exception_message"] = Json::Value(_aidl_status.exceptionMessage());
      _log_status["transaction_error"] = Json::Value(_aidl_status.transactionError());
      _log_status["service_specific_error_code"] = Json::Value(_aidl_status.serviceSpecificErrorCode());
      _log_transaction["binder_status"] = _log_status;
      _log_transaction["output_args"] = _log_output_args;
      _log_transaction["_aidl_return"] = Json::Value(_aidl_return);
      BnLpdump::logFunc(_log_transaction);
    }
    _aidl_ret_status = _aidl_status.writeToParcel(_aidl_reply);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    if (!_aidl_status.isOk()) {
      break;
    }
    _aidl_ret_status = _aidl_reply->writeUtf8AsUtf16(_aidl_return);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

std::function<void(const Json::Value&)> BnLpdump::logFunc;

}  // namespace lpdump

}  // namespace android
