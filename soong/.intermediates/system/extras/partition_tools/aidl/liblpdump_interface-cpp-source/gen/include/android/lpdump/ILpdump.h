#ifndef AIDL_GENERATED_ANDROID_LPDUMP_I_LPDUMP_H_
#define AIDL_GENERATED_ANDROID_LPDUMP_I_LPDUMP_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <string>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace lpdump {

class ILpdump : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(Lpdump)
  virtual ::android::binder::Status run(const ::std::vector<::std::string>& args, ::std::string* _aidl_return) = 0;
};  // class ILpdump

class ILpdumpDefault : public ILpdump {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status run(const ::std::vector<::std::string>& args, ::std::string* _aidl_return) override;

};

}  // namespace lpdump

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_LPDUMP_I_LPDUMP_H_
