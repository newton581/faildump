/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.apex;
public class ApexSessionInfo implements android.os.Parcelable
{

  public int sessionId;
  // Maps to apex::proto::SessionState::State enum.

  public boolean isUnknown;

  public boolean isVerified;

  public boolean isStaged;

  public boolean isActivated;

  public boolean isRollbackInProgress;

  public boolean isActivationFailed;

  public boolean isSuccess;

  public boolean isRolledBack;

  public boolean isRollbackFailed;
  public static final android.os.Parcelable.Creator<ApexSessionInfo> CREATOR = new android.os.Parcelable.Creator<ApexSessionInfo>() {
    @Override
    public ApexSessionInfo createFromParcel(android.os.Parcel _aidl_source) {
      ApexSessionInfo _aidl_out = new ApexSessionInfo();
      _aidl_out.readFromParcel(_aidl_source);
      return _aidl_out;
    }
    @Override
    public ApexSessionInfo[] newArray(int _aidl_size) {
      return new ApexSessionInfo[_aidl_size];
    }
  };
  @Override public final void writeToParcel(android.os.Parcel _aidl_parcel, int _aidl_flag)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.writeInt(0);
    _aidl_parcel.writeInt(sessionId);
    _aidl_parcel.writeInt(((isUnknown)?(1):(0)));
    _aidl_parcel.writeInt(((isVerified)?(1):(0)));
    _aidl_parcel.writeInt(((isStaged)?(1):(0)));
    _aidl_parcel.writeInt(((isActivated)?(1):(0)));
    _aidl_parcel.writeInt(((isRollbackInProgress)?(1):(0)));
    _aidl_parcel.writeInt(((isActivationFailed)?(1):(0)));
    _aidl_parcel.writeInt(((isSuccess)?(1):(0)));
    _aidl_parcel.writeInt(((isRolledBack)?(1):(0)));
    _aidl_parcel.writeInt(((isRollbackFailed)?(1):(0)));
    int _aidl_end_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.setDataPosition(_aidl_start_pos);
    _aidl_parcel.writeInt(_aidl_end_pos - _aidl_start_pos);
    _aidl_parcel.setDataPosition(_aidl_end_pos);
  }
  public final void readFromParcel(android.os.Parcel _aidl_parcel)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    int _aidl_parcelable_size = _aidl_parcel.readInt();
    if (_aidl_parcelable_size < 0) return;
    try {
      sessionId = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isUnknown = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isVerified = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isStaged = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isActivated = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isRollbackInProgress = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isActivationFailed = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isSuccess = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isRolledBack = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      isRollbackFailed = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
    } finally {
      _aidl_parcel.setDataPosition(_aidl_start_pos + _aidl_parcelable_size);
    }
  }
  @Override public int describeContents()
  {
    return 0;
  }
}
