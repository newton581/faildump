/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.apex;
public interface IApexService extends android.os.IInterface
{
  /** Default implementation for IApexService. */
  public static class Default implements android.apex.IApexService
  {
    @Override public boolean submitStagedSession(int session_id, int[] child_session_ids, android.apex.ApexInfoList packages) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean markStagedSessionReady(int session_id) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void markStagedSessionSuccessful(int session_id) throws android.os.RemoteException
    {
    }
    @Override public android.apex.ApexSessionInfo[] getSessions() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.apex.ApexSessionInfo getStagedSessionInfo(int session_id) throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.apex.ApexInfo[] getActivePackages() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.apex.ApexInfo[] getAllPackages() throws android.os.RemoteException
    {
      return null;
    }
    @Override public void abortActiveSession() throws android.os.RemoteException
    {
    }
    @Override public void unstagePackages(java.util.List<java.lang.String> active_package_paths) throws android.os.RemoteException
    {
    }
    /**
        * Returns the active package corresponding to |package_name| and null
        * if none exists.
        */
    @Override public android.apex.ApexInfo getActivePackage(java.lang.String package_name) throws android.os.RemoteException
    {
      return null;
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public void activatePackage(java.lang.String package_path) throws android.os.RemoteException
    {
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public void deactivatePackage(java.lang.String package_path) throws android.os.RemoteException
    {
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public void preinstallPackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException
    {
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public void postinstallPackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException
    {
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public boolean stagePackage(java.lang.String package_tmp_path) throws android.os.RemoteException
    {
      return false;
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public boolean stagePackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException
    {
      return false;
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public void rollbackActiveSession() throws android.os.RemoteException
    {
    }
    /**
        * Not meant for use outside of testing. The call will not be
        * functional on user builds.
        */
    @Override public void resumeRollbackIfNeeded() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.apex.IApexService
  {
    private static final java.lang.String DESCRIPTOR = "android.apex.IApexService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.apex.IApexService interface,
     * generating a proxy if needed.
     */
    public static android.apex.IApexService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.apex.IApexService))) {
        return ((android.apex.IApexService)iin);
      }
      return new android.apex.IApexService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_submitStagedSession:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int[] _arg1;
          _arg1 = data.createIntArray();
          android.apex.ApexInfoList _arg2;
          _arg2 = new android.apex.ApexInfoList();
          boolean _result = this.submitStagedSession(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          if ((_arg2!=null)) {
            reply.writeInt(1);
            _arg2.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_markStagedSessionReady:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.markStagedSessionReady(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_markStagedSessionSuccessful:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.markStagedSessionSuccessful(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getSessions:
        {
          data.enforceInterface(descriptor);
          android.apex.ApexSessionInfo[] _result = this.getSessions();
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_getStagedSessionInfo:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.apex.ApexSessionInfo _result = this.getStagedSessionInfo(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getActivePackages:
        {
          data.enforceInterface(descriptor);
          android.apex.ApexInfo[] _result = this.getActivePackages();
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_getAllPackages:
        {
          data.enforceInterface(descriptor);
          android.apex.ApexInfo[] _result = this.getAllPackages();
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_abortActiveSession:
        {
          data.enforceInterface(descriptor);
          this.abortActiveSession();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unstagePackages:
        {
          data.enforceInterface(descriptor);
          java.util.List<java.lang.String> _arg0;
          _arg0 = data.createStringArrayList();
          this.unstagePackages(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getActivePackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.apex.ApexInfo _result = this.getActivePackage(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_activatePackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.activatePackage(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_deactivatePackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.deactivatePackage(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_preinstallPackages:
        {
          data.enforceInterface(descriptor);
          java.util.List<java.lang.String> _arg0;
          _arg0 = data.createStringArrayList();
          this.preinstallPackages(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_postinstallPackages:
        {
          data.enforceInterface(descriptor);
          java.util.List<java.lang.String> _arg0;
          _arg0 = data.createStringArrayList();
          this.postinstallPackages(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_stagePackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.stagePackage(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_stagePackages:
        {
          data.enforceInterface(descriptor);
          java.util.List<java.lang.String> _arg0;
          _arg0 = data.createStringArrayList();
          boolean _result = this.stagePackages(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_rollbackActiveSession:
        {
          data.enforceInterface(descriptor);
          this.rollbackActiveSession();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_resumeRollbackIfNeeded:
        {
          data.enforceInterface(descriptor);
          this.resumeRollbackIfNeeded();
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.apex.IApexService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public boolean submitStagedSession(int session_id, int[] child_session_ids, android.apex.ApexInfoList packages) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(session_id);
          _data.writeIntArray(child_session_ids);
          boolean _status = mRemote.transact(Stub.TRANSACTION_submitStagedSession, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().submitStagedSession(session_id, child_session_ids, packages);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
          if ((0!=_reply.readInt())) {
            packages.readFromParcel(_reply);
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean markStagedSessionReady(int session_id) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(session_id);
          boolean _status = mRemote.transact(Stub.TRANSACTION_markStagedSessionReady, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().markStagedSessionReady(session_id);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void markStagedSessionSuccessful(int session_id) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(session_id);
          boolean _status = mRemote.transact(Stub.TRANSACTION_markStagedSessionSuccessful, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().markStagedSessionSuccessful(session_id);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.apex.ApexSessionInfo[] getSessions() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.apex.ApexSessionInfo[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSessions, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSessions();
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.apex.ApexSessionInfo.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.apex.ApexSessionInfo getStagedSessionInfo(int session_id) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.apex.ApexSessionInfo _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(session_id);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getStagedSessionInfo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getStagedSessionInfo(session_id);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.apex.ApexSessionInfo.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.apex.ApexInfo[] getActivePackages() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.apex.ApexInfo[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getActivePackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getActivePackages();
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.apex.ApexInfo.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.apex.ApexInfo[] getAllPackages() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.apex.ApexInfo[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAllPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAllPackages();
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.apex.ApexInfo.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void abortActiveSession() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_abortActiveSession, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().abortActiveSession();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void unstagePackages(java.util.List<java.lang.String> active_package_paths) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringList(active_package_paths);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unstagePackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unstagePackages(active_package_paths);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
          * Returns the active package corresponding to |package_name| and null
          * if none exists.
          */
      @Override public android.apex.ApexInfo getActivePackage(java.lang.String package_name) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.apex.ApexInfo _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(package_name);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getActivePackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getActivePackage(package_name);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.apex.ApexInfo.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public void activatePackage(java.lang.String package_path) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(package_path);
          boolean _status = mRemote.transact(Stub.TRANSACTION_activatePackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().activatePackage(package_path);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public void deactivatePackage(java.lang.String package_path) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(package_path);
          boolean _status = mRemote.transact(Stub.TRANSACTION_deactivatePackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().deactivatePackage(package_path);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public void preinstallPackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringList(package_tmp_paths);
          boolean _status = mRemote.transact(Stub.TRANSACTION_preinstallPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().preinstallPackages(package_tmp_paths);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public void postinstallPackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringList(package_tmp_paths);
          boolean _status = mRemote.transact(Stub.TRANSACTION_postinstallPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().postinstallPackages(package_tmp_paths);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public boolean stagePackage(java.lang.String package_tmp_path) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(package_tmp_path);
          boolean _status = mRemote.transact(Stub.TRANSACTION_stagePackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().stagePackage(package_tmp_path);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public boolean stagePackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringList(package_tmp_paths);
          boolean _status = mRemote.transact(Stub.TRANSACTION_stagePackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().stagePackages(package_tmp_paths);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public void rollbackActiveSession() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_rollbackActiveSession, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().rollbackActiveSession();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
          * Not meant for use outside of testing. The call will not be
          * functional on user builds.
          */
      @Override public void resumeRollbackIfNeeded() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_resumeRollbackIfNeeded, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().resumeRollbackIfNeeded();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.apex.IApexService sDefaultImpl;
    }
    static final int TRANSACTION_submitStagedSession = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_markStagedSessionReady = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_markStagedSessionSuccessful = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_getSessions = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_getStagedSessionInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_getActivePackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_getAllPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_abortActiveSession = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_unstagePackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_getActivePackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_activatePackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_deactivatePackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_preinstallPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_postinstallPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_stagePackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_stagePackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_rollbackActiveSession = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_resumeRollbackIfNeeded = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    public static boolean setDefaultImpl(android.apex.IApexService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.apex.IApexService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public boolean submitStagedSession(int session_id, int[] child_session_ids, android.apex.ApexInfoList packages) throws android.os.RemoteException;
  public boolean markStagedSessionReady(int session_id) throws android.os.RemoteException;
  public void markStagedSessionSuccessful(int session_id) throws android.os.RemoteException;
  public android.apex.ApexSessionInfo[] getSessions() throws android.os.RemoteException;
  public android.apex.ApexSessionInfo getStagedSessionInfo(int session_id) throws android.os.RemoteException;
  public android.apex.ApexInfo[] getActivePackages() throws android.os.RemoteException;
  public android.apex.ApexInfo[] getAllPackages() throws android.os.RemoteException;
  public void abortActiveSession() throws android.os.RemoteException;
  public void unstagePackages(java.util.List<java.lang.String> active_package_paths) throws android.os.RemoteException;
  /**
      * Returns the active package corresponding to |package_name| and null
      * if none exists.
      */
  public android.apex.ApexInfo getActivePackage(java.lang.String package_name) throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public void activatePackage(java.lang.String package_path) throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public void deactivatePackage(java.lang.String package_path) throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public void preinstallPackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public void postinstallPackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public boolean stagePackage(java.lang.String package_tmp_path) throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public boolean stagePackages(java.util.List<java.lang.String> package_tmp_paths) throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public void rollbackActiveSession() throws android.os.RemoteException;
  /**
      * Not meant for use outside of testing. The call will not be
      * functional on user builds.
      */
  public void resumeRollbackIfNeeded() throws android.os.RemoteException;
}
