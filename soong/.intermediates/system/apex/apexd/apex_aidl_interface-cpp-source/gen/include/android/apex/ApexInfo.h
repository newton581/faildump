#ifndef AIDL_GENERATED_ANDROID_APEX_APEX_INFO_H_
#define AIDL_GENERATED_ANDROID_APEX_APEX_INFO_H_

#include <binder/Parcel.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>

namespace android {

namespace apex {

class ApexInfo : public ::android::Parcelable {
public:
  ::std::string packageName;
  ::std::string packagePath;
  int64_t versionCode;
  ::std::string versionName;
  bool isFactory;
  bool isActive;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class ApexInfo

}  // namespace apex

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_APEX_APEX_INFO_H_
