#ifndef AIDL_GENERATED_ANDROID_APEX_BP_APEX_SERVICE_H_
#define AIDL_GENERATED_ANDROID_APEX_BP_APEX_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/apex/IApexService.h>

namespace android {

namespace apex {

class BpApexService : public ::android::BpInterface<IApexService> {
public:
  explicit BpApexService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpApexService() = default;
  ::android::binder::Status submitStagedSession(int32_t session_id, const ::std::vector<int32_t>& child_session_ids, ::android::apex::ApexInfoList* packages, bool* _aidl_return) override;
  ::android::binder::Status markStagedSessionReady(int32_t session_id, bool* _aidl_return) override;
  ::android::binder::Status markStagedSessionSuccessful(int32_t session_id) override;
  ::android::binder::Status getSessions(::std::vector<::android::apex::ApexSessionInfo>* _aidl_return) override;
  ::android::binder::Status getStagedSessionInfo(int32_t session_id, ::android::apex::ApexSessionInfo* _aidl_return) override;
  ::android::binder::Status getActivePackages(::std::vector<::android::apex::ApexInfo>* _aidl_return) override;
  ::android::binder::Status getAllPackages(::std::vector<::android::apex::ApexInfo>* _aidl_return) override;
  ::android::binder::Status abortActiveSession() override;
  ::android::binder::Status unstagePackages(const ::std::vector<::std::string>& active_package_paths) override;
  ::android::binder::Status getActivePackage(const ::std::string& package_name, ::android::apex::ApexInfo* _aidl_return) override;
  ::android::binder::Status activatePackage(const ::std::string& package_path) override;
  ::android::binder::Status deactivatePackage(const ::std::string& package_path) override;
  ::android::binder::Status preinstallPackages(const ::std::vector<::std::string>& package_tmp_paths) override;
  ::android::binder::Status postinstallPackages(const ::std::vector<::std::string>& package_tmp_paths) override;
  ::android::binder::Status stagePackage(const ::std::string& package_tmp_path, bool* _aidl_return) override;
  ::android::binder::Status stagePackages(const ::std::vector<::std::string>& package_tmp_paths, bool* _aidl_return) override;
  ::android::binder::Status rollbackActiveSession() override;
  ::android::binder::Status resumeRollbackIfNeeded() override;
};  // class BpApexService

}  // namespace apex

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_APEX_BP_APEX_SERVICE_H_
