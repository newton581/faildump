#ifndef AIDL_GENERATED_ANDROID_APEX_BN_APEX_SERVICE_H_
#define AIDL_GENERATED_ANDROID_APEX_BN_APEX_SERVICE_H_

#include <binder/IInterface.h>
#include <android/apex/IApexService.h>

namespace android {

namespace apex {

class BnApexService : public ::android::BnInterface<IApexService> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnApexService

}  // namespace apex

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_APEX_BN_APEX_SERVICE_H_
