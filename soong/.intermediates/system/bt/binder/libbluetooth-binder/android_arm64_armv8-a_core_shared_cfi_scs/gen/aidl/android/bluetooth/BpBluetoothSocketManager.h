#ifndef AIDL_GENERATED_ANDROID_BLUETOOTH_BP_BLUETOOTH_SOCKET_MANAGER_H_
#define AIDL_GENERATED_ANDROID_BLUETOOTH_BP_BLUETOOTH_SOCKET_MANAGER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/bluetooth/IBluetoothSocketManager.h>

namespace android {

namespace bluetooth {

class BpBluetoothSocketManager : public ::android::BpInterface<IBluetoothSocketManager> {
public:
  explicit BpBluetoothSocketManager(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpBluetoothSocketManager() = default;
  ::android::binder::Status connectSocket(const ::android::bluetooth::BluetoothDevice& device, int32_t type, const ::std::unique_ptr<::android::os::ParcelUuid>& uuid, int32_t port, int32_t flag, ::std::unique_ptr<::android::os::ParcelFileDescriptor>* _aidl_return) override;
  ::android::binder::Status createSocketChannel(int32_t type, const ::std::unique_ptr<::android::String16>& serviceName, const ::std::unique_ptr<::android::os::ParcelUuid>& uuid, int32_t port, int32_t flag, ::std::unique_ptr<::android::os::ParcelFileDescriptor>* _aidl_return) override;
  ::android::binder::Status requestMaximumTxDataLength(const ::android::bluetooth::BluetoothDevice& device) override;
};  // class BpBluetoothSocketManager

}  // namespace bluetooth

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_BLUETOOTH_BP_BLUETOOTH_SOCKET_MANAGER_H_
