#ifndef AIDL_GENERATED_ANDROID_OS_I_UPDATE_ENGINE_H_
#define AIDL_GENERATED_ANDROID_OS_I_UPDATE_ENGINE_H_

#include <android/os/IUpdateEngineCallback.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/String16.h>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace os {

class IUpdateEngine : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(UpdateEngine)
  virtual ::android::binder::Status applyPayload(const ::android::String16& url, int64_t payload_offset, int64_t payload_size, const ::std::vector<::android::String16>& headerKeyValuePairs) = 0;
  virtual ::android::binder::Status bind(const ::android::sp<::android::os::IUpdateEngineCallback>& callback, bool* _aidl_return) = 0;
  virtual ::android::binder::Status unbind(const ::android::sp<::android::os::IUpdateEngineCallback>& callback, bool* _aidl_return) = 0;
  virtual ::android::binder::Status suspend() = 0;
  virtual ::android::binder::Status resume() = 0;
  virtual ::android::binder::Status cancel() = 0;
  virtual ::android::binder::Status resetStatus() = 0;
  virtual ::android::binder::Status verifyPayloadApplicable(const ::android::String16& metadataFilename, bool* _aidl_return) = 0;
  virtual ::android::binder::Status setPerformanceMode(bool enable) = 0;
};  // class IUpdateEngine

class IUpdateEngineDefault : public IUpdateEngine {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status applyPayload(const ::android::String16& url, int64_t payload_offset, int64_t payload_size, const ::std::vector<::android::String16>& headerKeyValuePairs) override;
  ::android::binder::Status bind(const ::android::sp<::android::os::IUpdateEngineCallback>& callback, bool* _aidl_return) override;
  ::android::binder::Status unbind(const ::android::sp<::android::os::IUpdateEngineCallback>& callback, bool* _aidl_return) override;
  ::android::binder::Status suspend() override;
  ::android::binder::Status resume() override;
  ::android::binder::Status cancel() override;
  ::android::binder::Status resetStatus() override;
  ::android::binder::Status verifyPayloadApplicable(const ::android::String16& metadataFilename, bool* _aidl_return) override;
  ::android::binder::Status setPerformanceMode(bool enable) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_UPDATE_ENGINE_H_
