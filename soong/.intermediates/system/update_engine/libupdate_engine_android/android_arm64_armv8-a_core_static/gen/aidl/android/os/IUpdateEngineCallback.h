#ifndef AIDL_GENERATED_ANDROID_OS_I_UPDATE_ENGINE_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_OS_I_UPDATE_ENGINE_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IUpdateEngineCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(UpdateEngineCallback)
  virtual ::android::binder::Status onStatusUpdate(int32_t status_code, float percentage) = 0;
  virtual ::android::binder::Status onPayloadApplicationComplete(int32_t error_code) = 0;
};  // class IUpdateEngineCallback

class IUpdateEngineCallbackDefault : public IUpdateEngineCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onStatusUpdate(int32_t status_code, float percentage) override;
  ::android::binder::Status onPayloadApplicationComplete(int32_t error_code) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_UPDATE_ENGINE_CALLBACK_H_
