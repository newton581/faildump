#ifndef AIDL_GENERATED_ANDROID_NET_I_DNS_RESOLVER_H_
#define AIDL_GENERATED_ANDROID_NET_I_DNS_RESOLVER_H_

#include <android/net/ResolverParamsParcel.h>
#include <android/net/metrics/INetdEventListener.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace net {

class IDnsResolver : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(DnsResolver)
  const int32_t VERSION = 2;
  enum  : int32_t {
    RESOLVER_PARAMS_SAMPLE_VALIDITY = 0,
    RESOLVER_PARAMS_SUCCESS_THRESHOLD = 1,
    RESOLVER_PARAMS_MIN_SAMPLES = 2,
    RESOLVER_PARAMS_MAX_SAMPLES = 3,
    RESOLVER_PARAMS_BASE_TIMEOUT_MSEC = 4,
    RESOLVER_PARAMS_RETRY_COUNT = 5,
    RESOLVER_PARAMS_COUNT = 6,
    RESOLVER_STATS_SUCCESSES = 0,
    RESOLVER_STATS_ERRORS = 1,
    RESOLVER_STATS_TIMEOUTS = 2,
    RESOLVER_STATS_INTERNAL_ERRORS = 3,
    RESOLVER_STATS_RTT_AVG = 4,
    RESOLVER_STATS_LAST_SAMPLE_TIME = 5,
    RESOLVER_STATS_USABLE = 6,
    RESOLVER_STATS_COUNT = 7,
    DNS_RESOLVER_LOG_VERBOSE = 0,
    DNS_RESOLVER_LOG_DEBUG = 1,
    DNS_RESOLVER_LOG_INFO = 2,
    DNS_RESOLVER_LOG_WARNING = 3,
    DNS_RESOLVER_LOG_ERROR = 4,
  };
  virtual ::android::binder::Status isAlive(bool* _aidl_return) = 0;
  virtual ::android::binder::Status registerEventListener(const ::android::sp<::android::net::metrics::INetdEventListener>& listener) = 0;
  virtual ::android::binder::Status setResolverConfiguration(const ::android::net::ResolverParamsParcel& resolverParams) = 0;
  virtual ::android::binder::Status getResolverInfo(int32_t netId, ::std::vector<::std::string>* servers, ::std::vector<::std::string>* domains, ::std::vector<::std::string>* tlsServers, ::std::vector<int32_t>* params, ::std::vector<int32_t>* stats, ::std::vector<int32_t>* wait_for_pending_req_timeout_count) = 0;
  virtual ::android::binder::Status startPrefix64Discovery(int32_t netId) = 0;
  virtual ::android::binder::Status stopPrefix64Discovery(int32_t netId) = 0;
  virtual ::android::binder::Status getPrefix64(int32_t netId, ::std::string* _aidl_return) = 0;
  virtual ::android::binder::Status createNetworkCache(int32_t netId) = 0;
  virtual ::android::binder::Status destroyNetworkCache(int32_t netId) = 0;
  virtual ::android::binder::Status setLogSeverity(int32_t logSeverity) = 0;
  virtual int32_t getInterfaceVersion() = 0;
};  // class IDnsResolver

class IDnsResolverDefault : public IDnsResolver {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status isAlive(bool* _aidl_return) override;
  ::android::binder::Status registerEventListener(const ::android::sp<::android::net::metrics::INetdEventListener>& listener) override;
  ::android::binder::Status setResolverConfiguration(const ::android::net::ResolverParamsParcel& resolverParams) override;
  ::android::binder::Status getResolverInfo(int32_t netId, ::std::vector<::std::string>* servers, ::std::vector<::std::string>* domains, ::std::vector<::std::string>* tlsServers, ::std::vector<int32_t>* params, ::std::vector<int32_t>* stats, ::std::vector<int32_t>* wait_for_pending_req_timeout_count) override;
  ::android::binder::Status startPrefix64Discovery(int32_t netId) override;
  ::android::binder::Status stopPrefix64Discovery(int32_t netId) override;
  ::android::binder::Status getPrefix64(int32_t netId, ::std::string* _aidl_return) override;
  ::android::binder::Status createNetworkCache(int32_t netId) override;
  ::android::binder::Status destroyNetworkCache(int32_t netId) override;
  ::android::binder::Status setLogSeverity(int32_t logSeverity) override;
  int32_t getInterfaceVersion() override;

};

}  // namespace net

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_NET_I_DNS_RESOLVER_H_
