#ifndef AIDL_GENERATED_ANDROID_NET_BP_DNS_RESOLVER_H_
#define AIDL_GENERATED_ANDROID_NET_BP_DNS_RESOLVER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/net/IDnsResolver.h>

namespace android {

namespace net {

class BpDnsResolver : public ::android::BpInterface<IDnsResolver> {
public:
  explicit BpDnsResolver(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpDnsResolver() = default;
  ::android::binder::Status isAlive(bool* _aidl_return) override;
  ::android::binder::Status registerEventListener(const ::android::sp<::android::net::metrics::INetdEventListener>& listener) override;
  ::android::binder::Status setResolverConfiguration(const ::android::net::ResolverParamsParcel& resolverParams) override;
  ::android::binder::Status getResolverInfo(int32_t netId, ::std::vector<::std::string>* servers, ::std::vector<::std::string>* domains, ::std::vector<::std::string>* tlsServers, ::std::vector<int32_t>* params, ::std::vector<int32_t>* stats, ::std::vector<int32_t>* wait_for_pending_req_timeout_count) override;
  ::android::binder::Status startPrefix64Discovery(int32_t netId) override;
  ::android::binder::Status stopPrefix64Discovery(int32_t netId) override;
  ::android::binder::Status getPrefix64(int32_t netId, ::std::string* _aidl_return) override;
  ::android::binder::Status createNetworkCache(int32_t netId) override;
  ::android::binder::Status destroyNetworkCache(int32_t netId) override;
  ::android::binder::Status setLogSeverity(int32_t logSeverity) override;
  int32_t getInterfaceVersion() override;
private:
  int32_t cached_version_ = -1;
};  // class BpDnsResolver

}  // namespace net

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_NET_BP_DNS_RESOLVER_H_
