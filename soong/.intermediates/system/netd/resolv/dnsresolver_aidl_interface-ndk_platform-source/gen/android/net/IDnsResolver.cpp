#include <aidl/android/net/BpDnsResolver.h>
#include <aidl/android/net/BnDnsResolver.h>
#include <aidl/android/net/IDnsResolver.h>
#include <aidl/android/net/metrics/BpNetdEventListener.h>
#include <aidl/android/net/metrics/BnNetdEventListener.h>
#include <aidl/android/net/metrics/INetdEventListener.h>

namespace aidl {
namespace android {
namespace net {
static binder_status_t _aidl_onTransact(AIBinder* _aidl_binder, transaction_code_t _aidl_code, const AParcel* _aidl_in, AParcel* _aidl_out) {
  (void)_aidl_in;
  (void)_aidl_out;
  binder_status_t _aidl_ret_status = STATUS_UNKNOWN_TRANSACTION;
  std::shared_ptr<BnDnsResolver> _aidl_impl = std::static_pointer_cast<BnDnsResolver>(::ndk::ICInterface::asInterface(_aidl_binder));
  switch (_aidl_code) {
    case (FIRST_CALL_TRANSACTION + 0 /*isAlive*/): {
      bool _aidl_return;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->isAlive(&_aidl_return);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("isAlive");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        _log_transaction["_aidl_return"] = Json::Value(_aidl_return? "true" : "false");
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      _aidl_ret_status = AParcel_writeBool(_aidl_out, _aidl_return);
      if (_aidl_ret_status != STATUS_OK) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 1 /*registerEventListener*/): {
      std::shared_ptr<::aidl::android::net::metrics::INetdEventListener> in_listener;

      _aidl_ret_status = ::aidl::android::net::metrics::INetdEventListener::readFromParcel(_aidl_in, &in_listener);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->registerEventListener(in_listener);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("registerEventListener");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 2 /*setResolverConfiguration*/): {
      ::aidl::android::net::ResolverParamsParcel in_resolverParams;

      _aidl_ret_status = (&in_resolverParams)->readFromParcel(_aidl_in);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->setResolverConfiguration(in_resolverParams);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("setResolverConfiguration");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 3 /*getResolverInfo*/): {
      int32_t in_netId;
      std::vector<std::string> out_servers;
      std::vector<std::string> out_domains;
      std::vector<std::string> out_tlsServers;
      std::vector<int32_t> out_params;
      std::vector<int32_t> out_stats;
      std::vector<int32_t> out_wait_for_pending_req_timeout_count;

      _aidl_ret_status = AParcel_readInt32(_aidl_in, &in_netId);
      if (_aidl_ret_status != STATUS_OK) break;

      _aidl_ret_status = ::ndk::AParcel_resizeVector(_aidl_in, &out_servers);
      _aidl_ret_status = ::ndk::AParcel_resizeVector(_aidl_in, &out_domains);
      _aidl_ret_status = ::ndk::AParcel_resizeVector(_aidl_in, &out_tlsServers);
      _aidl_ret_status = ::ndk::AParcel_resizeVector(_aidl_in, &out_params);
      _aidl_ret_status = ::ndk::AParcel_resizeVector(_aidl_in, &out_stats);
      _aidl_ret_status = ::ndk::AParcel_resizeVector(_aidl_in, &out_wait_for_pending_req_timeout_count);
      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "in_netId";
          _log_arg_element["value"] = Json::Value(in_netId);
          _log_input_args.append(_log_arg_element);
          }
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->getResolverInfo(in_netId, &out_servers, &out_domains, &out_tlsServers, &out_params, &out_stats, &out_wait_for_pending_req_timeout_count);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("getResolverInfo");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "out_servers";
          _log_arg_element["value"] = Json::Value(Json::arrayValue);
          for (const auto& v: out_servers) _log_arg_element["value"].append(Json::Value(v));
          _log_output_args.append(_log_arg_element);
          }
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "out_domains";
          _log_arg_element["value"] = Json::Value(Json::arrayValue);
          for (const auto& v: out_domains) _log_arg_element["value"].append(Json::Value(v));
          _log_output_args.append(_log_arg_element);
          }
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "out_tlsServers";
          _log_arg_element["value"] = Json::Value(Json::arrayValue);
          for (const auto& v: out_tlsServers) _log_arg_element["value"].append(Json::Value(v));
          _log_output_args.append(_log_arg_element);
          }
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "out_params";
          _log_arg_element["value"] = Json::Value(Json::arrayValue);
          for (const auto& v: out_params) _log_arg_element["value"].append(Json::Value(v));
          _log_output_args.append(_log_arg_element);
          }
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "out_stats";
          _log_arg_element["value"] = Json::Value(Json::arrayValue);
          for (const auto& v: out_stats) _log_arg_element["value"].append(Json::Value(v));
          _log_output_args.append(_log_arg_element);
          }
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "out_wait_for_pending_req_timeout_count";
          _log_arg_element["value"] = Json::Value(Json::arrayValue);
          for (const auto& v: out_wait_for_pending_req_timeout_count) _log_arg_element["value"].append(Json::Value(v));
          _log_output_args.append(_log_arg_element);
          }
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      _aidl_ret_status = ::ndk::AParcel_writeVector(_aidl_out, out_servers);
      if (_aidl_ret_status != STATUS_OK) break;

      _aidl_ret_status = ::ndk::AParcel_writeVector(_aidl_out, out_domains);
      if (_aidl_ret_status != STATUS_OK) break;

      _aidl_ret_status = ::ndk::AParcel_writeVector(_aidl_out, out_tlsServers);
      if (_aidl_ret_status != STATUS_OK) break;

      _aidl_ret_status = ::ndk::AParcel_writeVector(_aidl_out, out_params);
      if (_aidl_ret_status != STATUS_OK) break;

      _aidl_ret_status = ::ndk::AParcel_writeVector(_aidl_out, out_stats);
      if (_aidl_ret_status != STATUS_OK) break;

      _aidl_ret_status = ::ndk::AParcel_writeVector(_aidl_out, out_wait_for_pending_req_timeout_count);
      if (_aidl_ret_status != STATUS_OK) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 4 /*startPrefix64Discovery*/): {
      int32_t in_netId;

      _aidl_ret_status = AParcel_readInt32(_aidl_in, &in_netId);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "in_netId";
          _log_arg_element["value"] = Json::Value(in_netId);
          _log_input_args.append(_log_arg_element);
          }
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->startPrefix64Discovery(in_netId);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("startPrefix64Discovery");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 5 /*stopPrefix64Discovery*/): {
      int32_t in_netId;

      _aidl_ret_status = AParcel_readInt32(_aidl_in, &in_netId);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "in_netId";
          _log_arg_element["value"] = Json::Value(in_netId);
          _log_input_args.append(_log_arg_element);
          }
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->stopPrefix64Discovery(in_netId);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("stopPrefix64Discovery");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 6 /*getPrefix64*/): {
      int32_t in_netId;
      std::string _aidl_return;

      _aidl_ret_status = AParcel_readInt32(_aidl_in, &in_netId);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "in_netId";
          _log_arg_element["value"] = Json::Value(in_netId);
          _log_input_args.append(_log_arg_element);
          }
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->getPrefix64(in_netId, &_aidl_return);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("getPrefix64");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        _log_transaction["_aidl_return"] = Json::Value(_aidl_return);
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      _aidl_ret_status = ::ndk::AParcel_writeString(_aidl_out, _aidl_return);
      if (_aidl_ret_status != STATUS_OK) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 7 /*createNetworkCache*/): {
      int32_t in_netId;

      _aidl_ret_status = AParcel_readInt32(_aidl_in, &in_netId);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "in_netId";
          _log_arg_element["value"] = Json::Value(in_netId);
          _log_input_args.append(_log_arg_element);
          }
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->createNetworkCache(in_netId);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("createNetworkCache");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 8 /*destroyNetworkCache*/): {
      int32_t in_netId;

      _aidl_ret_status = AParcel_readInt32(_aidl_in, &in_netId);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "in_netId";
          _log_arg_element["value"] = Json::Value(in_netId);
          _log_input_args.append(_log_arg_element);
          }
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->destroyNetworkCache(in_netId);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("destroyNetworkCache");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 9 /*setLogSeverity*/): {
      int32_t in_logSeverity;

      _aidl_ret_status = AParcel_readInt32(_aidl_in, &in_logSeverity);
      if (_aidl_ret_status != STATUS_OK) break;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
        {
          Json::Value _log_arg_element(Json::objectValue);
          _log_arg_element["name"] = "in_logSeverity";
          _log_arg_element["value"] = Json::Value(in_logSeverity);
          _log_input_args.append(_log_arg_element);
          }
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->setLogSeverity(in_logSeverity);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("setLogSeverity");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      break;
    }
    case (FIRST_CALL_TRANSACTION + 16777214 /*getInterfaceVersion*/): {
      int32_t _aidl_return;

      Json::Value _log_input_args(Json::arrayValue);
      if (BnDnsResolver::logFunc != nullptr) {
      }
      auto _log_start = std::chrono::steady_clock::now();
      ::ndk::ScopedAStatus _aidl_status = _aidl_impl->getInterfaceVersion(&_aidl_return);
      if (BnDnsResolver::logFunc != nullptr) {
        auto _log_end = std::chrono::steady_clock::now();
        Json::Value _log_transaction(Json::objectValue);
        _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
        _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
        _log_transaction["method_name"] = Json::Value("getInterfaceVersion");
        _log_transaction["stub_address"] = Json::Value((std::ostringstream() << _aidl_impl).str());
        _log_transaction["input_args"] = _log_input_args;
        Json::Value _log_output_args(Json::arrayValue);
        Json::Value _log_status(Json::objectValue);
        _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
        _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
        _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
        _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
        _log_transaction["binder_status"] = _log_status;
        _log_transaction["output_args"] = _log_output_args;
        _log_transaction["_aidl_return"] = Json::Value(_aidl_return);
        BnDnsResolver::logFunc(_log_transaction);
      }
      _aidl_ret_status = AParcel_writeStatusHeader(_aidl_out, _aidl_status.get());
      if (_aidl_ret_status != STATUS_OK) break;

      if (!AStatus_isOk(_aidl_status.get())) break;

      _aidl_ret_status = AParcel_writeInt32(_aidl_out, _aidl_return);
      if (_aidl_ret_status != STATUS_OK) break;

      break;
    }
  }
  return _aidl_ret_status;
};

static AIBinder_Class* _g_aidl_clazz = ::ndk::ICInterface::defineClass(IDnsResolver::descriptor, _aidl_onTransact);

BpDnsResolver::BpDnsResolver(const ::ndk::SpAIBinder& binder) : BpCInterface(binder) {}
BpDnsResolver::~BpDnsResolver() {}
std::function<void(const Json::Value&)> BpDnsResolver::logFunc;

::ndk::ScopedAStatus BpDnsResolver::isAlive(bool* _aidl_return) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 0 /*isAlive*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->isAlive(_aidl_return);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_ret_status = AParcel_readBool(_aidl_out.get(), _aidl_return);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("isAlive");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    _log_transaction["_aidl_return"] = Json::Value(*_aidl_return? "true" : "false");
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::registerEventListener(const std::shared_ptr<::aidl::android::net::metrics::INetdEventListener>& in_listener) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = ::aidl::android::net::metrics::INetdEventListener::writeToParcel(_aidl_in.get(), in_listener);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 1 /*registerEventListener*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->registerEventListener(in_listener);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("registerEventListener");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::setResolverConfiguration(const ::aidl::android::net::ResolverParamsParcel& in_resolverParams) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = (in_resolverParams).writeToParcel(_aidl_in.get());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 2 /*setResolverConfiguration*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->setResolverConfiguration(in_resolverParams);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("setResolverConfiguration");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::getResolverInfo(int32_t in_netId, std::vector<std::string>* out_servers, std::vector<std::string>* out_domains, std::vector<std::string>* out_tlsServers, std::vector<int32_t>* out_params, std::vector<int32_t>* out_stats, std::vector<int32_t>* out_wait_for_pending_req_timeout_count) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "in_netId";
      _log_arg_element["value"] = Json::Value(in_netId);
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_writeInt32(_aidl_in.get(), in_netId);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = ::ndk::AParcel_writeVectorSize(_aidl_in.get(), *out_servers);
  _aidl_ret_status = ::ndk::AParcel_writeVectorSize(_aidl_in.get(), *out_domains);
  _aidl_ret_status = ::ndk::AParcel_writeVectorSize(_aidl_in.get(), *out_tlsServers);
  _aidl_ret_status = ::ndk::AParcel_writeVectorSize(_aidl_in.get(), *out_params);
  _aidl_ret_status = ::ndk::AParcel_writeVectorSize(_aidl_in.get(), *out_stats);
  _aidl_ret_status = ::ndk::AParcel_writeVectorSize(_aidl_in.get(), *out_wait_for_pending_req_timeout_count);
  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 3 /*getResolverInfo*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->getResolverInfo(in_netId, out_servers, out_domains, out_tlsServers, out_params, out_stats, out_wait_for_pending_req_timeout_count);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_ret_status = ::ndk::AParcel_readVector(_aidl_out.get(), out_servers);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = ::ndk::AParcel_readVector(_aidl_out.get(), out_domains);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = ::ndk::AParcel_readVector(_aidl_out.get(), out_tlsServers);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = ::ndk::AParcel_readVector(_aidl_out.get(), out_params);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = ::ndk::AParcel_readVector(_aidl_out.get(), out_stats);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = ::ndk::AParcel_readVector(_aidl_out.get(), out_wait_for_pending_req_timeout_count);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("getResolverInfo");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "out_servers";
      _log_arg_element["value"] = Json::Value(Json::arrayValue);
      for (const auto& v: *out_servers) _log_arg_element["value"].append(Json::Value(v));
      _log_output_args.append(_log_arg_element);
      }
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "out_domains";
      _log_arg_element["value"] = Json::Value(Json::arrayValue);
      for (const auto& v: *out_domains) _log_arg_element["value"].append(Json::Value(v));
      _log_output_args.append(_log_arg_element);
      }
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "out_tlsServers";
      _log_arg_element["value"] = Json::Value(Json::arrayValue);
      for (const auto& v: *out_tlsServers) _log_arg_element["value"].append(Json::Value(v));
      _log_output_args.append(_log_arg_element);
      }
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "out_params";
      _log_arg_element["value"] = Json::Value(Json::arrayValue);
      for (const auto& v: *out_params) _log_arg_element["value"].append(Json::Value(v));
      _log_output_args.append(_log_arg_element);
      }
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "out_stats";
      _log_arg_element["value"] = Json::Value(Json::arrayValue);
      for (const auto& v: *out_stats) _log_arg_element["value"].append(Json::Value(v));
      _log_output_args.append(_log_arg_element);
      }
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "out_wait_for_pending_req_timeout_count";
      _log_arg_element["value"] = Json::Value(Json::arrayValue);
      for (const auto& v: *out_wait_for_pending_req_timeout_count) _log_arg_element["value"].append(Json::Value(v));
      _log_output_args.append(_log_arg_element);
      }
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::startPrefix64Discovery(int32_t in_netId) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "in_netId";
      _log_arg_element["value"] = Json::Value(in_netId);
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_writeInt32(_aidl_in.get(), in_netId);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 4 /*startPrefix64Discovery*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->startPrefix64Discovery(in_netId);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("startPrefix64Discovery");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::stopPrefix64Discovery(int32_t in_netId) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "in_netId";
      _log_arg_element["value"] = Json::Value(in_netId);
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_writeInt32(_aidl_in.get(), in_netId);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 5 /*stopPrefix64Discovery*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->stopPrefix64Discovery(in_netId);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("stopPrefix64Discovery");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::getPrefix64(int32_t in_netId, std::string* _aidl_return) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "in_netId";
      _log_arg_element["value"] = Json::Value(in_netId);
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_writeInt32(_aidl_in.get(), in_netId);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 6 /*getPrefix64*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->getPrefix64(in_netId, _aidl_return);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_ret_status = ::ndk::AParcel_readString(_aidl_out.get(), _aidl_return);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("getPrefix64");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    _log_transaction["_aidl_return"] = Json::Value(*_aidl_return);
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::createNetworkCache(int32_t in_netId) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "in_netId";
      _log_arg_element["value"] = Json::Value(in_netId);
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_writeInt32(_aidl_in.get(), in_netId);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 7 /*createNetworkCache*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->createNetworkCache(in_netId);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("createNetworkCache");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::destroyNetworkCache(int32_t in_netId) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "in_netId";
      _log_arg_element["value"] = Json::Value(in_netId);
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_writeInt32(_aidl_in.get(), in_netId);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 8 /*destroyNetworkCache*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->destroyNetworkCache(in_netId);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("destroyNetworkCache");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::setLogSeverity(int32_t in_logSeverity) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
    {
      Json::Value _log_arg_element(Json::objectValue);
      _log_arg_element["name"] = "in_logSeverity";
      _log_arg_element["value"] = Json::Value(in_logSeverity);
      _log_input_args.append(_log_arg_element);
      }
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_writeInt32(_aidl_in.get(), in_logSeverity);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 9 /*setLogSeverity*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->setLogSeverity(in_logSeverity);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("setLogSeverity");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
::ndk::ScopedAStatus BpDnsResolver::getInterfaceVersion(int32_t* _aidl_return) {
  binder_status_t _aidl_ret_status = STATUS_OK;
  ::ndk::ScopedAStatus _aidl_status;
  if (_aidl_cached_value != -1) {
    *_aidl_return = _aidl_cached_value;
    _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
    return _aidl_status;
  }
  ::ndk::ScopedAParcel _aidl_in;
  ::ndk::ScopedAParcel _aidl_out;

  Json::Value _log_input_args(Json::arrayValue);
  if (BpDnsResolver::logFunc != nullptr) {
  }
  auto _log_start = std::chrono::steady_clock::now();
  _aidl_ret_status = AIBinder_prepareTransaction(asBinder().get(), _aidl_in.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AIBinder_transact(
    asBinder().get(),
    (FIRST_CALL_TRANSACTION + 16777214 /*getInterfaceVersion*/),
    _aidl_in.getR(),
    _aidl_out.getR(),
    0);
  if (_aidl_ret_status == STATUS_UNKNOWN_TRANSACTION && IDnsResolver::getDefaultImpl()) {
    return IDnsResolver::getDefaultImpl()->getInterfaceVersion(_aidl_return);
  }
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_ret_status = AParcel_readStatusHeader(_aidl_out.get(), _aidl_status.getR());
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  if (!AStatus_isOk(_aidl_status.get())) return _aidl_status;

  _aidl_ret_status = AParcel_readInt32(_aidl_out.get(), _aidl_return);
  if (_aidl_ret_status != STATUS_OK) goto _aidl_error;

  _aidl_cached_value = *_aidl_return;
  _aidl_error:
  _aidl_status.set(AStatus_fromStatus(_aidl_ret_status));
  if (BpDnsResolver::logFunc != nullptr) {
    auto _log_end = std::chrono::steady_clock::now();
    Json::Value _log_transaction(Json::objectValue);
    _log_transaction["duration_ms"] = std::chrono::duration<double, std::milli>(_log_end - _log_start).count();
    _log_transaction["interface_name"] = Json::Value("android.net.IDnsResolver");
    _log_transaction["method_name"] = Json::Value("getInterfaceVersion");
    _log_transaction["proxy_address"] = Json::Value((std::ostringstream() << static_cast<const void*>(this)).str());
    _log_transaction["input_args"] = _log_input_args;
    Json::Value _log_output_args(Json::arrayValue);
    Json::Value _log_status(Json::objectValue);
    _log_status["exception_code"] = Json::Value(AStatus_getExceptionCode(_aidl_status.get()));
    _log_status["exception_message"] = Json::Value(AStatus_getMessage(_aidl_status.get()));
    _log_status["transaction_error"] = Json::Value(AStatus_getStatus(_aidl_status.get()));
    _log_status["service_specific_error_code"] = Json::Value(AStatus_getServiceSpecificError(_aidl_status.get()));
    _log_transaction["binder_status"] = _log_status;
    _log_transaction["output_args"] = _log_output_args;
    _log_transaction["_aidl_return"] = Json::Value(*_aidl_return);
    BpDnsResolver::logFunc(_log_transaction);
  }
  return _aidl_status;
}
// Source for BnDnsResolver
BnDnsResolver::BnDnsResolver() {}
BnDnsResolver::~BnDnsResolver() {}
std::function<void(const Json::Value&)> BnDnsResolver::logFunc;
::ndk::SpAIBinder BnDnsResolver::createBinder() {
  AIBinder* binder = AIBinder_new(_g_aidl_clazz, static_cast<void*>(this));
  return ::ndk::SpAIBinder(binder);
}
::ndk::ScopedAStatus BnDnsResolver::getInterfaceVersion(int32_t* _aidl_return) {
  *_aidl_return = IDnsResolver::version;
  return ::ndk::ScopedAStatus(AStatus_newOk());
}
// Source for IDnsResolver
const char* IDnsResolver::descriptor = "android.net.IDnsResolver";
IDnsResolver::IDnsResolver() {}
IDnsResolver::~IDnsResolver() {}


std::shared_ptr<IDnsResolver> IDnsResolver::fromBinder(const ::ndk::SpAIBinder& binder) {
  if (!AIBinder_associateClass(binder.get(), _g_aidl_clazz)) { return nullptr; }
  std::shared_ptr<::ndk::ICInterface> interface = ::ndk::ICInterface::asInterface(binder.get());
  if (interface) {
    return std::static_pointer_cast<IDnsResolver>(interface);
  }
  return (new BpDnsResolver(binder))->ref<IDnsResolver>();
}

binder_status_t IDnsResolver::writeToParcel(AParcel* parcel, const std::shared_ptr<IDnsResolver>& instance) {
  return AParcel_writeStrongBinder(parcel, instance ? instance->asBinder().get() : nullptr);
}
binder_status_t IDnsResolver::readFromParcel(const AParcel* parcel, std::shared_ptr<IDnsResolver>* instance) {
  ::ndk::SpAIBinder binder;
  binder_status_t status = AParcel_readStrongBinder(parcel, binder.getR());
  if (status != STATUS_OK) return status;
  *instance = IDnsResolver::fromBinder(binder);
  return STATUS_OK;
}
bool IDnsResolver::setDefaultImpl(std::shared_ptr<IDnsResolver> impl) {
  if (!IDnsResolver::default_impl && impl) {
    IDnsResolver::default_impl = impl;
    return true;
  }
  return false;
}
const std::shared_ptr<IDnsResolver>& IDnsResolver::getDefaultImpl() {
  return IDnsResolver::default_impl;
}
std::shared_ptr<IDnsResolver> IDnsResolver::default_impl = nullptr;
::ndk::ScopedAStatus IDnsResolverDefault::isAlive(bool* /*_aidl_return*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::registerEventListener(const std::shared_ptr<::aidl::android::net::metrics::INetdEventListener>& /*in_listener*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::setResolverConfiguration(const ::aidl::android::net::ResolverParamsParcel& /*in_resolverParams*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::getResolverInfo(int32_t /*in_netId*/, std::vector<std::string>* /*out_servers*/, std::vector<std::string>* /*out_domains*/, std::vector<std::string>* /*out_tlsServers*/, std::vector<int32_t>* /*out_params*/, std::vector<int32_t>* /*out_stats*/, std::vector<int32_t>* /*out_wait_for_pending_req_timeout_count*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::startPrefix64Discovery(int32_t /*in_netId*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::stopPrefix64Discovery(int32_t /*in_netId*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::getPrefix64(int32_t /*in_netId*/, std::string* /*_aidl_return*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::createNetworkCache(int32_t /*in_netId*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::destroyNetworkCache(int32_t /*in_netId*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::setLogSeverity(int32_t /*in_logSeverity*/) {
  ::ndk::ScopedAStatus _aidl_status;
  _aidl_status.set(AStatus_fromStatus(STATUS_UNKNOWN_TRANSACTION));
  return _aidl_status;
}
::ndk::ScopedAStatus IDnsResolverDefault::getInterfaceVersion(int32_t* _aidl_return) {
  *_aidl_return = 0;
  return ::ndk::ScopedAStatus(AStatus_newOk());
}
::ndk::SpAIBinder IDnsResolverDefault::asBinder() {
  return ::ndk::SpAIBinder();
}
bool IDnsResolverDefault::isRemote() {
  return false;
}
}  // namespace net
}  // namespace android
}  // namespace aidl
