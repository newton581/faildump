#pragma once
#include <android/binder_interface_utils.h>

#include <android/binder_parcel_utils.h>
namespace aidl {
namespace android {
namespace net {
class ResolverParamsParcel {
public:
  static const char* descriptor;

  int32_t netId;
  int32_t sampleValiditySeconds;
  int32_t successThreshold;
  int32_t minSamples;
  int32_t maxSamples;
  int32_t baseTimeoutMsec;
  int32_t retryCount;
  std::vector<std::string> servers;
  std::vector<std::string> domains;
  std::string tlsName;
  std::vector<std::string> tlsServers;
  std::vector<std::string> tlsFingerprints;

  binder_status_t readFromParcel(const AParcel* parcel);
  binder_status_t writeToParcel(AParcel* parcel) const;
};
}  // namespace net
}  // namespace android
}  // namespace aidl
