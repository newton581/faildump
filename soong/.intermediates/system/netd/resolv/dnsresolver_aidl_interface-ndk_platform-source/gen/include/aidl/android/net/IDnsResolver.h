#pragma once

#include <android/binder_interface_utils.h>
#include <json/value.h>
#include <functional>
#include <chrono>
#include <sstream>

#include <android/binder_parcel_utils.h>
#include <aidl/android/net/ResolverParamsParcel.h>
#include <aidl/android/net/metrics/INetdEventListener.h>

namespace aidl {
namespace android {
namespace net {
class IDnsResolver : public ::ndk::ICInterface {
public:
  static const char* descriptor;
  IDnsResolver();
  virtual ~IDnsResolver();


  enum : int32_t {
    RESOLVER_PARAMS_SAMPLE_VALIDITY = 0,
    RESOLVER_PARAMS_SUCCESS_THRESHOLD = 1,
    RESOLVER_PARAMS_MIN_SAMPLES = 2,
    RESOLVER_PARAMS_MAX_SAMPLES = 3,
    RESOLVER_PARAMS_BASE_TIMEOUT_MSEC = 4,
    RESOLVER_PARAMS_RETRY_COUNT = 5,
    RESOLVER_PARAMS_COUNT = 6,
    RESOLVER_STATS_SUCCESSES = 0,
    RESOLVER_STATS_ERRORS = 1,
    RESOLVER_STATS_TIMEOUTS = 2,
    RESOLVER_STATS_INTERNAL_ERRORS = 3,
    RESOLVER_STATS_RTT_AVG = 4,
    RESOLVER_STATS_LAST_SAMPLE_TIME = 5,
    RESOLVER_STATS_USABLE = 6,
    RESOLVER_STATS_COUNT = 7,
    DNS_RESOLVER_LOG_VERBOSE = 0,
    DNS_RESOLVER_LOG_DEBUG = 1,
    DNS_RESOLVER_LOG_INFO = 2,
    DNS_RESOLVER_LOG_WARNING = 3,
    DNS_RESOLVER_LOG_ERROR = 4,
  };
  static const int32_t version = 10000;

  static std::shared_ptr<IDnsResolver> fromBinder(const ::ndk::SpAIBinder& binder);
  static binder_status_t writeToParcel(AParcel* parcel, const std::shared_ptr<IDnsResolver>& instance);
  static binder_status_t readFromParcel(const AParcel* parcel, std::shared_ptr<IDnsResolver>* instance);
  static bool setDefaultImpl(std::shared_ptr<IDnsResolver> impl);
  static const std::shared_ptr<IDnsResolver>& getDefaultImpl();
  virtual ::ndk::ScopedAStatus isAlive(bool* _aidl_return) = 0;
  virtual ::ndk::ScopedAStatus registerEventListener(const std::shared_ptr<::aidl::android::net::metrics::INetdEventListener>& in_listener) = 0;
  virtual ::ndk::ScopedAStatus setResolverConfiguration(const ::aidl::android::net::ResolverParamsParcel& in_resolverParams) = 0;
  virtual ::ndk::ScopedAStatus getResolverInfo(int32_t in_netId, std::vector<std::string>* out_servers, std::vector<std::string>* out_domains, std::vector<std::string>* out_tlsServers, std::vector<int32_t>* out_params, std::vector<int32_t>* out_stats, std::vector<int32_t>* out_wait_for_pending_req_timeout_count) = 0;
  virtual ::ndk::ScopedAStatus startPrefix64Discovery(int32_t in_netId) = 0;
  virtual ::ndk::ScopedAStatus stopPrefix64Discovery(int32_t in_netId) = 0;
  virtual ::ndk::ScopedAStatus getPrefix64(int32_t in_netId, std::string* _aidl_return) = 0;
  virtual ::ndk::ScopedAStatus createNetworkCache(int32_t in_netId) = 0;
  virtual ::ndk::ScopedAStatus destroyNetworkCache(int32_t in_netId) = 0;
  virtual ::ndk::ScopedAStatus setLogSeverity(int32_t in_logSeverity) = 0;
  virtual ::ndk::ScopedAStatus getInterfaceVersion(int32_t* _aidl_return) = 0;
private:
  static std::shared_ptr<IDnsResolver> default_impl;
};
class IDnsResolverDefault : public IDnsResolver {
public:
  ::ndk::ScopedAStatus isAlive(bool* _aidl_return) override;
  ::ndk::ScopedAStatus registerEventListener(const std::shared_ptr<::aidl::android::net::metrics::INetdEventListener>& in_listener) override;
  ::ndk::ScopedAStatus setResolverConfiguration(const ::aidl::android::net::ResolverParamsParcel& in_resolverParams) override;
  ::ndk::ScopedAStatus getResolverInfo(int32_t in_netId, std::vector<std::string>* out_servers, std::vector<std::string>* out_domains, std::vector<std::string>* out_tlsServers, std::vector<int32_t>* out_params, std::vector<int32_t>* out_stats, std::vector<int32_t>* out_wait_for_pending_req_timeout_count) override;
  ::ndk::ScopedAStatus startPrefix64Discovery(int32_t in_netId) override;
  ::ndk::ScopedAStatus stopPrefix64Discovery(int32_t in_netId) override;
  ::ndk::ScopedAStatus getPrefix64(int32_t in_netId, std::string* _aidl_return) override;
  ::ndk::ScopedAStatus createNetworkCache(int32_t in_netId) override;
  ::ndk::ScopedAStatus destroyNetworkCache(int32_t in_netId) override;
  ::ndk::ScopedAStatus setLogSeverity(int32_t in_logSeverity) override;
  ::ndk::ScopedAStatus getInterfaceVersion(int32_t* _aidl_return) override;
  ::ndk::SpAIBinder asBinder() override;
  bool isRemote() override;
};
}  // namespace net
}  // namespace android
}  // namespace aidl
