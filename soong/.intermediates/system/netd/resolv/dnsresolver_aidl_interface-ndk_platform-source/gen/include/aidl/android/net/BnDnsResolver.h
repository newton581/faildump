#pragma once

#include "aidl/android/net/IDnsResolver.h"

#include <android/binder_ibinder.h>

namespace aidl {
namespace android {
namespace net {
class BnDnsResolver : public ::ndk::BnCInterface<IDnsResolver> {
public:
  BnDnsResolver();
  virtual ~BnDnsResolver();
  ::ndk::ScopedAStatus getInterfaceVersion(int32_t* _aidl_return) final override;
  static std::function<void(const Json::Value&)> logFunc;
protected:
  ::ndk::SpAIBinder createBinder() override;
private:
};
}  // namespace net
}  // namespace android
}  // namespace aidl
