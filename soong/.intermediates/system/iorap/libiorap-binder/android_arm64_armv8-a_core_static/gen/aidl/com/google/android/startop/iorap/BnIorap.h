#ifndef AIDL_GENERATED_COM_GOOGLE_ANDROID_STARTOP_IORAP_BN_IORAP_H_
#define AIDL_GENERATED_COM_GOOGLE_ANDROID_STARTOP_IORAP_BN_IORAP_H_

#include <binder/IInterface.h>
#include <com/google/android/startop/iorap/IIorap.h>

namespace com {

namespace google {

namespace android {

namespace startop {

namespace iorap {

class BnIorap : public ::android::BnInterface<IIorap> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnIorap

}  // namespace iorap

}  // namespace startop

}  // namespace android

}  // namespace google

}  // namespace com

#endif  // AIDL_GENERATED_COM_GOOGLE_ANDROID_STARTOP_IORAP_BN_IORAP_H_
