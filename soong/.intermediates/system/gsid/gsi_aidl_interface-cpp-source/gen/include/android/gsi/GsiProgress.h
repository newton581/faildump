#ifndef AIDL_GENERATED_ANDROID_GSI_GSI_PROGRESS_H_
#define AIDL_GENERATED_ANDROID_GSI_GSI_PROGRESS_H_

#include <binder/Parcel.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>

namespace android {

namespace gsi {

class GsiProgress : public ::android::Parcelable {
public:
  ::std::string step;
  int32_t status;
  int64_t bytes_processed;
  int64_t total_bytes;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class GsiProgress

}  // namespace gsi

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_GSI_GSI_PROGRESS_H_
