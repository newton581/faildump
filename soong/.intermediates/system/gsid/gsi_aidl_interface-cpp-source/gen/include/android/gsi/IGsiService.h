#ifndef AIDL_GENERATED_ANDROID_GSI_I_GSI_SERVICE_H_
#define AIDL_GENERATED_ANDROID_GSI_I_GSI_SERVICE_H_

#include <android/gsi/GsiInstallParams.h>
#include <android/gsi/GsiProgress.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/ParcelFileDescriptor.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace gsi {

class IGsiService : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(GsiService)
  enum  : int32_t {
    STATUS_NO_OPERATION = 0,
    STATUS_WORKING = 1,
    STATUS_COMPLETE = 2,
    INSTALL_OK = 0,
    INSTALL_ERROR_GENERIC = 1,
    INSTALL_ERROR_NO_SPACE = 2,
    INSTALL_ERROR_FILE_SYSTEM_CLUTTERED = 3,
    BOOT_STATUS_NOT_INSTALLED = 0,
    BOOT_STATUS_DISABLED = 1,
    BOOT_STATUS_SINGLE_BOOT = 2,
    BOOT_STATUS_ENABLED = 3,
    BOOT_STATUS_WILL_WIPE = 4,
  };
  virtual ::android::binder::Status startGsiInstall(int64_t gsiSize, int64_t userdataSize, bool wipeUserdata, int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status commitGsiChunkFromStream(const ::android::os::ParcelFileDescriptor& stream, int64_t bytes, bool* _aidl_return) = 0;
  virtual ::android::binder::Status getInstallProgress(::android::gsi::GsiProgress* _aidl_return) = 0;
  virtual ::android::binder::Status commitGsiChunkFromMemory(const ::std::vector<uint8_t>& bytes, bool* _aidl_return) = 0;
  virtual ::android::binder::Status setGsiBootable(bool oneShot, int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status isGsiEnabled(bool* _aidl_return) = 0;
  virtual ::android::binder::Status cancelGsiInstall(bool* _aidl_return) = 0;
  virtual ::android::binder::Status isGsiInstallInProgress(bool* _aidl_return) = 0;
  virtual ::android::binder::Status removeGsiInstall(bool* _aidl_return) = 0;
  virtual ::android::binder::Status disableGsiInstall(bool* _aidl_return) = 0;
  virtual ::android::binder::Status getUserdataImageSize(int64_t* _aidl_return) = 0;
  virtual ::android::binder::Status isGsiRunning(bool* _aidl_return) = 0;
  virtual ::android::binder::Status isGsiInstalled(bool* _aidl_return) = 0;
  virtual ::android::binder::Status getGsiBootStatus(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getInstalledGsiImageDir(::std::string* _aidl_return) = 0;
  virtual ::android::binder::Status beginGsiInstall(const ::android::gsi::GsiInstallParams& params, int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status wipeGsiUserdata(int32_t* _aidl_return) = 0;
};  // class IGsiService

class IGsiServiceDefault : public IGsiService {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status startGsiInstall(int64_t gsiSize, int64_t userdataSize, bool wipeUserdata, int32_t* _aidl_return) override;
  ::android::binder::Status commitGsiChunkFromStream(const ::android::os::ParcelFileDescriptor& stream, int64_t bytes, bool* _aidl_return) override;
  ::android::binder::Status getInstallProgress(::android::gsi::GsiProgress* _aidl_return) override;
  ::android::binder::Status commitGsiChunkFromMemory(const ::std::vector<uint8_t>& bytes, bool* _aidl_return) override;
  ::android::binder::Status setGsiBootable(bool oneShot, int32_t* _aidl_return) override;
  ::android::binder::Status isGsiEnabled(bool* _aidl_return) override;
  ::android::binder::Status cancelGsiInstall(bool* _aidl_return) override;
  ::android::binder::Status isGsiInstallInProgress(bool* _aidl_return) override;
  ::android::binder::Status removeGsiInstall(bool* _aidl_return) override;
  ::android::binder::Status disableGsiInstall(bool* _aidl_return) override;
  ::android::binder::Status getUserdataImageSize(int64_t* _aidl_return) override;
  ::android::binder::Status isGsiRunning(bool* _aidl_return) override;
  ::android::binder::Status isGsiInstalled(bool* _aidl_return) override;
  ::android::binder::Status getGsiBootStatus(int32_t* _aidl_return) override;
  ::android::binder::Status getInstalledGsiImageDir(::std::string* _aidl_return) override;
  ::android::binder::Status beginGsiInstall(const ::android::gsi::GsiInstallParams& params, int32_t* _aidl_return) override;
  ::android::binder::Status wipeGsiUserdata(int32_t* _aidl_return) override;

};

}  // namespace gsi

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_GSI_I_GSI_SERVICE_H_
