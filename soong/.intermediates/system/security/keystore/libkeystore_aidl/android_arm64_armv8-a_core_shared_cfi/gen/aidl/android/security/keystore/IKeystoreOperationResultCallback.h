#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_OPERATION_RESULT_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_OPERATION_RESULT_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <keystore/OperationResult.h>
#include <utils/StrongPointer.h>

namespace android {

namespace security {

namespace keystore {

class IKeystoreOperationResultCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(KeystoreOperationResultCallback)
  virtual ::android::binder::Status onFinished(const ::android::security::keymaster::OperationResult& result) = 0;
};  // class IKeystoreOperationResultCallback

class IKeystoreOperationResultCallbackDefault : public IKeystoreOperationResultCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onFinished(const ::android::security::keymaster::OperationResult& result) override;

};

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_OPERATION_RESULT_CALLBACK_H_
