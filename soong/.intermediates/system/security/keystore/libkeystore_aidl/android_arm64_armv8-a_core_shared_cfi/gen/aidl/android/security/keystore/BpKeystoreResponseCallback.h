#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_RESPONSE_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_RESPONSE_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/security/keystore/IKeystoreResponseCallback.h>

namespace android {

namespace security {

namespace keystore {

class BpKeystoreResponseCallback : public ::android::BpInterface<IKeystoreResponseCallback> {
public:
  explicit BpKeystoreResponseCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpKeystoreResponseCallback() = default;
  ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response) override;
};  // class BpKeystoreResponseCallback

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_RESPONSE_CALLBACK_H_
