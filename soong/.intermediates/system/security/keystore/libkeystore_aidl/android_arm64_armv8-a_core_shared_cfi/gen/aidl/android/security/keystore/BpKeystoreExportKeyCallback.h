#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_EXPORT_KEY_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_EXPORT_KEY_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/security/keystore/IKeystoreExportKeyCallback.h>

namespace android {

namespace security {

namespace keystore {

class BpKeystoreExportKeyCallback : public ::android::BpInterface<IKeystoreExportKeyCallback> {
public:
  explicit BpKeystoreExportKeyCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpKeystoreExportKeyCallback() = default;
  ::android::binder::Status onFinished(const ::android::security::keymaster::ExportResult& result) override;
};  // class BpKeystoreExportKeyCallback

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_EXPORT_KEY_CALLBACK_H_
