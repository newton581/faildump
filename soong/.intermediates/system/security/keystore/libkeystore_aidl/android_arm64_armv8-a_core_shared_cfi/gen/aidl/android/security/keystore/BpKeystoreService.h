#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_SERVICE_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/security/keystore/IKeystoreService.h>

namespace android {

namespace security {

namespace keystore {

class BpKeystoreService : public ::android::BpInterface<IKeystoreService> {
public:
  explicit BpKeystoreService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpKeystoreService() = default;
  ::android::binder::Status getState(int32_t userId, int32_t* _aidl_return) override;
  ::android::binder::Status get(const ::android::String16& name, int32_t uid, ::std::vector<uint8_t>* _aidl_return) override;
  ::android::binder::Status insert(const ::android::String16& name, const ::std::vector<uint8_t>& item, int32_t uid, int32_t flags, int32_t* _aidl_return) override;
  ::android::binder::Status del(const ::android::String16& name, int32_t uid, int32_t* _aidl_return) override;
  ::android::binder::Status exist(const ::android::String16& name, int32_t uid, int32_t* _aidl_return) override;
  ::android::binder::Status list(const ::android::String16& namePrefix, int32_t uid, ::std::vector<::android::String16>* _aidl_return) override;
  ::android::binder::Status reset(int32_t* _aidl_return) override;
  ::android::binder::Status onUserPasswordChanged(int32_t userId, const ::android::String16& newPassword, int32_t* _aidl_return) override;
  ::android::binder::Status lock(int32_t userId, int32_t* _aidl_return) override;
  ::android::binder::Status unlock(int32_t userId, const ::android::String16& userPassword, int32_t* _aidl_return) override;
  ::android::binder::Status isEmpty(int32_t userId, int32_t* _aidl_return) override;
  ::android::binder::Status grant(const ::android::String16& name, int32_t granteeUid, ::android::String16* _aidl_return) override;
  ::android::binder::Status ungrant(const ::android::String16& name, int32_t granteeUid, int32_t* _aidl_return) override;
  ::android::binder::Status getmtime(const ::android::String16& name, int32_t uid, int64_t* _aidl_return) override;
  ::android::binder::Status is_hardware_backed(const ::android::String16& string, int32_t* _aidl_return) override;
  ::android::binder::Status clear_uid(int64_t uid, int32_t* _aidl_return) override;
  ::android::binder::Status addRngEntropy(const ::android::sp<::android::security::keystore::IKeystoreResponseCallback>& cb, const ::std::vector<uint8_t>& data, int32_t flags, int32_t* _aidl_return) override;
  ::android::binder::Status generateKey(const ::android::sp<::android::security::keystore::IKeystoreKeyCharacteristicsCallback>& cb, const ::android::String16& alias, const ::android::security::keymaster::KeymasterArguments& arguments, const ::std::vector<uint8_t>& entropy, int32_t uid, int32_t flags, int32_t* _aidl_return) override;
  ::android::binder::Status getKeyCharacteristics(const ::android::sp<::android::security::keystore::IKeystoreKeyCharacteristicsCallback>& cb, const ::android::String16& alias, const ::android::security::keymaster::KeymasterBlob& clientId, const ::android::security::keymaster::KeymasterBlob& appData, int32_t uid, int32_t* _aidl_return) override;
  ::android::binder::Status importKey(const ::android::sp<::android::security::keystore::IKeystoreKeyCharacteristicsCallback>& cb, const ::android::String16& alias, const ::android::security::keymaster::KeymasterArguments& arguments, int32_t format, const ::std::vector<uint8_t>& keyData, int32_t uid, int32_t flags, int32_t* _aidl_return) override;
  ::android::binder::Status exportKey(const ::android::sp<::android::security::keystore::IKeystoreExportKeyCallback>& cb, const ::android::String16& alias, int32_t format, const ::android::security::keymaster::KeymasterBlob& clientId, const ::android::security::keymaster::KeymasterBlob& appData, int32_t uid, int32_t* _aidl_return) override;
  ::android::binder::Status begin(const ::android::sp<::android::security::keystore::IKeystoreOperationResultCallback>& cb, const ::android::sp<::android::IBinder>& appToken, const ::android::String16& alias, int32_t purpose, bool pruneable, const ::android::security::keymaster::KeymasterArguments& params, const ::std::vector<uint8_t>& entropy, int32_t uid, int32_t* _aidl_return) override;
  ::android::binder::Status update(const ::android::sp<::android::security::keystore::IKeystoreOperationResultCallback>& cb, const ::android::sp<::android::IBinder>& token, const ::android::security::keymaster::KeymasterArguments& params, const ::std::vector<uint8_t>& input, int32_t* _aidl_return) override;
  ::android::binder::Status finish(const ::android::sp<::android::security::keystore::IKeystoreOperationResultCallback>& cb, const ::android::sp<::android::IBinder>& token, const ::android::security::keymaster::KeymasterArguments& params, const ::std::vector<uint8_t>& signature, const ::std::vector<uint8_t>& entropy, int32_t* _aidl_return) override;
  ::android::binder::Status abort(const ::android::sp<::android::security::keystore::IKeystoreResponseCallback>& cb, const ::android::sp<::android::IBinder>& token, int32_t* _aidl_return) override;
  ::android::binder::Status addAuthToken(const ::std::vector<uint8_t>& authToken, int32_t* _aidl_return) override;
  ::android::binder::Status onUserAdded(int32_t userId, int32_t parentId, int32_t* _aidl_return) override;
  ::android::binder::Status onUserRemoved(int32_t userId, int32_t* _aidl_return) override;
  ::android::binder::Status attestKey(const ::android::sp<::android::security::keystore::IKeystoreCertificateChainCallback>& cb, const ::android::String16& alias, const ::android::security::keymaster::KeymasterArguments& params, int32_t* _aidl_return) override;
  ::android::binder::Status attestDeviceIds(const ::android::sp<::android::security::keystore::IKeystoreCertificateChainCallback>& cb, const ::android::security::keymaster::KeymasterArguments& params, int32_t* _aidl_return) override;
  ::android::binder::Status onDeviceOffBody(int32_t* _aidl_return) override;
  ::android::binder::Status importWrappedKey(const ::android::sp<::android::security::keystore::IKeystoreKeyCharacteristicsCallback>& cb, const ::android::String16& wrappedKeyAlias, const ::std::vector<uint8_t>& wrappedKey, const ::android::String16& wrappingKeyAlias, const ::std::vector<uint8_t>& maskingKey, const ::android::security::keymaster::KeymasterArguments& arguments, int64_t rootSid, int64_t fingerprintSid, int32_t* _aidl_return) override;
  ::android::binder::Status presentConfirmationPrompt(const ::android::sp<::android::IBinder>& listener, const ::android::String16& promptText, const ::std::vector<uint8_t>& extraData, const ::android::String16& locale, int32_t uiOptionsAsFlags, int32_t* _aidl_return) override;
  ::android::binder::Status cancelConfirmationPrompt(const ::android::sp<::android::IBinder>& listener, int32_t* _aidl_return) override;
  ::android::binder::Status isConfirmationPromptSupported(bool* _aidl_return) override;
  ::android::binder::Status onKeyguardVisibilityChanged(bool isShowing, int32_t userId, int32_t* _aidl_return) override;
  ::android::binder::Status listUidsOfAuthBoundKeys(::std::vector<::std::string>* uids, int32_t* _aidl_return) override;
};  // class BpKeystoreService

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_SERVICE_H_
