#ifndef AIDL_GENERATED_ANDROID_SECURITY_I_CONFIRMATION_PROMPT_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_I_CONFIRMATION_PROMPT_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace security {

class IConfirmationPromptCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(ConfirmationPromptCallback)
  virtual ::android::binder::Status onConfirmationPromptCompleted(int32_t result, const ::std::vector<uint8_t>& dataThatWasConfirmed) = 0;
};  // class IConfirmationPromptCallback

class IConfirmationPromptCallbackDefault : public IConfirmationPromptCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onConfirmationPromptCompleted(int32_t result, const ::std::vector<uint8_t>& dataThatWasConfirmed) override;

};

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_I_CONFIRMATION_PROMPT_CALLBACK_H_
