#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYMASTER_BN_KEY_ATTESTATION_APPLICATION_ID_PROVIDER_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYMASTER_BN_KEY_ATTESTATION_APPLICATION_ID_PROVIDER_H_

#include <binder/IInterface.h>
#include <android/security/keymaster/IKeyAttestationApplicationIdProvider.h>

namespace android {

namespace security {

namespace keymaster {

class BnKeyAttestationApplicationIdProvider : public ::android::BnInterface<IKeyAttestationApplicationIdProvider> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnKeyAttestationApplicationIdProvider

}  // namespace keymaster

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYMASTER_BN_KEY_ATTESTATION_APPLICATION_ID_PROVIDER_H_
