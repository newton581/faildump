#ifndef AIDL_GENERATED_ANDROID_ASHMEMD_I_ASHMEM_DEVICE_SERVICE_H_
#define AIDL_GENERATED_ANDROID_ASHMEMD_I_ASHMEM_DEVICE_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/ParcelFileDescriptor.h>
#include <binder/Status.h>
#include <utils/StrongPointer.h>

namespace android {

namespace ashmemd {

class IAshmemDeviceService : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(AshmemDeviceService)
  virtual ::android::binder::Status open(::android::os::ParcelFileDescriptor* _aidl_return) = 0;
};  // class IAshmemDeviceService

class IAshmemDeviceServiceDefault : public IAshmemDeviceService {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status open(::android::os::ParcelFileDescriptor* _aidl_return) override;

};

}  // namespace ashmemd

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_ASHMEMD_I_ASHMEM_DEVICE_SERVICE_H_
