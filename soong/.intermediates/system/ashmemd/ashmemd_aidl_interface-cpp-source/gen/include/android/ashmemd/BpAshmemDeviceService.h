#ifndef AIDL_GENERATED_ANDROID_ASHMEMD_BP_ASHMEM_DEVICE_SERVICE_H_
#define AIDL_GENERATED_ANDROID_ASHMEMD_BP_ASHMEM_DEVICE_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/ashmemd/IAshmemDeviceService.h>

namespace android {

namespace ashmemd {

class BpAshmemDeviceService : public ::android::BpInterface<IAshmemDeviceService> {
public:
  explicit BpAshmemDeviceService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpAshmemDeviceService() = default;
  ::android::binder::Status open(::android::os::ParcelFileDescriptor* _aidl_return) override;
};  // class BpAshmemDeviceService

}  // namespace ashmemd

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_ASHMEMD_BP_ASHMEM_DEVICE_SERVICE_H_
