#ifndef AIDL_GENERATED_ANDROID_OS_STORAGED_I_STORAGED_PRIVATE_H_
#define AIDL_GENERATED_ANDROID_OS_STORAGED_I_STORAGED_PRIVATE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <include/uid_info.h>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace os {

namespace storaged {

class IStoragedPrivate : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(StoragedPrivate)
  virtual ::android::binder::Status dumpUids(::std::vector<::android::os::storaged::UidInfo>* _aidl_return) = 0;
  virtual ::android::binder::Status dumpPerfHistory(::std::vector<int32_t>* _aidl_return) = 0;
};  // class IStoragedPrivate

class IStoragedPrivateDefault : public IStoragedPrivate {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status dumpUids(::std::vector<::android::os::storaged::UidInfo>* _aidl_return) override;
  ::android::binder::Status dumpPerfHistory(::std::vector<int32_t>* _aidl_return) override;

};

}  // namespace storaged

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_STORAGED_I_STORAGED_PRIVATE_H_
