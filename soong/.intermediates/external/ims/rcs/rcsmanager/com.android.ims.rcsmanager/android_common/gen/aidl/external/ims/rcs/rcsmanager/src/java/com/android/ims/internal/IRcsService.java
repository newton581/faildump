/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.android.ims.internal;
/**
 * @hide
 */
public interface IRcsService extends android.os.IInterface
{
  /** Default implementation for IRcsService. */
  public static class Default implements com.android.ims.internal.IRcsService
  {
    /**
         * Detect if the rcs service is available.
         */
    @Override public boolean isRcsServiceAvailable() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * RcsPresence interface to get the presence information.
         *
         * @see IRcsPresence
         */
    @Override public com.android.ims.internal.IRcsPresence getRcsPresenceInterface() throws android.os.RemoteException
    {
      return null;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.android.ims.internal.IRcsService
  {
    private static final java.lang.String DESCRIPTOR = "com.android.ims.internal.IRcsService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.android.ims.internal.IRcsService interface,
     * generating a proxy if needed.
     */
    public static com.android.ims.internal.IRcsService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.android.ims.internal.IRcsService))) {
        return ((com.android.ims.internal.IRcsService)iin);
      }
      return new com.android.ims.internal.IRcsService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_isRcsServiceAvailable:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isRcsServiceAvailable();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getRcsPresenceInterface:
        {
          data.enforceInterface(descriptor);
          com.android.ims.internal.IRcsPresence _result = this.getRcsPresenceInterface();
          reply.writeNoException();
          reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.android.ims.internal.IRcsService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Detect if the rcs service is available.
           */
      @Override public boolean isRcsServiceAvailable() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isRcsServiceAvailable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isRcsServiceAvailable();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * RcsPresence interface to get the presence information.
           *
           * @see IRcsPresence
           */
      @Override public com.android.ims.internal.IRcsPresence getRcsPresenceInterface() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        com.android.ims.internal.IRcsPresence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRcsPresenceInterface, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRcsPresenceInterface();
          }
          _reply.readException();
          _result = com.android.ims.internal.IRcsPresence.Stub.asInterface(_reply.readStrongBinder());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static com.android.ims.internal.IRcsService sDefaultImpl;
    }
    static final int TRANSACTION_isRcsServiceAvailable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getRcsPresenceInterface = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(com.android.ims.internal.IRcsService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.android.ims.internal.IRcsService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * Detect if the rcs service is available.
       */
  public boolean isRcsServiceAvailable() throws android.os.RemoteException;
  /**
       * RcsPresence interface to get the presence information.
       *
       * @see IRcsPresence
       */
  public com.android.ims.internal.IRcsPresence getRcsPresenceInterface() throws android.os.RemoteException;
}
