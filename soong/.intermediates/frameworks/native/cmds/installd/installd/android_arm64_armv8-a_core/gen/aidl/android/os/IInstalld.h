#ifndef AIDL_GENERATED_ANDROID_OS_I_INSTALLD_H_
#define AIDL_GENERATED_ANDROID_OS_I_INSTALLD_H_

#include <android-base/unique_fd.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <memory>
#include <string>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace os {

class IInstalld : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(Installd)
  enum  : int32_t {
    FLAG_STORAGE_DE = 1,
    FLAG_STORAGE_CE = 2,
    FLAG_STORAGE_EXTERNAL = 4,
    FLAG_CLEAR_CACHE_ONLY = 16,
    FLAG_CLEAR_CODE_CACHE_ONLY = 32,
    FLAG_FREE_CACHE_V2 = 256,
    FLAG_FREE_CACHE_V2_DEFY_QUOTA = 512,
    FLAG_FREE_CACHE_NOOP = 1024,
    FLAG_USE_QUOTA = 4096,
    FLAG_FORCE = 8192,
    FLAG_CLEAR_APP_DATA_KEEP_ART_PROFILES = 131072,
  };
  virtual ::android::binder::Status createUserData(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t userSerial, int32_t flags) = 0;
  virtual ::android::binder::Status destroyUserData(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t flags) = 0;
  virtual ::android::binder::Status createAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int32_t appId, const ::std::string& seInfo, int32_t targetSdkVersion, int64_t* _aidl_return) = 0;
  virtual ::android::binder::Status restoreconAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int32_t appId, const ::std::string& seInfo) = 0;
  virtual ::android::binder::Status migrateAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags) = 0;
  virtual ::android::binder::Status clearAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int64_t ceDataInode) = 0;
  virtual ::android::binder::Status destroyAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int64_t ceDataInode) = 0;
  virtual ::android::binder::Status fixupAppData(const ::std::unique_ptr<::std::string>& uuid, int32_t flags) = 0;
  virtual ::android::binder::Status getAppSize(const ::std::unique_ptr<::std::string>& uuid, const ::std::vector<::std::string>& packageNames, int32_t userId, int32_t flags, int32_t appId, const ::std::vector<int64_t>& ceDataInodes, const ::std::vector<::std::string>& codePaths, ::std::vector<int64_t>* _aidl_return) = 0;
  virtual ::android::binder::Status getUserSize(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t flags, const ::std::vector<int32_t>& appIds, ::std::vector<int64_t>* _aidl_return) = 0;
  virtual ::android::binder::Status getExternalSize(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t flags, const ::std::vector<int32_t>& appIds, ::std::vector<int64_t>* _aidl_return) = 0;
  virtual ::android::binder::Status setAppQuota(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t appId, int64_t cacheQuota) = 0;
  virtual ::android::binder::Status moveCompleteApp(const ::std::unique_ptr<::std::string>& fromUuid, const ::std::unique_ptr<::std::string>& toUuid, const ::std::string& packageName, const ::std::string& dataAppName, int32_t appId, const ::std::string& seInfo, int32_t targetSdkVersion) = 0;
  virtual ::android::binder::Status dexopt(const ::std::string& apkPath, int32_t uid, const ::std::unique_ptr<::std::string>& packageName, const ::std::string& instructionSet, int32_t dexoptNeeded, const ::std::unique_ptr<::std::string>& outputPath, int32_t dexFlags, const ::std::string& compilerFilter, const ::std::unique_ptr<::std::string>& uuid, const ::std::unique_ptr<::std::string>& sharedLibraries, const ::std::unique_ptr<::std::string>& seInfo, bool downgrade, int32_t targetSdkVersion, const ::std::unique_ptr<::std::string>& profileName, const ::std::unique_ptr<::std::string>& dexMetadataPath, const ::std::unique_ptr<::std::string>& compilationReason) = 0;
  virtual ::android::binder::Status compileLayouts(const ::std::string& apkPath, const ::std::string& packageName, const ::std::string& outDexFile, int32_t uid, bool* _aidl_return) = 0;
  virtual ::android::binder::Status rmdex(const ::std::string& codePath, const ::std::string& instructionSet) = 0;
  virtual ::android::binder::Status mergeProfiles(int32_t uid, const ::std::string& packageName, const ::std::string& profileName, bool* _aidl_return) = 0;
  virtual ::android::binder::Status dumpProfiles(int32_t uid, const ::std::string& packageName, const ::std::string& profileName, const ::std::string& codePath, bool* _aidl_return) = 0;
  virtual ::android::binder::Status copySystemProfile(const ::std::string& systemProfile, int32_t uid, const ::std::string& packageName, const ::std::string& profileName, bool* _aidl_return) = 0;
  virtual ::android::binder::Status clearAppProfiles(const ::std::string& packageName, const ::std::string& profileName) = 0;
  virtual ::android::binder::Status destroyAppProfiles(const ::std::string& packageName) = 0;
  virtual ::android::binder::Status createProfileSnapshot(int32_t appId, const ::std::string& packageName, const ::std::string& profileName, const ::std::string& classpath, bool* _aidl_return) = 0;
  virtual ::android::binder::Status destroyProfileSnapshot(const ::std::string& packageName, const ::std::string& profileName) = 0;
  virtual ::android::binder::Status idmap(const ::std::string& targetApkPath, const ::std::string& overlayApkPath, int32_t uid) = 0;
  virtual ::android::binder::Status removeIdmap(const ::std::string& overlayApkPath) = 0;
  virtual ::android::binder::Status rmPackageDir(const ::std::string& packageDir) = 0;
  virtual ::android::binder::Status freeCache(const ::std::unique_ptr<::std::string>& uuid, int64_t targetFreeBytes, int64_t cacheReservedBytes, int32_t flags) = 0;
  virtual ::android::binder::Status linkNativeLibraryDirectory(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, const ::std::string& nativeLibPath32, int32_t userId) = 0;
  virtual ::android::binder::Status createOatDir(const ::std::string& oatDir, const ::std::string& instructionSet) = 0;
  virtual ::android::binder::Status linkFile(const ::std::string& relativePath, const ::std::string& fromBase, const ::std::string& toBase) = 0;
  virtual ::android::binder::Status moveAb(const ::std::string& apkPath, const ::std::string& instructionSet, const ::std::string& outputPath) = 0;
  virtual ::android::binder::Status deleteOdex(const ::std::string& apkPath, const ::std::string& instructionSet, const ::std::unique_ptr<::std::string>& outputPath) = 0;
  virtual ::android::binder::Status installApkVerity(const ::std::string& filePath, const ::android::base::unique_fd& verityInput, int32_t contentSize) = 0;
  virtual ::android::binder::Status assertFsverityRootHashMatches(const ::std::string& filePath, const ::std::vector<uint8_t>& expectedHash) = 0;
  virtual ::android::binder::Status reconcileSecondaryDexFile(const ::std::string& dexPath, const ::std::string& pkgName, int32_t uid, const ::std::vector<::std::string>& isas, const ::std::unique_ptr<::std::string>& volume_uuid, int32_t storage_flag, bool* _aidl_return) = 0;
  virtual ::android::binder::Status hashSecondaryDexFile(const ::std::string& dexPath, const ::std::string& pkgName, int32_t uid, const ::std::unique_ptr<::std::string>& volumeUuid, int32_t storageFlag, ::std::vector<uint8_t>* _aidl_return) = 0;
  virtual ::android::binder::Status invalidateMounts() = 0;
  virtual ::android::binder::Status isQuotaSupported(const ::std::unique_ptr<::std::string>& uuid, bool* _aidl_return) = 0;
  virtual ::android::binder::Status prepareAppProfile(const ::std::string& packageName, int32_t userId, int32_t appId, const ::std::string& profileName, const ::std::string& codePath, const ::std::unique_ptr<::std::string>& dexMetadata, bool* _aidl_return) = 0;
  virtual ::android::binder::Status snapshotAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t snapshotId, int32_t storageFlags, int64_t* _aidl_return) = 0;
  virtual ::android::binder::Status restoreAppDataSnapshot(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t appId, const ::std::string& seInfo, int32_t user, int32_t snapshotId, int32_t storageflags) = 0;
  virtual ::android::binder::Status destroyAppDataSnapshot(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int64_t ceSnapshotInode, int32_t snapshotId, int32_t storageFlags) = 0;
  virtual ::android::binder::Status migrateLegacyObbData() = 0;
};  // class IInstalld

class IInstalldDefault : public IInstalld {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status createUserData(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t userSerial, int32_t flags) override;
  ::android::binder::Status destroyUserData(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t flags) override;
  ::android::binder::Status createAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int32_t appId, const ::std::string& seInfo, int32_t targetSdkVersion, int64_t* _aidl_return) override;
  ::android::binder::Status restoreconAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int32_t appId, const ::std::string& seInfo) override;
  ::android::binder::Status migrateAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags) override;
  ::android::binder::Status clearAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int64_t ceDataInode) override;
  ::android::binder::Status destroyAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t flags, int64_t ceDataInode) override;
  ::android::binder::Status fixupAppData(const ::std::unique_ptr<::std::string>& uuid, int32_t flags) override;
  ::android::binder::Status getAppSize(const ::std::unique_ptr<::std::string>& uuid, const ::std::vector<::std::string>& packageNames, int32_t userId, int32_t flags, int32_t appId, const ::std::vector<int64_t>& ceDataInodes, const ::std::vector<::std::string>& codePaths, ::std::vector<int64_t>* _aidl_return) override;
  ::android::binder::Status getUserSize(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t flags, const ::std::vector<int32_t>& appIds, ::std::vector<int64_t>* _aidl_return) override;
  ::android::binder::Status getExternalSize(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t flags, const ::std::vector<int32_t>& appIds, ::std::vector<int64_t>* _aidl_return) override;
  ::android::binder::Status setAppQuota(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t appId, int64_t cacheQuota) override;
  ::android::binder::Status moveCompleteApp(const ::std::unique_ptr<::std::string>& fromUuid, const ::std::unique_ptr<::std::string>& toUuid, const ::std::string& packageName, const ::std::string& dataAppName, int32_t appId, const ::std::string& seInfo, int32_t targetSdkVersion) override;
  ::android::binder::Status dexopt(const ::std::string& apkPath, int32_t uid, const ::std::unique_ptr<::std::string>& packageName, const ::std::string& instructionSet, int32_t dexoptNeeded, const ::std::unique_ptr<::std::string>& outputPath, int32_t dexFlags, const ::std::string& compilerFilter, const ::std::unique_ptr<::std::string>& uuid, const ::std::unique_ptr<::std::string>& sharedLibraries, const ::std::unique_ptr<::std::string>& seInfo, bool downgrade, int32_t targetSdkVersion, const ::std::unique_ptr<::std::string>& profileName, const ::std::unique_ptr<::std::string>& dexMetadataPath, const ::std::unique_ptr<::std::string>& compilationReason) override;
  ::android::binder::Status compileLayouts(const ::std::string& apkPath, const ::std::string& packageName, const ::std::string& outDexFile, int32_t uid, bool* _aidl_return) override;
  ::android::binder::Status rmdex(const ::std::string& codePath, const ::std::string& instructionSet) override;
  ::android::binder::Status mergeProfiles(int32_t uid, const ::std::string& packageName, const ::std::string& profileName, bool* _aidl_return) override;
  ::android::binder::Status dumpProfiles(int32_t uid, const ::std::string& packageName, const ::std::string& profileName, const ::std::string& codePath, bool* _aidl_return) override;
  ::android::binder::Status copySystemProfile(const ::std::string& systemProfile, int32_t uid, const ::std::string& packageName, const ::std::string& profileName, bool* _aidl_return) override;
  ::android::binder::Status clearAppProfiles(const ::std::string& packageName, const ::std::string& profileName) override;
  ::android::binder::Status destroyAppProfiles(const ::std::string& packageName) override;
  ::android::binder::Status createProfileSnapshot(int32_t appId, const ::std::string& packageName, const ::std::string& profileName, const ::std::string& classpath, bool* _aidl_return) override;
  ::android::binder::Status destroyProfileSnapshot(const ::std::string& packageName, const ::std::string& profileName) override;
  ::android::binder::Status idmap(const ::std::string& targetApkPath, const ::std::string& overlayApkPath, int32_t uid) override;
  ::android::binder::Status removeIdmap(const ::std::string& overlayApkPath) override;
  ::android::binder::Status rmPackageDir(const ::std::string& packageDir) override;
  ::android::binder::Status freeCache(const ::std::unique_ptr<::std::string>& uuid, int64_t targetFreeBytes, int64_t cacheReservedBytes, int32_t flags) override;
  ::android::binder::Status linkNativeLibraryDirectory(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, const ::std::string& nativeLibPath32, int32_t userId) override;
  ::android::binder::Status createOatDir(const ::std::string& oatDir, const ::std::string& instructionSet) override;
  ::android::binder::Status linkFile(const ::std::string& relativePath, const ::std::string& fromBase, const ::std::string& toBase) override;
  ::android::binder::Status moveAb(const ::std::string& apkPath, const ::std::string& instructionSet, const ::std::string& outputPath) override;
  ::android::binder::Status deleteOdex(const ::std::string& apkPath, const ::std::string& instructionSet, const ::std::unique_ptr<::std::string>& outputPath) override;
  ::android::binder::Status installApkVerity(const ::std::string& filePath, const ::android::base::unique_fd& verityInput, int32_t contentSize) override;
  ::android::binder::Status assertFsverityRootHashMatches(const ::std::string& filePath, const ::std::vector<uint8_t>& expectedHash) override;
  ::android::binder::Status reconcileSecondaryDexFile(const ::std::string& dexPath, const ::std::string& pkgName, int32_t uid, const ::std::vector<::std::string>& isas, const ::std::unique_ptr<::std::string>& volume_uuid, int32_t storage_flag, bool* _aidl_return) override;
  ::android::binder::Status hashSecondaryDexFile(const ::std::string& dexPath, const ::std::string& pkgName, int32_t uid, const ::std::unique_ptr<::std::string>& volumeUuid, int32_t storageFlag, ::std::vector<uint8_t>* _aidl_return) override;
  ::android::binder::Status invalidateMounts() override;
  ::android::binder::Status isQuotaSupported(const ::std::unique_ptr<::std::string>& uuid, bool* _aidl_return) override;
  ::android::binder::Status prepareAppProfile(const ::std::string& packageName, int32_t userId, int32_t appId, const ::std::string& profileName, const ::std::string& codePath, const ::std::unique_ptr<::std::string>& dexMetadata, bool* _aidl_return) override;
  ::android::binder::Status snapshotAppData(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int32_t snapshotId, int32_t storageFlags, int64_t* _aidl_return) override;
  ::android::binder::Status restoreAppDataSnapshot(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t appId, const ::std::string& seInfo, int32_t user, int32_t snapshotId, int32_t storageflags) override;
  ::android::binder::Status destroyAppDataSnapshot(const ::std::unique_ptr<::std::string>& uuid, const ::std::string& packageName, int32_t userId, int64_t ceSnapshotInode, int32_t snapshotId, int32_t storageFlags) override;
  ::android::binder::Status migrateLegacyObbData() override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_INSTALLD_H_
