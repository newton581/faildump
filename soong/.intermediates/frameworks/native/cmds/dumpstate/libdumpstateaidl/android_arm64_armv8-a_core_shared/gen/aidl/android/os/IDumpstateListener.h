#ifndef AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_LISTENER_H_
#define AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IDumpstateListener : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(DumpstateListener)
  enum  : int32_t {
    BUGREPORT_ERROR_INVALID_INPUT = 1,
    BUGREPORT_ERROR_RUNTIME_ERROR = 2,
    BUGREPORT_ERROR_USER_DENIED_CONSENT = 3,
    BUGREPORT_ERROR_USER_CONSENT_TIMED_OUT = 4,
    BUGREPORT_ERROR_ANOTHER_REPORT_IN_PROGRESS = 5,
  };
  virtual ::android::binder::Status onProgress(int32_t progress) = 0;
  virtual ::android::binder::Status onError(int32_t errorCode) = 0;
  virtual ::android::binder::Status onFinished() = 0;
  virtual ::android::binder::Status onProgressUpdated(int32_t progress) = 0;
  virtual ::android::binder::Status onMaxProgressUpdated(int32_t maxProgress) = 0;
  virtual ::android::binder::Status onSectionComplete(const ::std::string& name, int32_t status, int32_t size, int32_t durationMs) = 0;
};  // class IDumpstateListener

class IDumpstateListenerDefault : public IDumpstateListener {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onProgress(int32_t progress) override;
  ::android::binder::Status onError(int32_t errorCode) override;
  ::android::binder::Status onFinished() override;
  ::android::binder::Status onProgressUpdated(int32_t progress) override;
  ::android::binder::Status onMaxProgressUpdated(int32_t maxProgress) override;
  ::android::binder::Status onSectionComplete(const ::std::string& name, int32_t status, int32_t size, int32_t durationMs) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_LISTENER_H_
