#ifndef AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_H_
#define AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_H_

#include <android-base/unique_fd.h>
#include <android/os/IDumpstateListener.h>
#include <android/os/IDumpstateToken.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IDumpstate : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(Dumpstate)
  enum  : int32_t {
    BUGREPORT_MODE_FULL = 0,
    BUGREPORT_MODE_INTERACTIVE = 1,
    BUGREPORT_MODE_REMOTE = 2,
    BUGREPORT_MODE_WEAR = 3,
    BUGREPORT_MODE_TELEPHONY = 4,
    BUGREPORT_MODE_WIFI = 5,
    BUGREPORT_MODE_DEFAULT = 6,
  };
  virtual ::android::binder::Status setListener(const ::std::string& name, const ::android::sp<::android::os::IDumpstateListener>& listener, bool getSectionDetails, ::android::sp<::android::os::IDumpstateToken>* _aidl_return) = 0;
  virtual ::android::binder::Status startBugreport(int32_t callingUid, const ::std::string& callingPackage, const ::android::base::unique_fd& bugreportFd, const ::android::base::unique_fd& screenshotFd, int32_t bugreportMode, const ::android::sp<::android::os::IDumpstateListener>& listener) = 0;
  virtual ::android::binder::Status cancelBugreport() = 0;
};  // class IDumpstate

class IDumpstateDefault : public IDumpstate {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status setListener(const ::std::string& name, const ::android::sp<::android::os::IDumpstateListener>& listener, bool getSectionDetails, ::android::sp<::android::os::IDumpstateToken>* _aidl_return) override;
  ::android::binder::Status startBugreport(int32_t callingUid, const ::std::string& callingPackage, const ::android::base::unique_fd& bugreportFd, const ::android::base::unique_fd& screenshotFd, int32_t bugreportMode, const ::android::sp<::android::os::IDumpstateListener>& listener) override;
  ::android::binder::Status cancelBugreport() override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_H_
