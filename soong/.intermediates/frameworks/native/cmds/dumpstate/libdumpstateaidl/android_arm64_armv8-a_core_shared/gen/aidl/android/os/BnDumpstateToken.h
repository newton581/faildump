#ifndef AIDL_GENERATED_ANDROID_OS_BN_DUMPSTATE_TOKEN_H_
#define AIDL_GENERATED_ANDROID_OS_BN_DUMPSTATE_TOKEN_H_

#include <binder/IInterface.h>
#include <android/os/IDumpstateToken.h>

namespace android {

namespace os {

class BnDumpstateToken : public ::android::BnInterface<IDumpstateToken> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnDumpstateToken

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BN_DUMPSTATE_TOKEN_H_
