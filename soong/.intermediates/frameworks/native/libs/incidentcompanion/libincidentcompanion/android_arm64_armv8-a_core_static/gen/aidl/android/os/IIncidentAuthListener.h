#ifndef AIDL_GENERATED_ANDROID_OS_I_INCIDENT_AUTH_LISTENER_H_
#define AIDL_GENERATED_ANDROID_OS_I_INCIDENT_AUTH_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IIncidentAuthListener : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(IncidentAuthListener)
  virtual ::android::binder::Status onReportApproved() = 0;
  virtual ::android::binder::Status onReportDenied() = 0;
};  // class IIncidentAuthListener

class IIncidentAuthListenerDefault : public IIncidentAuthListener {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onReportApproved() override;
  ::android::binder::Status onReportDenied() override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_INCIDENT_AUTH_LISTENER_H_
