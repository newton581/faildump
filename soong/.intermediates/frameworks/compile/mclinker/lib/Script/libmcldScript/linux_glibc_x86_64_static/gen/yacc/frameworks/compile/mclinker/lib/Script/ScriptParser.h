/* A Bison parser, made by GNU Bison 2.7.  */

/* Skeleton interface for Bison GLR parsers in C++
   
      Copyright (C) 2002-2006, 2009-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C++ GLR parser skeleton written by Akim Demaille.  */

#ifndef YY_YY_OUT_SOONG_INTERMEDIATES_FRAMEWORKS_COMPILE_MCLINKER_LIB_SCRIPT_LIBMCLDSCRIPT_LINUX_GLIBC_X86_64_STATIC_GEN_YACC_FRAMEWORKS_COMPILE_MCLINKER_LIB_SCRIPT_SCRIPTPARSER_H_INCLUDED
# define YY_YY_OUT_SOONG_INTERMEDIATES_FRAMEWORKS_COMPILE_MCLINKER_LIB_SCRIPT_LIBMCLDSCRIPT_LINUX_GLIBC_X86_64_STATIC_GEN_YACC_FRAMEWORKS_COMPILE_MCLINKER_LIB_SCRIPT_SCRIPTPARSER_H_INCLUDED

/* "%code requires" blocks.  */
/* Line 241 of glr.cc  */
#line 28 "frameworks/compile/mclinker/lib/Script/ScriptParser.yy"

#include "mcld/Script/StrToken.h"
#include "mcld/Script/StringList.h"
#include "mcld/Script/OutputSectDesc.h"
#include "mcld/Script/InputSectDesc.h"
#include <llvm/Support/DataTypes.h>

using namespace mcld;



/* Line 241 of glr.cc  */
#line 54 "out/soong/.intermediates/frameworks/compile/mclinker/lib/Script/libmcldScript/linux_glibc_x86_64_static/gen/yacc/frameworks/compile/mclinker/lib/Script/ScriptParser.h"


# include <string>
# include <iostream>
# include "location.hh"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Line 250 of glr.cc  */
#line 48 "frameworks/compile/mclinker/lib/Script/ScriptParser.yy"
namespace mcld {
/* Line 250 of glr.cc  */
#line 70 "out/soong/.intermediates/frameworks/compile/mclinker/lib/Script/libmcldScript/linux_glibc_x86_64_static/gen/yacc/frameworks/compile/mclinker/lib/Script/ScriptParser.h"
  /// A Bison parser.
  class ScriptParser
  {
  public:
    /// Symbol semantic values.
# ifndef YYSTYPE
    union semantic_type
    {
/* Line 257 of glr.cc  */
#line 68 "frameworks/compile/mclinker/lib/Script/ScriptParser.yy"

  const std::string* string;
  uint64_t integer;
  RpnExpr* rpn_expr;
  StrToken* str_token;
  StringList* str_tokens;
  OutputSectDesc::Prolog output_prolog;
  OutputSectDesc::Type output_type;
  OutputSectDesc::Constraint output_constraint;
  OutputSectDesc::Epilog output_epilog;
  WildcardPattern* wildcard;
  InputSectDesc::Spec input_spec;


/* Line 257 of glr.cc  */
#line 96 "out/soong/.intermediates/frameworks/compile/mclinker/lib/Script/libmcldScript/linux_glibc_x86_64_static/gen/yacc/frameworks/compile/mclinker/lib/Script/ScriptParser.h"
    };
# else
    typedef YYSTYPE semantic_type;
# endif
    /// Symbol locations.
    typedef location location_type;
    /// Tokens.
    struct token
    {
      /* Tokens.  */
   enum yytokentype {
     END = 0,
     STRING = 258,
     LNAMESPEC = 259,
     INTEGER = 260,
     LINKER_SCRIPT = 261,
     DEFSYM = 262,
     VERSION_SCRIPT = 263,
     DYNAMIC_LIST = 264,
     ENTRY = 265,
     INCLUDE = 266,
     INPUT = 267,
     GROUP = 268,
     AS_NEEDED = 269,
     OUTPUT = 270,
     SEARCH_DIR = 271,
     STARTUP = 272,
     OUTPUT_FORMAT = 273,
     TARGET = 274,
     ASSERT = 275,
     EXTERN = 276,
     FORCE_COMMON_ALLOCATION = 277,
     INHIBIT_COMMON_ALLOCATION = 278,
     INSERT = 279,
     NOCROSSREFS = 280,
     OUTPUT_ARCH = 281,
     LD_FEATURE = 282,
     HIDDEN = 283,
     PROVIDE = 284,
     PROVIDE_HIDDEN = 285,
     SECTIONS = 286,
     MEMORY = 287,
     PHDRS = 288,
     ABSOLUTE = 289,
     ADDR = 290,
     ALIGN = 291,
     ALIGNOF = 292,
     BLOCK = 293,
     DATA_SEGMENT_ALIGN = 294,
     DATA_SEGMENT_END = 295,
     DATA_SEGMENT_RELRO_END = 296,
     DEFINED = 297,
     LENGTH = 298,
     LOADADDR = 299,
     MAX = 300,
     MIN = 301,
     NEXT = 302,
     ORIGIN = 303,
     SEGMENT_START = 304,
     SIZEOF = 305,
     SIZEOF_HEADERS = 306,
     CONSTANT = 307,
     MAXPAGESIZE = 308,
     COMMONPAGESIZE = 309,
     EXCLUDE_FILE = 310,
     COMMON = 311,
     KEEP = 312,
     SORT_BY_NAME = 313,
     SORT_BY_ALIGNMENT = 314,
     SORT_NONE = 315,
     SORT_BY_INIT_PRIORITY = 316,
     BYTE = 317,
     SHORT = 318,
     LONG = 319,
     QUAD = 320,
     SQUAD = 321,
     FILL = 322,
     DISCARD = 323,
     CREATE_OBJECT_SYMBOLS = 324,
     CONSTRUCTORS = 325,
     NOLOAD = 326,
     DSECT = 327,
     COPY = 328,
     INFO = 329,
     OVERLAY = 330,
     AT = 331,
     SUBALIGN = 332,
     ONLY_IF_RO = 333,
     ONLY_IF_RW = 334,
     RS_ASSIGN = 335,
     LS_ASSIGN = 336,
     OR_ASSIGN = 337,
     AND_ASSIGN = 338,
     DIV_ASSIGN = 339,
     MUL_ASSIGN = 340,
     SUB_ASSIGN = 341,
     ADD_ASSIGN = 342,
     LOGICAL_OR = 343,
     LOGICAL_AND = 344,
     NE = 345,
     EQ = 346,
     GE = 347,
     LE = 348,
     RSHIFT = 349,
     LSHIFT = 350,
     UNARY_MINUS = 351,
     UNARY_PLUS = 352
   };

    };
    /// Token type.
    typedef token::yytokentype token_type;

    /// Build a parser object.
    ScriptParser (const class LinkerConfig& m_LDConfig_yyarg, class ScriptFile& m_ScriptFile_yyarg, class ScriptScanner& m_ScriptScanner_yyarg, class ObjectReader& m_ObjectReader_yyarg, class ArchiveReader& m_ArchiveReader_yyarg, class DynObjReader& m_DynObjReader_yyarg, class GroupReader& m_GroupReader_yyarg);
    virtual ~ScriptParser ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

    /// The current debugging stream.
    std::ostream& debug_stream () const;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);

  private:

  public:
    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);
  private:

# if YYDEBUG
  public:
    /// \brief Report a symbol value on the debug stream.
    /// \param yytype       The token type.
    /// \param yyvaluep     Its semantic value.
    /// \param yylocationp  Its location.
    virtual void yy_symbol_value_print_ (int yytype,
                                         const semantic_type* yyvaluep,
                                         const location_type* yylocationp);
    /// \brief Report a symbol on the debug stream.
    /// \param yytype       The token type.
    /// \param yyvaluep     Its semantic value.
    /// \param yylocationp  Its location.
    virtual void yy_symbol_print_ (int yytype,
                                   const semantic_type* yyvaluep,
                                   const location_type* yylocationp);
  private:
    /* Debugging.  */
    std::ostream* yycdebug_;
# endif


    /* User arguments.  */
    const class LinkerConfig& m_LDConfig;
    class ScriptFile& m_ScriptFile;
    class ScriptScanner& m_ScriptScanner;
    class ObjectReader& m_ObjectReader;
    class ArchiveReader& m_ArchiveReader;
    class DynObjReader& m_DynObjReader;
    class GroupReader& m_GroupReader;
  };



#ifndef YYSTYPE
# define YYSTYPE mcld::ScriptParser::semantic_type
#endif
#ifndef YYLTYPE
# define YYLTYPE mcld::ScriptParser::location_type
#endif

/* Line 343 of glr.cc  */
#line 48 "frameworks/compile/mclinker/lib/Script/ScriptParser.yy"
} // mcld
/* Line 343 of glr.cc  */
#line 284 "out/soong/.intermediates/frameworks/compile/mclinker/lib/Script/libmcldScript/linux_glibc_x86_64_static/gen/yacc/frameworks/compile/mclinker/lib/Script/ScriptParser.h"

#endif /* !YY_YY_OUT_SOONG_INTERMEDIATES_FRAMEWORKS_COMPILE_MCLINKER_LIB_SCRIPT_LIBMCLDSCRIPT_LINUX_GLIBC_X86_64_STATIC_GEN_YACC_FRAMEWORKS_COMPILE_MCLINKER_LIB_SCRIPT_SCRIPTPARSER_H_INCLUDED  */
