#ifndef AIDL_GENERATED_ANDROID_MEDIA_MIDI_I_MIDI_DEVICE_SERVER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_MIDI_I_MIDI_DEVICE_SERVER_H_

#include <android-base/unique_fd.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <media/MidiDeviceInfo.h>
#include <utils/StrongPointer.h>

namespace android {

namespace media {

namespace midi {

class IMidiDeviceServer : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(MidiDeviceServer)
  virtual ::android::binder::Status openInputPort(const ::android::sp<::android::IBinder>& token, int32_t portNumber, ::android::base::unique_fd* _aidl_return) = 0;
  virtual ::android::binder::Status openOutputPort(const ::android::sp<::android::IBinder>& token, int32_t portNumber, ::android::base::unique_fd* _aidl_return) = 0;
  virtual ::android::binder::Status closePort(const ::android::sp<::android::IBinder>& token) = 0;
  virtual ::android::binder::Status closeDevice() = 0;
  virtual ::android::binder::Status connectPorts(const ::android::sp<::android::IBinder>& token, const ::android::base::unique_fd& fd, int32_t outputPortNumber, int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status getDeviceInfo(::android::media::midi::MidiDeviceInfo* _aidl_return) = 0;
  virtual ::android::binder::Status setDeviceInfo(const ::android::media::midi::MidiDeviceInfo& deviceInfo) = 0;
};  // class IMidiDeviceServer

class IMidiDeviceServerDefault : public IMidiDeviceServer {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status openInputPort(const ::android::sp<::android::IBinder>& token, int32_t portNumber, ::android::base::unique_fd* _aidl_return) override;
  ::android::binder::Status openOutputPort(const ::android::sp<::android::IBinder>& token, int32_t portNumber, ::android::base::unique_fd* _aidl_return) override;
  ::android::binder::Status closePort(const ::android::sp<::android::IBinder>& token) override;
  ::android::binder::Status closeDevice() override;
  ::android::binder::Status connectPorts(const ::android::sp<::android::IBinder>& token, const ::android::base::unique_fd& fd, int32_t outputPortNumber, int32_t* _aidl_return) override;
  ::android::binder::Status getDeviceInfo(::android::media::midi::MidiDeviceInfo* _aidl_return) override;
  ::android::binder::Status setDeviceInfo(const ::android::media::midi::MidiDeviceInfo& deviceInfo) override;

};

}  // namespace midi

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_MIDI_I_MIDI_DEVICE_SERVER_H_
