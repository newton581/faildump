#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_BN_DROP_BOX_MANAGER_SERVICE_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_BN_DROP_BOX_MANAGER_SERVICE_H_

#include <binder/IInterface.h>
#include <com/android/internal/os/IDropBoxManagerService.h>

namespace com {

namespace android {

namespace internal {

namespace os {

class BnDropBoxManagerService : public ::android::BnInterface<IDropBoxManagerService> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnDropBoxManagerService

}  // namespace os

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_BN_DROP_BOX_MANAGER_SERVICE_H_
