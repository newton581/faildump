/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.net;
public interface IIpMemoryStoreCallbacks extends android.os.IInterface
{
  /**
   * The version of this interface that the caller is built against.
   * This might be different from what {@link #getInterfaceVersion()
   * getInterfaceVersion} returns as that is the version of the interface
   * that the remote object is implementing.
   */
  public static final int VERSION = 3;
  /** Default implementation for IIpMemoryStoreCallbacks. */
  public static class Default implements android.net.IIpMemoryStoreCallbacks
  {
    @Override public void onIpMemoryStoreFetched(android.net.IIpMemoryStore ipMemoryStore) throws android.os.RemoteException
    {
    }
    @Override
    public int getInterfaceVersion() {
      return -1;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.net.IIpMemoryStoreCallbacks
  {
    private static final java.lang.String DESCRIPTOR = "android.net.IIpMemoryStoreCallbacks";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.net.IIpMemoryStoreCallbacks interface,
     * generating a proxy if needed.
     */
    public static android.net.IIpMemoryStoreCallbacks asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.net.IIpMemoryStoreCallbacks))) {
        return ((android.net.IIpMemoryStoreCallbacks)iin);
      }
      return new android.net.IIpMemoryStoreCallbacks.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onIpMemoryStoreFetched:
        {
          data.enforceInterface(descriptor);
          android.net.IIpMemoryStore _arg0;
          _arg0 = android.net.IIpMemoryStore.Stub.asInterface(data.readStrongBinder());
          this.onIpMemoryStoreFetched(_arg0);
          return true;
        }
        case TRANSACTION_getInterfaceVersion:
        {
          data.enforceInterface(descriptor);
          reply.writeNoException();
          reply.writeInt(getInterfaceVersion());
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.net.IIpMemoryStoreCallbacks
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      private int mCachedVersion = -1;
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onIpMemoryStoreFetched(android.net.IIpMemoryStore ipMemoryStore) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((ipMemoryStore!=null))?(ipMemoryStore.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_onIpMemoryStoreFetched, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onIpMemoryStoreFetched(ipMemoryStore);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override
      public int getInterfaceVersion() throws android.os.RemoteException {
        if (mCachedVersion == -1) {
          android.os.Parcel data = android.os.Parcel.obtain();
          android.os.Parcel reply = android.os.Parcel.obtain();
          try {
            data.writeInterfaceToken(DESCRIPTOR);
            mRemote.transact(Stub.TRANSACTION_getInterfaceVersion, data, reply, 0);
            reply.readException();
            mCachedVersion = reply.readInt();
          } finally {
            reply.recycle();
            data.recycle();
          }
        }
        return mCachedVersion;
      }
      public static android.net.IIpMemoryStoreCallbacks sDefaultImpl;
    }
    static final int TRANSACTION_onIpMemoryStoreFetched = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getInterfaceVersion = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16777214);
    public static boolean setDefaultImpl(android.net.IIpMemoryStoreCallbacks impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.net.IIpMemoryStoreCallbacks getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onIpMemoryStoreFetched(android.net.IIpMemoryStore ipMemoryStore) throws android.os.RemoteException;
  public int getInterfaceVersion() throws android.os.RemoteException;
}
