/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.app.admin;
/**
 * Internal IPC interface to the device policy service.
 * {@hide}
 */
public interface IDevicePolicyManager extends android.os.IInterface
{
  /** Default implementation for IDevicePolicyManager. */
  public static class Default implements android.app.admin.IDevicePolicyManager
  {
    @Override public void setPasswordQuality(android.content.ComponentName who, int quality, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordQuality(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordMinimumLength(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordMinimumLength(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordMinimumUpperCase(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordMinimumUpperCase(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordMinimumLowerCase(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordMinimumLowerCase(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordMinimumLetters(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordMinimumLetters(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordMinimumNumeric(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordMinimumNumeric(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordMinimumSymbols(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordMinimumSymbols(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordMinimumNonLetter(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordMinimumNonLetter(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordHistoryLength(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getPasswordHistoryLength(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPasswordExpirationTimeout(android.content.ComponentName who, long expiration, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public long getPasswordExpirationTimeout(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public long getPasswordExpiration(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public boolean isActivePasswordSufficient(int userHandle, boolean parent) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isProfileActivePasswordSufficientForParent(int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getPasswordComplexity() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean isUsingUnifiedPassword(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getCurrentFailedPasswordAttempts(int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int getProfileWithMinimumFailedPasswordsForWipe(int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setMaximumFailedPasswordsForWipe(android.content.ComponentName admin, int num, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getMaximumFailedPasswordsForWipe(android.content.ComponentName admin, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean resetPassword(java.lang.String password, int flags) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setMaximumTimeToLock(android.content.ComponentName who, long timeMs, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public long getMaximumTimeToLock(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public void setRequiredStrongAuthTimeout(android.content.ComponentName who, long timeMs, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public long getRequiredStrongAuthTimeout(android.content.ComponentName who, int userId, boolean parent) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public void lockNow(int flags, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public void wipeDataWithReason(int flags, java.lang.String wipeReasonForUser) throws android.os.RemoteException
    {
    }
    @Override public android.content.ComponentName setGlobalProxy(android.content.ComponentName admin, java.lang.String proxySpec, java.lang.String exclusionList) throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.content.ComponentName getGlobalProxyAdmin(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setRecommendedGlobalProxy(android.content.ComponentName admin, android.net.ProxyInfo proxyInfo) throws android.os.RemoteException
    {
    }
    @Override public int setStorageEncryption(android.content.ComponentName who, boolean encrypt) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean getStorageEncryption(android.content.ComponentName who, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getStorageEncryptionStatus(java.lang.String callerPackage, int userHandle) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean requestBugreport(android.content.ComponentName who) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setCameraDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
    {
    }
    @Override public boolean getCameraDisabled(android.content.ComponentName who, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setScreenCaptureDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
    {
    }
    @Override public boolean getScreenCaptureDisabled(android.content.ComponentName who, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setKeyguardDisabledFeatures(android.content.ComponentName who, int which, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public int getKeyguardDisabledFeatures(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setActiveAdmin(android.content.ComponentName policyReceiver, boolean refreshing, int userHandle) throws android.os.RemoteException
    {
    }
    @Override public boolean isAdminActive(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<android.content.ComponentName> getActiveAdmins(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean packageHasActiveAdmins(java.lang.String packageName, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void getRemoveWarning(android.content.ComponentName policyReceiver, android.os.RemoteCallback result, int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void removeActiveAdmin(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void forceRemoveActiveAdmin(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException
    {
    }
    @Override public boolean hasGrantedPolicy(android.content.ComponentName policyReceiver, int usesPolicy, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setActivePasswordState(android.app.admin.PasswordMetrics metrics, int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void reportPasswordChanged(int userId) throws android.os.RemoteException
    {
    }
    @Override public void reportFailedPasswordAttempt(int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void reportSuccessfulPasswordAttempt(int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void reportFailedBiometricAttempt(int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void reportSuccessfulBiometricAttempt(int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void reportKeyguardDismissed(int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void reportKeyguardSecured(int userHandle) throws android.os.RemoteException
    {
    }
    @Override public boolean setDeviceOwner(android.content.ComponentName who, java.lang.String ownerName, int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public android.content.ComponentName getDeviceOwnerComponent(boolean callingUserOnly) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean hasDeviceOwner() throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String getDeviceOwnerName() throws android.os.RemoteException
    {
      return null;
    }
    @Override public void clearDeviceOwner(java.lang.String packageName) throws android.os.RemoteException
    {
    }
    @Override public int getDeviceOwnerUserId() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setProfileOwner(android.content.ComponentName who, java.lang.String ownerName, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public android.content.ComponentName getProfileOwnerAsUser(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.content.ComponentName getProfileOwner(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String getProfileOwnerName(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setProfileEnabled(android.content.ComponentName who) throws android.os.RemoteException
    {
    }
    @Override public void setProfileName(android.content.ComponentName who, java.lang.String profileName) throws android.os.RemoteException
    {
    }
    @Override public void clearProfileOwner(android.content.ComponentName who) throws android.os.RemoteException
    {
    }
    @Override public boolean hasUserSetupCompleted() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean checkDeviceIdentifierAccess(java.lang.String packageName, int pid, int uid) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setDeviceOwnerLockScreenInfo(android.content.ComponentName who, java.lang.CharSequence deviceOwnerInfo) throws android.os.RemoteException
    {
    }
    @Override public java.lang.CharSequence getDeviceOwnerLockScreenInfo() throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String[] setPackagesSuspended(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String[] packageNames, boolean suspended) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isPackageSuspended(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean installCaCert(android.content.ComponentName admin, java.lang.String callerPackage, byte[] certBuffer) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void uninstallCaCerts(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String[] aliases) throws android.os.RemoteException
    {
    }
    @Override public void enforceCanManageCaCerts(android.content.ComponentName admin, java.lang.String callerPackage) throws android.os.RemoteException
    {
    }
    @Override public boolean approveCaCert(java.lang.String alias, int userHandle, boolean approval) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isCaCertApproved(java.lang.String alias, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean installKeyPair(android.content.ComponentName who, java.lang.String callerPackage, byte[] privKeyBuffer, byte[] certBuffer, byte[] certChainBuffer, java.lang.String alias, boolean requestAccess, boolean isUserSelectable) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean removeKeyPair(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String alias) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean generateKeyPair(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String algorithm, android.security.keystore.ParcelableKeyGenParameterSpec keySpec, int idAttestationFlags, android.security.keymaster.KeymasterCertificateChain attestationChain) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setKeyPairCertificate(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String alias, byte[] certBuffer, byte[] certChainBuffer, boolean isUserSelectable) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void choosePrivateKeyAlias(int uid, android.net.Uri uri, java.lang.String alias, android.os.IBinder aliasCallback) throws android.os.RemoteException
    {
    }
    @Override public void setDelegatedScopes(android.content.ComponentName who, java.lang.String delegatePackage, java.util.List<java.lang.String> scopes) throws android.os.RemoteException
    {
    }
    @Override public java.util.List<java.lang.String> getDelegatedScopes(android.content.ComponentName who, java.lang.String delegatePackage) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.util.List<java.lang.String> getDelegatePackages(android.content.ComponentName who, java.lang.String scope) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setCertInstallerPackage(android.content.ComponentName who, java.lang.String installerPackage) throws android.os.RemoteException
    {
    }
    @Override public java.lang.String getCertInstallerPackage(android.content.ComponentName who) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setAlwaysOnVpnPackage(android.content.ComponentName who, java.lang.String vpnPackage, boolean lockdown, java.util.List<java.lang.String> lockdownWhitelist) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String getAlwaysOnVpnPackage(android.content.ComponentName who) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isAlwaysOnVpnLockdownEnabled(android.content.ComponentName who) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<java.lang.String> getAlwaysOnVpnLockdownWhitelist(android.content.ComponentName who) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void addPersistentPreferredActivity(android.content.ComponentName admin, android.content.IntentFilter filter, android.content.ComponentName activity) throws android.os.RemoteException
    {
    }
    @Override public void clearPackagePersistentPreferredActivities(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
    {
    }
    @Override public void setDefaultSmsApplication(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
    {
    }
    @Override public void setApplicationRestrictions(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String packageName, android.os.Bundle settings) throws android.os.RemoteException
    {
    }
    @Override public android.os.Bundle getApplicationRestrictions(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setApplicationRestrictionsManagingPackage(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String getApplicationRestrictionsManagingPackage(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isCallerApplicationRestrictionsManagingPackage(java.lang.String callerPackage) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setRestrictionsProvider(android.content.ComponentName who, android.content.ComponentName provider) throws android.os.RemoteException
    {
    }
    @Override public android.content.ComponentName getRestrictionsProvider(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setUserRestriction(android.content.ComponentName who, java.lang.String key, boolean enable) throws android.os.RemoteException
    {
    }
    @Override public android.os.Bundle getUserRestrictions(android.content.ComponentName who) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void addCrossProfileIntentFilter(android.content.ComponentName admin, android.content.IntentFilter filter, int flags) throws android.os.RemoteException
    {
    }
    @Override public void clearCrossProfileIntentFilters(android.content.ComponentName admin) throws android.os.RemoteException
    {
    }
    @Override public boolean setPermittedAccessibilityServices(android.content.ComponentName admin, java.util.List packageList) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List getPermittedAccessibilityServices(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.util.List getPermittedAccessibilityServicesForUser(int userId) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isAccessibilityServicePermittedByAdmin(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setPermittedInputMethods(android.content.ComponentName admin, java.util.List packageList) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List getPermittedInputMethods(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.util.List getPermittedInputMethodsForCurrentUser() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isInputMethodPermittedByAdmin(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setPermittedCrossProfileNotificationListeners(android.content.ComponentName admin, java.util.List<java.lang.String> packageList) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<java.lang.String> getPermittedCrossProfileNotificationListeners(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isNotificationListenerServicePermitted(java.lang.String packageName, int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public android.content.Intent createAdminSupportIntent(java.lang.String restriction) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setApplicationHidden(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, boolean hidden) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isApplicationHidden(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public android.os.UserHandle createAndManageUser(android.content.ComponentName who, java.lang.String name, android.content.ComponentName profileOwner, android.os.PersistableBundle adminExtras, int flags) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean removeUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean switchUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int startUserInBackground(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int stopUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int logoutUser(android.content.ComponentName who) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public java.util.List<android.os.UserHandle> getSecondaryUsers(android.content.ComponentName who) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void enableSystemApp(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
    {
    }
    @Override public int enableSystemAppWithIntent(android.content.ComponentName admin, java.lang.String callerPackage, android.content.Intent intent) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean installExistingPackage(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setAccountManagementDisabled(android.content.ComponentName who, java.lang.String accountType, boolean disabled) throws android.os.RemoteException
    {
    }
    @Override public java.lang.String[] getAccountTypesWithManagementDisabled() throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String[] getAccountTypesWithManagementDisabledAsUser(int userId) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setLockTaskPackages(android.content.ComponentName who, java.lang.String[] packages) throws android.os.RemoteException
    {
    }
    @Override public java.lang.String[] getLockTaskPackages(android.content.ComponentName who) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isLockTaskPermitted(java.lang.String pkg) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setLockTaskFeatures(android.content.ComponentName who, int flags) throws android.os.RemoteException
    {
    }
    @Override public int getLockTaskFeatures(android.content.ComponentName who) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setGlobalSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException
    {
    }
    @Override public void setSystemSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException
    {
    }
    @Override public void setSecureSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException
    {
    }
    @Override public boolean setTime(android.content.ComponentName who, long millis) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setTimeZone(android.content.ComponentName who, java.lang.String timeZone) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setMasterVolumeMuted(android.content.ComponentName admin, boolean on) throws android.os.RemoteException
    {
    }
    @Override public boolean isMasterVolumeMuted(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void notifyLockTaskModeChanged(boolean isEnabled, java.lang.String pkg, int userId) throws android.os.RemoteException
    {
    }
    @Override public void setUninstallBlocked(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, boolean uninstallBlocked) throws android.os.RemoteException
    {
    }
    @Override public boolean isUninstallBlocked(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setCrossProfileCallerIdDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
    {
    }
    @Override public boolean getCrossProfileCallerIdDisabled(android.content.ComponentName who) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean getCrossProfileCallerIdDisabledForUser(int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setCrossProfileContactsSearchDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
    {
    }
    @Override public boolean getCrossProfileContactsSearchDisabled(android.content.ComponentName who) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean getCrossProfileContactsSearchDisabledForUser(int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void startManagedQuickContact(java.lang.String lookupKey, long contactId, boolean isContactIdIgnored, long directoryId, android.content.Intent originalIntent) throws android.os.RemoteException
    {
    }
    @Override public void setBluetoothContactSharingDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
    {
    }
    @Override public boolean getBluetoothContactSharingDisabled(android.content.ComponentName who) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean getBluetoothContactSharingDisabledForUser(int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setTrustAgentConfiguration(android.content.ComponentName admin, android.content.ComponentName agent, android.os.PersistableBundle args, boolean parent) throws android.os.RemoteException
    {
    }
    @Override public java.util.List<android.os.PersistableBundle> getTrustAgentConfiguration(android.content.ComponentName admin, android.content.ComponentName agent, int userId, boolean parent) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean addCrossProfileWidgetProvider(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean removeCrossProfileWidgetProvider(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<java.lang.String> getCrossProfileWidgetProviders(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setAutoTimeRequired(android.content.ComponentName who, boolean required) throws android.os.RemoteException
    {
    }
    @Override public boolean getAutoTimeRequired() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setForceEphemeralUsers(android.content.ComponentName who, boolean forceEpehemeralUsers) throws android.os.RemoteException
    {
    }
    @Override public boolean getForceEphemeralUsers(android.content.ComponentName who) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isRemovingAdmin(android.content.ComponentName adminReceiver, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setUserIcon(android.content.ComponentName admin, android.graphics.Bitmap icon) throws android.os.RemoteException
    {
    }
    @Override public void setSystemUpdatePolicy(android.content.ComponentName who, android.app.admin.SystemUpdatePolicy policy) throws android.os.RemoteException
    {
    }
    @Override public android.app.admin.SystemUpdatePolicy getSystemUpdatePolicy() throws android.os.RemoteException
    {
      return null;
    }
    @Override public void clearSystemUpdatePolicyFreezePeriodRecord() throws android.os.RemoteException
    {
    }
    @Override public boolean setKeyguardDisabled(android.content.ComponentName admin, boolean disabled) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setStatusBarDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean getDoNotAskCredentialsOnBoot() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void notifyPendingSystemUpdate(android.app.admin.SystemUpdateInfo info) throws android.os.RemoteException
    {
    }
    @Override public android.app.admin.SystemUpdateInfo getPendingSystemUpdate(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setPermissionPolicy(android.content.ComponentName admin, java.lang.String callerPackage, int policy) throws android.os.RemoteException
    {
    }
    @Override public int getPermissionPolicy(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setPermissionGrantState(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, java.lang.String permission, int grantState, android.os.RemoteCallback resultReceiver) throws android.os.RemoteException
    {
    }
    @Override public int getPermissionGrantState(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, java.lang.String permission) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean isProvisioningAllowed(java.lang.String action, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int checkProvisioningPreCondition(java.lang.String action, java.lang.String packageName) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setKeepUninstalledPackages(android.content.ComponentName admin, java.lang.String callerPackage, java.util.List<java.lang.String> packageList) throws android.os.RemoteException
    {
    }
    @Override public java.util.List<java.lang.String> getKeepUninstalledPackages(android.content.ComponentName admin, java.lang.String callerPackage) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isManagedProfile(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isSystemOnlyUser(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String getWifiMacAddress(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void reboot(android.content.ComponentName admin) throws android.os.RemoteException
    {
    }
    @Override public void setShortSupportMessage(android.content.ComponentName admin, java.lang.CharSequence message) throws android.os.RemoteException
    {
    }
    @Override public java.lang.CharSequence getShortSupportMessage(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setLongSupportMessage(android.content.ComponentName admin, java.lang.CharSequence message) throws android.os.RemoteException
    {
    }
    @Override public java.lang.CharSequence getLongSupportMessage(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.CharSequence getShortSupportMessageForUser(android.content.ComponentName admin, int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.CharSequence getLongSupportMessageForUser(android.content.ComponentName admin, int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isSeparateProfileChallengeAllowed(int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setOrganizationColor(android.content.ComponentName admin, int color) throws android.os.RemoteException
    {
    }
    @Override public void setOrganizationColorForUser(int color, int userId) throws android.os.RemoteException
    {
    }
    @Override public int getOrganizationColor(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int getOrganizationColorForUser(int userHandle) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setOrganizationName(android.content.ComponentName admin, java.lang.CharSequence title) throws android.os.RemoteException
    {
    }
    @Override public java.lang.CharSequence getOrganizationName(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.CharSequence getDeviceOwnerOrganizationName() throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.CharSequence getOrganizationNameForUser(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public int getUserProvisioningState() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void setUserProvisioningState(int state, int userHandle) throws android.os.RemoteException
    {
    }
    @Override public void setAffiliationIds(android.content.ComponentName admin, java.util.List<java.lang.String> ids) throws android.os.RemoteException
    {
    }
    @Override public java.util.List<java.lang.String> getAffiliationIds(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isAffiliatedUser() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setSecurityLoggingEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
    {
    }
    @Override public boolean isSecurityLoggingEnabled(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public android.content.pm.ParceledListSlice retrieveSecurityLogs(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.content.pm.ParceledListSlice retrievePreRebootSecurityLogs(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public long forceNetworkLogs() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public long forceSecurityLogs() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public boolean isUninstallInQueue(java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void uninstallPackageWithActiveAdmins(java.lang.String packageName) throws android.os.RemoteException
    {
    }
    @Override public boolean isDeviceProvisioned() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isDeviceProvisioningConfigApplied() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setDeviceProvisioningConfigApplied() throws android.os.RemoteException
    {
    }
    @Override public void forceUpdateUserSetupComplete() throws android.os.RemoteException
    {
    }
    @Override public void setBackupServiceEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
    {
    }
    @Override public boolean isBackupServiceEnabled(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setNetworkLoggingEnabled(android.content.ComponentName admin, java.lang.String packageName, boolean enabled) throws android.os.RemoteException
    {
    }
    @Override public boolean isNetworkLoggingEnabled(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<android.app.admin.NetworkEvent> retrieveNetworkLogs(android.content.ComponentName admin, java.lang.String packageName, long batchToken) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean bindDeviceAdminServiceAsUser(android.content.ComponentName admin, android.app.IApplicationThread caller, android.os.IBinder token, android.content.Intent service, android.app.IServiceConnection connection, int flags, int targetUserId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<android.os.UserHandle> getBindDeviceAdminTargetUsers(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isEphemeralUser(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public long getLastSecurityLogRetrievalTime() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public long getLastBugReportRequestTime() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public long getLastNetworkLogRetrievalTime() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public boolean setResetPasswordToken(android.content.ComponentName admin, byte[] token) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean clearResetPasswordToken(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isResetPasswordTokenActive(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean resetPasswordWithToken(android.content.ComponentName admin, java.lang.String password, byte[] token, int flags) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isCurrentInputMethodSetByOwner() throws android.os.RemoteException
    {
      return false;
    }
    @Override public android.content.pm.StringParceledListSlice getOwnerInstalledCaCerts(android.os.UserHandle user) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void clearApplicationUserData(android.content.ComponentName admin, java.lang.String packageName, android.content.pm.IPackageDataObserver callback) throws android.os.RemoteException
    {
    }
    @Override public void setLogoutEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
    {
    }
    @Override public boolean isLogoutEnabled() throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<java.lang.String> getDisallowedSystemApps(android.content.ComponentName admin, int userId, java.lang.String provisioningAction) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void transferOwnership(android.content.ComponentName admin, android.content.ComponentName target, android.os.PersistableBundle bundle) throws android.os.RemoteException
    {
    }
    @Override public android.os.PersistableBundle getTransferOwnershipBundle() throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setStartUserSessionMessage(android.content.ComponentName admin, java.lang.CharSequence startUserSessionMessage) throws android.os.RemoteException
    {
    }
    @Override public void setEndUserSessionMessage(android.content.ComponentName admin, java.lang.CharSequence endUserSessionMessage) throws android.os.RemoteException
    {
    }
    @Override public java.lang.CharSequence getStartUserSessionMessage(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.CharSequence getEndUserSessionMessage(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.util.List<java.lang.String> setMeteredDataDisabledPackages(android.content.ComponentName admin, java.util.List<java.lang.String> packageNames) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.util.List<java.lang.String> getMeteredDataDisabledPackages(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public int addOverrideApn(android.content.ComponentName admin, android.telephony.data.ApnSetting apnSetting) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean updateOverrideApn(android.content.ComponentName admin, int apnId, android.telephony.data.ApnSetting apnSetting) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean removeOverrideApn(android.content.ComponentName admin, int apnId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<android.telephony.data.ApnSetting> getOverrideApns(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setOverrideApnsEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
    {
    }
    @Override public boolean isOverrideApnEnabled(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isMeteredDataDisabledPackageForUser(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int setGlobalPrivateDns(android.content.ComponentName admin, int mode, java.lang.String privateDnsHost) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int getGlobalPrivateDnsMode(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public java.lang.String getGlobalPrivateDnsHost(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void grantDeviceIdsAccessToProfileOwner(android.content.ComponentName who, int userId) throws android.os.RemoteException
    {
    }
    @Override public void installUpdateFromFile(android.content.ComponentName admin, android.os.ParcelFileDescriptor updateFileDescriptor, android.app.admin.StartInstallingUpdateCallback listener) throws android.os.RemoteException
    {
    }
    @Override public void setCrossProfileCalendarPackages(android.content.ComponentName admin, java.util.List<java.lang.String> packageNames) throws android.os.RemoteException
    {
    }
    @Override public java.util.List<java.lang.String> getCrossProfileCalendarPackages(android.content.ComponentName admin) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isPackageAllowedToAccessCalendarForUser(java.lang.String packageName, int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<java.lang.String> getCrossProfileCalendarPackagesForUser(int userHandle) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isManagedKiosk() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isUnattendedManagedKiosk() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean startViewCalendarEventInManagedProfile(java.lang.String packageName, long eventId, long start, long end, boolean allDay, int flags) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean requireSecureKeyguard(int userHandle) throws android.os.RemoteException
    {
      return false;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.app.admin.IDevicePolicyManager
  {
    private static final java.lang.String DESCRIPTOR = "android.app.admin.IDevicePolicyManager";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.app.admin.IDevicePolicyManager interface,
     * generating a proxy if needed.
     */
    public static android.app.admin.IDevicePolicyManager asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.app.admin.IDevicePolicyManager))) {
        return ((android.app.admin.IDevicePolicyManager)iin);
      }
      return new android.app.admin.IDevicePolicyManager.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_setPasswordQuality:
        {
          return "setPasswordQuality";
        }
        case TRANSACTION_getPasswordQuality:
        {
          return "getPasswordQuality";
        }
        case TRANSACTION_setPasswordMinimumLength:
        {
          return "setPasswordMinimumLength";
        }
        case TRANSACTION_getPasswordMinimumLength:
        {
          return "getPasswordMinimumLength";
        }
        case TRANSACTION_setPasswordMinimumUpperCase:
        {
          return "setPasswordMinimumUpperCase";
        }
        case TRANSACTION_getPasswordMinimumUpperCase:
        {
          return "getPasswordMinimumUpperCase";
        }
        case TRANSACTION_setPasswordMinimumLowerCase:
        {
          return "setPasswordMinimumLowerCase";
        }
        case TRANSACTION_getPasswordMinimumLowerCase:
        {
          return "getPasswordMinimumLowerCase";
        }
        case TRANSACTION_setPasswordMinimumLetters:
        {
          return "setPasswordMinimumLetters";
        }
        case TRANSACTION_getPasswordMinimumLetters:
        {
          return "getPasswordMinimumLetters";
        }
        case TRANSACTION_setPasswordMinimumNumeric:
        {
          return "setPasswordMinimumNumeric";
        }
        case TRANSACTION_getPasswordMinimumNumeric:
        {
          return "getPasswordMinimumNumeric";
        }
        case TRANSACTION_setPasswordMinimumSymbols:
        {
          return "setPasswordMinimumSymbols";
        }
        case TRANSACTION_getPasswordMinimumSymbols:
        {
          return "getPasswordMinimumSymbols";
        }
        case TRANSACTION_setPasswordMinimumNonLetter:
        {
          return "setPasswordMinimumNonLetter";
        }
        case TRANSACTION_getPasswordMinimumNonLetter:
        {
          return "getPasswordMinimumNonLetter";
        }
        case TRANSACTION_setPasswordHistoryLength:
        {
          return "setPasswordHistoryLength";
        }
        case TRANSACTION_getPasswordHistoryLength:
        {
          return "getPasswordHistoryLength";
        }
        case TRANSACTION_setPasswordExpirationTimeout:
        {
          return "setPasswordExpirationTimeout";
        }
        case TRANSACTION_getPasswordExpirationTimeout:
        {
          return "getPasswordExpirationTimeout";
        }
        case TRANSACTION_getPasswordExpiration:
        {
          return "getPasswordExpiration";
        }
        case TRANSACTION_isActivePasswordSufficient:
        {
          return "isActivePasswordSufficient";
        }
        case TRANSACTION_isProfileActivePasswordSufficientForParent:
        {
          return "isProfileActivePasswordSufficientForParent";
        }
        case TRANSACTION_getPasswordComplexity:
        {
          return "getPasswordComplexity";
        }
        case TRANSACTION_isUsingUnifiedPassword:
        {
          return "isUsingUnifiedPassword";
        }
        case TRANSACTION_getCurrentFailedPasswordAttempts:
        {
          return "getCurrentFailedPasswordAttempts";
        }
        case TRANSACTION_getProfileWithMinimumFailedPasswordsForWipe:
        {
          return "getProfileWithMinimumFailedPasswordsForWipe";
        }
        case TRANSACTION_setMaximumFailedPasswordsForWipe:
        {
          return "setMaximumFailedPasswordsForWipe";
        }
        case TRANSACTION_getMaximumFailedPasswordsForWipe:
        {
          return "getMaximumFailedPasswordsForWipe";
        }
        case TRANSACTION_resetPassword:
        {
          return "resetPassword";
        }
        case TRANSACTION_setMaximumTimeToLock:
        {
          return "setMaximumTimeToLock";
        }
        case TRANSACTION_getMaximumTimeToLock:
        {
          return "getMaximumTimeToLock";
        }
        case TRANSACTION_setRequiredStrongAuthTimeout:
        {
          return "setRequiredStrongAuthTimeout";
        }
        case TRANSACTION_getRequiredStrongAuthTimeout:
        {
          return "getRequiredStrongAuthTimeout";
        }
        case TRANSACTION_lockNow:
        {
          return "lockNow";
        }
        case TRANSACTION_wipeDataWithReason:
        {
          return "wipeDataWithReason";
        }
        case TRANSACTION_setGlobalProxy:
        {
          return "setGlobalProxy";
        }
        case TRANSACTION_getGlobalProxyAdmin:
        {
          return "getGlobalProxyAdmin";
        }
        case TRANSACTION_setRecommendedGlobalProxy:
        {
          return "setRecommendedGlobalProxy";
        }
        case TRANSACTION_setStorageEncryption:
        {
          return "setStorageEncryption";
        }
        case TRANSACTION_getStorageEncryption:
        {
          return "getStorageEncryption";
        }
        case TRANSACTION_getStorageEncryptionStatus:
        {
          return "getStorageEncryptionStatus";
        }
        case TRANSACTION_requestBugreport:
        {
          return "requestBugreport";
        }
        case TRANSACTION_setCameraDisabled:
        {
          return "setCameraDisabled";
        }
        case TRANSACTION_getCameraDisabled:
        {
          return "getCameraDisabled";
        }
        case TRANSACTION_setScreenCaptureDisabled:
        {
          return "setScreenCaptureDisabled";
        }
        case TRANSACTION_getScreenCaptureDisabled:
        {
          return "getScreenCaptureDisabled";
        }
        case TRANSACTION_setKeyguardDisabledFeatures:
        {
          return "setKeyguardDisabledFeatures";
        }
        case TRANSACTION_getKeyguardDisabledFeatures:
        {
          return "getKeyguardDisabledFeatures";
        }
        case TRANSACTION_setActiveAdmin:
        {
          return "setActiveAdmin";
        }
        case TRANSACTION_isAdminActive:
        {
          return "isAdminActive";
        }
        case TRANSACTION_getActiveAdmins:
        {
          return "getActiveAdmins";
        }
        case TRANSACTION_packageHasActiveAdmins:
        {
          return "packageHasActiveAdmins";
        }
        case TRANSACTION_getRemoveWarning:
        {
          return "getRemoveWarning";
        }
        case TRANSACTION_removeActiveAdmin:
        {
          return "removeActiveAdmin";
        }
        case TRANSACTION_forceRemoveActiveAdmin:
        {
          return "forceRemoveActiveAdmin";
        }
        case TRANSACTION_hasGrantedPolicy:
        {
          return "hasGrantedPolicy";
        }
        case TRANSACTION_setActivePasswordState:
        {
          return "setActivePasswordState";
        }
        case TRANSACTION_reportPasswordChanged:
        {
          return "reportPasswordChanged";
        }
        case TRANSACTION_reportFailedPasswordAttempt:
        {
          return "reportFailedPasswordAttempt";
        }
        case TRANSACTION_reportSuccessfulPasswordAttempt:
        {
          return "reportSuccessfulPasswordAttempt";
        }
        case TRANSACTION_reportFailedBiometricAttempt:
        {
          return "reportFailedBiometricAttempt";
        }
        case TRANSACTION_reportSuccessfulBiometricAttempt:
        {
          return "reportSuccessfulBiometricAttempt";
        }
        case TRANSACTION_reportKeyguardDismissed:
        {
          return "reportKeyguardDismissed";
        }
        case TRANSACTION_reportKeyguardSecured:
        {
          return "reportKeyguardSecured";
        }
        case TRANSACTION_setDeviceOwner:
        {
          return "setDeviceOwner";
        }
        case TRANSACTION_getDeviceOwnerComponent:
        {
          return "getDeviceOwnerComponent";
        }
        case TRANSACTION_hasDeviceOwner:
        {
          return "hasDeviceOwner";
        }
        case TRANSACTION_getDeviceOwnerName:
        {
          return "getDeviceOwnerName";
        }
        case TRANSACTION_clearDeviceOwner:
        {
          return "clearDeviceOwner";
        }
        case TRANSACTION_getDeviceOwnerUserId:
        {
          return "getDeviceOwnerUserId";
        }
        case TRANSACTION_setProfileOwner:
        {
          return "setProfileOwner";
        }
        case TRANSACTION_getProfileOwnerAsUser:
        {
          return "getProfileOwnerAsUser";
        }
        case TRANSACTION_getProfileOwner:
        {
          return "getProfileOwner";
        }
        case TRANSACTION_getProfileOwnerName:
        {
          return "getProfileOwnerName";
        }
        case TRANSACTION_setProfileEnabled:
        {
          return "setProfileEnabled";
        }
        case TRANSACTION_setProfileName:
        {
          return "setProfileName";
        }
        case TRANSACTION_clearProfileOwner:
        {
          return "clearProfileOwner";
        }
        case TRANSACTION_hasUserSetupCompleted:
        {
          return "hasUserSetupCompleted";
        }
        case TRANSACTION_checkDeviceIdentifierAccess:
        {
          return "checkDeviceIdentifierAccess";
        }
        case TRANSACTION_setDeviceOwnerLockScreenInfo:
        {
          return "setDeviceOwnerLockScreenInfo";
        }
        case TRANSACTION_getDeviceOwnerLockScreenInfo:
        {
          return "getDeviceOwnerLockScreenInfo";
        }
        case TRANSACTION_setPackagesSuspended:
        {
          return "setPackagesSuspended";
        }
        case TRANSACTION_isPackageSuspended:
        {
          return "isPackageSuspended";
        }
        case TRANSACTION_installCaCert:
        {
          return "installCaCert";
        }
        case TRANSACTION_uninstallCaCerts:
        {
          return "uninstallCaCerts";
        }
        case TRANSACTION_enforceCanManageCaCerts:
        {
          return "enforceCanManageCaCerts";
        }
        case TRANSACTION_approveCaCert:
        {
          return "approveCaCert";
        }
        case TRANSACTION_isCaCertApproved:
        {
          return "isCaCertApproved";
        }
        case TRANSACTION_installKeyPair:
        {
          return "installKeyPair";
        }
        case TRANSACTION_removeKeyPair:
        {
          return "removeKeyPair";
        }
        case TRANSACTION_generateKeyPair:
        {
          return "generateKeyPair";
        }
        case TRANSACTION_setKeyPairCertificate:
        {
          return "setKeyPairCertificate";
        }
        case TRANSACTION_choosePrivateKeyAlias:
        {
          return "choosePrivateKeyAlias";
        }
        case TRANSACTION_setDelegatedScopes:
        {
          return "setDelegatedScopes";
        }
        case TRANSACTION_getDelegatedScopes:
        {
          return "getDelegatedScopes";
        }
        case TRANSACTION_getDelegatePackages:
        {
          return "getDelegatePackages";
        }
        case TRANSACTION_setCertInstallerPackage:
        {
          return "setCertInstallerPackage";
        }
        case TRANSACTION_getCertInstallerPackage:
        {
          return "getCertInstallerPackage";
        }
        case TRANSACTION_setAlwaysOnVpnPackage:
        {
          return "setAlwaysOnVpnPackage";
        }
        case TRANSACTION_getAlwaysOnVpnPackage:
        {
          return "getAlwaysOnVpnPackage";
        }
        case TRANSACTION_isAlwaysOnVpnLockdownEnabled:
        {
          return "isAlwaysOnVpnLockdownEnabled";
        }
        case TRANSACTION_getAlwaysOnVpnLockdownWhitelist:
        {
          return "getAlwaysOnVpnLockdownWhitelist";
        }
        case TRANSACTION_addPersistentPreferredActivity:
        {
          return "addPersistentPreferredActivity";
        }
        case TRANSACTION_clearPackagePersistentPreferredActivities:
        {
          return "clearPackagePersistentPreferredActivities";
        }
        case TRANSACTION_setDefaultSmsApplication:
        {
          return "setDefaultSmsApplication";
        }
        case TRANSACTION_setApplicationRestrictions:
        {
          return "setApplicationRestrictions";
        }
        case TRANSACTION_getApplicationRestrictions:
        {
          return "getApplicationRestrictions";
        }
        case TRANSACTION_setApplicationRestrictionsManagingPackage:
        {
          return "setApplicationRestrictionsManagingPackage";
        }
        case TRANSACTION_getApplicationRestrictionsManagingPackage:
        {
          return "getApplicationRestrictionsManagingPackage";
        }
        case TRANSACTION_isCallerApplicationRestrictionsManagingPackage:
        {
          return "isCallerApplicationRestrictionsManagingPackage";
        }
        case TRANSACTION_setRestrictionsProvider:
        {
          return "setRestrictionsProvider";
        }
        case TRANSACTION_getRestrictionsProvider:
        {
          return "getRestrictionsProvider";
        }
        case TRANSACTION_setUserRestriction:
        {
          return "setUserRestriction";
        }
        case TRANSACTION_getUserRestrictions:
        {
          return "getUserRestrictions";
        }
        case TRANSACTION_addCrossProfileIntentFilter:
        {
          return "addCrossProfileIntentFilter";
        }
        case TRANSACTION_clearCrossProfileIntentFilters:
        {
          return "clearCrossProfileIntentFilters";
        }
        case TRANSACTION_setPermittedAccessibilityServices:
        {
          return "setPermittedAccessibilityServices";
        }
        case TRANSACTION_getPermittedAccessibilityServices:
        {
          return "getPermittedAccessibilityServices";
        }
        case TRANSACTION_getPermittedAccessibilityServicesForUser:
        {
          return "getPermittedAccessibilityServicesForUser";
        }
        case TRANSACTION_isAccessibilityServicePermittedByAdmin:
        {
          return "isAccessibilityServicePermittedByAdmin";
        }
        case TRANSACTION_setPermittedInputMethods:
        {
          return "setPermittedInputMethods";
        }
        case TRANSACTION_getPermittedInputMethods:
        {
          return "getPermittedInputMethods";
        }
        case TRANSACTION_getPermittedInputMethodsForCurrentUser:
        {
          return "getPermittedInputMethodsForCurrentUser";
        }
        case TRANSACTION_isInputMethodPermittedByAdmin:
        {
          return "isInputMethodPermittedByAdmin";
        }
        case TRANSACTION_setPermittedCrossProfileNotificationListeners:
        {
          return "setPermittedCrossProfileNotificationListeners";
        }
        case TRANSACTION_getPermittedCrossProfileNotificationListeners:
        {
          return "getPermittedCrossProfileNotificationListeners";
        }
        case TRANSACTION_isNotificationListenerServicePermitted:
        {
          return "isNotificationListenerServicePermitted";
        }
        case TRANSACTION_createAdminSupportIntent:
        {
          return "createAdminSupportIntent";
        }
        case TRANSACTION_setApplicationHidden:
        {
          return "setApplicationHidden";
        }
        case TRANSACTION_isApplicationHidden:
        {
          return "isApplicationHidden";
        }
        case TRANSACTION_createAndManageUser:
        {
          return "createAndManageUser";
        }
        case TRANSACTION_removeUser:
        {
          return "removeUser";
        }
        case TRANSACTION_switchUser:
        {
          return "switchUser";
        }
        case TRANSACTION_startUserInBackground:
        {
          return "startUserInBackground";
        }
        case TRANSACTION_stopUser:
        {
          return "stopUser";
        }
        case TRANSACTION_logoutUser:
        {
          return "logoutUser";
        }
        case TRANSACTION_getSecondaryUsers:
        {
          return "getSecondaryUsers";
        }
        case TRANSACTION_enableSystemApp:
        {
          return "enableSystemApp";
        }
        case TRANSACTION_enableSystemAppWithIntent:
        {
          return "enableSystemAppWithIntent";
        }
        case TRANSACTION_installExistingPackage:
        {
          return "installExistingPackage";
        }
        case TRANSACTION_setAccountManagementDisabled:
        {
          return "setAccountManagementDisabled";
        }
        case TRANSACTION_getAccountTypesWithManagementDisabled:
        {
          return "getAccountTypesWithManagementDisabled";
        }
        case TRANSACTION_getAccountTypesWithManagementDisabledAsUser:
        {
          return "getAccountTypesWithManagementDisabledAsUser";
        }
        case TRANSACTION_setLockTaskPackages:
        {
          return "setLockTaskPackages";
        }
        case TRANSACTION_getLockTaskPackages:
        {
          return "getLockTaskPackages";
        }
        case TRANSACTION_isLockTaskPermitted:
        {
          return "isLockTaskPermitted";
        }
        case TRANSACTION_setLockTaskFeatures:
        {
          return "setLockTaskFeatures";
        }
        case TRANSACTION_getLockTaskFeatures:
        {
          return "getLockTaskFeatures";
        }
        case TRANSACTION_setGlobalSetting:
        {
          return "setGlobalSetting";
        }
        case TRANSACTION_setSystemSetting:
        {
          return "setSystemSetting";
        }
        case TRANSACTION_setSecureSetting:
        {
          return "setSecureSetting";
        }
        case TRANSACTION_setTime:
        {
          return "setTime";
        }
        case TRANSACTION_setTimeZone:
        {
          return "setTimeZone";
        }
        case TRANSACTION_setMasterVolumeMuted:
        {
          return "setMasterVolumeMuted";
        }
        case TRANSACTION_isMasterVolumeMuted:
        {
          return "isMasterVolumeMuted";
        }
        case TRANSACTION_notifyLockTaskModeChanged:
        {
          return "notifyLockTaskModeChanged";
        }
        case TRANSACTION_setUninstallBlocked:
        {
          return "setUninstallBlocked";
        }
        case TRANSACTION_isUninstallBlocked:
        {
          return "isUninstallBlocked";
        }
        case TRANSACTION_setCrossProfileCallerIdDisabled:
        {
          return "setCrossProfileCallerIdDisabled";
        }
        case TRANSACTION_getCrossProfileCallerIdDisabled:
        {
          return "getCrossProfileCallerIdDisabled";
        }
        case TRANSACTION_getCrossProfileCallerIdDisabledForUser:
        {
          return "getCrossProfileCallerIdDisabledForUser";
        }
        case TRANSACTION_setCrossProfileContactsSearchDisabled:
        {
          return "setCrossProfileContactsSearchDisabled";
        }
        case TRANSACTION_getCrossProfileContactsSearchDisabled:
        {
          return "getCrossProfileContactsSearchDisabled";
        }
        case TRANSACTION_getCrossProfileContactsSearchDisabledForUser:
        {
          return "getCrossProfileContactsSearchDisabledForUser";
        }
        case TRANSACTION_startManagedQuickContact:
        {
          return "startManagedQuickContact";
        }
        case TRANSACTION_setBluetoothContactSharingDisabled:
        {
          return "setBluetoothContactSharingDisabled";
        }
        case TRANSACTION_getBluetoothContactSharingDisabled:
        {
          return "getBluetoothContactSharingDisabled";
        }
        case TRANSACTION_getBluetoothContactSharingDisabledForUser:
        {
          return "getBluetoothContactSharingDisabledForUser";
        }
        case TRANSACTION_setTrustAgentConfiguration:
        {
          return "setTrustAgentConfiguration";
        }
        case TRANSACTION_getTrustAgentConfiguration:
        {
          return "getTrustAgentConfiguration";
        }
        case TRANSACTION_addCrossProfileWidgetProvider:
        {
          return "addCrossProfileWidgetProvider";
        }
        case TRANSACTION_removeCrossProfileWidgetProvider:
        {
          return "removeCrossProfileWidgetProvider";
        }
        case TRANSACTION_getCrossProfileWidgetProviders:
        {
          return "getCrossProfileWidgetProviders";
        }
        case TRANSACTION_setAutoTimeRequired:
        {
          return "setAutoTimeRequired";
        }
        case TRANSACTION_getAutoTimeRequired:
        {
          return "getAutoTimeRequired";
        }
        case TRANSACTION_setForceEphemeralUsers:
        {
          return "setForceEphemeralUsers";
        }
        case TRANSACTION_getForceEphemeralUsers:
        {
          return "getForceEphemeralUsers";
        }
        case TRANSACTION_isRemovingAdmin:
        {
          return "isRemovingAdmin";
        }
        case TRANSACTION_setUserIcon:
        {
          return "setUserIcon";
        }
        case TRANSACTION_setSystemUpdatePolicy:
        {
          return "setSystemUpdatePolicy";
        }
        case TRANSACTION_getSystemUpdatePolicy:
        {
          return "getSystemUpdatePolicy";
        }
        case TRANSACTION_clearSystemUpdatePolicyFreezePeriodRecord:
        {
          return "clearSystemUpdatePolicyFreezePeriodRecord";
        }
        case TRANSACTION_setKeyguardDisabled:
        {
          return "setKeyguardDisabled";
        }
        case TRANSACTION_setStatusBarDisabled:
        {
          return "setStatusBarDisabled";
        }
        case TRANSACTION_getDoNotAskCredentialsOnBoot:
        {
          return "getDoNotAskCredentialsOnBoot";
        }
        case TRANSACTION_notifyPendingSystemUpdate:
        {
          return "notifyPendingSystemUpdate";
        }
        case TRANSACTION_getPendingSystemUpdate:
        {
          return "getPendingSystemUpdate";
        }
        case TRANSACTION_setPermissionPolicy:
        {
          return "setPermissionPolicy";
        }
        case TRANSACTION_getPermissionPolicy:
        {
          return "getPermissionPolicy";
        }
        case TRANSACTION_setPermissionGrantState:
        {
          return "setPermissionGrantState";
        }
        case TRANSACTION_getPermissionGrantState:
        {
          return "getPermissionGrantState";
        }
        case TRANSACTION_isProvisioningAllowed:
        {
          return "isProvisioningAllowed";
        }
        case TRANSACTION_checkProvisioningPreCondition:
        {
          return "checkProvisioningPreCondition";
        }
        case TRANSACTION_setKeepUninstalledPackages:
        {
          return "setKeepUninstalledPackages";
        }
        case TRANSACTION_getKeepUninstalledPackages:
        {
          return "getKeepUninstalledPackages";
        }
        case TRANSACTION_isManagedProfile:
        {
          return "isManagedProfile";
        }
        case TRANSACTION_isSystemOnlyUser:
        {
          return "isSystemOnlyUser";
        }
        case TRANSACTION_getWifiMacAddress:
        {
          return "getWifiMacAddress";
        }
        case TRANSACTION_reboot:
        {
          return "reboot";
        }
        case TRANSACTION_setShortSupportMessage:
        {
          return "setShortSupportMessage";
        }
        case TRANSACTION_getShortSupportMessage:
        {
          return "getShortSupportMessage";
        }
        case TRANSACTION_setLongSupportMessage:
        {
          return "setLongSupportMessage";
        }
        case TRANSACTION_getLongSupportMessage:
        {
          return "getLongSupportMessage";
        }
        case TRANSACTION_getShortSupportMessageForUser:
        {
          return "getShortSupportMessageForUser";
        }
        case TRANSACTION_getLongSupportMessageForUser:
        {
          return "getLongSupportMessageForUser";
        }
        case TRANSACTION_isSeparateProfileChallengeAllowed:
        {
          return "isSeparateProfileChallengeAllowed";
        }
        case TRANSACTION_setOrganizationColor:
        {
          return "setOrganizationColor";
        }
        case TRANSACTION_setOrganizationColorForUser:
        {
          return "setOrganizationColorForUser";
        }
        case TRANSACTION_getOrganizationColor:
        {
          return "getOrganizationColor";
        }
        case TRANSACTION_getOrganizationColorForUser:
        {
          return "getOrganizationColorForUser";
        }
        case TRANSACTION_setOrganizationName:
        {
          return "setOrganizationName";
        }
        case TRANSACTION_getOrganizationName:
        {
          return "getOrganizationName";
        }
        case TRANSACTION_getDeviceOwnerOrganizationName:
        {
          return "getDeviceOwnerOrganizationName";
        }
        case TRANSACTION_getOrganizationNameForUser:
        {
          return "getOrganizationNameForUser";
        }
        case TRANSACTION_getUserProvisioningState:
        {
          return "getUserProvisioningState";
        }
        case TRANSACTION_setUserProvisioningState:
        {
          return "setUserProvisioningState";
        }
        case TRANSACTION_setAffiliationIds:
        {
          return "setAffiliationIds";
        }
        case TRANSACTION_getAffiliationIds:
        {
          return "getAffiliationIds";
        }
        case TRANSACTION_isAffiliatedUser:
        {
          return "isAffiliatedUser";
        }
        case TRANSACTION_setSecurityLoggingEnabled:
        {
          return "setSecurityLoggingEnabled";
        }
        case TRANSACTION_isSecurityLoggingEnabled:
        {
          return "isSecurityLoggingEnabled";
        }
        case TRANSACTION_retrieveSecurityLogs:
        {
          return "retrieveSecurityLogs";
        }
        case TRANSACTION_retrievePreRebootSecurityLogs:
        {
          return "retrievePreRebootSecurityLogs";
        }
        case TRANSACTION_forceNetworkLogs:
        {
          return "forceNetworkLogs";
        }
        case TRANSACTION_forceSecurityLogs:
        {
          return "forceSecurityLogs";
        }
        case TRANSACTION_isUninstallInQueue:
        {
          return "isUninstallInQueue";
        }
        case TRANSACTION_uninstallPackageWithActiveAdmins:
        {
          return "uninstallPackageWithActiveAdmins";
        }
        case TRANSACTION_isDeviceProvisioned:
        {
          return "isDeviceProvisioned";
        }
        case TRANSACTION_isDeviceProvisioningConfigApplied:
        {
          return "isDeviceProvisioningConfigApplied";
        }
        case TRANSACTION_setDeviceProvisioningConfigApplied:
        {
          return "setDeviceProvisioningConfigApplied";
        }
        case TRANSACTION_forceUpdateUserSetupComplete:
        {
          return "forceUpdateUserSetupComplete";
        }
        case TRANSACTION_setBackupServiceEnabled:
        {
          return "setBackupServiceEnabled";
        }
        case TRANSACTION_isBackupServiceEnabled:
        {
          return "isBackupServiceEnabled";
        }
        case TRANSACTION_setNetworkLoggingEnabled:
        {
          return "setNetworkLoggingEnabled";
        }
        case TRANSACTION_isNetworkLoggingEnabled:
        {
          return "isNetworkLoggingEnabled";
        }
        case TRANSACTION_retrieveNetworkLogs:
        {
          return "retrieveNetworkLogs";
        }
        case TRANSACTION_bindDeviceAdminServiceAsUser:
        {
          return "bindDeviceAdminServiceAsUser";
        }
        case TRANSACTION_getBindDeviceAdminTargetUsers:
        {
          return "getBindDeviceAdminTargetUsers";
        }
        case TRANSACTION_isEphemeralUser:
        {
          return "isEphemeralUser";
        }
        case TRANSACTION_getLastSecurityLogRetrievalTime:
        {
          return "getLastSecurityLogRetrievalTime";
        }
        case TRANSACTION_getLastBugReportRequestTime:
        {
          return "getLastBugReportRequestTime";
        }
        case TRANSACTION_getLastNetworkLogRetrievalTime:
        {
          return "getLastNetworkLogRetrievalTime";
        }
        case TRANSACTION_setResetPasswordToken:
        {
          return "setResetPasswordToken";
        }
        case TRANSACTION_clearResetPasswordToken:
        {
          return "clearResetPasswordToken";
        }
        case TRANSACTION_isResetPasswordTokenActive:
        {
          return "isResetPasswordTokenActive";
        }
        case TRANSACTION_resetPasswordWithToken:
        {
          return "resetPasswordWithToken";
        }
        case TRANSACTION_isCurrentInputMethodSetByOwner:
        {
          return "isCurrentInputMethodSetByOwner";
        }
        case TRANSACTION_getOwnerInstalledCaCerts:
        {
          return "getOwnerInstalledCaCerts";
        }
        case TRANSACTION_clearApplicationUserData:
        {
          return "clearApplicationUserData";
        }
        case TRANSACTION_setLogoutEnabled:
        {
          return "setLogoutEnabled";
        }
        case TRANSACTION_isLogoutEnabled:
        {
          return "isLogoutEnabled";
        }
        case TRANSACTION_getDisallowedSystemApps:
        {
          return "getDisallowedSystemApps";
        }
        case TRANSACTION_transferOwnership:
        {
          return "transferOwnership";
        }
        case TRANSACTION_getTransferOwnershipBundle:
        {
          return "getTransferOwnershipBundle";
        }
        case TRANSACTION_setStartUserSessionMessage:
        {
          return "setStartUserSessionMessage";
        }
        case TRANSACTION_setEndUserSessionMessage:
        {
          return "setEndUserSessionMessage";
        }
        case TRANSACTION_getStartUserSessionMessage:
        {
          return "getStartUserSessionMessage";
        }
        case TRANSACTION_getEndUserSessionMessage:
        {
          return "getEndUserSessionMessage";
        }
        case TRANSACTION_setMeteredDataDisabledPackages:
        {
          return "setMeteredDataDisabledPackages";
        }
        case TRANSACTION_getMeteredDataDisabledPackages:
        {
          return "getMeteredDataDisabledPackages";
        }
        case TRANSACTION_addOverrideApn:
        {
          return "addOverrideApn";
        }
        case TRANSACTION_updateOverrideApn:
        {
          return "updateOverrideApn";
        }
        case TRANSACTION_removeOverrideApn:
        {
          return "removeOverrideApn";
        }
        case TRANSACTION_getOverrideApns:
        {
          return "getOverrideApns";
        }
        case TRANSACTION_setOverrideApnsEnabled:
        {
          return "setOverrideApnsEnabled";
        }
        case TRANSACTION_isOverrideApnEnabled:
        {
          return "isOverrideApnEnabled";
        }
        case TRANSACTION_isMeteredDataDisabledPackageForUser:
        {
          return "isMeteredDataDisabledPackageForUser";
        }
        case TRANSACTION_setGlobalPrivateDns:
        {
          return "setGlobalPrivateDns";
        }
        case TRANSACTION_getGlobalPrivateDnsMode:
        {
          return "getGlobalPrivateDnsMode";
        }
        case TRANSACTION_getGlobalPrivateDnsHost:
        {
          return "getGlobalPrivateDnsHost";
        }
        case TRANSACTION_grantDeviceIdsAccessToProfileOwner:
        {
          return "grantDeviceIdsAccessToProfileOwner";
        }
        case TRANSACTION_installUpdateFromFile:
        {
          return "installUpdateFromFile";
        }
        case TRANSACTION_setCrossProfileCalendarPackages:
        {
          return "setCrossProfileCalendarPackages";
        }
        case TRANSACTION_getCrossProfileCalendarPackages:
        {
          return "getCrossProfileCalendarPackages";
        }
        case TRANSACTION_isPackageAllowedToAccessCalendarForUser:
        {
          return "isPackageAllowedToAccessCalendarForUser";
        }
        case TRANSACTION_getCrossProfileCalendarPackagesForUser:
        {
          return "getCrossProfileCalendarPackagesForUser";
        }
        case TRANSACTION_isManagedKiosk:
        {
          return "isManagedKiosk";
        }
        case TRANSACTION_isUnattendedManagedKiosk:
        {
          return "isUnattendedManagedKiosk";
        }
        case TRANSACTION_startViewCalendarEventInManagedProfile:
        {
          return "startViewCalendarEventInManagedProfile";
        }
        case TRANSACTION_requireSecureKeyguard:
        {
          return "requireSecureKeyguard";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_setPasswordQuality:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordQuality(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordQuality:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordQuality(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordMinimumLength:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordMinimumLength(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordMinimumLength:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordMinimumLength(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordMinimumUpperCase:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordMinimumUpperCase(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordMinimumUpperCase:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordMinimumUpperCase(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordMinimumLowerCase:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordMinimumLowerCase(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordMinimumLowerCase:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordMinimumLowerCase(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordMinimumLetters:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordMinimumLetters(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordMinimumLetters:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordMinimumLetters(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordMinimumNumeric:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordMinimumNumeric(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordMinimumNumeric:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordMinimumNumeric(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordMinimumSymbols:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordMinimumSymbols(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordMinimumSymbols:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordMinimumSymbols(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordMinimumNonLetter:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordMinimumNonLetter(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordMinimumNonLetter:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordMinimumNonLetter(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordHistoryLength:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordHistoryLength(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordHistoryLength:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getPasswordHistoryLength(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPasswordExpirationTimeout:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          long _arg1;
          _arg1 = data.readLong();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setPasswordExpirationTimeout(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordExpirationTimeout:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          long _result = this.getPasswordExpirationTimeout(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getPasswordExpiration:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          long _result = this.getPasswordExpiration(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_isActivePasswordSufficient:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          boolean _result = this.isActivePasswordSufficient(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isProfileActivePasswordSufficientForParent:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.isProfileActivePasswordSufficientForParent(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getPasswordComplexity:
        {
          data.enforceInterface(descriptor);
          int _result = this.getPasswordComplexity();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isUsingUnifiedPassword:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isUsingUnifiedPassword(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getCurrentFailedPasswordAttempts:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          int _result = this.getCurrentFailedPasswordAttempts(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getProfileWithMinimumFailedPasswordsForWipe:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          int _result = this.getProfileWithMinimumFailedPasswordsForWipe(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setMaximumFailedPasswordsForWipe:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setMaximumFailedPasswordsForWipe(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getMaximumFailedPasswordsForWipe:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getMaximumFailedPasswordsForWipe(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_resetPassword:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.resetPassword(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setMaximumTimeToLock:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          long _arg1;
          _arg1 = data.readLong();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setMaximumTimeToLock(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getMaximumTimeToLock:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          long _result = this.getMaximumTimeToLock(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_setRequiredStrongAuthTimeout:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          long _arg1;
          _arg1 = data.readLong();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setRequiredStrongAuthTimeout(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getRequiredStrongAuthTimeout:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          long _result = this.getRequiredStrongAuthTimeout(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_lockNow:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.lockNow(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_wipeDataWithReason:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.wipeDataWithReason(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setGlobalProxy:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          android.content.ComponentName _result = this.setGlobalProxy(_arg0, _arg1, _arg2);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getGlobalProxyAdmin:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.content.ComponentName _result = this.getGlobalProxyAdmin(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setRecommendedGlobalProxy:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.net.ProxyInfo _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.net.ProxyInfo.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setRecommendedGlobalProxy(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setStorageEncryption:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          int _result = this.setStorageEncryption(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getStorageEncryption:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.getStorageEncryption(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getStorageEncryptionStatus:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.getStorageEncryptionStatus(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_requestBugreport:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.requestBugreport(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setCameraDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setCameraDisabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getCameraDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.getCameraDisabled(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setScreenCaptureDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setScreenCaptureDisabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getScreenCaptureDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.getScreenCaptureDisabled(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setKeyguardDisabledFeatures:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setKeyguardDisabledFeatures(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getKeyguardDisabledFeatures:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.getKeyguardDisabledFeatures(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setActiveAdmin:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          int _arg2;
          _arg2 = data.readInt();
          this.setActiveAdmin(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isAdminActive:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.isAdminActive(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getActiveAdmins:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.util.List<android.content.ComponentName> _result = this.getActiveAdmins(_arg0);
          reply.writeNoException();
          reply.writeTypedList(_result);
          return true;
        }
        case TRANSACTION_packageHasActiveAdmins:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.packageHasActiveAdmins(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getRemoveWarning:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.RemoteCallback _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.RemoteCallback.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _arg2;
          _arg2 = data.readInt();
          this.getRemoveWarning(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_removeActiveAdmin:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.removeActiveAdmin(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_forceRemoveActiveAdmin:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.forceRemoveActiveAdmin(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_hasGrantedPolicy:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          boolean _result = this.hasGrantedPolicy(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setActivePasswordState:
        {
          data.enforceInterface(descriptor);
          android.app.admin.PasswordMetrics _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.app.admin.PasswordMetrics.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.setActivePasswordState(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reportPasswordChanged:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.reportPasswordChanged(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reportFailedPasswordAttempt:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.reportFailedPasswordAttempt(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reportSuccessfulPasswordAttempt:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.reportSuccessfulPasswordAttempt(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reportFailedBiometricAttempt:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.reportFailedBiometricAttempt(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reportSuccessfulBiometricAttempt:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.reportSuccessfulBiometricAttempt(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reportKeyguardDismissed:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.reportKeyguardDismissed(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reportKeyguardSecured:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.reportKeyguardSecured(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setDeviceOwner:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          boolean _result = this.setDeviceOwner(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getDeviceOwnerComponent:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          android.content.ComponentName _result = this.getDeviceOwnerComponent(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_hasDeviceOwner:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.hasDeviceOwner();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getDeviceOwnerName:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getDeviceOwnerName();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_clearDeviceOwner:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.clearDeviceOwner(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getDeviceOwnerUserId:
        {
          data.enforceInterface(descriptor);
          int _result = this.getDeviceOwnerUserId();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setProfileOwner:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          boolean _result = this.setProfileOwner(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getProfileOwnerAsUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.content.ComponentName _result = this.getProfileOwnerAsUser(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getProfileOwner:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.content.ComponentName _result = this.getProfileOwner(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getProfileOwnerName:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _result = this.getProfileOwnerName(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_setProfileEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.setProfileEnabled(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setProfileName:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.setProfileName(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_clearProfileOwner:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.clearProfileOwner(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_hasUserSetupCompleted:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.hasUserSetupCompleted();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_checkDeviceIdentifierAccess:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          boolean _result = this.checkDeviceIdentifierAccess(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setDeviceOwnerLockScreenInfo:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _arg1;
          if (0!=data.readInt()) {
            _arg1 = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setDeviceOwnerLockScreenInfo(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getDeviceOwnerLockScreenInfo:
        {
          data.enforceInterface(descriptor);
          java.lang.CharSequence _result = this.getDeviceOwnerLockScreenInfo();
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setPackagesSuspended:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String[] _arg2;
          _arg2 = data.createStringArray();
          boolean _arg3;
          _arg3 = (0!=data.readInt());
          java.lang.String[] _result = this.setPackagesSuspended(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeStringArray(_result);
          return true;
        }
        case TRANSACTION_isPackageSuspended:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _result = this.isPackageSuspended(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_installCaCert:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          boolean _result = this.installCaCert(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_uninstallCaCerts:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String[] _arg2;
          _arg2 = data.createStringArray();
          this.uninstallCaCerts(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_enforceCanManageCaCerts:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.enforceCanManageCaCerts(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_approveCaCert:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          boolean _result = this.approveCaCert(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isCaCertApproved:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.isCaCertApproved(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_installKeyPair:
        {
          return this.onTransact$installKeyPair$(data, reply);
        }
        case TRANSACTION_removeKeyPair:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _result = this.removeKeyPair(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_generateKeyPair:
        {
          return this.onTransact$generateKeyPair$(data, reply);
        }
        case TRANSACTION_setKeyPairCertificate:
        {
          return this.onTransact$setKeyPairCertificate$(data, reply);
        }
        case TRANSACTION_choosePrivateKeyAlias:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.net.Uri _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.net.Uri.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          java.lang.String _arg2;
          _arg2 = data.readString();
          android.os.IBinder _arg3;
          _arg3 = data.readStrongBinder();
          this.choosePrivateKeyAlias(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setDelegatedScopes:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.util.List<java.lang.String> _arg2;
          _arg2 = data.createStringArrayList();
          this.setDelegatedScopes(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getDelegatedScopes:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.util.List<java.lang.String> _result = this.getDelegatedScopes(_arg0, _arg1);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_getDelegatePackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.util.List<java.lang.String> _result = this.getDelegatePackages(_arg0, _arg1);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_setCertInstallerPackage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.setCertInstallerPackage(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getCertInstallerPackage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getCertInstallerPackage(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_setAlwaysOnVpnPackage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          java.util.List<java.lang.String> _arg3;
          _arg3 = data.createStringArrayList();
          boolean _result = this.setAlwaysOnVpnPackage(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getAlwaysOnVpnPackage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getAlwaysOnVpnPackage(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_isAlwaysOnVpnLockdownEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isAlwaysOnVpnLockdownEnabled(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getAlwaysOnVpnLockdownWhitelist:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _result = this.getAlwaysOnVpnLockdownWhitelist(_arg0);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_addPersistentPreferredActivity:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.IntentFilter _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.content.IntentFilter.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          android.content.ComponentName _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          this.addPersistentPreferredActivity(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_clearPackagePersistentPreferredActivities:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.clearPackagePersistentPreferredActivities(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setDefaultSmsApplication:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.setDefaultSmsApplication(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setApplicationRestrictions:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          android.os.Bundle _arg3;
          if ((0!=data.readInt())) {
            _arg3 = android.os.Bundle.CREATOR.createFromParcel(data);
          }
          else {
            _arg3 = null;
          }
          this.setApplicationRestrictions(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getApplicationRestrictions:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          android.os.Bundle _result = this.getApplicationRestrictions(_arg0, _arg1, _arg2);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setApplicationRestrictionsManagingPackage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.setApplicationRestrictionsManagingPackage(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getApplicationRestrictionsManagingPackage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getApplicationRestrictionsManagingPackage(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_isCallerApplicationRestrictionsManagingPackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.isCallerApplicationRestrictionsManagingPackage(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setRestrictionsProvider:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.ComponentName _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setRestrictionsProvider(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getRestrictionsProvider:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.content.ComponentName _result = this.getRestrictionsProvider(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setUserRestriction:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setUserRestriction(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getUserRestrictions:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.Bundle _result = this.getUserRestrictions(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_addCrossProfileIntentFilter:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.IntentFilter _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.content.IntentFilter.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _arg2;
          _arg2 = data.readInt();
          this.addCrossProfileIntentFilter(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_clearCrossProfileIntentFilters:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.clearCrossProfileIntentFilters(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setPermittedAccessibilityServices:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List _arg1;
          java.lang.ClassLoader cl = (java.lang.ClassLoader)this.getClass().getClassLoader();
          _arg1 = data.readArrayList(cl);
          boolean _result = this.setPermittedAccessibilityServices(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getPermittedAccessibilityServices:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List _result = this.getPermittedAccessibilityServices(_arg0);
          reply.writeNoException();
          reply.writeList(_result);
          return true;
        }
        case TRANSACTION_getPermittedAccessibilityServicesForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.util.List _result = this.getPermittedAccessibilityServicesForUser(_arg0);
          reply.writeNoException();
          reply.writeList(_result);
          return true;
        }
        case TRANSACTION_isAccessibilityServicePermittedByAdmin:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          boolean _result = this.isAccessibilityServicePermittedByAdmin(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setPermittedInputMethods:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List _arg1;
          java.lang.ClassLoader cl = (java.lang.ClassLoader)this.getClass().getClassLoader();
          _arg1 = data.readArrayList(cl);
          boolean _result = this.setPermittedInputMethods(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getPermittedInputMethods:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List _result = this.getPermittedInputMethods(_arg0);
          reply.writeNoException();
          reply.writeList(_result);
          return true;
        }
        case TRANSACTION_getPermittedInputMethodsForCurrentUser:
        {
          data.enforceInterface(descriptor);
          java.util.List _result = this.getPermittedInputMethodsForCurrentUser();
          reply.writeNoException();
          reply.writeList(_result);
          return true;
        }
        case TRANSACTION_isInputMethodPermittedByAdmin:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          boolean _result = this.isInputMethodPermittedByAdmin(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setPermittedCrossProfileNotificationListeners:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _arg1;
          _arg1 = data.createStringArrayList();
          boolean _result = this.setPermittedCrossProfileNotificationListeners(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getPermittedCrossProfileNotificationListeners:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _result = this.getPermittedCrossProfileNotificationListeners(_arg0);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_isNotificationListenerServicePermitted:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.isNotificationListenerServicePermitted(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_createAdminSupportIntent:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.content.Intent _result = this.createAdminSupportIntent(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setApplicationHidden:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _arg3;
          _arg3 = (0!=data.readInt());
          boolean _result = this.setApplicationHidden(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isApplicationHidden:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _result = this.isApplicationHidden(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_createAndManageUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.content.ComponentName _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          android.os.PersistableBundle _arg3;
          if ((0!=data.readInt())) {
            _arg3 = android.os.PersistableBundle.CREATOR.createFromParcel(data);
          }
          else {
            _arg3 = null;
          }
          int _arg4;
          _arg4 = data.readInt();
          android.os.UserHandle _result = this.createAndManageUser(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_removeUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.UserHandle _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.UserHandle.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          boolean _result = this.removeUser(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_switchUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.UserHandle _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.UserHandle.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          boolean _result = this.switchUser(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_startUserInBackground:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.UserHandle _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.UserHandle.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _result = this.startUserInBackground(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_stopUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.UserHandle _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.UserHandle.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _result = this.stopUser(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_logoutUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.logoutUser(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getSecondaryUsers:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<android.os.UserHandle> _result = this.getSecondaryUsers(_arg0);
          reply.writeNoException();
          reply.writeTypedList(_result);
          return true;
        }
        case TRANSACTION_enableSystemApp:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.enableSystemApp(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_enableSystemAppWithIntent:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.content.Intent _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.content.Intent.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          int _result = this.enableSystemAppWithIntent(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_installExistingPackage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _result = this.installExistingPackage(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setAccountManagementDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setAccountManagementDisabled(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getAccountTypesWithManagementDisabled:
        {
          data.enforceInterface(descriptor);
          java.lang.String[] _result = this.getAccountTypesWithManagementDisabled();
          reply.writeNoException();
          reply.writeStringArray(_result);
          return true;
        }
        case TRANSACTION_getAccountTypesWithManagementDisabledAsUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String[] _result = this.getAccountTypesWithManagementDisabledAsUser(_arg0);
          reply.writeNoException();
          reply.writeStringArray(_result);
          return true;
        }
        case TRANSACTION_setLockTaskPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String[] _arg1;
          _arg1 = data.createStringArray();
          this.setLockTaskPackages(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getLockTaskPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String[] _result = this.getLockTaskPackages(_arg0);
          reply.writeNoException();
          reply.writeStringArray(_result);
          return true;
        }
        case TRANSACTION_isLockTaskPermitted:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.isLockTaskPermitted(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setLockTaskFeatures:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.setLockTaskFeatures(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getLockTaskFeatures:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getLockTaskFeatures(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setGlobalSetting:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.setGlobalSetting(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setSystemSetting:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.setSystemSetting(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setSecureSetting:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.setSecureSetting(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setTime:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          long _arg1;
          _arg1 = data.readLong();
          boolean _result = this.setTime(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setTimeZone:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.setTimeZone(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setMasterVolumeMuted:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setMasterVolumeMuted(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isMasterVolumeMuted:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isMasterVolumeMuted(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_notifyLockTaskModeChanged:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.notifyLockTaskModeChanged(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setUninstallBlocked:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _arg3;
          _arg3 = (0!=data.readInt());
          this.setUninstallBlocked(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isUninstallBlocked:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.isUninstallBlocked(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setCrossProfileCallerIdDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setCrossProfileCallerIdDisabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getCrossProfileCallerIdDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.getCrossProfileCallerIdDisabled(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getCrossProfileCallerIdDisabledForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.getCrossProfileCallerIdDisabledForUser(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setCrossProfileContactsSearchDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setCrossProfileContactsSearchDisabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getCrossProfileContactsSearchDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.getCrossProfileContactsSearchDisabled(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getCrossProfileContactsSearchDisabledForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.getCrossProfileContactsSearchDisabledForUser(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_startManagedQuickContact:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          long _arg1;
          _arg1 = data.readLong();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          long _arg3;
          _arg3 = data.readLong();
          android.content.Intent _arg4;
          if ((0!=data.readInt())) {
            _arg4 = android.content.Intent.CREATOR.createFromParcel(data);
          }
          else {
            _arg4 = null;
          }
          this.startManagedQuickContact(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setBluetoothContactSharingDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setBluetoothContactSharingDisabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getBluetoothContactSharingDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.getBluetoothContactSharingDisabled(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getBluetoothContactSharingDisabledForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.getBluetoothContactSharingDisabledForUser(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setTrustAgentConfiguration:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.ComponentName _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          android.os.PersistableBundle _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.os.PersistableBundle.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          boolean _arg3;
          _arg3 = (0!=data.readInt());
          this.setTrustAgentConfiguration(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getTrustAgentConfiguration:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.ComponentName _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _arg2;
          _arg2 = data.readInt();
          boolean _arg3;
          _arg3 = (0!=data.readInt());
          java.util.List<android.os.PersistableBundle> _result = this.getTrustAgentConfiguration(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeTypedList(_result);
          return true;
        }
        case TRANSACTION_addCrossProfileWidgetProvider:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.addCrossProfileWidgetProvider(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_removeCrossProfileWidgetProvider:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.removeCrossProfileWidgetProvider(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getCrossProfileWidgetProviders:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _result = this.getCrossProfileWidgetProviders(_arg0);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_setAutoTimeRequired:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setAutoTimeRequired(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getAutoTimeRequired:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.getAutoTimeRequired();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setForceEphemeralUsers:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setForceEphemeralUsers(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getForceEphemeralUsers:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.getForceEphemeralUsers(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isRemovingAdmin:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.isRemovingAdmin(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setUserIcon:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.graphics.Bitmap _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.graphics.Bitmap.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setUserIcon(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setSystemUpdatePolicy:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.app.admin.SystemUpdatePolicy _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.app.admin.SystemUpdatePolicy.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setSystemUpdatePolicy(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getSystemUpdatePolicy:
        {
          data.enforceInterface(descriptor);
          android.app.admin.SystemUpdatePolicy _result = this.getSystemUpdatePolicy();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_clearSystemUpdatePolicyFreezePeriodRecord:
        {
          data.enforceInterface(descriptor);
          this.clearSystemUpdatePolicyFreezePeriodRecord();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setKeyguardDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          boolean _result = this.setKeyguardDisabled(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setStatusBarDisabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          boolean _result = this.setStatusBarDisabled(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getDoNotAskCredentialsOnBoot:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.getDoNotAskCredentialsOnBoot();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_notifyPendingSystemUpdate:
        {
          data.enforceInterface(descriptor);
          android.app.admin.SystemUpdateInfo _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.app.admin.SystemUpdateInfo.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.notifyPendingSystemUpdate(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPendingSystemUpdate:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.app.admin.SystemUpdateInfo _result = this.getPendingSystemUpdate(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setPermissionPolicy:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.setPermissionPolicy(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPermissionPolicy:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getPermissionPolicy(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setPermissionGrantState:
        {
          return this.onTransact$setPermissionGrantState$(data, reply);
        }
        case TRANSACTION_getPermissionGrantState:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _result = this.getPermissionGrantState(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isProvisioningAllowed:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.isProvisioningAllowed(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_checkProvisioningPreCondition:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _result = this.checkProvisioningPreCondition(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setKeepUninstalledPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.util.List<java.lang.String> _arg2;
          _arg2 = data.createStringArrayList();
          this.setKeepUninstalledPackages(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getKeepUninstalledPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.util.List<java.lang.String> _result = this.getKeepUninstalledPackages(_arg0, _arg1);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_isManagedProfile:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isManagedProfile(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isSystemOnlyUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isSystemOnlyUser(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getWifiMacAddress:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getWifiMacAddress(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_reboot:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.reboot(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setShortSupportMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _arg1;
          if (0!=data.readInt()) {
            _arg1 = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setShortSupportMessage(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getShortSupportMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _result = this.getShortSupportMessage(_arg0);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setLongSupportMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _arg1;
          if (0!=data.readInt()) {
            _arg1 = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setLongSupportMessage(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getLongSupportMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _result = this.getLongSupportMessage(_arg0);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getShortSupportMessageForUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          java.lang.CharSequence _result = this.getShortSupportMessageForUser(_arg0, _arg1);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getLongSupportMessageForUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          java.lang.CharSequence _result = this.getLongSupportMessageForUser(_arg0, _arg1);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_isSeparateProfileChallengeAllowed:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.isSeparateProfileChallengeAllowed(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setOrganizationColor:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.setOrganizationColor(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setOrganizationColorForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.setOrganizationColorForUser(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getOrganizationColor:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getOrganizationColor(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getOrganizationColorForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _result = this.getOrganizationColorForUser(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setOrganizationName:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _arg1;
          if (0!=data.readInt()) {
            _arg1 = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setOrganizationName(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getOrganizationName:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _result = this.getOrganizationName(_arg0);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getDeviceOwnerOrganizationName:
        {
          data.enforceInterface(descriptor);
          java.lang.CharSequence _result = this.getDeviceOwnerOrganizationName();
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getOrganizationNameForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.CharSequence _result = this.getOrganizationNameForUser(_arg0);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getUserProvisioningState:
        {
          data.enforceInterface(descriptor);
          int _result = this.getUserProvisioningState();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setUserProvisioningState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.setUserProvisioningState(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setAffiliationIds:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _arg1;
          _arg1 = data.createStringArrayList();
          this.setAffiliationIds(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getAffiliationIds:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _result = this.getAffiliationIds(_arg0);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_isAffiliatedUser:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isAffiliatedUser();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setSecurityLoggingEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setSecurityLoggingEnabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isSecurityLoggingEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isSecurityLoggingEnabled(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_retrieveSecurityLogs:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.pm.ParceledListSlice _result = this.retrieveSecurityLogs(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_retrievePreRebootSecurityLogs:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.pm.ParceledListSlice _result = this.retrievePreRebootSecurityLogs(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_forceNetworkLogs:
        {
          data.enforceInterface(descriptor);
          long _result = this.forceNetworkLogs();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_forceSecurityLogs:
        {
          data.enforceInterface(descriptor);
          long _result = this.forceSecurityLogs();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_isUninstallInQueue:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.isUninstallInQueue(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_uninstallPackageWithActiveAdmins:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.uninstallPackageWithActiveAdmins(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isDeviceProvisioned:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isDeviceProvisioned();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isDeviceProvisioningConfigApplied:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isDeviceProvisioningConfigApplied();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setDeviceProvisioningConfigApplied:
        {
          data.enforceInterface(descriptor);
          this.setDeviceProvisioningConfigApplied();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_forceUpdateUserSetupComplete:
        {
          data.enforceInterface(descriptor);
          this.forceUpdateUserSetupComplete();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setBackupServiceEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setBackupServiceEnabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isBackupServiceEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isBackupServiceEnabled(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setNetworkLoggingEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setNetworkLoggingEnabled(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isNetworkLoggingEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.isNetworkLoggingEnabled(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_retrieveNetworkLogs:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          long _arg2;
          _arg2 = data.readLong();
          java.util.List<android.app.admin.NetworkEvent> _result = this.retrieveNetworkLogs(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeTypedList(_result);
          return true;
        }
        case TRANSACTION_bindDeviceAdminServiceAsUser:
        {
          return this.onTransact$bindDeviceAdminServiceAsUser$(data, reply);
        }
        case TRANSACTION_getBindDeviceAdminTargetUsers:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<android.os.UserHandle> _result = this.getBindDeviceAdminTargetUsers(_arg0);
          reply.writeNoException();
          reply.writeTypedList(_result);
          return true;
        }
        case TRANSACTION_isEphemeralUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isEphemeralUser(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getLastSecurityLogRetrievalTime:
        {
          data.enforceInterface(descriptor);
          long _result = this.getLastSecurityLogRetrievalTime();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getLastBugReportRequestTime:
        {
          data.enforceInterface(descriptor);
          long _result = this.getLastBugReportRequestTime();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getLastNetworkLogRetrievalTime:
        {
          data.enforceInterface(descriptor);
          long _result = this.getLastNetworkLogRetrievalTime();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_setResetPasswordToken:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          byte[] _arg1;
          _arg1 = data.createByteArray();
          boolean _result = this.setResetPasswordToken(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_clearResetPasswordToken:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.clearResetPasswordToken(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isResetPasswordTokenActive:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isResetPasswordTokenActive(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_resetPasswordWithToken:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          int _arg3;
          _arg3 = data.readInt();
          boolean _result = this.resetPasswordWithToken(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isCurrentInputMethodSetByOwner:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isCurrentInputMethodSetByOwner();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getOwnerInstalledCaCerts:
        {
          data.enforceInterface(descriptor);
          android.os.UserHandle _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.UserHandle.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.pm.StringParceledListSlice _result = this.getOwnerInstalledCaCerts(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_clearApplicationUserData:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.content.pm.IPackageDataObserver _arg2;
          _arg2 = android.content.pm.IPackageDataObserver.Stub.asInterface(data.readStrongBinder());
          this.clearApplicationUserData(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setLogoutEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setLogoutEnabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isLogoutEnabled:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isLogoutEnabled();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getDisallowedSystemApps:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.util.List<java.lang.String> _result = this.getDisallowedSystemApps(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_transferOwnership:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.content.ComponentName _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          android.os.PersistableBundle _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.os.PersistableBundle.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          this.transferOwnership(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getTransferOwnershipBundle:
        {
          data.enforceInterface(descriptor);
          android.os.PersistableBundle _result = this.getTransferOwnershipBundle();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setStartUserSessionMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _arg1;
          if (0!=data.readInt()) {
            _arg1 = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setStartUserSessionMessage(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setEndUserSessionMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _arg1;
          if (0!=data.readInt()) {
            _arg1 = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.setEndUserSessionMessage(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getStartUserSessionMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _result = this.getStartUserSessionMessage(_arg0);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getEndUserSessionMessage:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.CharSequence _result = this.getEndUserSessionMessage(_arg0);
          reply.writeNoException();
          if (_result!=null) {
            reply.writeInt(1);
            android.text.TextUtils.writeToParcel(_result, reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setMeteredDataDisabledPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _arg1;
          _arg1 = data.createStringArrayList();
          java.util.List<java.lang.String> _result = this.setMeteredDataDisabledPackages(_arg0, _arg1);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_getMeteredDataDisabledPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _result = this.getMeteredDataDisabledPackages(_arg0);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_addOverrideApn:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.telephony.data.ApnSetting _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.telephony.data.ApnSetting.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _result = this.addOverrideApn(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_updateOverrideApn:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          android.telephony.data.ApnSetting _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.telephony.data.ApnSetting.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          boolean _result = this.updateOverrideApn(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_removeOverrideApn:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.removeOverrideApn(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getOverrideApns:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<android.telephony.data.ApnSetting> _result = this.getOverrideApns(_arg0);
          reply.writeNoException();
          reply.writeTypedList(_result);
          return true;
        }
        case TRANSACTION_setOverrideApnsEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setOverrideApnsEnabled(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isOverrideApnEnabled:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isOverrideApnEnabled(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isMeteredDataDisabledPackageForUser:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          boolean _result = this.isMeteredDataDisabledPackageForUser(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setGlobalPrivateDns:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          int _result = this.setGlobalPrivateDns(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getGlobalPrivateDnsMode:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getGlobalPrivateDnsMode(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getGlobalPrivateDnsHost:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getGlobalPrivateDnsHost(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_grantDeviceIdsAccessToProfileOwner:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.grantDeviceIdsAccessToProfileOwner(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_installUpdateFromFile:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.ParcelFileDescriptor _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.ParcelFileDescriptor.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          android.app.admin.StartInstallingUpdateCallback _arg2;
          _arg2 = android.app.admin.StartInstallingUpdateCallback.Stub.asInterface(data.readStrongBinder());
          this.installUpdateFromFile(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setCrossProfileCalendarPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _arg1;
          _arg1 = data.createStringArrayList();
          this.setCrossProfileCalendarPackages(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getCrossProfileCalendarPackages:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.util.List<java.lang.String> _result = this.getCrossProfileCalendarPackages(_arg0);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_isPackageAllowedToAccessCalendarForUser:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.isPackageAllowedToAccessCalendarForUser(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getCrossProfileCalendarPackagesForUser:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.util.List<java.lang.String> _result = this.getCrossProfileCalendarPackagesForUser(_arg0);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_isManagedKiosk:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isManagedKiosk();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isUnattendedManagedKiosk:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isUnattendedManagedKiosk();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_startViewCalendarEventInManagedProfile:
        {
          return this.onTransact$startViewCalendarEventInManagedProfile$(data, reply);
        }
        case TRANSACTION_requireSecureKeyguard:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.requireSecureKeyguard(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.app.admin.IDevicePolicyManager
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void setPasswordQuality(android.content.ComponentName who, int quality, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(quality);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordQuality, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordQuality(who, quality, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordQuality(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordQuality, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordQuality(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordMinimumLength(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordMinimumLength, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordMinimumLength(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordMinimumLength(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordMinimumLength, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordMinimumLength(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordMinimumUpperCase(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordMinimumUpperCase, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordMinimumUpperCase(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordMinimumUpperCase(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordMinimumUpperCase, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordMinimumUpperCase(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordMinimumLowerCase(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordMinimumLowerCase, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordMinimumLowerCase(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordMinimumLowerCase(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordMinimumLowerCase, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordMinimumLowerCase(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordMinimumLetters(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordMinimumLetters, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordMinimumLetters(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordMinimumLetters(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordMinimumLetters, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordMinimumLetters(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordMinimumNumeric(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordMinimumNumeric, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordMinimumNumeric(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordMinimumNumeric(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordMinimumNumeric, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordMinimumNumeric(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordMinimumSymbols(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordMinimumSymbols, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordMinimumSymbols(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordMinimumSymbols(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordMinimumSymbols, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordMinimumSymbols(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordMinimumNonLetter(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordMinimumNonLetter, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordMinimumNonLetter(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordMinimumNonLetter(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordMinimumNonLetter, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordMinimumNonLetter(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordHistoryLength(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(length);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordHistoryLength, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordHistoryLength(who, length, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPasswordHistoryLength(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordHistoryLength, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordHistoryLength(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPasswordExpirationTimeout(android.content.ComponentName who, long expiration, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeLong(expiration);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasswordExpirationTimeout, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPasswordExpirationTimeout(who, expiration, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public long getPasswordExpirationTimeout(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordExpirationTimeout, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordExpirationTimeout(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getPasswordExpiration(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordExpiration, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordExpiration(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isActivePasswordSufficient(int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_isActivePasswordSufficient, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isActivePasswordSufficient(userHandle, parent);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isProfileActivePasswordSufficientForParent(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isProfileActivePasswordSufficientForParent, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isProfileActivePasswordSufficientForParent(userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getPasswordComplexity() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordComplexity, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordComplexity();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isUsingUnifiedPassword(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isUsingUnifiedPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isUsingUnifiedPassword(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getCurrentFailedPasswordAttempts(int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCurrentFailedPasswordAttempts, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCurrentFailedPasswordAttempts(userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getProfileWithMinimumFailedPasswordsForWipe(int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getProfileWithMinimumFailedPasswordsForWipe, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getProfileWithMinimumFailedPasswordsForWipe(userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setMaximumFailedPasswordsForWipe(android.content.ComponentName admin, int num, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(num);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setMaximumFailedPasswordsForWipe, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setMaximumFailedPasswordsForWipe(admin, num, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getMaximumFailedPasswordsForWipe(android.content.ComponentName admin, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMaximumFailedPasswordsForWipe, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMaximumFailedPasswordsForWipe(admin, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean resetPassword(java.lang.String password, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(password);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_resetPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().resetPassword(password, flags);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setMaximumTimeToLock(android.content.ComponentName who, long timeMs, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeLong(timeMs);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setMaximumTimeToLock, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setMaximumTimeToLock(who, timeMs, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public long getMaximumTimeToLock(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMaximumTimeToLock, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMaximumTimeToLock(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setRequiredStrongAuthTimeout(android.content.ComponentName who, long timeMs, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeLong(timeMs);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setRequiredStrongAuthTimeout, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setRequiredStrongAuthTimeout(who, timeMs, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public long getRequiredStrongAuthTimeout(android.content.ComponentName who, int userId, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userId);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRequiredStrongAuthTimeout, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRequiredStrongAuthTimeout(who, userId, parent);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void lockNow(int flags, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(flags);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_lockNow, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().lockNow(flags, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void wipeDataWithReason(int flags, java.lang.String wipeReasonForUser) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(flags);
          _data.writeString(wipeReasonForUser);
          boolean _status = mRemote.transact(Stub.TRANSACTION_wipeDataWithReason, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().wipeDataWithReason(flags, wipeReasonForUser);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.content.ComponentName setGlobalProxy(android.content.ComponentName admin, java.lang.String proxySpec, java.lang.String exclusionList) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.ComponentName _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(proxySpec);
          _data.writeString(exclusionList);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setGlobalProxy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setGlobalProxy(admin, proxySpec, exclusionList);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.ComponentName.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.ComponentName getGlobalProxyAdmin(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.ComponentName _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getGlobalProxyAdmin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getGlobalProxyAdmin(userHandle);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.ComponentName.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setRecommendedGlobalProxy(android.content.ComponentName admin, android.net.ProxyInfo proxyInfo) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((proxyInfo!=null)) {
            _data.writeInt(1);
            proxyInfo.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setRecommendedGlobalProxy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setRecommendedGlobalProxy(admin, proxyInfo);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int setStorageEncryption(android.content.ComponentName who, boolean encrypt) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((encrypt)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setStorageEncryption, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setStorageEncryption(who, encrypt);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean getStorageEncryption(android.content.ComponentName who, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getStorageEncryption, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getStorageEncryption(who, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getStorageEncryptionStatus(java.lang.String callerPackage, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(callerPackage);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getStorageEncryptionStatus, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getStorageEncryptionStatus(callerPackage, userHandle);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean requestBugreport(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_requestBugreport, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().requestBugreport(who);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setCameraDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setCameraDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setCameraDisabled(who, disabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean getCameraDisabled(android.content.ComponentName who, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCameraDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCameraDisabled(who, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setScreenCaptureDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setScreenCaptureDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setScreenCaptureDisabled(who, disabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean getScreenCaptureDisabled(android.content.ComponentName who, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getScreenCaptureDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getScreenCaptureDisabled(who, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setKeyguardDisabledFeatures(android.content.ComponentName who, int which, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(which);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setKeyguardDisabledFeatures, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setKeyguardDisabledFeatures(who, which, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getKeyguardDisabledFeatures(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getKeyguardDisabledFeatures, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getKeyguardDisabledFeatures(who, userHandle, parent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setActiveAdmin(android.content.ComponentName policyReceiver, boolean refreshing, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((policyReceiver!=null)) {
            _data.writeInt(1);
            policyReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((refreshing)?(1):(0)));
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setActiveAdmin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setActiveAdmin(policyReceiver, refreshing, userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isAdminActive(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((policyReceiver!=null)) {
            _data.writeInt(1);
            policyReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isAdminActive, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isAdminActive(policyReceiver, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<android.content.ComponentName> getActiveAdmins(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<android.content.ComponentName> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getActiveAdmins, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getActiveAdmins(userHandle);
          }
          _reply.readException();
          _result = _reply.createTypedArrayList(android.content.ComponentName.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean packageHasActiveAdmins(java.lang.String packageName, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_packageHasActiveAdmins, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().packageHasActiveAdmins(packageName, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void getRemoveWarning(android.content.ComponentName policyReceiver, android.os.RemoteCallback result, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((policyReceiver!=null)) {
            _data.writeInt(1);
            policyReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((result!=null)) {
            _data.writeInt(1);
            result.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRemoveWarning, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().getRemoveWarning(policyReceiver, result, userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void removeActiveAdmin(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((policyReceiver!=null)) {
            _data.writeInt(1);
            policyReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeActiveAdmin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().removeActiveAdmin(policyReceiver, userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void forceRemoveActiveAdmin(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((policyReceiver!=null)) {
            _data.writeInt(1);
            policyReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_forceRemoveActiveAdmin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().forceRemoveActiveAdmin(policyReceiver, userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean hasGrantedPolicy(android.content.ComponentName policyReceiver, int usesPolicy, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((policyReceiver!=null)) {
            _data.writeInt(1);
            policyReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(usesPolicy);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_hasGrantedPolicy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().hasGrantedPolicy(policyReceiver, usesPolicy, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setActivePasswordState(android.app.admin.PasswordMetrics metrics, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((metrics!=null)) {
            _data.writeInt(1);
            metrics.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setActivePasswordState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setActivePasswordState(metrics, userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reportPasswordChanged(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportPasswordChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportPasswordChanged(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reportFailedPasswordAttempt(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportFailedPasswordAttempt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportFailedPasswordAttempt(userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reportSuccessfulPasswordAttempt(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportSuccessfulPasswordAttempt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportSuccessfulPasswordAttempt(userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reportFailedBiometricAttempt(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportFailedBiometricAttempt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportFailedBiometricAttempt(userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reportSuccessfulBiometricAttempt(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportSuccessfulBiometricAttempt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportSuccessfulBiometricAttempt(userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reportKeyguardDismissed(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportKeyguardDismissed, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportKeyguardDismissed(userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reportKeyguardSecured(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportKeyguardSecured, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportKeyguardSecured(userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean setDeviceOwner(android.content.ComponentName who, java.lang.String ownerName, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(ownerName);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setDeviceOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setDeviceOwner(who, ownerName, userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.ComponentName getDeviceOwnerComponent(boolean callingUserOnly) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.ComponentName _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((callingUserOnly)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDeviceOwnerComponent, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDeviceOwnerComponent(callingUserOnly);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.ComponentName.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean hasDeviceOwner() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_hasDeviceOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().hasDeviceOwner();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getDeviceOwnerName() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDeviceOwnerName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDeviceOwnerName();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void clearDeviceOwner(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearDeviceOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearDeviceOwner(packageName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getDeviceOwnerUserId() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDeviceOwnerUserId, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDeviceOwnerUserId();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setProfileOwner(android.content.ComponentName who, java.lang.String ownerName, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(ownerName);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setProfileOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setProfileOwner(who, ownerName, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.ComponentName getProfileOwnerAsUser(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.ComponentName _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getProfileOwnerAsUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getProfileOwnerAsUser(userHandle);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.ComponentName.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.ComponentName getProfileOwner(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.ComponentName _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getProfileOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getProfileOwner(userHandle);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.ComponentName.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getProfileOwnerName(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getProfileOwnerName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getProfileOwnerName(userHandle);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setProfileEnabled(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setProfileEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setProfileEnabled(who);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setProfileName(android.content.ComponentName who, java.lang.String profileName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(profileName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setProfileName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setProfileName(who, profileName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void clearProfileOwner(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearProfileOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearProfileOwner(who);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean hasUserSetupCompleted() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_hasUserSetupCompleted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().hasUserSetupCompleted();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean checkDeviceIdentifierAccess(java.lang.String packageName, int pid, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(pid);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_checkDeviceIdentifierAccess, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().checkDeviceIdentifierAccess(packageName, pid, uid);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setDeviceOwnerLockScreenInfo(android.content.ComponentName who, java.lang.CharSequence deviceOwnerInfo) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if (deviceOwnerInfo!=null) {
            _data.writeInt(1);
            android.text.TextUtils.writeToParcel(deviceOwnerInfo, _data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setDeviceOwnerLockScreenInfo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setDeviceOwnerLockScreenInfo(who, deviceOwnerInfo);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.CharSequence getDeviceOwnerLockScreenInfo() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDeviceOwnerLockScreenInfo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDeviceOwnerLockScreenInfo();
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String[] setPackagesSuspended(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String[] packageNames, boolean suspended) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeStringArray(packageNames);
          _data.writeInt(((suspended)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPackagesSuspended, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPackagesSuspended(admin, callerPackage, packageNames, suspended);
          }
          _reply.readException();
          _result = _reply.createStringArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isPackageSuspended(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isPackageSuspended, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isPackageSuspended(admin, callerPackage, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean installCaCert(android.content.ComponentName admin, java.lang.String callerPackage, byte[] certBuffer) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeByteArray(certBuffer);
          boolean _status = mRemote.transact(Stub.TRANSACTION_installCaCert, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().installCaCert(admin, callerPackage, certBuffer);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void uninstallCaCerts(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String[] aliases) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeStringArray(aliases);
          boolean _status = mRemote.transact(Stub.TRANSACTION_uninstallCaCerts, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().uninstallCaCerts(admin, callerPackage, aliases);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void enforceCanManageCaCerts(android.content.ComponentName admin, java.lang.String callerPackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_enforceCanManageCaCerts, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().enforceCanManageCaCerts(admin, callerPackage);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean approveCaCert(java.lang.String alias, int userHandle, boolean approval) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          _data.writeInt(userHandle);
          _data.writeInt(((approval)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_approveCaCert, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().approveCaCert(alias, userHandle, approval);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isCaCertApproved(java.lang.String alias, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isCaCertApproved, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isCaCertApproved(alias, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean installKeyPair(android.content.ComponentName who, java.lang.String callerPackage, byte[] privKeyBuffer, byte[] certBuffer, byte[] certChainBuffer, java.lang.String alias, boolean requestAccess, boolean isUserSelectable) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeByteArray(privKeyBuffer);
          _data.writeByteArray(certBuffer);
          _data.writeByteArray(certChainBuffer);
          _data.writeString(alias);
          _data.writeInt(((requestAccess)?(1):(0)));
          _data.writeInt(((isUserSelectable)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_installKeyPair, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().installKeyPair(who, callerPackage, privKeyBuffer, certBuffer, certChainBuffer, alias, requestAccess, isUserSelectable);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean removeKeyPair(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeKeyPair, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().removeKeyPair(who, callerPackage, alias);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean generateKeyPair(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String algorithm, android.security.keystore.ParcelableKeyGenParameterSpec keySpec, int idAttestationFlags, android.security.keymaster.KeymasterCertificateChain attestationChain) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(algorithm);
          if ((keySpec!=null)) {
            _data.writeInt(1);
            keySpec.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(idAttestationFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_generateKeyPair, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().generateKeyPair(who, callerPackage, algorithm, keySpec, idAttestationFlags, attestationChain);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
          if ((0!=_reply.readInt())) {
            attestationChain.readFromParcel(_reply);
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setKeyPairCertificate(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String alias, byte[] certBuffer, byte[] certChainBuffer, boolean isUserSelectable) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(alias);
          _data.writeByteArray(certBuffer);
          _data.writeByteArray(certChainBuffer);
          _data.writeInt(((isUserSelectable)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setKeyPairCertificate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setKeyPairCertificate(who, callerPackage, alias, certBuffer, certChainBuffer, isUserSelectable);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void choosePrivateKeyAlias(int uid, android.net.Uri uri, java.lang.String alias, android.os.IBinder aliasCallback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          if ((uri!=null)) {
            _data.writeInt(1);
            uri.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(alias);
          _data.writeStrongBinder(aliasCallback);
          boolean _status = mRemote.transact(Stub.TRANSACTION_choosePrivateKeyAlias, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().choosePrivateKeyAlias(uid, uri, alias, aliasCallback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setDelegatedScopes(android.content.ComponentName who, java.lang.String delegatePackage, java.util.List<java.lang.String> scopes) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(delegatePackage);
          _data.writeStringList(scopes);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setDelegatedScopes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setDelegatedScopes(who, delegatePackage, scopes);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.util.List<java.lang.String> getDelegatedScopes(android.content.ComponentName who, java.lang.String delegatePackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(delegatePackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDelegatedScopes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDelegatedScopes(who, delegatePackage);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getDelegatePackages(android.content.ComponentName who, java.lang.String scope) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(scope);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDelegatePackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDelegatePackages(who, scope);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setCertInstallerPackage(android.content.ComponentName who, java.lang.String installerPackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(installerPackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setCertInstallerPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setCertInstallerPackage(who, installerPackage);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.String getCertInstallerPackage(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCertInstallerPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCertInstallerPackage(who);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setAlwaysOnVpnPackage(android.content.ComponentName who, java.lang.String vpnPackage, boolean lockdown, java.util.List<java.lang.String> lockdownWhitelist) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(vpnPackage);
          _data.writeInt(((lockdown)?(1):(0)));
          _data.writeStringList(lockdownWhitelist);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setAlwaysOnVpnPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setAlwaysOnVpnPackage(who, vpnPackage, lockdown, lockdownWhitelist);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getAlwaysOnVpnPackage(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAlwaysOnVpnPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAlwaysOnVpnPackage(who);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isAlwaysOnVpnLockdownEnabled(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isAlwaysOnVpnLockdownEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isAlwaysOnVpnLockdownEnabled(who);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getAlwaysOnVpnLockdownWhitelist(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAlwaysOnVpnLockdownWhitelist, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAlwaysOnVpnLockdownWhitelist(who);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void addPersistentPreferredActivity(android.content.ComponentName admin, android.content.IntentFilter filter, android.content.ComponentName activity) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((filter!=null)) {
            _data.writeInt(1);
            filter.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((activity!=null)) {
            _data.writeInt(1);
            activity.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_addPersistentPreferredActivity, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().addPersistentPreferredActivity(admin, filter, activity);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void clearPackagePersistentPreferredActivities(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearPackagePersistentPreferredActivities, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearPackagePersistentPreferredActivities(admin, packageName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setDefaultSmsApplication(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setDefaultSmsApplication, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setDefaultSmsApplication(admin, packageName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setApplicationRestrictions(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String packageName, android.os.Bundle settings) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          if ((settings!=null)) {
            _data.writeInt(1);
            settings.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setApplicationRestrictions, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setApplicationRestrictions(who, callerPackage, packageName, settings);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.os.Bundle getApplicationRestrictions(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.Bundle _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getApplicationRestrictions, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getApplicationRestrictions(who, callerPackage, packageName);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.Bundle.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setApplicationRestrictionsManagingPackage(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setApplicationRestrictionsManagingPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setApplicationRestrictionsManagingPackage(admin, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getApplicationRestrictionsManagingPackage(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getApplicationRestrictionsManagingPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getApplicationRestrictionsManagingPackage(admin);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isCallerApplicationRestrictionsManagingPackage(java.lang.String callerPackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(callerPackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isCallerApplicationRestrictionsManagingPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isCallerApplicationRestrictionsManagingPackage(callerPackage);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setRestrictionsProvider(android.content.ComponentName who, android.content.ComponentName provider) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((provider!=null)) {
            _data.writeInt(1);
            provider.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setRestrictionsProvider, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setRestrictionsProvider(who, provider);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.content.ComponentName getRestrictionsProvider(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.ComponentName _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRestrictionsProvider, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRestrictionsProvider(userHandle);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.ComponentName.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setUserRestriction(android.content.ComponentName who, java.lang.String key, boolean enable) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(key);
          _data.writeInt(((enable)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setUserRestriction, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setUserRestriction(who, key, enable);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.os.Bundle getUserRestrictions(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.Bundle _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getUserRestrictions, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getUserRestrictions(who);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.Bundle.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void addCrossProfileIntentFilter(android.content.ComponentName admin, android.content.IntentFilter filter, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((filter!=null)) {
            _data.writeInt(1);
            filter.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addCrossProfileIntentFilter, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().addCrossProfileIntentFilter(admin, filter, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void clearCrossProfileIntentFilters(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearCrossProfileIntentFilters, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearCrossProfileIntentFilters(admin);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean setPermittedAccessibilityServices(android.content.ComponentName admin, java.util.List packageList) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeList(packageList);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPermittedAccessibilityServices, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPermittedAccessibilityServices(admin, packageList);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List getPermittedAccessibilityServices(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPermittedAccessibilityServices, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPermittedAccessibilityServices(admin);
          }
          _reply.readException();
          java.lang.ClassLoader cl = (java.lang.ClassLoader)this.getClass().getClassLoader();
          _result = _reply.readArrayList(cl);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List getPermittedAccessibilityServicesForUser(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPermittedAccessibilityServicesForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPermittedAccessibilityServicesForUser(userId);
          }
          _reply.readException();
          java.lang.ClassLoader cl = (java.lang.ClassLoader)this.getClass().getClassLoader();
          _result = _reply.readArrayList(cl);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isAccessibilityServicePermittedByAdmin(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isAccessibilityServicePermittedByAdmin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isAccessibilityServicePermittedByAdmin(admin, packageName, userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setPermittedInputMethods(android.content.ComponentName admin, java.util.List packageList) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeList(packageList);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPermittedInputMethods, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPermittedInputMethods(admin, packageList);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List getPermittedInputMethods(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPermittedInputMethods, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPermittedInputMethods(admin);
          }
          _reply.readException();
          java.lang.ClassLoader cl = (java.lang.ClassLoader)this.getClass().getClassLoader();
          _result = _reply.readArrayList(cl);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List getPermittedInputMethodsForCurrentUser() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPermittedInputMethodsForCurrentUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPermittedInputMethodsForCurrentUser();
          }
          _reply.readException();
          java.lang.ClassLoader cl = (java.lang.ClassLoader)this.getClass().getClassLoader();
          _result = _reply.readArrayList(cl);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isInputMethodPermittedByAdmin(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isInputMethodPermittedByAdmin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isInputMethodPermittedByAdmin(admin, packageName, userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setPermittedCrossProfileNotificationListeners(android.content.ComponentName admin, java.util.List<java.lang.String> packageList) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStringList(packageList);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPermittedCrossProfileNotificationListeners, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPermittedCrossProfileNotificationListeners(admin, packageList);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getPermittedCrossProfileNotificationListeners(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPermittedCrossProfileNotificationListeners, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPermittedCrossProfileNotificationListeners(admin);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isNotificationListenerServicePermitted(java.lang.String packageName, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isNotificationListenerServicePermitted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isNotificationListenerServicePermitted(packageName, userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.Intent createAdminSupportIntent(java.lang.String restriction) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.Intent _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(restriction);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createAdminSupportIntent, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createAdminSupportIntent(restriction);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.Intent.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setApplicationHidden(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, boolean hidden) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          _data.writeInt(((hidden)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setApplicationHidden, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setApplicationHidden(admin, callerPackage, packageName, hidden);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isApplicationHidden(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isApplicationHidden, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isApplicationHidden(admin, callerPackage, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.UserHandle createAndManageUser(android.content.ComponentName who, java.lang.String name, android.content.ComponentName profileOwner, android.os.PersistableBundle adminExtras, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.UserHandle _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(name);
          if ((profileOwner!=null)) {
            _data.writeInt(1);
            profileOwner.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((adminExtras!=null)) {
            _data.writeInt(1);
            adminExtras.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createAndManageUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createAndManageUser(who, name, profileOwner, adminExtras, flags);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.UserHandle.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean removeUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((userHandle!=null)) {
            _data.writeInt(1);
            userHandle.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().removeUser(who, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean switchUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((userHandle!=null)) {
            _data.writeInt(1);
            userHandle.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_switchUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().switchUser(who, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int startUserInBackground(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((userHandle!=null)) {
            _data.writeInt(1);
            userHandle.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_startUserInBackground, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().startUserInBackground(who, userHandle);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int stopUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((userHandle!=null)) {
            _data.writeInt(1);
            userHandle.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_stopUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().stopUser(who, userHandle);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int logoutUser(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_logoutUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().logoutUser(who);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<android.os.UserHandle> getSecondaryUsers(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<android.os.UserHandle> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSecondaryUsers, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSecondaryUsers(who);
          }
          _reply.readException();
          _result = _reply.createTypedArrayList(android.os.UserHandle.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void enableSystemApp(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_enableSystemApp, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().enableSystemApp(admin, callerPackage, packageName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int enableSystemAppWithIntent(android.content.ComponentName admin, java.lang.String callerPackage, android.content.Intent intent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          if ((intent!=null)) {
            _data.writeInt(1);
            intent.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_enableSystemAppWithIntent, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().enableSystemAppWithIntent(admin, callerPackage, intent);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean installExistingPackage(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_installExistingPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().installExistingPackage(admin, callerPackage, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setAccountManagementDisabled(android.content.ComponentName who, java.lang.String accountType, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(accountType);
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setAccountManagementDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setAccountManagementDisabled(who, accountType, disabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.String[] getAccountTypesWithManagementDisabled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAccountTypesWithManagementDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAccountTypesWithManagementDisabled();
          }
          _reply.readException();
          _result = _reply.createStringArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String[] getAccountTypesWithManagementDisabledAsUser(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAccountTypesWithManagementDisabledAsUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAccountTypesWithManagementDisabledAsUser(userId);
          }
          _reply.readException();
          _result = _reply.createStringArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setLockTaskPackages(android.content.ComponentName who, java.lang.String[] packages) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStringArray(packages);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setLockTaskPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setLockTaskPackages(who, packages);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.String[] getLockTaskPackages(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLockTaskPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLockTaskPackages(who);
          }
          _reply.readException();
          _result = _reply.createStringArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isLockTaskPermitted(java.lang.String pkg) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkg);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isLockTaskPermitted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isLockTaskPermitted(pkg);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setLockTaskFeatures(android.content.ComponentName who, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setLockTaskFeatures, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setLockTaskFeatures(who, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getLockTaskFeatures(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLockTaskFeatures, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLockTaskFeatures(who);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setGlobalSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(setting);
          _data.writeString(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setGlobalSetting, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setGlobalSetting(who, setting, value);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setSystemSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(setting);
          _data.writeString(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setSystemSetting, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setSystemSetting(who, setting, value);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setSecureSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(setting);
          _data.writeString(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setSecureSetting, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setSecureSetting(who, setting, value);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean setTime(android.content.ComponentName who, long millis) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeLong(millis);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setTime, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setTime(who, millis);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setTimeZone(android.content.ComponentName who, java.lang.String timeZone) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(timeZone);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setTimeZone, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setTimeZone(who, timeZone);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setMasterVolumeMuted(android.content.ComponentName admin, boolean on) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((on)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setMasterVolumeMuted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setMasterVolumeMuted(admin, on);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isMasterVolumeMuted(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isMasterVolumeMuted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isMasterVolumeMuted(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void notifyLockTaskModeChanged(boolean isEnabled, java.lang.String pkg, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((isEnabled)?(1):(0)));
          _data.writeString(pkg);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_notifyLockTaskModeChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notifyLockTaskModeChanged(isEnabled, pkg, userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setUninstallBlocked(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, boolean uninstallBlocked) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          _data.writeInt(((uninstallBlocked)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setUninstallBlocked, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setUninstallBlocked(admin, callerPackage, packageName, uninstallBlocked);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isUninstallBlocked(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isUninstallBlocked, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isUninstallBlocked(admin, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setCrossProfileCallerIdDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setCrossProfileCallerIdDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setCrossProfileCallerIdDisabled(who, disabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean getCrossProfileCallerIdDisabled(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCrossProfileCallerIdDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCrossProfileCallerIdDisabled(who);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean getCrossProfileCallerIdDisabledForUser(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCrossProfileCallerIdDisabledForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCrossProfileCallerIdDisabledForUser(userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setCrossProfileContactsSearchDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setCrossProfileContactsSearchDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setCrossProfileContactsSearchDisabled(who, disabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean getCrossProfileContactsSearchDisabled(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCrossProfileContactsSearchDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCrossProfileContactsSearchDisabled(who);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean getCrossProfileContactsSearchDisabledForUser(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCrossProfileContactsSearchDisabledForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCrossProfileContactsSearchDisabledForUser(userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void startManagedQuickContact(java.lang.String lookupKey, long contactId, boolean isContactIdIgnored, long directoryId, android.content.Intent originalIntent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(lookupKey);
          _data.writeLong(contactId);
          _data.writeInt(((isContactIdIgnored)?(1):(0)));
          _data.writeLong(directoryId);
          if ((originalIntent!=null)) {
            _data.writeInt(1);
            originalIntent.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_startManagedQuickContact, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().startManagedQuickContact(lookupKey, contactId, isContactIdIgnored, directoryId, originalIntent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setBluetoothContactSharingDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setBluetoothContactSharingDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setBluetoothContactSharingDisabled(who, disabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean getBluetoothContactSharingDisabled(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBluetoothContactSharingDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBluetoothContactSharingDisabled(who);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean getBluetoothContactSharingDisabledForUser(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBluetoothContactSharingDisabledForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBluetoothContactSharingDisabledForUser(userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setTrustAgentConfiguration(android.content.ComponentName admin, android.content.ComponentName agent, android.os.PersistableBundle args, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((agent!=null)) {
            _data.writeInt(1);
            agent.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((args!=null)) {
            _data.writeInt(1);
            args.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setTrustAgentConfiguration, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setTrustAgentConfiguration(admin, agent, args, parent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.util.List<android.os.PersistableBundle> getTrustAgentConfiguration(android.content.ComponentName admin, android.content.ComponentName agent, int userId, boolean parent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<android.os.PersistableBundle> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((agent!=null)) {
            _data.writeInt(1);
            agent.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userId);
          _data.writeInt(((parent)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getTrustAgentConfiguration, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getTrustAgentConfiguration(admin, agent, userId, parent);
          }
          _reply.readException();
          _result = _reply.createTypedArrayList(android.os.PersistableBundle.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean addCrossProfileWidgetProvider(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addCrossProfileWidgetProvider, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().addCrossProfileWidgetProvider(admin, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean removeCrossProfileWidgetProvider(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeCrossProfileWidgetProvider, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().removeCrossProfileWidgetProvider(admin, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getCrossProfileWidgetProviders(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCrossProfileWidgetProviders, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCrossProfileWidgetProviders(admin);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setAutoTimeRequired(android.content.ComponentName who, boolean required) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((required)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setAutoTimeRequired, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setAutoTimeRequired(who, required);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean getAutoTimeRequired() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAutoTimeRequired, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAutoTimeRequired();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setForceEphemeralUsers(android.content.ComponentName who, boolean forceEpehemeralUsers) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((forceEpehemeralUsers)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setForceEphemeralUsers, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setForceEphemeralUsers(who, forceEpehemeralUsers);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean getForceEphemeralUsers(android.content.ComponentName who) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getForceEphemeralUsers, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getForceEphemeralUsers(who);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isRemovingAdmin(android.content.ComponentName adminReceiver, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((adminReceiver!=null)) {
            _data.writeInt(1);
            adminReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isRemovingAdmin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isRemovingAdmin(adminReceiver, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setUserIcon(android.content.ComponentName admin, android.graphics.Bitmap icon) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((icon!=null)) {
            _data.writeInt(1);
            icon.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setUserIcon, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setUserIcon(admin, icon);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setSystemUpdatePolicy(android.content.ComponentName who, android.app.admin.SystemUpdatePolicy policy) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((policy!=null)) {
            _data.writeInt(1);
            policy.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setSystemUpdatePolicy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setSystemUpdatePolicy(who, policy);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.app.admin.SystemUpdatePolicy getSystemUpdatePolicy() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.app.admin.SystemUpdatePolicy _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSystemUpdatePolicy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSystemUpdatePolicy();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.app.admin.SystemUpdatePolicy.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void clearSystemUpdatePolicyFreezePeriodRecord() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearSystemUpdatePolicyFreezePeriodRecord, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearSystemUpdatePolicyFreezePeriodRecord();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean setKeyguardDisabled(android.content.ComponentName admin, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setKeyguardDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setKeyguardDisabled(admin, disabled);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setStatusBarDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((disabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setStatusBarDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setStatusBarDisabled(who, disabled);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean getDoNotAskCredentialsOnBoot() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDoNotAskCredentialsOnBoot, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDoNotAskCredentialsOnBoot();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void notifyPendingSystemUpdate(android.app.admin.SystemUpdateInfo info) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((info!=null)) {
            _data.writeInt(1);
            info.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_notifyPendingSystemUpdate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notifyPendingSystemUpdate(info);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.app.admin.SystemUpdateInfo getPendingSystemUpdate(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.app.admin.SystemUpdateInfo _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPendingSystemUpdate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPendingSystemUpdate(admin);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.app.admin.SystemUpdateInfo.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPermissionPolicy(android.content.ComponentName admin, java.lang.String callerPackage, int policy) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeInt(policy);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPermissionPolicy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPermissionPolicy(admin, callerPackage, policy);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPermissionPolicy(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPermissionPolicy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPermissionPolicy(admin);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPermissionGrantState(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, java.lang.String permission, int grantState, android.os.RemoteCallback resultReceiver) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          _data.writeString(permission);
          _data.writeInt(grantState);
          if ((resultReceiver!=null)) {
            _data.writeInt(1);
            resultReceiver.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPermissionGrantState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPermissionGrantState(admin, callerPackage, packageName, permission, grantState, resultReceiver);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getPermissionGrantState(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, java.lang.String permission) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeString(packageName);
          _data.writeString(permission);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPermissionGrantState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPermissionGrantState(admin, callerPackage, packageName, permission);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isProvisioningAllowed(java.lang.String action, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(action);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isProvisioningAllowed, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isProvisioningAllowed(action, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int checkProvisioningPreCondition(java.lang.String action, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(action);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_checkProvisioningPreCondition, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().checkProvisioningPreCondition(action, packageName);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setKeepUninstalledPackages(android.content.ComponentName admin, java.lang.String callerPackage, java.util.List<java.lang.String> packageList) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          _data.writeStringList(packageList);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setKeepUninstalledPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setKeepUninstalledPackages(admin, callerPackage, packageList);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.util.List<java.lang.String> getKeepUninstalledPackages(android.content.ComponentName admin, java.lang.String callerPackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(callerPackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getKeepUninstalledPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getKeepUninstalledPackages(admin, callerPackage);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isManagedProfile(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isManagedProfile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isManagedProfile(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isSystemOnlyUser(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isSystemOnlyUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isSystemOnlyUser(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getWifiMacAddress(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getWifiMacAddress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getWifiMacAddress(admin);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void reboot(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_reboot, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reboot(admin);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setShortSupportMessage(android.content.ComponentName admin, java.lang.CharSequence message) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if (message!=null) {
            _data.writeInt(1);
            android.text.TextUtils.writeToParcel(message, _data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setShortSupportMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setShortSupportMessage(admin, message);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.CharSequence getShortSupportMessage(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getShortSupportMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getShortSupportMessage(admin);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setLongSupportMessage(android.content.ComponentName admin, java.lang.CharSequence message) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if (message!=null) {
            _data.writeInt(1);
            android.text.TextUtils.writeToParcel(message, _data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setLongSupportMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setLongSupportMessage(admin, message);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.CharSequence getLongSupportMessage(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLongSupportMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLongSupportMessage(admin);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.CharSequence getShortSupportMessageForUser(android.content.ComponentName admin, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getShortSupportMessageForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getShortSupportMessageForUser(admin, userHandle);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.CharSequence getLongSupportMessageForUser(android.content.ComponentName admin, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLongSupportMessageForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLongSupportMessageForUser(admin, userHandle);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isSeparateProfileChallengeAllowed(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isSeparateProfileChallengeAllowed, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isSeparateProfileChallengeAllowed(userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setOrganizationColor(android.content.ComponentName admin, int color) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(color);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setOrganizationColor, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setOrganizationColor(admin, color);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setOrganizationColorForUser(int color, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(color);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setOrganizationColorForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setOrganizationColorForUser(color, userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int getOrganizationColor(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getOrganizationColor, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getOrganizationColor(admin);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getOrganizationColorForUser(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getOrganizationColorForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getOrganizationColorForUser(userHandle);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setOrganizationName(android.content.ComponentName admin, java.lang.CharSequence title) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if (title!=null) {
            _data.writeInt(1);
            android.text.TextUtils.writeToParcel(title, _data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setOrganizationName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setOrganizationName(admin, title);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.CharSequence getOrganizationName(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getOrganizationName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getOrganizationName(admin);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.CharSequence getDeviceOwnerOrganizationName() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDeviceOwnerOrganizationName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDeviceOwnerOrganizationName();
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.CharSequence getOrganizationNameForUser(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getOrganizationNameForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getOrganizationNameForUser(userHandle);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getUserProvisioningState() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getUserProvisioningState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getUserProvisioningState();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setUserProvisioningState(int state, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(state);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setUserProvisioningState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setUserProvisioningState(state, userHandle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setAffiliationIds(android.content.ComponentName admin, java.util.List<java.lang.String> ids) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStringList(ids);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setAffiliationIds, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setAffiliationIds(admin, ids);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.util.List<java.lang.String> getAffiliationIds(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAffiliationIds, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAffiliationIds(admin);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isAffiliatedUser() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isAffiliatedUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isAffiliatedUser();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setSecurityLoggingEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((enabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setSecurityLoggingEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setSecurityLoggingEnabled(admin, enabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isSecurityLoggingEnabled(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isSecurityLoggingEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isSecurityLoggingEnabled(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.pm.ParceledListSlice retrieveSecurityLogs(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.pm.ParceledListSlice _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_retrieveSecurityLogs, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().retrieveSecurityLogs(admin);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.pm.ParceledListSlice.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.pm.ParceledListSlice retrievePreRebootSecurityLogs(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.pm.ParceledListSlice _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_retrievePreRebootSecurityLogs, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().retrievePreRebootSecurityLogs(admin);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.pm.ParceledListSlice.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long forceNetworkLogs() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_forceNetworkLogs, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().forceNetworkLogs();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long forceSecurityLogs() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_forceSecurityLogs, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().forceSecurityLogs();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isUninstallInQueue(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isUninstallInQueue, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isUninstallInQueue(packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void uninstallPackageWithActiveAdmins(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_uninstallPackageWithActiveAdmins, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().uninstallPackageWithActiveAdmins(packageName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isDeviceProvisioned() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isDeviceProvisioned, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isDeviceProvisioned();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isDeviceProvisioningConfigApplied() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isDeviceProvisioningConfigApplied, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isDeviceProvisioningConfigApplied();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setDeviceProvisioningConfigApplied() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setDeviceProvisioningConfigApplied, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setDeviceProvisioningConfigApplied();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void forceUpdateUserSetupComplete() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_forceUpdateUserSetupComplete, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().forceUpdateUserSetupComplete();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setBackupServiceEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((enabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setBackupServiceEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setBackupServiceEnabled(admin, enabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isBackupServiceEnabled(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isBackupServiceEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isBackupServiceEnabled(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setNetworkLoggingEnabled(android.content.ComponentName admin, java.lang.String packageName, boolean enabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          _data.writeInt(((enabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setNetworkLoggingEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setNetworkLoggingEnabled(admin, packageName, enabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isNetworkLoggingEnabled(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isNetworkLoggingEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isNetworkLoggingEnabled(admin, packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<android.app.admin.NetworkEvent> retrieveNetworkLogs(android.content.ComponentName admin, java.lang.String packageName, long batchToken) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<android.app.admin.NetworkEvent> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          _data.writeLong(batchToken);
          boolean _status = mRemote.transact(Stub.TRANSACTION_retrieveNetworkLogs, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().retrieveNetworkLogs(admin, packageName, batchToken);
          }
          _reply.readException();
          _result = _reply.createTypedArrayList(android.app.admin.NetworkEvent.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean bindDeviceAdminServiceAsUser(android.content.ComponentName admin, android.app.IApplicationThread caller, android.os.IBinder token, android.content.Intent service, android.app.IServiceConnection connection, int flags, int targetUserId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStrongBinder((((caller!=null))?(caller.asBinder()):(null)));
          _data.writeStrongBinder(token);
          if ((service!=null)) {
            _data.writeInt(1);
            service.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStrongBinder((((connection!=null))?(connection.asBinder()):(null)));
          _data.writeInt(flags);
          _data.writeInt(targetUserId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_bindDeviceAdminServiceAsUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().bindDeviceAdminServiceAsUser(admin, caller, token, service, connection, flags, targetUserId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<android.os.UserHandle> getBindDeviceAdminTargetUsers(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<android.os.UserHandle> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBindDeviceAdminTargetUsers, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBindDeviceAdminTargetUsers(admin);
          }
          _reply.readException();
          _result = _reply.createTypedArrayList(android.os.UserHandle.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isEphemeralUser(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isEphemeralUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isEphemeralUser(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getLastSecurityLogRetrievalTime() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLastSecurityLogRetrievalTime, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLastSecurityLogRetrievalTime();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getLastBugReportRequestTime() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLastBugReportRequestTime, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLastBugReportRequestTime();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getLastNetworkLogRetrievalTime() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLastNetworkLogRetrievalTime, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLastNetworkLogRetrievalTime();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setResetPasswordToken(android.content.ComponentName admin, byte[] token) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeByteArray(token);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setResetPasswordToken, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setResetPasswordToken(admin, token);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean clearResetPasswordToken(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearResetPasswordToken, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().clearResetPasswordToken(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isResetPasswordTokenActive(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isResetPasswordTokenActive, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isResetPasswordTokenActive(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean resetPasswordWithToken(android.content.ComponentName admin, java.lang.String password, byte[] token, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(password);
          _data.writeByteArray(token);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_resetPasswordWithToken, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().resetPasswordWithToken(admin, password, token, flags);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isCurrentInputMethodSetByOwner() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isCurrentInputMethodSetByOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isCurrentInputMethodSetByOwner();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.pm.StringParceledListSlice getOwnerInstalledCaCerts(android.os.UserHandle user) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.pm.StringParceledListSlice _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((user!=null)) {
            _data.writeInt(1);
            user.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getOwnerInstalledCaCerts, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getOwnerInstalledCaCerts(user);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.pm.StringParceledListSlice.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void clearApplicationUserData(android.content.ComponentName admin, java.lang.String packageName, android.content.pm.IPackageDataObserver callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearApplicationUserData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearApplicationUserData(admin, packageName, callback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setLogoutEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((enabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setLogoutEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setLogoutEnabled(admin, enabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isLogoutEnabled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isLogoutEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isLogoutEnabled();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getDisallowedSystemApps(android.content.ComponentName admin, int userId, java.lang.String provisioningAction) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userId);
          _data.writeString(provisioningAction);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDisallowedSystemApps, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDisallowedSystemApps(admin, userId, provisioningAction);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void transferOwnership(android.content.ComponentName admin, android.content.ComponentName target, android.os.PersistableBundle bundle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((target!=null)) {
            _data.writeInt(1);
            target.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((bundle!=null)) {
            _data.writeInt(1);
            bundle.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_transferOwnership, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().transferOwnership(admin, target, bundle);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.os.PersistableBundle getTransferOwnershipBundle() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.PersistableBundle _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getTransferOwnershipBundle, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getTransferOwnershipBundle();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.PersistableBundle.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setStartUserSessionMessage(android.content.ComponentName admin, java.lang.CharSequence startUserSessionMessage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if (startUserSessionMessage!=null) {
            _data.writeInt(1);
            android.text.TextUtils.writeToParcel(startUserSessionMessage, _data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setStartUserSessionMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setStartUserSessionMessage(admin, startUserSessionMessage);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setEndUserSessionMessage(android.content.ComponentName admin, java.lang.CharSequence endUserSessionMessage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if (endUserSessionMessage!=null) {
            _data.writeInt(1);
            android.text.TextUtils.writeToParcel(endUserSessionMessage, _data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setEndUserSessionMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setEndUserSessionMessage(admin, endUserSessionMessage);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.CharSequence getStartUserSessionMessage(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getStartUserSessionMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getStartUserSessionMessage(admin);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.CharSequence getEndUserSessionMessage(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.CharSequence _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getEndUserSessionMessage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getEndUserSessionMessage(admin);
          }
          _reply.readException();
          if (0!=_reply.readInt()) {
            _result = android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> setMeteredDataDisabledPackages(android.content.ComponentName admin, java.util.List<java.lang.String> packageNames) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStringList(packageNames);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setMeteredDataDisabledPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setMeteredDataDisabledPackages(admin, packageNames);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getMeteredDataDisabledPackages(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMeteredDataDisabledPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMeteredDataDisabledPackages(admin);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int addOverrideApn(android.content.ComponentName admin, android.telephony.data.ApnSetting apnSetting) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((apnSetting!=null)) {
            _data.writeInt(1);
            apnSetting.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_addOverrideApn, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().addOverrideApn(admin, apnSetting);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean updateOverrideApn(android.content.ComponentName admin, int apnId, android.telephony.data.ApnSetting apnSetting) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(apnId);
          if ((apnSetting!=null)) {
            _data.writeInt(1);
            apnSetting.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_updateOverrideApn, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().updateOverrideApn(admin, apnId, apnSetting);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean removeOverrideApn(android.content.ComponentName admin, int apnId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(apnId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeOverrideApn, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().removeOverrideApn(admin, apnId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<android.telephony.data.ApnSetting> getOverrideApns(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<android.telephony.data.ApnSetting> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getOverrideApns, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getOverrideApns(admin);
          }
          _reply.readException();
          _result = _reply.createTypedArrayList(android.telephony.data.ApnSetting.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setOverrideApnsEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((enabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setOverrideApnsEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setOverrideApnsEnabled(admin, enabled);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isOverrideApnEnabled(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isOverrideApnEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isOverrideApnEnabled(admin);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isMeteredDataDisabledPackageForUser(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(packageName);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isMeteredDataDisabledPackageForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isMeteredDataDisabledPackageForUser(admin, packageName, userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int setGlobalPrivateDns(android.content.ComponentName admin, int mode, java.lang.String privateDnsHost) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(mode);
          _data.writeString(privateDnsHost);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setGlobalPrivateDns, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setGlobalPrivateDns(admin, mode, privateDnsHost);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getGlobalPrivateDnsMode(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getGlobalPrivateDnsMode, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getGlobalPrivateDnsMode(admin);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getGlobalPrivateDnsHost(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getGlobalPrivateDnsHost, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getGlobalPrivateDnsHost(admin);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void grantDeviceIdsAccessToProfileOwner(android.content.ComponentName who, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((who!=null)) {
            _data.writeInt(1);
            who.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_grantDeviceIdsAccessToProfileOwner, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().grantDeviceIdsAccessToProfileOwner(who, userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void installUpdateFromFile(android.content.ComponentName admin, android.os.ParcelFileDescriptor updateFileDescriptor, android.app.admin.StartInstallingUpdateCallback listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((updateFileDescriptor!=null)) {
            _data.writeInt(1);
            updateFileDescriptor.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_installUpdateFromFile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().installUpdateFromFile(admin, updateFileDescriptor, listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setCrossProfileCalendarPackages(android.content.ComponentName admin, java.util.List<java.lang.String> packageNames) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStringList(packageNames);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setCrossProfileCalendarPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setCrossProfileCalendarPackages(admin, packageNames);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.util.List<java.lang.String> getCrossProfileCalendarPackages(android.content.ComponentName admin) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((admin!=null)) {
            _data.writeInt(1);
            admin.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCrossProfileCalendarPackages, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCrossProfileCalendarPackages(admin);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isPackageAllowedToAccessCalendarForUser(java.lang.String packageName, int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isPackageAllowedToAccessCalendarForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isPackageAllowedToAccessCalendarForUser(packageName, userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getCrossProfileCalendarPackagesForUser(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCrossProfileCalendarPackagesForUser, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCrossProfileCalendarPackagesForUser(userHandle);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isManagedKiosk() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isManagedKiosk, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isManagedKiosk();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isUnattendedManagedKiosk() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isUnattendedManagedKiosk, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isUnattendedManagedKiosk();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean startViewCalendarEventInManagedProfile(java.lang.String packageName, long eventId, long start, long end, boolean allDay, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeLong(eventId);
          _data.writeLong(start);
          _data.writeLong(end);
          _data.writeInt(((allDay)?(1):(0)));
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_startViewCalendarEventInManagedProfile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().startViewCalendarEventInManagedProfile(packageName, eventId, start, end, allDay, flags);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean requireSecureKeyguard(int userHandle) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userHandle);
          boolean _status = mRemote.transact(Stub.TRANSACTION_requireSecureKeyguard, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().requireSecureKeyguard(userHandle);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.app.admin.IDevicePolicyManager sDefaultImpl;
    }
    static final int TRANSACTION_setPasswordQuality = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getPasswordQuality = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_setPasswordMinimumLength = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_getPasswordMinimumLength = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_setPasswordMinimumUpperCase = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_getPasswordMinimumUpperCase = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_setPasswordMinimumLowerCase = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_getPasswordMinimumLowerCase = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_setPasswordMinimumLetters = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_getPasswordMinimumLetters = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_setPasswordMinimumNumeric = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_getPasswordMinimumNumeric = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_setPasswordMinimumSymbols = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_getPasswordMinimumSymbols = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_setPasswordMinimumNonLetter = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_getPasswordMinimumNonLetter = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_setPasswordHistoryLength = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_getPasswordHistoryLength = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_setPasswordExpirationTimeout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    static final int TRANSACTION_getPasswordExpirationTimeout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    static final int TRANSACTION_getPasswordExpiration = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
    static final int TRANSACTION_isActivePasswordSufficient = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
    static final int TRANSACTION_isProfileActivePasswordSufficientForParent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
    static final int TRANSACTION_getPasswordComplexity = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
    static final int TRANSACTION_isUsingUnifiedPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
    static final int TRANSACTION_getCurrentFailedPasswordAttempts = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
    static final int TRANSACTION_getProfileWithMinimumFailedPasswordsForWipe = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
    static final int TRANSACTION_setMaximumFailedPasswordsForWipe = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
    static final int TRANSACTION_getMaximumFailedPasswordsForWipe = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
    static final int TRANSACTION_resetPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
    static final int TRANSACTION_setMaximumTimeToLock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
    static final int TRANSACTION_getMaximumTimeToLock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
    static final int TRANSACTION_setRequiredStrongAuthTimeout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
    static final int TRANSACTION_getRequiredStrongAuthTimeout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
    static final int TRANSACTION_lockNow = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
    static final int TRANSACTION_wipeDataWithReason = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
    static final int TRANSACTION_setGlobalProxy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
    static final int TRANSACTION_getGlobalProxyAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 37);
    static final int TRANSACTION_setRecommendedGlobalProxy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 38);
    static final int TRANSACTION_setStorageEncryption = (android.os.IBinder.FIRST_CALL_TRANSACTION + 39);
    static final int TRANSACTION_getStorageEncryption = (android.os.IBinder.FIRST_CALL_TRANSACTION + 40);
    static final int TRANSACTION_getStorageEncryptionStatus = (android.os.IBinder.FIRST_CALL_TRANSACTION + 41);
    static final int TRANSACTION_requestBugreport = (android.os.IBinder.FIRST_CALL_TRANSACTION + 42);
    static final int TRANSACTION_setCameraDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 43);
    static final int TRANSACTION_getCameraDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 44);
    static final int TRANSACTION_setScreenCaptureDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 45);
    static final int TRANSACTION_getScreenCaptureDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 46);
    static final int TRANSACTION_setKeyguardDisabledFeatures = (android.os.IBinder.FIRST_CALL_TRANSACTION + 47);
    static final int TRANSACTION_getKeyguardDisabledFeatures = (android.os.IBinder.FIRST_CALL_TRANSACTION + 48);
    static final int TRANSACTION_setActiveAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 49);
    static final int TRANSACTION_isAdminActive = (android.os.IBinder.FIRST_CALL_TRANSACTION + 50);
    static final int TRANSACTION_getActiveAdmins = (android.os.IBinder.FIRST_CALL_TRANSACTION + 51);
    static final int TRANSACTION_packageHasActiveAdmins = (android.os.IBinder.FIRST_CALL_TRANSACTION + 52);
    static final int TRANSACTION_getRemoveWarning = (android.os.IBinder.FIRST_CALL_TRANSACTION + 53);
    static final int TRANSACTION_removeActiveAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 54);
    static final int TRANSACTION_forceRemoveActiveAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 55);
    static final int TRANSACTION_hasGrantedPolicy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 56);
    static final int TRANSACTION_setActivePasswordState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 57);
    static final int TRANSACTION_reportPasswordChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 58);
    static final int TRANSACTION_reportFailedPasswordAttempt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 59);
    static final int TRANSACTION_reportSuccessfulPasswordAttempt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 60);
    static final int TRANSACTION_reportFailedBiometricAttempt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 61);
    static final int TRANSACTION_reportSuccessfulBiometricAttempt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 62);
    static final int TRANSACTION_reportKeyguardDismissed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 63);
    static final int TRANSACTION_reportKeyguardSecured = (android.os.IBinder.FIRST_CALL_TRANSACTION + 64);
    static final int TRANSACTION_setDeviceOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 65);
    static final int TRANSACTION_getDeviceOwnerComponent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 66);
    static final int TRANSACTION_hasDeviceOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 67);
    static final int TRANSACTION_getDeviceOwnerName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 68);
    static final int TRANSACTION_clearDeviceOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 69);
    static final int TRANSACTION_getDeviceOwnerUserId = (android.os.IBinder.FIRST_CALL_TRANSACTION + 70);
    static final int TRANSACTION_setProfileOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 71);
    static final int TRANSACTION_getProfileOwnerAsUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 72);
    static final int TRANSACTION_getProfileOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 73);
    static final int TRANSACTION_getProfileOwnerName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 74);
    static final int TRANSACTION_setProfileEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 75);
    static final int TRANSACTION_setProfileName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 76);
    static final int TRANSACTION_clearProfileOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 77);
    static final int TRANSACTION_hasUserSetupCompleted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 78);
    static final int TRANSACTION_checkDeviceIdentifierAccess = (android.os.IBinder.FIRST_CALL_TRANSACTION + 79);
    static final int TRANSACTION_setDeviceOwnerLockScreenInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 80);
    static final int TRANSACTION_getDeviceOwnerLockScreenInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 81);
    static final int TRANSACTION_setPackagesSuspended = (android.os.IBinder.FIRST_CALL_TRANSACTION + 82);
    static final int TRANSACTION_isPackageSuspended = (android.os.IBinder.FIRST_CALL_TRANSACTION + 83);
    static final int TRANSACTION_installCaCert = (android.os.IBinder.FIRST_CALL_TRANSACTION + 84);
    static final int TRANSACTION_uninstallCaCerts = (android.os.IBinder.FIRST_CALL_TRANSACTION + 85);
    static final int TRANSACTION_enforceCanManageCaCerts = (android.os.IBinder.FIRST_CALL_TRANSACTION + 86);
    static final int TRANSACTION_approveCaCert = (android.os.IBinder.FIRST_CALL_TRANSACTION + 87);
    static final int TRANSACTION_isCaCertApproved = (android.os.IBinder.FIRST_CALL_TRANSACTION + 88);
    static final int TRANSACTION_installKeyPair = (android.os.IBinder.FIRST_CALL_TRANSACTION + 89);
    private boolean onTransact$installKeyPair$(android.os.Parcel data, android.os.Parcel reply) throws android.os.RemoteException
    {
      data.enforceInterface(DESCRIPTOR);
      android.content.ComponentName _arg0;
      if ((0!=data.readInt())) {
        _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
      }
      else {
        _arg0 = null;
      }
      java.lang.String _arg1;
      _arg1 = data.readString();
      byte[] _arg2;
      _arg2 = data.createByteArray();
      byte[] _arg3;
      _arg3 = data.createByteArray();
      byte[] _arg4;
      _arg4 = data.createByteArray();
      java.lang.String _arg5;
      _arg5 = data.readString();
      boolean _arg6;
      _arg6 = (0!=data.readInt());
      boolean _arg7;
      _arg7 = (0!=data.readInt());
      boolean _result = this.installKeyPair(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7);
      reply.writeNoException();
      reply.writeInt(((_result)?(1):(0)));
      return true;
    }
    static final int TRANSACTION_removeKeyPair = (android.os.IBinder.FIRST_CALL_TRANSACTION + 90);
    static final int TRANSACTION_generateKeyPair = (android.os.IBinder.FIRST_CALL_TRANSACTION + 91);
    private boolean onTransact$generateKeyPair$(android.os.Parcel data, android.os.Parcel reply) throws android.os.RemoteException
    {
      data.enforceInterface(DESCRIPTOR);
      android.content.ComponentName _arg0;
      if ((0!=data.readInt())) {
        _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
      }
      else {
        _arg0 = null;
      }
      java.lang.String _arg1;
      _arg1 = data.readString();
      java.lang.String _arg2;
      _arg2 = data.readString();
      android.security.keystore.ParcelableKeyGenParameterSpec _arg3;
      if ((0!=data.readInt())) {
        _arg3 = android.security.keystore.ParcelableKeyGenParameterSpec.CREATOR.createFromParcel(data);
      }
      else {
        _arg3 = null;
      }
      int _arg4;
      _arg4 = data.readInt();
      android.security.keymaster.KeymasterCertificateChain _arg5;
      _arg5 = new android.security.keymaster.KeymasterCertificateChain();
      boolean _result = this.generateKeyPair(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
      reply.writeNoException();
      reply.writeInt(((_result)?(1):(0)));
      if ((_arg5!=null)) {
        reply.writeInt(1);
        _arg5.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
      }
      else {
        reply.writeInt(0);
      }
      return true;
    }
    static final int TRANSACTION_setKeyPairCertificate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 92);
    private boolean onTransact$setKeyPairCertificate$(android.os.Parcel data, android.os.Parcel reply) throws android.os.RemoteException
    {
      data.enforceInterface(DESCRIPTOR);
      android.content.ComponentName _arg0;
      if ((0!=data.readInt())) {
        _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
      }
      else {
        _arg0 = null;
      }
      java.lang.String _arg1;
      _arg1 = data.readString();
      java.lang.String _arg2;
      _arg2 = data.readString();
      byte[] _arg3;
      _arg3 = data.createByteArray();
      byte[] _arg4;
      _arg4 = data.createByteArray();
      boolean _arg5;
      _arg5 = (0!=data.readInt());
      boolean _result = this.setKeyPairCertificate(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
      reply.writeNoException();
      reply.writeInt(((_result)?(1):(0)));
      return true;
    }
    static final int TRANSACTION_choosePrivateKeyAlias = (android.os.IBinder.FIRST_CALL_TRANSACTION + 93);
    static final int TRANSACTION_setDelegatedScopes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 94);
    static final int TRANSACTION_getDelegatedScopes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 95);
    static final int TRANSACTION_getDelegatePackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 96);
    static final int TRANSACTION_setCertInstallerPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 97);
    static final int TRANSACTION_getCertInstallerPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 98);
    static final int TRANSACTION_setAlwaysOnVpnPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 99);
    static final int TRANSACTION_getAlwaysOnVpnPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 100);
    static final int TRANSACTION_isAlwaysOnVpnLockdownEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 101);
    static final int TRANSACTION_getAlwaysOnVpnLockdownWhitelist = (android.os.IBinder.FIRST_CALL_TRANSACTION + 102);
    static final int TRANSACTION_addPersistentPreferredActivity = (android.os.IBinder.FIRST_CALL_TRANSACTION + 103);
    static final int TRANSACTION_clearPackagePersistentPreferredActivities = (android.os.IBinder.FIRST_CALL_TRANSACTION + 104);
    static final int TRANSACTION_setDefaultSmsApplication = (android.os.IBinder.FIRST_CALL_TRANSACTION + 105);
    static final int TRANSACTION_setApplicationRestrictions = (android.os.IBinder.FIRST_CALL_TRANSACTION + 106);
    static final int TRANSACTION_getApplicationRestrictions = (android.os.IBinder.FIRST_CALL_TRANSACTION + 107);
    static final int TRANSACTION_setApplicationRestrictionsManagingPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 108);
    static final int TRANSACTION_getApplicationRestrictionsManagingPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 109);
    static final int TRANSACTION_isCallerApplicationRestrictionsManagingPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 110);
    static final int TRANSACTION_setRestrictionsProvider = (android.os.IBinder.FIRST_CALL_TRANSACTION + 111);
    static final int TRANSACTION_getRestrictionsProvider = (android.os.IBinder.FIRST_CALL_TRANSACTION + 112);
    static final int TRANSACTION_setUserRestriction = (android.os.IBinder.FIRST_CALL_TRANSACTION + 113);
    static final int TRANSACTION_getUserRestrictions = (android.os.IBinder.FIRST_CALL_TRANSACTION + 114);
    static final int TRANSACTION_addCrossProfileIntentFilter = (android.os.IBinder.FIRST_CALL_TRANSACTION + 115);
    static final int TRANSACTION_clearCrossProfileIntentFilters = (android.os.IBinder.FIRST_CALL_TRANSACTION + 116);
    static final int TRANSACTION_setPermittedAccessibilityServices = (android.os.IBinder.FIRST_CALL_TRANSACTION + 117);
    static final int TRANSACTION_getPermittedAccessibilityServices = (android.os.IBinder.FIRST_CALL_TRANSACTION + 118);
    static final int TRANSACTION_getPermittedAccessibilityServicesForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 119);
    static final int TRANSACTION_isAccessibilityServicePermittedByAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 120);
    static final int TRANSACTION_setPermittedInputMethods = (android.os.IBinder.FIRST_CALL_TRANSACTION + 121);
    static final int TRANSACTION_getPermittedInputMethods = (android.os.IBinder.FIRST_CALL_TRANSACTION + 122);
    static final int TRANSACTION_getPermittedInputMethodsForCurrentUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 123);
    static final int TRANSACTION_isInputMethodPermittedByAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 124);
    static final int TRANSACTION_setPermittedCrossProfileNotificationListeners = (android.os.IBinder.FIRST_CALL_TRANSACTION + 125);
    static final int TRANSACTION_getPermittedCrossProfileNotificationListeners = (android.os.IBinder.FIRST_CALL_TRANSACTION + 126);
    static final int TRANSACTION_isNotificationListenerServicePermitted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 127);
    static final int TRANSACTION_createAdminSupportIntent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 128);
    static final int TRANSACTION_setApplicationHidden = (android.os.IBinder.FIRST_CALL_TRANSACTION + 129);
    static final int TRANSACTION_isApplicationHidden = (android.os.IBinder.FIRST_CALL_TRANSACTION + 130);
    static final int TRANSACTION_createAndManageUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 131);
    static final int TRANSACTION_removeUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 132);
    static final int TRANSACTION_switchUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 133);
    static final int TRANSACTION_startUserInBackground = (android.os.IBinder.FIRST_CALL_TRANSACTION + 134);
    static final int TRANSACTION_stopUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 135);
    static final int TRANSACTION_logoutUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 136);
    static final int TRANSACTION_getSecondaryUsers = (android.os.IBinder.FIRST_CALL_TRANSACTION + 137);
    static final int TRANSACTION_enableSystemApp = (android.os.IBinder.FIRST_CALL_TRANSACTION + 138);
    static final int TRANSACTION_enableSystemAppWithIntent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 139);
    static final int TRANSACTION_installExistingPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 140);
    static final int TRANSACTION_setAccountManagementDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 141);
    static final int TRANSACTION_getAccountTypesWithManagementDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 142);
    static final int TRANSACTION_getAccountTypesWithManagementDisabledAsUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 143);
    static final int TRANSACTION_setLockTaskPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 144);
    static final int TRANSACTION_getLockTaskPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 145);
    static final int TRANSACTION_isLockTaskPermitted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 146);
    static final int TRANSACTION_setLockTaskFeatures = (android.os.IBinder.FIRST_CALL_TRANSACTION + 147);
    static final int TRANSACTION_getLockTaskFeatures = (android.os.IBinder.FIRST_CALL_TRANSACTION + 148);
    static final int TRANSACTION_setGlobalSetting = (android.os.IBinder.FIRST_CALL_TRANSACTION + 149);
    static final int TRANSACTION_setSystemSetting = (android.os.IBinder.FIRST_CALL_TRANSACTION + 150);
    static final int TRANSACTION_setSecureSetting = (android.os.IBinder.FIRST_CALL_TRANSACTION + 151);
    static final int TRANSACTION_setTime = (android.os.IBinder.FIRST_CALL_TRANSACTION + 152);
    static final int TRANSACTION_setTimeZone = (android.os.IBinder.FIRST_CALL_TRANSACTION + 153);
    static final int TRANSACTION_setMasterVolumeMuted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 154);
    static final int TRANSACTION_isMasterVolumeMuted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 155);
    static final int TRANSACTION_notifyLockTaskModeChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 156);
    static final int TRANSACTION_setUninstallBlocked = (android.os.IBinder.FIRST_CALL_TRANSACTION + 157);
    static final int TRANSACTION_isUninstallBlocked = (android.os.IBinder.FIRST_CALL_TRANSACTION + 158);
    static final int TRANSACTION_setCrossProfileCallerIdDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 159);
    static final int TRANSACTION_getCrossProfileCallerIdDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 160);
    static final int TRANSACTION_getCrossProfileCallerIdDisabledForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 161);
    static final int TRANSACTION_setCrossProfileContactsSearchDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 162);
    static final int TRANSACTION_getCrossProfileContactsSearchDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 163);
    static final int TRANSACTION_getCrossProfileContactsSearchDisabledForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 164);
    static final int TRANSACTION_startManagedQuickContact = (android.os.IBinder.FIRST_CALL_TRANSACTION + 165);
    static final int TRANSACTION_setBluetoothContactSharingDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 166);
    static final int TRANSACTION_getBluetoothContactSharingDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 167);
    static final int TRANSACTION_getBluetoothContactSharingDisabledForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 168);
    static final int TRANSACTION_setTrustAgentConfiguration = (android.os.IBinder.FIRST_CALL_TRANSACTION + 169);
    static final int TRANSACTION_getTrustAgentConfiguration = (android.os.IBinder.FIRST_CALL_TRANSACTION + 170);
    static final int TRANSACTION_addCrossProfileWidgetProvider = (android.os.IBinder.FIRST_CALL_TRANSACTION + 171);
    static final int TRANSACTION_removeCrossProfileWidgetProvider = (android.os.IBinder.FIRST_CALL_TRANSACTION + 172);
    static final int TRANSACTION_getCrossProfileWidgetProviders = (android.os.IBinder.FIRST_CALL_TRANSACTION + 173);
    static final int TRANSACTION_setAutoTimeRequired = (android.os.IBinder.FIRST_CALL_TRANSACTION + 174);
    static final int TRANSACTION_getAutoTimeRequired = (android.os.IBinder.FIRST_CALL_TRANSACTION + 175);
    static final int TRANSACTION_setForceEphemeralUsers = (android.os.IBinder.FIRST_CALL_TRANSACTION + 176);
    static final int TRANSACTION_getForceEphemeralUsers = (android.os.IBinder.FIRST_CALL_TRANSACTION + 177);
    static final int TRANSACTION_isRemovingAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 178);
    static final int TRANSACTION_setUserIcon = (android.os.IBinder.FIRST_CALL_TRANSACTION + 179);
    static final int TRANSACTION_setSystemUpdatePolicy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 180);
    static final int TRANSACTION_getSystemUpdatePolicy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 181);
    static final int TRANSACTION_clearSystemUpdatePolicyFreezePeriodRecord = (android.os.IBinder.FIRST_CALL_TRANSACTION + 182);
    static final int TRANSACTION_setKeyguardDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 183);
    static final int TRANSACTION_setStatusBarDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 184);
    static final int TRANSACTION_getDoNotAskCredentialsOnBoot = (android.os.IBinder.FIRST_CALL_TRANSACTION + 185);
    static final int TRANSACTION_notifyPendingSystemUpdate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 186);
    static final int TRANSACTION_getPendingSystemUpdate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 187);
    static final int TRANSACTION_setPermissionPolicy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 188);
    static final int TRANSACTION_getPermissionPolicy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 189);
    static final int TRANSACTION_setPermissionGrantState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 190);
    private boolean onTransact$setPermissionGrantState$(android.os.Parcel data, android.os.Parcel reply) throws android.os.RemoteException
    {
      data.enforceInterface(DESCRIPTOR);
      android.content.ComponentName _arg0;
      if ((0!=data.readInt())) {
        _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
      }
      else {
        _arg0 = null;
      }
      java.lang.String _arg1;
      _arg1 = data.readString();
      java.lang.String _arg2;
      _arg2 = data.readString();
      java.lang.String _arg3;
      _arg3 = data.readString();
      int _arg4;
      _arg4 = data.readInt();
      android.os.RemoteCallback _arg5;
      if ((0!=data.readInt())) {
        _arg5 = android.os.RemoteCallback.CREATOR.createFromParcel(data);
      }
      else {
        _arg5 = null;
      }
      this.setPermissionGrantState(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
      reply.writeNoException();
      return true;
    }
    static final int TRANSACTION_getPermissionGrantState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 191);
    static final int TRANSACTION_isProvisioningAllowed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 192);
    static final int TRANSACTION_checkProvisioningPreCondition = (android.os.IBinder.FIRST_CALL_TRANSACTION + 193);
    static final int TRANSACTION_setKeepUninstalledPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 194);
    static final int TRANSACTION_getKeepUninstalledPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 195);
    static final int TRANSACTION_isManagedProfile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 196);
    static final int TRANSACTION_isSystemOnlyUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 197);
    static final int TRANSACTION_getWifiMacAddress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 198);
    static final int TRANSACTION_reboot = (android.os.IBinder.FIRST_CALL_TRANSACTION + 199);
    static final int TRANSACTION_setShortSupportMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 200);
    static final int TRANSACTION_getShortSupportMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 201);
    static final int TRANSACTION_setLongSupportMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 202);
    static final int TRANSACTION_getLongSupportMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 203);
    static final int TRANSACTION_getShortSupportMessageForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 204);
    static final int TRANSACTION_getLongSupportMessageForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 205);
    static final int TRANSACTION_isSeparateProfileChallengeAllowed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 206);
    static final int TRANSACTION_setOrganizationColor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 207);
    static final int TRANSACTION_setOrganizationColorForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 208);
    static final int TRANSACTION_getOrganizationColor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 209);
    static final int TRANSACTION_getOrganizationColorForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 210);
    static final int TRANSACTION_setOrganizationName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 211);
    static final int TRANSACTION_getOrganizationName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 212);
    static final int TRANSACTION_getDeviceOwnerOrganizationName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 213);
    static final int TRANSACTION_getOrganizationNameForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 214);
    static final int TRANSACTION_getUserProvisioningState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 215);
    static final int TRANSACTION_setUserProvisioningState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 216);
    static final int TRANSACTION_setAffiliationIds = (android.os.IBinder.FIRST_CALL_TRANSACTION + 217);
    static final int TRANSACTION_getAffiliationIds = (android.os.IBinder.FIRST_CALL_TRANSACTION + 218);
    static final int TRANSACTION_isAffiliatedUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 219);
    static final int TRANSACTION_setSecurityLoggingEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 220);
    static final int TRANSACTION_isSecurityLoggingEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 221);
    static final int TRANSACTION_retrieveSecurityLogs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 222);
    static final int TRANSACTION_retrievePreRebootSecurityLogs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 223);
    static final int TRANSACTION_forceNetworkLogs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 224);
    static final int TRANSACTION_forceSecurityLogs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 225);
    static final int TRANSACTION_isUninstallInQueue = (android.os.IBinder.FIRST_CALL_TRANSACTION + 226);
    static final int TRANSACTION_uninstallPackageWithActiveAdmins = (android.os.IBinder.FIRST_CALL_TRANSACTION + 227);
    static final int TRANSACTION_isDeviceProvisioned = (android.os.IBinder.FIRST_CALL_TRANSACTION + 228);
    static final int TRANSACTION_isDeviceProvisioningConfigApplied = (android.os.IBinder.FIRST_CALL_TRANSACTION + 229);
    static final int TRANSACTION_setDeviceProvisioningConfigApplied = (android.os.IBinder.FIRST_CALL_TRANSACTION + 230);
    static final int TRANSACTION_forceUpdateUserSetupComplete = (android.os.IBinder.FIRST_CALL_TRANSACTION + 231);
    static final int TRANSACTION_setBackupServiceEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 232);
    static final int TRANSACTION_isBackupServiceEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 233);
    static final int TRANSACTION_setNetworkLoggingEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 234);
    static final int TRANSACTION_isNetworkLoggingEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 235);
    static final int TRANSACTION_retrieveNetworkLogs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 236);
    static final int TRANSACTION_bindDeviceAdminServiceAsUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 237);
    private boolean onTransact$bindDeviceAdminServiceAsUser$(android.os.Parcel data, android.os.Parcel reply) throws android.os.RemoteException
    {
      data.enforceInterface(DESCRIPTOR);
      android.content.ComponentName _arg0;
      if ((0!=data.readInt())) {
        _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
      }
      else {
        _arg0 = null;
      }
      android.app.IApplicationThread _arg1;
      _arg1 = android.app.IApplicationThread.Stub.asInterface(data.readStrongBinder());
      android.os.IBinder _arg2;
      _arg2 = data.readStrongBinder();
      android.content.Intent _arg3;
      if ((0!=data.readInt())) {
        _arg3 = android.content.Intent.CREATOR.createFromParcel(data);
      }
      else {
        _arg3 = null;
      }
      android.app.IServiceConnection _arg4;
      _arg4 = android.app.IServiceConnection.Stub.asInterface(data.readStrongBinder());
      int _arg5;
      _arg5 = data.readInt();
      int _arg6;
      _arg6 = data.readInt();
      boolean _result = this.bindDeviceAdminServiceAsUser(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
      reply.writeNoException();
      reply.writeInt(((_result)?(1):(0)));
      return true;
    }
    static final int TRANSACTION_getBindDeviceAdminTargetUsers = (android.os.IBinder.FIRST_CALL_TRANSACTION + 238);
    static final int TRANSACTION_isEphemeralUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 239);
    static final int TRANSACTION_getLastSecurityLogRetrievalTime = (android.os.IBinder.FIRST_CALL_TRANSACTION + 240);
    static final int TRANSACTION_getLastBugReportRequestTime = (android.os.IBinder.FIRST_CALL_TRANSACTION + 241);
    static final int TRANSACTION_getLastNetworkLogRetrievalTime = (android.os.IBinder.FIRST_CALL_TRANSACTION + 242);
    static final int TRANSACTION_setResetPasswordToken = (android.os.IBinder.FIRST_CALL_TRANSACTION + 243);
    static final int TRANSACTION_clearResetPasswordToken = (android.os.IBinder.FIRST_CALL_TRANSACTION + 244);
    static final int TRANSACTION_isResetPasswordTokenActive = (android.os.IBinder.FIRST_CALL_TRANSACTION + 245);
    static final int TRANSACTION_resetPasswordWithToken = (android.os.IBinder.FIRST_CALL_TRANSACTION + 246);
    static final int TRANSACTION_isCurrentInputMethodSetByOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 247);
    static final int TRANSACTION_getOwnerInstalledCaCerts = (android.os.IBinder.FIRST_CALL_TRANSACTION + 248);
    static final int TRANSACTION_clearApplicationUserData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 249);
    static final int TRANSACTION_setLogoutEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 250);
    static final int TRANSACTION_isLogoutEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 251);
    static final int TRANSACTION_getDisallowedSystemApps = (android.os.IBinder.FIRST_CALL_TRANSACTION + 252);
    static final int TRANSACTION_transferOwnership = (android.os.IBinder.FIRST_CALL_TRANSACTION + 253);
    static final int TRANSACTION_getTransferOwnershipBundle = (android.os.IBinder.FIRST_CALL_TRANSACTION + 254);
    static final int TRANSACTION_setStartUserSessionMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 255);
    static final int TRANSACTION_setEndUserSessionMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 256);
    static final int TRANSACTION_getStartUserSessionMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 257);
    static final int TRANSACTION_getEndUserSessionMessage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 258);
    static final int TRANSACTION_setMeteredDataDisabledPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 259);
    static final int TRANSACTION_getMeteredDataDisabledPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 260);
    static final int TRANSACTION_addOverrideApn = (android.os.IBinder.FIRST_CALL_TRANSACTION + 261);
    static final int TRANSACTION_updateOverrideApn = (android.os.IBinder.FIRST_CALL_TRANSACTION + 262);
    static final int TRANSACTION_removeOverrideApn = (android.os.IBinder.FIRST_CALL_TRANSACTION + 263);
    static final int TRANSACTION_getOverrideApns = (android.os.IBinder.FIRST_CALL_TRANSACTION + 264);
    static final int TRANSACTION_setOverrideApnsEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 265);
    static final int TRANSACTION_isOverrideApnEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 266);
    static final int TRANSACTION_isMeteredDataDisabledPackageForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 267);
    static final int TRANSACTION_setGlobalPrivateDns = (android.os.IBinder.FIRST_CALL_TRANSACTION + 268);
    static final int TRANSACTION_getGlobalPrivateDnsMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 269);
    static final int TRANSACTION_getGlobalPrivateDnsHost = (android.os.IBinder.FIRST_CALL_TRANSACTION + 270);
    static final int TRANSACTION_grantDeviceIdsAccessToProfileOwner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 271);
    static final int TRANSACTION_installUpdateFromFile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 272);
    static final int TRANSACTION_setCrossProfileCalendarPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 273);
    static final int TRANSACTION_getCrossProfileCalendarPackages = (android.os.IBinder.FIRST_CALL_TRANSACTION + 274);
    static final int TRANSACTION_isPackageAllowedToAccessCalendarForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 275);
    static final int TRANSACTION_getCrossProfileCalendarPackagesForUser = (android.os.IBinder.FIRST_CALL_TRANSACTION + 276);
    static final int TRANSACTION_isManagedKiosk = (android.os.IBinder.FIRST_CALL_TRANSACTION + 277);
    static final int TRANSACTION_isUnattendedManagedKiosk = (android.os.IBinder.FIRST_CALL_TRANSACTION + 278);
    static final int TRANSACTION_startViewCalendarEventInManagedProfile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 279);
    private boolean onTransact$startViewCalendarEventInManagedProfile$(android.os.Parcel data, android.os.Parcel reply) throws android.os.RemoteException
    {
      data.enforceInterface(DESCRIPTOR);
      java.lang.String _arg0;
      _arg0 = data.readString();
      long _arg1;
      _arg1 = data.readLong();
      long _arg2;
      _arg2 = data.readLong();
      long _arg3;
      _arg3 = data.readLong();
      boolean _arg4;
      _arg4 = (0!=data.readInt());
      int _arg5;
      _arg5 = data.readInt();
      boolean _result = this.startViewCalendarEventInManagedProfile(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
      reply.writeNoException();
      reply.writeInt(((_result)?(1):(0)));
      return true;
    }
    static final int TRANSACTION_requireSecureKeyguard = (android.os.IBinder.FIRST_CALL_TRANSACTION + 280);
    public static boolean setDefaultImpl(android.app.admin.IDevicePolicyManager impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.app.admin.IDevicePolicyManager getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void setPasswordQuality(android.content.ComponentName who, int quality, boolean parent) throws android.os.RemoteException;
  public int getPasswordQuality(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordMinimumLength(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordMinimumLength(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordMinimumUpperCase(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordMinimumUpperCase(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordMinimumLowerCase(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordMinimumLowerCase(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordMinimumLetters(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordMinimumLetters(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordMinimumNumeric(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordMinimumNumeric(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordMinimumSymbols(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordMinimumSymbols(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordMinimumNonLetter(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordMinimumNonLetter(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordHistoryLength(android.content.ComponentName who, int length, boolean parent) throws android.os.RemoteException;
  public int getPasswordHistoryLength(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setPasswordExpirationTimeout(android.content.ComponentName who, long expiration, boolean parent) throws android.os.RemoteException;
  public long getPasswordExpirationTimeout(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public long getPasswordExpiration(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public boolean isActivePasswordSufficient(int userHandle, boolean parent) throws android.os.RemoteException;
  public boolean isProfileActivePasswordSufficientForParent(int userHandle) throws android.os.RemoteException;
  public int getPasswordComplexity() throws android.os.RemoteException;
  public boolean isUsingUnifiedPassword(android.content.ComponentName admin) throws android.os.RemoteException;
  public int getCurrentFailedPasswordAttempts(int userHandle, boolean parent) throws android.os.RemoteException;
  public int getProfileWithMinimumFailedPasswordsForWipe(int userHandle, boolean parent) throws android.os.RemoteException;
  public void setMaximumFailedPasswordsForWipe(android.content.ComponentName admin, int num, boolean parent) throws android.os.RemoteException;
  public int getMaximumFailedPasswordsForWipe(android.content.ComponentName admin, int userHandle, boolean parent) throws android.os.RemoteException;
  public boolean resetPassword(java.lang.String password, int flags) throws android.os.RemoteException;
  public void setMaximumTimeToLock(android.content.ComponentName who, long timeMs, boolean parent) throws android.os.RemoteException;
  public long getMaximumTimeToLock(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setRequiredStrongAuthTimeout(android.content.ComponentName who, long timeMs, boolean parent) throws android.os.RemoteException;
  public long getRequiredStrongAuthTimeout(android.content.ComponentName who, int userId, boolean parent) throws android.os.RemoteException;
  public void lockNow(int flags, boolean parent) throws android.os.RemoteException;
  public void wipeDataWithReason(int flags, java.lang.String wipeReasonForUser) throws android.os.RemoteException;
  public android.content.ComponentName setGlobalProxy(android.content.ComponentName admin, java.lang.String proxySpec, java.lang.String exclusionList) throws android.os.RemoteException;
  public android.content.ComponentName getGlobalProxyAdmin(int userHandle) throws android.os.RemoteException;
  public void setRecommendedGlobalProxy(android.content.ComponentName admin, android.net.ProxyInfo proxyInfo) throws android.os.RemoteException;
  public int setStorageEncryption(android.content.ComponentName who, boolean encrypt) throws android.os.RemoteException;
  public boolean getStorageEncryption(android.content.ComponentName who, int userHandle) throws android.os.RemoteException;
  public int getStorageEncryptionStatus(java.lang.String callerPackage, int userHandle) throws android.os.RemoteException;
  public boolean requestBugreport(android.content.ComponentName who) throws android.os.RemoteException;
  public void setCameraDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException;
  public boolean getCameraDisabled(android.content.ComponentName who, int userHandle) throws android.os.RemoteException;
  public void setScreenCaptureDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException;
  public boolean getScreenCaptureDisabled(android.content.ComponentName who, int userHandle) throws android.os.RemoteException;
  public void setKeyguardDisabledFeatures(android.content.ComponentName who, int which, boolean parent) throws android.os.RemoteException;
  public int getKeyguardDisabledFeatures(android.content.ComponentName who, int userHandle, boolean parent) throws android.os.RemoteException;
  public void setActiveAdmin(android.content.ComponentName policyReceiver, boolean refreshing, int userHandle) throws android.os.RemoteException;
  public boolean isAdminActive(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException;
  public java.util.List<android.content.ComponentName> getActiveAdmins(int userHandle) throws android.os.RemoteException;
  @android.annotation.UnsupportedAppUsage
  public boolean packageHasActiveAdmins(java.lang.String packageName, int userHandle) throws android.os.RemoteException;
  public void getRemoveWarning(android.content.ComponentName policyReceiver, android.os.RemoteCallback result, int userHandle) throws android.os.RemoteException;
  public void removeActiveAdmin(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException;
  public void forceRemoveActiveAdmin(android.content.ComponentName policyReceiver, int userHandle) throws android.os.RemoteException;
  public boolean hasGrantedPolicy(android.content.ComponentName policyReceiver, int usesPolicy, int userHandle) throws android.os.RemoteException;
  public void setActivePasswordState(android.app.admin.PasswordMetrics metrics, int userHandle) throws android.os.RemoteException;
  public void reportPasswordChanged(int userId) throws android.os.RemoteException;
  public void reportFailedPasswordAttempt(int userHandle) throws android.os.RemoteException;
  public void reportSuccessfulPasswordAttempt(int userHandle) throws android.os.RemoteException;
  public void reportFailedBiometricAttempt(int userHandle) throws android.os.RemoteException;
  public void reportSuccessfulBiometricAttempt(int userHandle) throws android.os.RemoteException;
  public void reportKeyguardDismissed(int userHandle) throws android.os.RemoteException;
  public void reportKeyguardSecured(int userHandle) throws android.os.RemoteException;
  public boolean setDeviceOwner(android.content.ComponentName who, java.lang.String ownerName, int userId) throws android.os.RemoteException;
  public android.content.ComponentName getDeviceOwnerComponent(boolean callingUserOnly) throws android.os.RemoteException;
  public boolean hasDeviceOwner() throws android.os.RemoteException;
  public java.lang.String getDeviceOwnerName() throws android.os.RemoteException;
  public void clearDeviceOwner(java.lang.String packageName) throws android.os.RemoteException;
  public int getDeviceOwnerUserId() throws android.os.RemoteException;
  public boolean setProfileOwner(android.content.ComponentName who, java.lang.String ownerName, int userHandle) throws android.os.RemoteException;
  public android.content.ComponentName getProfileOwnerAsUser(int userHandle) throws android.os.RemoteException;
  public android.content.ComponentName getProfileOwner(int userHandle) throws android.os.RemoteException;
  public java.lang.String getProfileOwnerName(int userHandle) throws android.os.RemoteException;
  public void setProfileEnabled(android.content.ComponentName who) throws android.os.RemoteException;
  public void setProfileName(android.content.ComponentName who, java.lang.String profileName) throws android.os.RemoteException;
  public void clearProfileOwner(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean hasUserSetupCompleted() throws android.os.RemoteException;
  public boolean checkDeviceIdentifierAccess(java.lang.String packageName, int pid, int uid) throws android.os.RemoteException;
  public void setDeviceOwnerLockScreenInfo(android.content.ComponentName who, java.lang.CharSequence deviceOwnerInfo) throws android.os.RemoteException;
  public java.lang.CharSequence getDeviceOwnerLockScreenInfo() throws android.os.RemoteException;
  public java.lang.String[] setPackagesSuspended(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String[] packageNames, boolean suspended) throws android.os.RemoteException;
  public boolean isPackageSuspended(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException;
  public boolean installCaCert(android.content.ComponentName admin, java.lang.String callerPackage, byte[] certBuffer) throws android.os.RemoteException;
  public void uninstallCaCerts(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String[] aliases) throws android.os.RemoteException;
  public void enforceCanManageCaCerts(android.content.ComponentName admin, java.lang.String callerPackage) throws android.os.RemoteException;
  public boolean approveCaCert(java.lang.String alias, int userHandle, boolean approval) throws android.os.RemoteException;
  public boolean isCaCertApproved(java.lang.String alias, int userHandle) throws android.os.RemoteException;
  public boolean installKeyPair(android.content.ComponentName who, java.lang.String callerPackage, byte[] privKeyBuffer, byte[] certBuffer, byte[] certChainBuffer, java.lang.String alias, boolean requestAccess, boolean isUserSelectable) throws android.os.RemoteException;
  public boolean removeKeyPair(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String alias) throws android.os.RemoteException;
  public boolean generateKeyPair(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String algorithm, android.security.keystore.ParcelableKeyGenParameterSpec keySpec, int idAttestationFlags, android.security.keymaster.KeymasterCertificateChain attestationChain) throws android.os.RemoteException;
  public boolean setKeyPairCertificate(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String alias, byte[] certBuffer, byte[] certChainBuffer, boolean isUserSelectable) throws android.os.RemoteException;
  public void choosePrivateKeyAlias(int uid, android.net.Uri uri, java.lang.String alias, android.os.IBinder aliasCallback) throws android.os.RemoteException;
  public void setDelegatedScopes(android.content.ComponentName who, java.lang.String delegatePackage, java.util.List<java.lang.String> scopes) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getDelegatedScopes(android.content.ComponentName who, java.lang.String delegatePackage) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getDelegatePackages(android.content.ComponentName who, java.lang.String scope) throws android.os.RemoteException;
  public void setCertInstallerPackage(android.content.ComponentName who, java.lang.String installerPackage) throws android.os.RemoteException;
  public java.lang.String getCertInstallerPackage(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean setAlwaysOnVpnPackage(android.content.ComponentName who, java.lang.String vpnPackage, boolean lockdown, java.util.List<java.lang.String> lockdownWhitelist) throws android.os.RemoteException;
  public java.lang.String getAlwaysOnVpnPackage(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean isAlwaysOnVpnLockdownEnabled(android.content.ComponentName who) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getAlwaysOnVpnLockdownWhitelist(android.content.ComponentName who) throws android.os.RemoteException;
  public void addPersistentPreferredActivity(android.content.ComponentName admin, android.content.IntentFilter filter, android.content.ComponentName activity) throws android.os.RemoteException;
  public void clearPackagePersistentPreferredActivities(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException;
  public void setDefaultSmsApplication(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException;
  public void setApplicationRestrictions(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String packageName, android.os.Bundle settings) throws android.os.RemoteException;
  public android.os.Bundle getApplicationRestrictions(android.content.ComponentName who, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException;
  public boolean setApplicationRestrictionsManagingPackage(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException;
  public java.lang.String getApplicationRestrictionsManagingPackage(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isCallerApplicationRestrictionsManagingPackage(java.lang.String callerPackage) throws android.os.RemoteException;
  public void setRestrictionsProvider(android.content.ComponentName who, android.content.ComponentName provider) throws android.os.RemoteException;
  public android.content.ComponentName getRestrictionsProvider(int userHandle) throws android.os.RemoteException;
  public void setUserRestriction(android.content.ComponentName who, java.lang.String key, boolean enable) throws android.os.RemoteException;
  public android.os.Bundle getUserRestrictions(android.content.ComponentName who) throws android.os.RemoteException;
  public void addCrossProfileIntentFilter(android.content.ComponentName admin, android.content.IntentFilter filter, int flags) throws android.os.RemoteException;
  public void clearCrossProfileIntentFilters(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean setPermittedAccessibilityServices(android.content.ComponentName admin, java.util.List packageList) throws android.os.RemoteException;
  public java.util.List getPermittedAccessibilityServices(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.util.List getPermittedAccessibilityServicesForUser(int userId) throws android.os.RemoteException;
  public boolean isAccessibilityServicePermittedByAdmin(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException;
  public boolean setPermittedInputMethods(android.content.ComponentName admin, java.util.List packageList) throws android.os.RemoteException;
  public java.util.List getPermittedInputMethods(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.util.List getPermittedInputMethodsForCurrentUser() throws android.os.RemoteException;
  public boolean isInputMethodPermittedByAdmin(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException;
  public boolean setPermittedCrossProfileNotificationListeners(android.content.ComponentName admin, java.util.List<java.lang.String> packageList) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getPermittedCrossProfileNotificationListeners(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isNotificationListenerServicePermitted(java.lang.String packageName, int userId) throws android.os.RemoteException;
  public android.content.Intent createAdminSupportIntent(java.lang.String restriction) throws android.os.RemoteException;
  public boolean setApplicationHidden(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, boolean hidden) throws android.os.RemoteException;
  public boolean isApplicationHidden(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException;
  public android.os.UserHandle createAndManageUser(android.content.ComponentName who, java.lang.String name, android.content.ComponentName profileOwner, android.os.PersistableBundle adminExtras, int flags) throws android.os.RemoteException;
  public boolean removeUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException;
  public boolean switchUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException;
  public int startUserInBackground(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException;
  public int stopUser(android.content.ComponentName who, android.os.UserHandle userHandle) throws android.os.RemoteException;
  public int logoutUser(android.content.ComponentName who) throws android.os.RemoteException;
  public java.util.List<android.os.UserHandle> getSecondaryUsers(android.content.ComponentName who) throws android.os.RemoteException;
  public void enableSystemApp(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException;
  public int enableSystemAppWithIntent(android.content.ComponentName admin, java.lang.String callerPackage, android.content.Intent intent) throws android.os.RemoteException;
  public boolean installExistingPackage(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName) throws android.os.RemoteException;
  public void setAccountManagementDisabled(android.content.ComponentName who, java.lang.String accountType, boolean disabled) throws android.os.RemoteException;
  public java.lang.String[] getAccountTypesWithManagementDisabled() throws android.os.RemoteException;
  public java.lang.String[] getAccountTypesWithManagementDisabledAsUser(int userId) throws android.os.RemoteException;
  public void setLockTaskPackages(android.content.ComponentName who, java.lang.String[] packages) throws android.os.RemoteException;
  public java.lang.String[] getLockTaskPackages(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean isLockTaskPermitted(java.lang.String pkg) throws android.os.RemoteException;
  public void setLockTaskFeatures(android.content.ComponentName who, int flags) throws android.os.RemoteException;
  public int getLockTaskFeatures(android.content.ComponentName who) throws android.os.RemoteException;
  public void setGlobalSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException;
  public void setSystemSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException;
  public void setSecureSetting(android.content.ComponentName who, java.lang.String setting, java.lang.String value) throws android.os.RemoteException;
  public boolean setTime(android.content.ComponentName who, long millis) throws android.os.RemoteException;
  public boolean setTimeZone(android.content.ComponentName who, java.lang.String timeZone) throws android.os.RemoteException;
  public void setMasterVolumeMuted(android.content.ComponentName admin, boolean on) throws android.os.RemoteException;
  public boolean isMasterVolumeMuted(android.content.ComponentName admin) throws android.os.RemoteException;
  public void notifyLockTaskModeChanged(boolean isEnabled, java.lang.String pkg, int userId) throws android.os.RemoteException;
  public void setUninstallBlocked(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, boolean uninstallBlocked) throws android.os.RemoteException;
  public boolean isUninstallBlocked(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException;
  public void setCrossProfileCallerIdDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException;
  public boolean getCrossProfileCallerIdDisabled(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean getCrossProfileCallerIdDisabledForUser(int userId) throws android.os.RemoteException;
  public void setCrossProfileContactsSearchDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException;
  public boolean getCrossProfileContactsSearchDisabled(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean getCrossProfileContactsSearchDisabledForUser(int userId) throws android.os.RemoteException;
  public void startManagedQuickContact(java.lang.String lookupKey, long contactId, boolean isContactIdIgnored, long directoryId, android.content.Intent originalIntent) throws android.os.RemoteException;
  public void setBluetoothContactSharingDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException;
  public boolean getBluetoothContactSharingDisabled(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean getBluetoothContactSharingDisabledForUser(int userId) throws android.os.RemoteException;
  public void setTrustAgentConfiguration(android.content.ComponentName admin, android.content.ComponentName agent, android.os.PersistableBundle args, boolean parent) throws android.os.RemoteException;
  public java.util.List<android.os.PersistableBundle> getTrustAgentConfiguration(android.content.ComponentName admin, android.content.ComponentName agent, int userId, boolean parent) throws android.os.RemoteException;
  public boolean addCrossProfileWidgetProvider(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException;
  public boolean removeCrossProfileWidgetProvider(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getCrossProfileWidgetProviders(android.content.ComponentName admin) throws android.os.RemoteException;
  public void setAutoTimeRequired(android.content.ComponentName who, boolean required) throws android.os.RemoteException;
  public boolean getAutoTimeRequired() throws android.os.RemoteException;
  public void setForceEphemeralUsers(android.content.ComponentName who, boolean forceEpehemeralUsers) throws android.os.RemoteException;
  public boolean getForceEphemeralUsers(android.content.ComponentName who) throws android.os.RemoteException;
  public boolean isRemovingAdmin(android.content.ComponentName adminReceiver, int userHandle) throws android.os.RemoteException;
  public void setUserIcon(android.content.ComponentName admin, android.graphics.Bitmap icon) throws android.os.RemoteException;
  public void setSystemUpdatePolicy(android.content.ComponentName who, android.app.admin.SystemUpdatePolicy policy) throws android.os.RemoteException;
  public android.app.admin.SystemUpdatePolicy getSystemUpdatePolicy() throws android.os.RemoteException;
  public void clearSystemUpdatePolicyFreezePeriodRecord() throws android.os.RemoteException;
  public boolean setKeyguardDisabled(android.content.ComponentName admin, boolean disabled) throws android.os.RemoteException;
  public boolean setStatusBarDisabled(android.content.ComponentName who, boolean disabled) throws android.os.RemoteException;
  public boolean getDoNotAskCredentialsOnBoot() throws android.os.RemoteException;
  public void notifyPendingSystemUpdate(android.app.admin.SystemUpdateInfo info) throws android.os.RemoteException;
  public android.app.admin.SystemUpdateInfo getPendingSystemUpdate(android.content.ComponentName admin) throws android.os.RemoteException;
  public void setPermissionPolicy(android.content.ComponentName admin, java.lang.String callerPackage, int policy) throws android.os.RemoteException;
  public int getPermissionPolicy(android.content.ComponentName admin) throws android.os.RemoteException;
  public void setPermissionGrantState(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, java.lang.String permission, int grantState, android.os.RemoteCallback resultReceiver) throws android.os.RemoteException;
  public int getPermissionGrantState(android.content.ComponentName admin, java.lang.String callerPackage, java.lang.String packageName, java.lang.String permission) throws android.os.RemoteException;
  public boolean isProvisioningAllowed(java.lang.String action, java.lang.String packageName) throws android.os.RemoteException;
  public int checkProvisioningPreCondition(java.lang.String action, java.lang.String packageName) throws android.os.RemoteException;
  public void setKeepUninstalledPackages(android.content.ComponentName admin, java.lang.String callerPackage, java.util.List<java.lang.String> packageList) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getKeepUninstalledPackages(android.content.ComponentName admin, java.lang.String callerPackage) throws android.os.RemoteException;
  public boolean isManagedProfile(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isSystemOnlyUser(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.lang.String getWifiMacAddress(android.content.ComponentName admin) throws android.os.RemoteException;
  public void reboot(android.content.ComponentName admin) throws android.os.RemoteException;
  public void setShortSupportMessage(android.content.ComponentName admin, java.lang.CharSequence message) throws android.os.RemoteException;
  public java.lang.CharSequence getShortSupportMessage(android.content.ComponentName admin) throws android.os.RemoteException;
  public void setLongSupportMessage(android.content.ComponentName admin, java.lang.CharSequence message) throws android.os.RemoteException;
  public java.lang.CharSequence getLongSupportMessage(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.lang.CharSequence getShortSupportMessageForUser(android.content.ComponentName admin, int userHandle) throws android.os.RemoteException;
  public java.lang.CharSequence getLongSupportMessageForUser(android.content.ComponentName admin, int userHandle) throws android.os.RemoteException;
  public boolean isSeparateProfileChallengeAllowed(int userHandle) throws android.os.RemoteException;
  public void setOrganizationColor(android.content.ComponentName admin, int color) throws android.os.RemoteException;
  public void setOrganizationColorForUser(int color, int userId) throws android.os.RemoteException;
  public int getOrganizationColor(android.content.ComponentName admin) throws android.os.RemoteException;
  public int getOrganizationColorForUser(int userHandle) throws android.os.RemoteException;
  public void setOrganizationName(android.content.ComponentName admin, java.lang.CharSequence title) throws android.os.RemoteException;
  public java.lang.CharSequence getOrganizationName(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.lang.CharSequence getDeviceOwnerOrganizationName() throws android.os.RemoteException;
  public java.lang.CharSequence getOrganizationNameForUser(int userHandle) throws android.os.RemoteException;
  public int getUserProvisioningState() throws android.os.RemoteException;
  public void setUserProvisioningState(int state, int userHandle) throws android.os.RemoteException;
  public void setAffiliationIds(android.content.ComponentName admin, java.util.List<java.lang.String> ids) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getAffiliationIds(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isAffiliatedUser() throws android.os.RemoteException;
  public void setSecurityLoggingEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException;
  public boolean isSecurityLoggingEnabled(android.content.ComponentName admin) throws android.os.RemoteException;
  public android.content.pm.ParceledListSlice retrieveSecurityLogs(android.content.ComponentName admin) throws android.os.RemoteException;
  public android.content.pm.ParceledListSlice retrievePreRebootSecurityLogs(android.content.ComponentName admin) throws android.os.RemoteException;
  public long forceNetworkLogs() throws android.os.RemoteException;
  public long forceSecurityLogs() throws android.os.RemoteException;
  public boolean isUninstallInQueue(java.lang.String packageName) throws android.os.RemoteException;
  public void uninstallPackageWithActiveAdmins(java.lang.String packageName) throws android.os.RemoteException;
  public boolean isDeviceProvisioned() throws android.os.RemoteException;
  public boolean isDeviceProvisioningConfigApplied() throws android.os.RemoteException;
  public void setDeviceProvisioningConfigApplied() throws android.os.RemoteException;
  public void forceUpdateUserSetupComplete() throws android.os.RemoteException;
  public void setBackupServiceEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException;
  public boolean isBackupServiceEnabled(android.content.ComponentName admin) throws android.os.RemoteException;
  public void setNetworkLoggingEnabled(android.content.ComponentName admin, java.lang.String packageName, boolean enabled) throws android.os.RemoteException;
  public boolean isNetworkLoggingEnabled(android.content.ComponentName admin, java.lang.String packageName) throws android.os.RemoteException;
  public java.util.List<android.app.admin.NetworkEvent> retrieveNetworkLogs(android.content.ComponentName admin, java.lang.String packageName, long batchToken) throws android.os.RemoteException;
  public boolean bindDeviceAdminServiceAsUser(android.content.ComponentName admin, android.app.IApplicationThread caller, android.os.IBinder token, android.content.Intent service, android.app.IServiceConnection connection, int flags, int targetUserId) throws android.os.RemoteException;
  public java.util.List<android.os.UserHandle> getBindDeviceAdminTargetUsers(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isEphemeralUser(android.content.ComponentName admin) throws android.os.RemoteException;
  public long getLastSecurityLogRetrievalTime() throws android.os.RemoteException;
  public long getLastBugReportRequestTime() throws android.os.RemoteException;
  public long getLastNetworkLogRetrievalTime() throws android.os.RemoteException;
  public boolean setResetPasswordToken(android.content.ComponentName admin, byte[] token) throws android.os.RemoteException;
  public boolean clearResetPasswordToken(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isResetPasswordTokenActive(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean resetPasswordWithToken(android.content.ComponentName admin, java.lang.String password, byte[] token, int flags) throws android.os.RemoteException;
  public boolean isCurrentInputMethodSetByOwner() throws android.os.RemoteException;
  public android.content.pm.StringParceledListSlice getOwnerInstalledCaCerts(android.os.UserHandle user) throws android.os.RemoteException;
  public void clearApplicationUserData(android.content.ComponentName admin, java.lang.String packageName, android.content.pm.IPackageDataObserver callback) throws android.os.RemoteException;
  public void setLogoutEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException;
  public boolean isLogoutEnabled() throws android.os.RemoteException;
  public java.util.List<java.lang.String> getDisallowedSystemApps(android.content.ComponentName admin, int userId, java.lang.String provisioningAction) throws android.os.RemoteException;
  public void transferOwnership(android.content.ComponentName admin, android.content.ComponentName target, android.os.PersistableBundle bundle) throws android.os.RemoteException;
  public android.os.PersistableBundle getTransferOwnershipBundle() throws android.os.RemoteException;
  public void setStartUserSessionMessage(android.content.ComponentName admin, java.lang.CharSequence startUserSessionMessage) throws android.os.RemoteException;
  public void setEndUserSessionMessage(android.content.ComponentName admin, java.lang.CharSequence endUserSessionMessage) throws android.os.RemoteException;
  public java.lang.CharSequence getStartUserSessionMessage(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.lang.CharSequence getEndUserSessionMessage(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.util.List<java.lang.String> setMeteredDataDisabledPackages(android.content.ComponentName admin, java.util.List<java.lang.String> packageNames) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getMeteredDataDisabledPackages(android.content.ComponentName admin) throws android.os.RemoteException;
  public int addOverrideApn(android.content.ComponentName admin, android.telephony.data.ApnSetting apnSetting) throws android.os.RemoteException;
  public boolean updateOverrideApn(android.content.ComponentName admin, int apnId, android.telephony.data.ApnSetting apnSetting) throws android.os.RemoteException;
  public boolean removeOverrideApn(android.content.ComponentName admin, int apnId) throws android.os.RemoteException;
  public java.util.List<android.telephony.data.ApnSetting> getOverrideApns(android.content.ComponentName admin) throws android.os.RemoteException;
  public void setOverrideApnsEnabled(android.content.ComponentName admin, boolean enabled) throws android.os.RemoteException;
  public boolean isOverrideApnEnabled(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isMeteredDataDisabledPackageForUser(android.content.ComponentName admin, java.lang.String packageName, int userId) throws android.os.RemoteException;
  public int setGlobalPrivateDns(android.content.ComponentName admin, int mode, java.lang.String privateDnsHost) throws android.os.RemoteException;
  public int getGlobalPrivateDnsMode(android.content.ComponentName admin) throws android.os.RemoteException;
  public java.lang.String getGlobalPrivateDnsHost(android.content.ComponentName admin) throws android.os.RemoteException;
  public void grantDeviceIdsAccessToProfileOwner(android.content.ComponentName who, int userId) throws android.os.RemoteException;
  public void installUpdateFromFile(android.content.ComponentName admin, android.os.ParcelFileDescriptor updateFileDescriptor, android.app.admin.StartInstallingUpdateCallback listener) throws android.os.RemoteException;
  public void setCrossProfileCalendarPackages(android.content.ComponentName admin, java.util.List<java.lang.String> packageNames) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getCrossProfileCalendarPackages(android.content.ComponentName admin) throws android.os.RemoteException;
  public boolean isPackageAllowedToAccessCalendarForUser(java.lang.String packageName, int userHandle) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getCrossProfileCalendarPackagesForUser(int userHandle) throws android.os.RemoteException;
  public boolean isManagedKiosk() throws android.os.RemoteException;
  public boolean isUnattendedManagedKiosk() throws android.os.RemoteException;
  public boolean startViewCalendarEventInManagedProfile(java.lang.String packageName, long eventId, long start, long end, boolean allDay, int flags) throws android.os.RemoteException;
  public boolean requireSecureKeyguard(int userHandle) throws android.os.RemoteException;
}
