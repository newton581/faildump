/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.nvidia.NvCPLSvc;
/** @hide */
public interface INvCPLRemoteService extends android.os.IInterface
{
  /** Default implementation for INvCPLRemoteService. */
  public static class Default implements com.nvidia.NvCPLSvc.INvCPLRemoteService
  {
    @Override public android.os.IBinder getToolsApiInterface(java.lang.String str) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String getAppProfileSettingString(java.lang.String pkgName, int settingId) throws android.os.RemoteException
    {
      return null;
    }
    @Override public int getAppProfileSettingInt(java.lang.String pkgName, int settingId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int getAppProfileSettingBoolean(java.lang.String pkgName, int settingId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public byte[] getAppProfileSetting3DVStruct(java.lang.String pkgName) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void handleIntent(android.content.Intent intent) throws android.os.RemoteException
    {
    }
    @Override public boolean setNvSaverAppInfo(java.lang.String pkgName, int list) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setNvSaverAppInfoAll(java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> appList) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> getNvSaverAppInfo(int i) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setAppProfileSetting(java.lang.String packageName, int typeId, int settingId, java.lang.String value) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getActiveProfileType(java.lang.String packageName) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int[] getProfileTypes(java.lang.String str) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setActiveProfileType(java.lang.String packageName, int typeId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public com.nvidia.NvCPLSvc.NvAppProfile[] getAppProfiles(java.lang.String[] strArr) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String getDeviceSerial() throws android.os.RemoteException
    {
      return null;
    }
    @Override public void powerHint(java.lang.String str) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.nvidia.NvCPLSvc.INvCPLRemoteService
  {
    private static final java.lang.String DESCRIPTOR = "com.nvidia.NvCPLSvc.INvCPLRemoteService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.nvidia.NvCPLSvc.INvCPLRemoteService interface,
     * generating a proxy if needed.
     */
    public static com.nvidia.NvCPLSvc.INvCPLRemoteService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.nvidia.NvCPLSvc.INvCPLRemoteService))) {
        return ((com.nvidia.NvCPLSvc.INvCPLRemoteService)iin);
      }
      return new com.nvidia.NvCPLSvc.INvCPLRemoteService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_getToolsApiInterface:
        {
          return "getToolsApiInterface";
        }
        case TRANSACTION_getAppProfileSettingString:
        {
          return "getAppProfileSettingString";
        }
        case TRANSACTION_getAppProfileSettingInt:
        {
          return "getAppProfileSettingInt";
        }
        case TRANSACTION_getAppProfileSettingBoolean:
        {
          return "getAppProfileSettingBoolean";
        }
        case TRANSACTION_getAppProfileSetting3DVStruct:
        {
          return "getAppProfileSetting3DVStruct";
        }
        case TRANSACTION_handleIntent:
        {
          return "handleIntent";
        }
        case TRANSACTION_setNvSaverAppInfo:
        {
          return "setNvSaverAppInfo";
        }
        case TRANSACTION_setNvSaverAppInfoAll:
        {
          return "setNvSaverAppInfoAll";
        }
        case TRANSACTION_getNvSaverAppInfo:
        {
          return "getNvSaverAppInfo";
        }
        case TRANSACTION_setAppProfileSetting:
        {
          return "setAppProfileSetting";
        }
        case TRANSACTION_getActiveProfileType:
        {
          return "getActiveProfileType";
        }
        case TRANSACTION_getProfileTypes:
        {
          return "getProfileTypes";
        }
        case TRANSACTION_setActiveProfileType:
        {
          return "setActiveProfileType";
        }
        case TRANSACTION_getAppProfiles:
        {
          return "getAppProfiles";
        }
        case TRANSACTION_getDeviceSerial:
        {
          return "getDeviceSerial";
        }
        case TRANSACTION_powerHint:
        {
          return "powerHint";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_getToolsApiInterface:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.os.IBinder _result = this.getToolsApiInterface(_arg0);
          reply.writeNoException();
          reply.writeStrongBinder(_result);
          return true;
        }
        case TRANSACTION_getAppProfileSettingString:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _result = this.getAppProfileSettingString(_arg0, _arg1);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getAppProfileSettingInt:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.getAppProfileSettingInt(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getAppProfileSettingBoolean:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.getAppProfileSettingBoolean(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getAppProfileSetting3DVStruct:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          byte[] _result = this.getAppProfileSetting3DVStruct(_arg0);
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_handleIntent:
        {
          data.enforceInterface(descriptor);
          android.content.Intent _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.Intent.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.handleIntent(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setNvSaverAppInfo:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.setNvSaverAppInfo(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setNvSaverAppInfoAll:
        {
          data.enforceInterface(descriptor);
          java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> _arg0;
          _arg0 = data.createTypedArrayList(com.nvidia.NvCPLSvc.NvSaverAppInfo.CREATOR);
          boolean _result = this.setNvSaverAppInfoAll(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getNvSaverAppInfo:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> _result = this.getNvSaverAppInfo(_arg0);
          reply.writeNoException();
          reply.writeTypedList(_result);
          return true;
        }
        case TRANSACTION_setAppProfileSetting:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _arg3;
          _arg3 = data.readString();
          boolean _result = this.setAppProfileSetting(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getActiveProfileType:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _result = this.getActiveProfileType(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getProfileTypes:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int[] _result = this.getProfileTypes(_arg0);
          reply.writeNoException();
          reply.writeIntArray(_result);
          return true;
        }
        case TRANSACTION_setActiveProfileType:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.setActiveProfileType(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getAppProfiles:
        {
          data.enforceInterface(descriptor);
          java.lang.String[] _arg0;
          _arg0 = data.createStringArray();
          com.nvidia.NvCPLSvc.NvAppProfile[] _result = this.getAppProfiles(_arg0);
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_getDeviceSerial:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getDeviceSerial();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_powerHint:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.powerHint(_arg0);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.nvidia.NvCPLSvc.INvCPLRemoteService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public android.os.IBinder getToolsApiInterface(java.lang.String str) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.IBinder _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(str);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getToolsApiInterface, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getToolsApiInterface(str);
          }
          _reply.readException();
          _result = _reply.readStrongBinder();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getAppProfileSettingString(java.lang.String pkgName, int settingId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkgName);
          _data.writeInt(settingId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAppProfileSettingString, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAppProfileSettingString(pkgName, settingId);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getAppProfileSettingInt(java.lang.String pkgName, int settingId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkgName);
          _data.writeInt(settingId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAppProfileSettingInt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAppProfileSettingInt(pkgName, settingId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getAppProfileSettingBoolean(java.lang.String pkgName, int settingId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkgName);
          _data.writeInt(settingId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAppProfileSettingBoolean, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAppProfileSettingBoolean(pkgName, settingId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public byte[] getAppProfileSetting3DVStruct(java.lang.String pkgName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkgName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAppProfileSetting3DVStruct, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAppProfileSetting3DVStruct(pkgName);
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void handleIntent(android.content.Intent intent) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((intent!=null)) {
            _data.writeInt(1);
            intent.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_handleIntent, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().handleIntent(intent);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean setNvSaverAppInfo(java.lang.String pkgName, int list) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkgName);
          _data.writeInt(list);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setNvSaverAppInfo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setNvSaverAppInfo(pkgName, list);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setNvSaverAppInfoAll(java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> appList) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeTypedList(appList);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setNvSaverAppInfoAll, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setNvSaverAppInfoAll(appList);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> getNvSaverAppInfo(int i) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(i);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getNvSaverAppInfo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getNvSaverAppInfo(i);
          }
          _reply.readException();
          _result = _reply.createTypedArrayList(com.nvidia.NvCPLSvc.NvSaverAppInfo.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setAppProfileSetting(java.lang.String packageName, int typeId, int settingId, java.lang.String value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(typeId);
          _data.writeInt(settingId);
          _data.writeString(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setAppProfileSetting, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setAppProfileSetting(packageName, typeId, settingId, value);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getActiveProfileType(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getActiveProfileType, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getActiveProfileType(packageName);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int[] getProfileTypes(java.lang.String str) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(str);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getProfileTypes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getProfileTypes(str);
          }
          _reply.readException();
          _result = _reply.createIntArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setActiveProfileType(java.lang.String packageName, int typeId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(typeId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setActiveProfileType, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setActiveProfileType(packageName, typeId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public com.nvidia.NvCPLSvc.NvAppProfile[] getAppProfiles(java.lang.String[] strArr) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        com.nvidia.NvCPLSvc.NvAppProfile[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringArray(strArr);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAppProfiles, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAppProfiles(strArr);
          }
          _reply.readException();
          _result = _reply.createTypedArray(com.nvidia.NvCPLSvc.NvAppProfile.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getDeviceSerial() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDeviceSerial, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDeviceSerial();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void powerHint(java.lang.String str) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(str);
          boolean _status = mRemote.transact(Stub.TRANSACTION_powerHint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().powerHint(str);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static com.nvidia.NvCPLSvc.INvCPLRemoteService sDefaultImpl;
    }
    static final int TRANSACTION_getToolsApiInterface = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getAppProfileSettingString = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_getAppProfileSettingInt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_getAppProfileSettingBoolean = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_getAppProfileSetting3DVStruct = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_handleIntent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_setNvSaverAppInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_setNvSaverAppInfoAll = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_getNvSaverAppInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_setAppProfileSetting = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_getActiveProfileType = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_getProfileTypes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_setActiveProfileType = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_getAppProfiles = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_getDeviceSerial = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_powerHint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    public static boolean setDefaultImpl(com.nvidia.NvCPLSvc.INvCPLRemoteService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.nvidia.NvCPLSvc.INvCPLRemoteService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public android.os.IBinder getToolsApiInterface(java.lang.String str) throws android.os.RemoteException;
  public java.lang.String getAppProfileSettingString(java.lang.String pkgName, int settingId) throws android.os.RemoteException;
  public int getAppProfileSettingInt(java.lang.String pkgName, int settingId) throws android.os.RemoteException;
  public int getAppProfileSettingBoolean(java.lang.String pkgName, int settingId) throws android.os.RemoteException;
  public byte[] getAppProfileSetting3DVStruct(java.lang.String pkgName) throws android.os.RemoteException;
  public void handleIntent(android.content.Intent intent) throws android.os.RemoteException;
  public boolean setNvSaverAppInfo(java.lang.String pkgName, int list) throws android.os.RemoteException;
  public boolean setNvSaverAppInfoAll(java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> appList) throws android.os.RemoteException;
  public java.util.List<com.nvidia.NvCPLSvc.NvSaverAppInfo> getNvSaverAppInfo(int i) throws android.os.RemoteException;
  public boolean setAppProfileSetting(java.lang.String packageName, int typeId, int settingId, java.lang.String value) throws android.os.RemoteException;
  public int getActiveProfileType(java.lang.String packageName) throws android.os.RemoteException;
  public int[] getProfileTypes(java.lang.String str) throws android.os.RemoteException;
  public boolean setActiveProfileType(java.lang.String packageName, int typeId) throws android.os.RemoteException;
  public com.nvidia.NvCPLSvc.NvAppProfile[] getAppProfiles(java.lang.String[] strArr) throws android.os.RemoteException;
  public java.lang.String getDeviceSerial() throws android.os.RemoteException;
  public void powerHint(java.lang.String str) throws android.os.RemoteException;
}
