/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/** @hide */
public interface IUpdateEngine extends android.os.IInterface
{
  /** Default implementation for IUpdateEngine. */
  public static class Default implements android.os.IUpdateEngine
  {
    /** @hide */
    @Override public void applyPayload(java.lang.String url, long payload_offset, long payload_size, java.lang.String[] headerKeyValuePairs) throws android.os.RemoteException
    {
    }
    /** @hide */
    @Override public boolean bind(android.os.IUpdateEngineCallback callback) throws android.os.RemoteException
    {
      return false;
    }
    /** @hide */
    @Override public boolean unbind(android.os.IUpdateEngineCallback callback) throws android.os.RemoteException
    {
      return false;
    }
    /** @hide */
    @Override public void suspend() throws android.os.RemoteException
    {
    }
    /** @hide */
    @Override public void resume() throws android.os.RemoteException
    {
    }
    /** @hide */
    @Override public void cancel() throws android.os.RemoteException
    {
    }
    /** @hide */
    @Override public void resetStatus() throws android.os.RemoteException
    {
    }
    /** @hide */
    @Override public boolean verifyPayloadApplicable(java.lang.String metadataFilename) throws android.os.RemoteException
    {
      return false;
    }
    /** @hide */
    @Override public void setPerformanceMode(boolean enable) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IUpdateEngine
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IUpdateEngine";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IUpdateEngine interface,
     * generating a proxy if needed.
     */
    public static android.os.IUpdateEngine asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IUpdateEngine))) {
        return ((android.os.IUpdateEngine)iin);
      }
      return new android.os.IUpdateEngine.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_applyPayload:
        {
          return "applyPayload";
        }
        case TRANSACTION_bind:
        {
          return "bind";
        }
        case TRANSACTION_unbind:
        {
          return "unbind";
        }
        case TRANSACTION_suspend:
        {
          return "suspend";
        }
        case TRANSACTION_resume:
        {
          return "resume";
        }
        case TRANSACTION_cancel:
        {
          return "cancel";
        }
        case TRANSACTION_resetStatus:
        {
          return "resetStatus";
        }
        case TRANSACTION_verifyPayloadApplicable:
        {
          return "verifyPayloadApplicable";
        }
        case TRANSACTION_setPerformanceMode:
        {
          return "setPerformanceMode";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_applyPayload:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          long _arg1;
          _arg1 = data.readLong();
          long _arg2;
          _arg2 = data.readLong();
          java.lang.String[] _arg3;
          _arg3 = data.createStringArray();
          this.applyPayload(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_bind:
        {
          data.enforceInterface(descriptor);
          android.os.IUpdateEngineCallback _arg0;
          _arg0 = android.os.IUpdateEngineCallback.Stub.asInterface(data.readStrongBinder());
          boolean _result = this.bind(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_unbind:
        {
          data.enforceInterface(descriptor);
          android.os.IUpdateEngineCallback _arg0;
          _arg0 = android.os.IUpdateEngineCallback.Stub.asInterface(data.readStrongBinder());
          boolean _result = this.unbind(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_suspend:
        {
          data.enforceInterface(descriptor);
          this.suspend();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_resume:
        {
          data.enforceInterface(descriptor);
          this.resume();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_cancel:
        {
          data.enforceInterface(descriptor);
          this.cancel();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_resetStatus:
        {
          data.enforceInterface(descriptor);
          this.resetStatus();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_verifyPayloadApplicable:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.verifyPayloadApplicable(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setPerformanceMode:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          this.setPerformanceMode(_arg0);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IUpdateEngine
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /** @hide */
      @Override public void applyPayload(java.lang.String url, long payload_offset, long payload_size, java.lang.String[] headerKeyValuePairs) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(url);
          _data.writeLong(payload_offset);
          _data.writeLong(payload_size);
          _data.writeStringArray(headerKeyValuePairs);
          boolean _status = mRemote.transact(Stub.TRANSACTION_applyPayload, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().applyPayload(url, payload_offset, payload_size, headerKeyValuePairs);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /** @hide */
      @Override public boolean bind(android.os.IUpdateEngineCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_bind, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().bind(callback);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /** @hide */
      @Override public boolean unbind(android.os.IUpdateEngineCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_unbind, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().unbind(callback);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /** @hide */
      @Override public void suspend() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_suspend, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().suspend();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /** @hide */
      @Override public void resume() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_resume, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().resume();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /** @hide */
      @Override public void cancel() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_cancel, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().cancel();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /** @hide */
      @Override public void resetStatus() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_resetStatus, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().resetStatus();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /** @hide */
      @Override public boolean verifyPayloadApplicable(java.lang.String metadataFilename) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(metadataFilename);
          boolean _status = mRemote.transact(Stub.TRANSACTION_verifyPayloadApplicable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().verifyPayloadApplicable(metadataFilename);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /** @hide */
      @Override public void setPerformanceMode(boolean enable) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((enable)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPerformanceMode, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPerformanceMode(enable);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.os.IUpdateEngine sDefaultImpl;
    }
    static final int TRANSACTION_applyPayload = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_bind = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_unbind = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_suspend = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_resume = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_cancel = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_resetStatus = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_verifyPayloadApplicable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_setPerformanceMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    public static boolean setDefaultImpl(android.os.IUpdateEngine impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IUpdateEngine getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /** @hide */
  public void applyPayload(java.lang.String url, long payload_offset, long payload_size, java.lang.String[] headerKeyValuePairs) throws android.os.RemoteException;
  /** @hide */
  public boolean bind(android.os.IUpdateEngineCallback callback) throws android.os.RemoteException;
  /** @hide */
  public boolean unbind(android.os.IUpdateEngineCallback callback) throws android.os.RemoteException;
  /** @hide */
  public void suspend() throws android.os.RemoteException;
  /** @hide */
  public void resume() throws android.os.RemoteException;
  /** @hide */
  public void cancel() throws android.os.RemoteException;
  /** @hide */
  public void resetStatus() throws android.os.RemoteException;
  /** @hide */
  public boolean verifyPayloadApplicable(java.lang.String metadataFilename) throws android.os.RemoteException;
  /** @hide */
  public void setPerformanceMode(boolean enable) throws android.os.RemoteException;
}
