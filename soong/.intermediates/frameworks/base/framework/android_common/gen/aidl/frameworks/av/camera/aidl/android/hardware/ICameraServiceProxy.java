/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.hardware;
/**
 * Binder interface for the camera service proxy running in system_server.
 *
 * Keep in sync with frameworks/av/include/camera/ICameraServiceProxy.h
 *
 * @hide
 */
public interface ICameraServiceProxy extends android.os.IInterface
{
  /** Default implementation for ICameraServiceProxy. */
  public static class Default implements android.hardware.ICameraServiceProxy
  {
    /**
         * Ping the service proxy to update the valid users for the camera service.
         */
    @Override public void pingForUserUpdate() throws android.os.RemoteException
    {
    }
    /**
         * Update the status of a camera device.
         */
    @Override public void notifyCameraState(java.lang.String cameraId, int facing, int newCameraState, java.lang.String clientName, int apiLevel) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.hardware.ICameraServiceProxy
  {
    private static final java.lang.String DESCRIPTOR = "android.hardware.ICameraServiceProxy";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.hardware.ICameraServiceProxy interface,
     * generating a proxy if needed.
     */
    public static android.hardware.ICameraServiceProxy asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.hardware.ICameraServiceProxy))) {
        return ((android.hardware.ICameraServiceProxy)iin);
      }
      return new android.hardware.ICameraServiceProxy.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_pingForUserUpdate:
        {
          return "pingForUserUpdate";
        }
        case TRANSACTION_notifyCameraState:
        {
          return "notifyCameraState";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_pingForUserUpdate:
        {
          data.enforceInterface(descriptor);
          this.pingForUserUpdate();
          return true;
        }
        case TRANSACTION_notifyCameraState:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          this.notifyCameraState(_arg0, _arg1, _arg2, _arg3, _arg4);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.hardware.ICameraServiceProxy
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Ping the service proxy to update the valid users for the camera service.
           */
      @Override public void pingForUserUpdate() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_pingForUserUpdate, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().pingForUserUpdate();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Update the status of a camera device.
           */
      @Override public void notifyCameraState(java.lang.String cameraId, int facing, int newCameraState, java.lang.String clientName, int apiLevel) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(cameraId);
          _data.writeInt(facing);
          _data.writeInt(newCameraState);
          _data.writeString(clientName);
          _data.writeInt(apiLevel);
          boolean _status = mRemote.transact(Stub.TRANSACTION_notifyCameraState, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notifyCameraState(cameraId, facing, newCameraState, clientName, apiLevel);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.hardware.ICameraServiceProxy sDefaultImpl;
    }
    static final int TRANSACTION_pingForUserUpdate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_notifyCameraState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(android.hardware.ICameraServiceProxy impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.hardware.ICameraServiceProxy getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int CAMERA_STATE_OPEN = 0;
  public static final int CAMERA_STATE_ACTIVE = 1;
  public static final int CAMERA_STATE_IDLE = 2;
  public static final int CAMERA_STATE_CLOSED = 3;
  public static final int CAMERA_FACING_BACK = 0;
  public static final int CAMERA_FACING_FRONT = 1;
  public static final int CAMERA_FACING_EXTERNAL = 2;
  public static final int CAMERA_API_LEVEL_1 = 1;
  public static final int CAMERA_API_LEVEL_2 = 2;
  /**
       * Ping the service proxy to update the valid users for the camera service.
       */
  public void pingForUserUpdate() throws android.os.RemoteException;
  /**
       * Update the status of a camera device.
       */
  public void notifyCameraState(java.lang.String cameraId, int facing, int newCameraState, java.lang.String clientName, int apiLevel) throws android.os.RemoteException;
}
