/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.android.net;
/** @hide */
public interface IProxyService extends android.os.IInterface
{
  /** Default implementation for IProxyService. */
  public static class Default implements com.android.net.IProxyService
  {
    @Override public java.lang.String resolvePacFile(java.lang.String host, java.lang.String url) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setPacFile(java.lang.String scriptContents) throws android.os.RemoteException
    {
    }
    @Override public void startPacSystem() throws android.os.RemoteException
    {
    }
    @Override public void stopPacSystem() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.android.net.IProxyService
  {
    private static final java.lang.String DESCRIPTOR = "com.android.net.IProxyService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.android.net.IProxyService interface,
     * generating a proxy if needed.
     */
    public static com.android.net.IProxyService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.android.net.IProxyService))) {
        return ((com.android.net.IProxyService)iin);
      }
      return new com.android.net.IProxyService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_resolvePacFile:
        {
          return "resolvePacFile";
        }
        case TRANSACTION_setPacFile:
        {
          return "setPacFile";
        }
        case TRANSACTION_startPacSystem:
        {
          return "startPacSystem";
        }
        case TRANSACTION_stopPacSystem:
        {
          return "stopPacSystem";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_resolvePacFile:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _result = this.resolvePacFile(_arg0, _arg1);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_setPacFile:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.setPacFile(_arg0);
          return true;
        }
        case TRANSACTION_startPacSystem:
        {
          data.enforceInterface(descriptor);
          this.startPacSystem();
          return true;
        }
        case TRANSACTION_stopPacSystem:
        {
          data.enforceInterface(descriptor);
          this.stopPacSystem();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.android.net.IProxyService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public java.lang.String resolvePacFile(java.lang.String host, java.lang.String url) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(host);
          _data.writeString(url);
          boolean _status = mRemote.transact(Stub.TRANSACTION_resolvePacFile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().resolvePacFile(host, url);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPacFile(java.lang.String scriptContents) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(scriptContents);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPacFile, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPacFile(scriptContents);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void startPacSystem() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_startPacSystem, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().startPacSystem();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void stopPacSystem() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_stopPacSystem, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().stopPacSystem();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static com.android.net.IProxyService sDefaultImpl;
    }
    static final int TRANSACTION_resolvePacFile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_setPacFile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_startPacSystem = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_stopPacSystem = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    public static boolean setDefaultImpl(com.android.net.IProxyService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.android.net.IProxyService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public java.lang.String resolvePacFile(java.lang.String host, java.lang.String url) throws android.os.RemoteException;
  public void setPacFile(java.lang.String scriptContents) throws android.os.RemoteException;
  public void startPacSystem() throws android.os.RemoteException;
  public void stopPacSystem() throws android.os.RemoteException;
}
