/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.security.keystore;
/**
 * @hide
 */
public interface IKeystoreExportKeyCallback extends android.os.IInterface
{
  /** Default implementation for IKeystoreExportKeyCallback. */
  public static class Default implements android.security.keystore.IKeystoreExportKeyCallback
  {
    @Override public void onFinished(android.security.keymaster.ExportResult result) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.security.keystore.IKeystoreExportKeyCallback
  {
    private static final java.lang.String DESCRIPTOR = "android.security.keystore.IKeystoreExportKeyCallback";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.security.keystore.IKeystoreExportKeyCallback interface,
     * generating a proxy if needed.
     */
    public static android.security.keystore.IKeystoreExportKeyCallback asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.security.keystore.IKeystoreExportKeyCallback))) {
        return ((android.security.keystore.IKeystoreExportKeyCallback)iin);
      }
      return new android.security.keystore.IKeystoreExportKeyCallback.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onFinished:
        {
          return "onFinished";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onFinished:
        {
          data.enforceInterface(descriptor);
          android.security.keymaster.ExportResult _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.security.keymaster.ExportResult.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.onFinished(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.security.keystore.IKeystoreExportKeyCallback
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onFinished(android.security.keymaster.ExportResult result) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((result!=null)) {
            _data.writeInt(1);
            result.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onFinished, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onFinished(result);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.security.keystore.IKeystoreExportKeyCallback sDefaultImpl;
    }
    static final int TRANSACTION_onFinished = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    public static boolean setDefaultImpl(android.security.keystore.IKeystoreExportKeyCallback impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.security.keystore.IKeystoreExportKeyCallback getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onFinished(android.security.keymaster.ExportResult result) throws android.os.RemoteException;
}
