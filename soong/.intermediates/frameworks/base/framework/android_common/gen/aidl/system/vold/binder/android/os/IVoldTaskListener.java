/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/** {@hide} */
public interface IVoldTaskListener extends android.os.IInterface
{
  /** Default implementation for IVoldTaskListener. */
  public static class Default implements android.os.IVoldTaskListener
  {
    @Override public void onStatus(int status, android.os.PersistableBundle extras) throws android.os.RemoteException
    {
    }
    @Override public void onFinished(int status, android.os.PersistableBundle extras) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IVoldTaskListener
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IVoldTaskListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IVoldTaskListener interface,
     * generating a proxy if needed.
     */
    public static android.os.IVoldTaskListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IVoldTaskListener))) {
        return ((android.os.IVoldTaskListener)iin);
      }
      return new android.os.IVoldTaskListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onStatus:
        {
          return "onStatus";
        }
        case TRANSACTION_onFinished:
        {
          return "onFinished";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onStatus:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.os.PersistableBundle _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.PersistableBundle.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onStatus(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onFinished:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.os.PersistableBundle _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.PersistableBundle.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onFinished(_arg0, _arg1);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IVoldTaskListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onStatus(int status, android.os.PersistableBundle extras) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(status);
          if ((extras!=null)) {
            _data.writeInt(1);
            extras.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onStatus, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onStatus(status, extras);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onFinished(int status, android.os.PersistableBundle extras) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(status);
          if ((extras!=null)) {
            _data.writeInt(1);
            extras.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onFinished, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onFinished(status, extras);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.os.IVoldTaskListener sDefaultImpl;
    }
    static final int TRANSACTION_onStatus = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onFinished = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(android.os.IVoldTaskListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IVoldTaskListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onStatus(int status, android.os.PersistableBundle extras) throws android.os.RemoteException;
  public void onFinished(int status, android.os.PersistableBundle extras) throws android.os.RemoteException;
}
