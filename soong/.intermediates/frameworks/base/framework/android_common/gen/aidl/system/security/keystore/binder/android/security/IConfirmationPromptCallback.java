/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.security;
/**
 * This must be kept manually in sync with system/security/keystore until AIDL
 * can generate both Java and C++ bindings.
 *
 * @hide
 */
public interface IConfirmationPromptCallback extends android.os.IInterface
{
  /** Default implementation for IConfirmationPromptCallback. */
  public static class Default implements android.security.IConfirmationPromptCallback
  {
    @Override public void onConfirmationPromptCompleted(int result, byte[] dataThatWasConfirmed) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.security.IConfirmationPromptCallback
  {
    private static final java.lang.String DESCRIPTOR = "android.security.IConfirmationPromptCallback";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.security.IConfirmationPromptCallback interface,
     * generating a proxy if needed.
     */
    public static android.security.IConfirmationPromptCallback asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.security.IConfirmationPromptCallback))) {
        return ((android.security.IConfirmationPromptCallback)iin);
      }
      return new android.security.IConfirmationPromptCallback.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onConfirmationPromptCompleted:
        {
          return "onConfirmationPromptCompleted";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onConfirmationPromptCompleted:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          byte[] _arg1;
          _arg1 = data.createByteArray();
          this.onConfirmationPromptCompleted(_arg0, _arg1);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.security.IConfirmationPromptCallback
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onConfirmationPromptCompleted(int result, byte[] dataThatWasConfirmed) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(result);
          _data.writeByteArray(dataThatWasConfirmed);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onConfirmationPromptCompleted, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onConfirmationPromptCompleted(result, dataThatWasConfirmed);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.security.IConfirmationPromptCallback sDefaultImpl;
    }
    static final int TRANSACTION_onConfirmationPromptCompleted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    public static boolean setDefaultImpl(android.security.IConfirmationPromptCallback impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.security.IConfirmationPromptCallback getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onConfirmationPromptCompleted(int result, byte[] dataThatWasConfirmed) throws android.os.RemoteException;
}
