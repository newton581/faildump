/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.gsi;
/** {@hide} */
public interface IGsiService extends android.os.IInterface
{
  /** Default implementation for IGsiService. */
  public static class Default implements android.gsi.IGsiService
  {
    /**
         * Starts a GSI installation. Use beginGsiInstall() to target external
         * media.
         *
         * If wipeUserData is true, a clean userdata image is always created to the
         * desired size.
         *
         * If wipeUserData is false, a userdata image is only created if one does
         * not already exist. If the size is zero, a default size of 8GiB is used.
         * If there is an existing image smaller than the desired size, it is
         * resized automatically.
         *
         * @param gsiSize       The size of the on-disk GSI image.
         * @param userdataSize  The desired size of the userdata partition.
         * @param wipeUserdata  True to wipe destination userdata.
         * @return              0 on success, an error code on failure.
         */
    @Override public int startGsiInstall(long gsiSize, long userdataSize, boolean wipeUserdata) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Write bytes from a stream to the on-disk GSI.
         *
         * @param stream        Stream descriptor.
         * @param bytes         Number of bytes that can be read from stream.
         * @return              true on success, false otherwise.
         */
    @Override public boolean commitGsiChunkFromStream(android.os.ParcelFileDescriptor stream, long bytes) throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Query the progress of the current asynchronous install operation. This
         * can be called while another operation is in progress.
         */
    @Override public android.gsi.GsiProgress getInstallProgress() throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Write bytes from memory to the on-disk GSI.
         *
         * @param bytes         Byte array.
         * @return              true on success, false otherwise.
         */
    @Override public boolean commitGsiChunkFromMemory(byte[] bytes) throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Complete a GSI installation and mark it as bootable. The caller is
         * responsible for rebooting the device as soon as possible.
         *
         * @param oneShot       If true, the GSI will boot once and then disable itself.
         *                      It can still be re-enabled again later with setGsiBootable.
         * @return              INSTALL_* error code.
         */
    @Override public int setGsiBootable(boolean oneShot) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * @return              True if Gsi is enabled
         */
    @Override public boolean isGsiEnabled() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Cancel an in-progress GSI install.
         */
    @Override public boolean cancelGsiInstall() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Return if a GSI installation is currently in-progress.
         */
    @Override public boolean isGsiInstallInProgress() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Remove a GSI install. This will completely remove and reclaim space used
         * by the GSI and its userdata. If currently running a GSI, space will be
         * reclaimed on the reboot.
         *
         * @return              true on success, false otherwise.
         */
    @Override public boolean removeGsiInstall() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Disables a GSI install. The image and userdata will be retained, but can
         * be re-enabled at any time with setGsiBootable.
         */
    @Override public boolean disableGsiInstall() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Return the size of the userdata partition for an installed GSI. If there
         * is no image, 0 is returned. On error, -1 is returned.
         */
    @Override public long getUserdataImageSize() throws android.os.RemoteException
    {
      return 0L;
    }
    /**
         * Returns true if the gsi is currently running, false otherwise.
         */
    @Override public boolean isGsiRunning() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Returns true if a gsi is installed.
         */
    @Override public boolean isGsiInstalled() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Returns the boot status of a GSI. See the BOOT_STATUS constants in IGsiService.
         *
         * GSI_STATE_NOT_INSTALLED will be returned if no GSI installation has been
         * fully completed. Any other value indicates a GSI is installed. If a GSI
         * currently running, DISABLED or SINGLE_BOOT can still be returned.
         */
    @Override public int getGsiBootStatus() throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * If a GSI is installed, returns the directory where the installed images
         * are located. Otherwise, returns an empty string.
         */
    @Override public java.lang.String getInstalledGsiImageDir() throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Begin a GSI installation.
         *
         * This is a replacement for startGsiInstall, in order to supply additional
         * options.
         *
         * @return              0 on success, an error code on failure.
         */
    @Override public int beginGsiInstall(android.gsi.GsiInstallParams params) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Wipe the userdata of an existing GSI install. This will not work if the
         * GSI is currently running. The userdata image will not be removed, but the
         * first block will be zeroed ensuring that the next GSI boot formats /data.
         *
         * @return              0 on success, an error code on failure.
         */
    @Override public int wipeGsiUserdata() throws android.os.RemoteException
    {
      return 0;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.gsi.IGsiService
  {
    private static final java.lang.String DESCRIPTOR = "android.gsi.IGsiService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.gsi.IGsiService interface,
     * generating a proxy if needed.
     */
    public static android.gsi.IGsiService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.gsi.IGsiService))) {
        return ((android.gsi.IGsiService)iin);
      }
      return new android.gsi.IGsiService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_startGsiInstall:
        {
          return "startGsiInstall";
        }
        case TRANSACTION_commitGsiChunkFromStream:
        {
          return "commitGsiChunkFromStream";
        }
        case TRANSACTION_getInstallProgress:
        {
          return "getInstallProgress";
        }
        case TRANSACTION_commitGsiChunkFromMemory:
        {
          return "commitGsiChunkFromMemory";
        }
        case TRANSACTION_setGsiBootable:
        {
          return "setGsiBootable";
        }
        case TRANSACTION_isGsiEnabled:
        {
          return "isGsiEnabled";
        }
        case TRANSACTION_cancelGsiInstall:
        {
          return "cancelGsiInstall";
        }
        case TRANSACTION_isGsiInstallInProgress:
        {
          return "isGsiInstallInProgress";
        }
        case TRANSACTION_removeGsiInstall:
        {
          return "removeGsiInstall";
        }
        case TRANSACTION_disableGsiInstall:
        {
          return "disableGsiInstall";
        }
        case TRANSACTION_getUserdataImageSize:
        {
          return "getUserdataImageSize";
        }
        case TRANSACTION_isGsiRunning:
        {
          return "isGsiRunning";
        }
        case TRANSACTION_isGsiInstalled:
        {
          return "isGsiInstalled";
        }
        case TRANSACTION_getGsiBootStatus:
        {
          return "getGsiBootStatus";
        }
        case TRANSACTION_getInstalledGsiImageDir:
        {
          return "getInstalledGsiImageDir";
        }
        case TRANSACTION_beginGsiInstall:
        {
          return "beginGsiInstall";
        }
        case TRANSACTION_wipeGsiUserdata:
        {
          return "wipeGsiUserdata";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_startGsiInstall:
        {
          data.enforceInterface(descriptor);
          long _arg0;
          _arg0 = data.readLong();
          long _arg1;
          _arg1 = data.readLong();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          int _result = this.startGsiInstall(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_commitGsiChunkFromStream:
        {
          data.enforceInterface(descriptor);
          android.os.ParcelFileDescriptor _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.ParcelFileDescriptor.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          long _arg1;
          _arg1 = data.readLong();
          boolean _result = this.commitGsiChunkFromStream(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getInstallProgress:
        {
          data.enforceInterface(descriptor);
          android.gsi.GsiProgress _result = this.getInstallProgress();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_commitGsiChunkFromMemory:
        {
          data.enforceInterface(descriptor);
          byte[] _arg0;
          _arg0 = data.createByteArray();
          boolean _result = this.commitGsiChunkFromMemory(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setGsiBootable:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          int _result = this.setGsiBootable(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isGsiEnabled:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isGsiEnabled();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_cancelGsiInstall:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.cancelGsiInstall();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isGsiInstallInProgress:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isGsiInstallInProgress();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_removeGsiInstall:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.removeGsiInstall();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_disableGsiInstall:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.disableGsiInstall();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getUserdataImageSize:
        {
          data.enforceInterface(descriptor);
          long _result = this.getUserdataImageSize();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_isGsiRunning:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isGsiRunning();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isGsiInstalled:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isGsiInstalled();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getGsiBootStatus:
        {
          data.enforceInterface(descriptor);
          int _result = this.getGsiBootStatus();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getInstalledGsiImageDir:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getInstalledGsiImageDir();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_beginGsiInstall:
        {
          data.enforceInterface(descriptor);
          android.gsi.GsiInstallParams _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.gsi.GsiInstallParams.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.beginGsiInstall(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_wipeGsiUserdata:
        {
          data.enforceInterface(descriptor);
          int _result = this.wipeGsiUserdata();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.gsi.IGsiService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Starts a GSI installation. Use beginGsiInstall() to target external
           * media.
           *
           * If wipeUserData is true, a clean userdata image is always created to the
           * desired size.
           *
           * If wipeUserData is false, a userdata image is only created if one does
           * not already exist. If the size is zero, a default size of 8GiB is used.
           * If there is an existing image smaller than the desired size, it is
           * resized automatically.
           *
           * @param gsiSize       The size of the on-disk GSI image.
           * @param userdataSize  The desired size of the userdata partition.
           * @param wipeUserdata  True to wipe destination userdata.
           * @return              0 on success, an error code on failure.
           */
      @Override public int startGsiInstall(long gsiSize, long userdataSize, boolean wipeUserdata) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeLong(gsiSize);
          _data.writeLong(userdataSize);
          _data.writeInt(((wipeUserdata)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_startGsiInstall, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().startGsiInstall(gsiSize, userdataSize, wipeUserdata);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Write bytes from a stream to the on-disk GSI.
           *
           * @param stream        Stream descriptor.
           * @param bytes         Number of bytes that can be read from stream.
           * @return              true on success, false otherwise.
           */
      @Override public boolean commitGsiChunkFromStream(android.os.ParcelFileDescriptor stream, long bytes) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((stream!=null)) {
            _data.writeInt(1);
            stream.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeLong(bytes);
          boolean _status = mRemote.transact(Stub.TRANSACTION_commitGsiChunkFromStream, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().commitGsiChunkFromStream(stream, bytes);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Query the progress of the current asynchronous install operation. This
           * can be called while another operation is in progress.
           */
      @Override public android.gsi.GsiProgress getInstallProgress() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.gsi.GsiProgress _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getInstallProgress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getInstallProgress();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.gsi.GsiProgress.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Write bytes from memory to the on-disk GSI.
           *
           * @param bytes         Byte array.
           * @return              true on success, false otherwise.
           */
      @Override public boolean commitGsiChunkFromMemory(byte[] bytes) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeByteArray(bytes);
          boolean _status = mRemote.transact(Stub.TRANSACTION_commitGsiChunkFromMemory, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().commitGsiChunkFromMemory(bytes);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Complete a GSI installation and mark it as bootable. The caller is
           * responsible for rebooting the device as soon as possible.
           *
           * @param oneShot       If true, the GSI will boot once and then disable itself.
           *                      It can still be re-enabled again later with setGsiBootable.
           * @return              INSTALL_* error code.
           */
      @Override public int setGsiBootable(boolean oneShot) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((oneShot)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setGsiBootable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setGsiBootable(oneShot);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * @return              True if Gsi is enabled
           */
      @Override public boolean isGsiEnabled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isGsiEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isGsiEnabled();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Cancel an in-progress GSI install.
           */
      @Override public boolean cancelGsiInstall() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_cancelGsiInstall, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().cancelGsiInstall();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Return if a GSI installation is currently in-progress.
           */
      @Override public boolean isGsiInstallInProgress() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isGsiInstallInProgress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isGsiInstallInProgress();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Remove a GSI install. This will completely remove and reclaim space used
           * by the GSI and its userdata. If currently running a GSI, space will be
           * reclaimed on the reboot.
           *
           * @return              true on success, false otherwise.
           */
      @Override public boolean removeGsiInstall() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeGsiInstall, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().removeGsiInstall();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Disables a GSI install. The image and userdata will be retained, but can
           * be re-enabled at any time with setGsiBootable.
           */
      @Override public boolean disableGsiInstall() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_disableGsiInstall, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().disableGsiInstall();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Return the size of the userdata partition for an installed GSI. If there
           * is no image, 0 is returned. On error, -1 is returned.
           */
      @Override public long getUserdataImageSize() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getUserdataImageSize, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getUserdataImageSize();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns true if the gsi is currently running, false otherwise.
           */
      @Override public boolean isGsiRunning() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isGsiRunning, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isGsiRunning();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns true if a gsi is installed.
           */
      @Override public boolean isGsiInstalled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isGsiInstalled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isGsiInstalled();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns the boot status of a GSI. See the BOOT_STATUS constants in IGsiService.
           *
           * GSI_STATE_NOT_INSTALLED will be returned if no GSI installation has been
           * fully completed. Any other value indicates a GSI is installed. If a GSI
           * currently running, DISABLED or SINGLE_BOOT can still be returned.
           */
      @Override public int getGsiBootStatus() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getGsiBootStatus, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getGsiBootStatus();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * If a GSI is installed, returns the directory where the installed images
           * are located. Otherwise, returns an empty string.
           */
      @Override public java.lang.String getInstalledGsiImageDir() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getInstalledGsiImageDir, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getInstalledGsiImageDir();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Begin a GSI installation.
           *
           * This is a replacement for startGsiInstall, in order to supply additional
           * options.
           *
           * @return              0 on success, an error code on failure.
           */
      @Override public int beginGsiInstall(android.gsi.GsiInstallParams params) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((params!=null)) {
            _data.writeInt(1);
            params.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_beginGsiInstall, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().beginGsiInstall(params);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Wipe the userdata of an existing GSI install. This will not work if the
           * GSI is currently running. The userdata image will not be removed, but the
           * first block will be zeroed ensuring that the next GSI boot formats /data.
           *
           * @return              0 on success, an error code on failure.
           */
      @Override public int wipeGsiUserdata() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_wipeGsiUserdata, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().wipeGsiUserdata();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.gsi.IGsiService sDefaultImpl;
    }
    static final int TRANSACTION_startGsiInstall = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_commitGsiChunkFromStream = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_getInstallProgress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_commitGsiChunkFromMemory = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_setGsiBootable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_isGsiEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_cancelGsiInstall = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_isGsiInstallInProgress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_removeGsiInstall = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_disableGsiInstall = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_getUserdataImageSize = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_isGsiRunning = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_isGsiInstalled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_getGsiBootStatus = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_getInstalledGsiImageDir = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_beginGsiInstall = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_wipeGsiUserdata = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    public static boolean setDefaultImpl(android.gsi.IGsiService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.gsi.IGsiService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int STATUS_NO_OPERATION = 0;
  public static final int STATUS_WORKING = 1;
  public static final int STATUS_COMPLETE = 2;
  public static final int INSTALL_OK = 0;
  public static final int INSTALL_ERROR_GENERIC = 1;
  public static final int INSTALL_ERROR_NO_SPACE = 2;
  public static final int INSTALL_ERROR_FILE_SYSTEM_CLUTTERED = 3;
  public static final int BOOT_STATUS_NOT_INSTALLED = 0;
  public static final int BOOT_STATUS_DISABLED = 1;
  public static final int BOOT_STATUS_SINGLE_BOOT = 2;
  public static final int BOOT_STATUS_ENABLED = 3;
  public static final int BOOT_STATUS_WILL_WIPE = 4;
  /**
       * Starts a GSI installation. Use beginGsiInstall() to target external
       * media.
       *
       * If wipeUserData is true, a clean userdata image is always created to the
       * desired size.
       *
       * If wipeUserData is false, a userdata image is only created if one does
       * not already exist. If the size is zero, a default size of 8GiB is used.
       * If there is an existing image smaller than the desired size, it is
       * resized automatically.
       *
       * @param gsiSize       The size of the on-disk GSI image.
       * @param userdataSize  The desired size of the userdata partition.
       * @param wipeUserdata  True to wipe destination userdata.
       * @return              0 on success, an error code on failure.
       */
  public int startGsiInstall(long gsiSize, long userdataSize, boolean wipeUserdata) throws android.os.RemoteException;
  /**
       * Write bytes from a stream to the on-disk GSI.
       *
       * @param stream        Stream descriptor.
       * @param bytes         Number of bytes that can be read from stream.
       * @return              true on success, false otherwise.
       */
  public boolean commitGsiChunkFromStream(android.os.ParcelFileDescriptor stream, long bytes) throws android.os.RemoteException;
  /**
       * Query the progress of the current asynchronous install operation. This
       * can be called while another operation is in progress.
       */
  public android.gsi.GsiProgress getInstallProgress() throws android.os.RemoteException;
  /**
       * Write bytes from memory to the on-disk GSI.
       *
       * @param bytes         Byte array.
       * @return              true on success, false otherwise.
       */
  public boolean commitGsiChunkFromMemory(byte[] bytes) throws android.os.RemoteException;
  /**
       * Complete a GSI installation and mark it as bootable. The caller is
       * responsible for rebooting the device as soon as possible.
       *
       * @param oneShot       If true, the GSI will boot once and then disable itself.
       *                      It can still be re-enabled again later with setGsiBootable.
       * @return              INSTALL_* error code.
       */
  public int setGsiBootable(boolean oneShot) throws android.os.RemoteException;
  /**
       * @return              True if Gsi is enabled
       */
  public boolean isGsiEnabled() throws android.os.RemoteException;
  /**
       * Cancel an in-progress GSI install.
       */
  public boolean cancelGsiInstall() throws android.os.RemoteException;
  /**
       * Return if a GSI installation is currently in-progress.
       */
  public boolean isGsiInstallInProgress() throws android.os.RemoteException;
  /**
       * Remove a GSI install. This will completely remove and reclaim space used
       * by the GSI and its userdata. If currently running a GSI, space will be
       * reclaimed on the reboot.
       *
       * @return              true on success, false otherwise.
       */
  public boolean removeGsiInstall() throws android.os.RemoteException;
  /**
       * Disables a GSI install. The image and userdata will be retained, but can
       * be re-enabled at any time with setGsiBootable.
       */
  public boolean disableGsiInstall() throws android.os.RemoteException;
  /**
       * Return the size of the userdata partition for an installed GSI. If there
       * is no image, 0 is returned. On error, -1 is returned.
       */
  public long getUserdataImageSize() throws android.os.RemoteException;
  /**
       * Returns true if the gsi is currently running, false otherwise.
       */
  public boolean isGsiRunning() throws android.os.RemoteException;
  /**
       * Returns true if a gsi is installed.
       */
  public boolean isGsiInstalled() throws android.os.RemoteException;
  /**
       * Returns the boot status of a GSI. See the BOOT_STATUS constants in IGsiService.
       *
       * GSI_STATE_NOT_INSTALLED will be returned if no GSI installation has been
       * fully completed. Any other value indicates a GSI is installed. If a GSI
       * currently running, DISABLED or SINGLE_BOOT can still be returned.
       */
  public int getGsiBootStatus() throws android.os.RemoteException;
  /**
       * If a GSI is installed, returns the directory where the installed images
       * are located. Otherwise, returns an empty string.
       */
  public java.lang.String getInstalledGsiImageDir() throws android.os.RemoteException;
  /**
       * Begin a GSI installation.
       *
       * This is a replacement for startGsiInstall, in order to supply additional
       * options.
       *
       * @return              0 on success, an error code on failure.
       */
  public int beginGsiInstall(android.gsi.GsiInstallParams params) throws android.os.RemoteException;
  /**
       * Wipe the userdata of an existing GSI install. This will not work if the
       * GSI is currently running. The userdata image will not be removed, but the
       * first block will be zeroed ensuring that the next GSI boot formats /data.
       *
       * @return              0 on success, an error code on failure.
       */
  public int wipeGsiUserdata() throws android.os.RemoteException;
}
