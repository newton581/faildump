/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/** {@hide} */
public interface IVold extends android.os.IInterface
{
  /** Default implementation for IVold. */
  public static class Default implements android.os.IVold
  {
    @Override public void setListener(android.os.IVoldListener listener) throws android.os.RemoteException
    {
    }
    @Override public void monitor() throws android.os.RemoteException
    {
    }
    @Override public void reset() throws android.os.RemoteException
    {
    }
    @Override public void shutdown() throws android.os.RemoteException
    {
    }
    @Override public void onUserAdded(int userId, int userSerial) throws android.os.RemoteException
    {
    }
    @Override public void onUserRemoved(int userId) throws android.os.RemoteException
    {
    }
    @Override public void onUserStarted(int userId) throws android.os.RemoteException
    {
    }
    @Override public void onUserStopped(int userId) throws android.os.RemoteException
    {
    }
    @Override public void addAppIds(java.lang.String[] packageNames, int[] appIds) throws android.os.RemoteException
    {
    }
    @Override public void addSandboxIds(int[] appIds, java.lang.String[] sandboxIds) throws android.os.RemoteException
    {
    }
    @Override public void onSecureKeyguardStateChanged(boolean isShowing) throws android.os.RemoteException
    {
    }
    @Override public void partition(java.lang.String diskId, int partitionType, int ratio) throws android.os.RemoteException
    {
    }
    @Override public void forgetPartition(java.lang.String partGuid, java.lang.String fsUuid) throws android.os.RemoteException
    {
    }
    @Override public void mount(java.lang.String volId, int mountFlags, int mountUserId) throws android.os.RemoteException
    {
    }
    @Override public void unmount(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override public void format(java.lang.String volId, java.lang.String fsType) throws android.os.RemoteException
    {
    }
    @Override public void benchmark(java.lang.String volId, android.os.IVoldTaskListener listener) throws android.os.RemoteException
    {
    }
    @Override public void checkEncryption(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override public void moveStorage(java.lang.String fromVolId, java.lang.String toVolId, android.os.IVoldTaskListener listener) throws android.os.RemoteException
    {
    }
    @Override public void remountUid(int uid, int remountMode) throws android.os.RemoteException
    {
    }
    @Override public void mkdirs(java.lang.String path) throws android.os.RemoteException
    {
    }
    @Override public java.lang.String createObb(java.lang.String sourcePath, java.lang.String sourceKey, int ownerGid) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void destroyObb(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override public void fstrim(int fstrimFlags, android.os.IVoldTaskListener listener) throws android.os.RemoteException
    {
    }
    @Override public void runIdleMaint(android.os.IVoldTaskListener listener) throws android.os.RemoteException
    {
    }
    @Override public void abortIdleMaint(android.os.IVoldTaskListener listener) throws android.os.RemoteException
    {
    }
    @Override public java.io.FileDescriptor mountAppFuse(int uid, int mountId) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void unmountAppFuse(int uid, int mountId) throws android.os.RemoteException
    {
    }
    @Override public void fdeCheckPassword(java.lang.String password) throws android.os.RemoteException
    {
    }
    @Override public void fdeRestart() throws android.os.RemoteException
    {
    }
    @Override public int fdeComplete() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public void fdeEnable(int passwordType, java.lang.String password, int encryptionFlags) throws android.os.RemoteException
    {
    }
    @Override public void fdeChangePassword(int passwordType, java.lang.String currentPassword, java.lang.String password) throws android.os.RemoteException
    {
    }
    @Override public void fdeVerifyPassword(java.lang.String password) throws android.os.RemoteException
    {
    }
    @Override public java.lang.String fdeGetField(java.lang.String key) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void fdeSetField(java.lang.String key, java.lang.String value) throws android.os.RemoteException
    {
    }
    @Override public int fdeGetPasswordType() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public java.lang.String fdeGetPassword() throws android.os.RemoteException
    {
      return null;
    }
    @Override public void fdeClearPassword() throws android.os.RemoteException
    {
    }
    @Override public void fbeEnable() throws android.os.RemoteException
    {
    }
    @Override public void mountDefaultEncrypted() throws android.os.RemoteException
    {
    }
    @Override public void initUser0() throws android.os.RemoteException
    {
    }
    @Override public boolean isConvertibleToFbe() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void mountFstab(java.lang.String blkDevice, java.lang.String mountPoint) throws android.os.RemoteException
    {
    }
    @Override public void encryptFstab(java.lang.String blkDevice, java.lang.String mountPoint) throws android.os.RemoteException
    {
    }
    @Override public void createUserKey(int userId, int userSerial, boolean ephemeral) throws android.os.RemoteException
    {
    }
    @Override public void destroyUserKey(int userId) throws android.os.RemoteException
    {
    }
    @Override public void addUserKeyAuth(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException
    {
    }
    @Override public void clearUserKeyAuth(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException
    {
    }
    @Override public void fixateNewestUserKeyAuth(int userId) throws android.os.RemoteException
    {
    }
    @Override public void unlockUserKey(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException
    {
    }
    @Override public void lockUserKey(int userId) throws android.os.RemoteException
    {
    }
    @Override public void prepareUserStorage(java.lang.String uuid, int userId, int userSerial, int storageFlags) throws android.os.RemoteException
    {
    }
    @Override public void destroyUserStorage(java.lang.String uuid, int userId, int storageFlags) throws android.os.RemoteException
    {
    }
    @Override public void prepareSandboxForApp(java.lang.String packageName, int appId, java.lang.String sandboxId, int userId) throws android.os.RemoteException
    {
    }
    @Override public void destroySandboxForApp(java.lang.String packageName, java.lang.String sandboxId, int userId) throws android.os.RemoteException
    {
    }
    @Override public void startCheckpoint(int retry) throws android.os.RemoteException
    {
    }
    @Override public boolean needsCheckpoint() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean needsRollback() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void abortChanges(java.lang.String device, boolean retry) throws android.os.RemoteException
    {
    }
    @Override public void commitChanges() throws android.os.RemoteException
    {
    }
    @Override public void prepareCheckpoint() throws android.os.RemoteException
    {
    }
    @Override public void restoreCheckpoint(java.lang.String device) throws android.os.RemoteException
    {
    }
    @Override public void restoreCheckpointPart(java.lang.String device, int count) throws android.os.RemoteException
    {
    }
    @Override public void markBootAttempt() throws android.os.RemoteException
    {
    }
    @Override public boolean supportsCheckpoint() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean supportsBlockCheckpoint() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean supportsFileCheckpoint() throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String createStubVolume(java.lang.String sourcePath, java.lang.String mountPath, java.lang.String fsType, java.lang.String fsUuid, java.lang.String fsLabel) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void destroyStubVolume(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override public java.io.FileDescriptor openAppFuseFile(int uid, int mountId, int fileId, int flags) throws android.os.RemoteException
    {
      return null;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IVold
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IVold";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IVold interface,
     * generating a proxy if needed.
     */
    public static android.os.IVold asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IVold))) {
        return ((android.os.IVold)iin);
      }
      return new android.os.IVold.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_setListener:
        {
          return "setListener";
        }
        case TRANSACTION_monitor:
        {
          return "monitor";
        }
        case TRANSACTION_reset:
        {
          return "reset";
        }
        case TRANSACTION_shutdown:
        {
          return "shutdown";
        }
        case TRANSACTION_onUserAdded:
        {
          return "onUserAdded";
        }
        case TRANSACTION_onUserRemoved:
        {
          return "onUserRemoved";
        }
        case TRANSACTION_onUserStarted:
        {
          return "onUserStarted";
        }
        case TRANSACTION_onUserStopped:
        {
          return "onUserStopped";
        }
        case TRANSACTION_addAppIds:
        {
          return "addAppIds";
        }
        case TRANSACTION_addSandboxIds:
        {
          return "addSandboxIds";
        }
        case TRANSACTION_onSecureKeyguardStateChanged:
        {
          return "onSecureKeyguardStateChanged";
        }
        case TRANSACTION_partition:
        {
          return "partition";
        }
        case TRANSACTION_forgetPartition:
        {
          return "forgetPartition";
        }
        case TRANSACTION_mount:
        {
          return "mount";
        }
        case TRANSACTION_unmount:
        {
          return "unmount";
        }
        case TRANSACTION_format:
        {
          return "format";
        }
        case TRANSACTION_benchmark:
        {
          return "benchmark";
        }
        case TRANSACTION_checkEncryption:
        {
          return "checkEncryption";
        }
        case TRANSACTION_moveStorage:
        {
          return "moveStorage";
        }
        case TRANSACTION_remountUid:
        {
          return "remountUid";
        }
        case TRANSACTION_mkdirs:
        {
          return "mkdirs";
        }
        case TRANSACTION_createObb:
        {
          return "createObb";
        }
        case TRANSACTION_destroyObb:
        {
          return "destroyObb";
        }
        case TRANSACTION_fstrim:
        {
          return "fstrim";
        }
        case TRANSACTION_runIdleMaint:
        {
          return "runIdleMaint";
        }
        case TRANSACTION_abortIdleMaint:
        {
          return "abortIdleMaint";
        }
        case TRANSACTION_mountAppFuse:
        {
          return "mountAppFuse";
        }
        case TRANSACTION_unmountAppFuse:
        {
          return "unmountAppFuse";
        }
        case TRANSACTION_fdeCheckPassword:
        {
          return "fdeCheckPassword";
        }
        case TRANSACTION_fdeRestart:
        {
          return "fdeRestart";
        }
        case TRANSACTION_fdeComplete:
        {
          return "fdeComplete";
        }
        case TRANSACTION_fdeEnable:
        {
          return "fdeEnable";
        }
        case TRANSACTION_fdeChangePassword:
        {
          return "fdeChangePassword";
        }
        case TRANSACTION_fdeVerifyPassword:
        {
          return "fdeVerifyPassword";
        }
        case TRANSACTION_fdeGetField:
        {
          return "fdeGetField";
        }
        case TRANSACTION_fdeSetField:
        {
          return "fdeSetField";
        }
        case TRANSACTION_fdeGetPasswordType:
        {
          return "fdeGetPasswordType";
        }
        case TRANSACTION_fdeGetPassword:
        {
          return "fdeGetPassword";
        }
        case TRANSACTION_fdeClearPassword:
        {
          return "fdeClearPassword";
        }
        case TRANSACTION_fbeEnable:
        {
          return "fbeEnable";
        }
        case TRANSACTION_mountDefaultEncrypted:
        {
          return "mountDefaultEncrypted";
        }
        case TRANSACTION_initUser0:
        {
          return "initUser0";
        }
        case TRANSACTION_isConvertibleToFbe:
        {
          return "isConvertibleToFbe";
        }
        case TRANSACTION_mountFstab:
        {
          return "mountFstab";
        }
        case TRANSACTION_encryptFstab:
        {
          return "encryptFstab";
        }
        case TRANSACTION_createUserKey:
        {
          return "createUserKey";
        }
        case TRANSACTION_destroyUserKey:
        {
          return "destroyUserKey";
        }
        case TRANSACTION_addUserKeyAuth:
        {
          return "addUserKeyAuth";
        }
        case TRANSACTION_clearUserKeyAuth:
        {
          return "clearUserKeyAuth";
        }
        case TRANSACTION_fixateNewestUserKeyAuth:
        {
          return "fixateNewestUserKeyAuth";
        }
        case TRANSACTION_unlockUserKey:
        {
          return "unlockUserKey";
        }
        case TRANSACTION_lockUserKey:
        {
          return "lockUserKey";
        }
        case TRANSACTION_prepareUserStorage:
        {
          return "prepareUserStorage";
        }
        case TRANSACTION_destroyUserStorage:
        {
          return "destroyUserStorage";
        }
        case TRANSACTION_prepareSandboxForApp:
        {
          return "prepareSandboxForApp";
        }
        case TRANSACTION_destroySandboxForApp:
        {
          return "destroySandboxForApp";
        }
        case TRANSACTION_startCheckpoint:
        {
          return "startCheckpoint";
        }
        case TRANSACTION_needsCheckpoint:
        {
          return "needsCheckpoint";
        }
        case TRANSACTION_needsRollback:
        {
          return "needsRollback";
        }
        case TRANSACTION_abortChanges:
        {
          return "abortChanges";
        }
        case TRANSACTION_commitChanges:
        {
          return "commitChanges";
        }
        case TRANSACTION_prepareCheckpoint:
        {
          return "prepareCheckpoint";
        }
        case TRANSACTION_restoreCheckpoint:
        {
          return "restoreCheckpoint";
        }
        case TRANSACTION_restoreCheckpointPart:
        {
          return "restoreCheckpointPart";
        }
        case TRANSACTION_markBootAttempt:
        {
          return "markBootAttempt";
        }
        case TRANSACTION_supportsCheckpoint:
        {
          return "supportsCheckpoint";
        }
        case TRANSACTION_supportsBlockCheckpoint:
        {
          return "supportsBlockCheckpoint";
        }
        case TRANSACTION_supportsFileCheckpoint:
        {
          return "supportsFileCheckpoint";
        }
        case TRANSACTION_createStubVolume:
        {
          return "createStubVolume";
        }
        case TRANSACTION_destroyStubVolume:
        {
          return "destroyStubVolume";
        }
        case TRANSACTION_openAppFuseFile:
        {
          return "openAppFuseFile";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_setListener:
        {
          data.enforceInterface(descriptor);
          android.os.IVoldListener _arg0;
          _arg0 = android.os.IVoldListener.Stub.asInterface(data.readStrongBinder());
          this.setListener(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_monitor:
        {
          data.enforceInterface(descriptor);
          this.monitor();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reset:
        {
          data.enforceInterface(descriptor);
          this.reset();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_shutdown:
        {
          data.enforceInterface(descriptor);
          this.shutdown();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onUserAdded:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.onUserAdded(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onUserRemoved:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onUserRemoved(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onUserStarted:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onUserStarted(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onUserStopped:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onUserStopped(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_addAppIds:
        {
          data.enforceInterface(descriptor);
          java.lang.String[] _arg0;
          _arg0 = data.createStringArray();
          int[] _arg1;
          _arg1 = data.createIntArray();
          this.addAppIds(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_addSandboxIds:
        {
          data.enforceInterface(descriptor);
          int[] _arg0;
          _arg0 = data.createIntArray();
          java.lang.String[] _arg1;
          _arg1 = data.createStringArray();
          this.addSandboxIds(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onSecureKeyguardStateChanged:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          this.onSecureKeyguardStateChanged(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_partition:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          this.partition(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_forgetPartition:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.forgetPartition(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_mount:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          this.mount(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unmount:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.unmount(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_format:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.format(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_benchmark:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.os.IVoldTaskListener _arg1;
          _arg1 = android.os.IVoldTaskListener.Stub.asInterface(data.readStrongBinder());
          this.benchmark(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_checkEncryption:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.checkEncryption(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_moveStorage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.os.IVoldTaskListener _arg2;
          _arg2 = android.os.IVoldTaskListener.Stub.asInterface(data.readStrongBinder());
          this.moveStorage(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_remountUid:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.remountUid(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_mkdirs:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.mkdirs(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_createObb:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _result = this.createObb(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_destroyObb:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.destroyObb(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fstrim:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.os.IVoldTaskListener _arg1;
          _arg1 = android.os.IVoldTaskListener.Stub.asInterface(data.readStrongBinder());
          this.fstrim(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_runIdleMaint:
        {
          data.enforceInterface(descriptor);
          android.os.IVoldTaskListener _arg0;
          _arg0 = android.os.IVoldTaskListener.Stub.asInterface(data.readStrongBinder());
          this.runIdleMaint(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_abortIdleMaint:
        {
          data.enforceInterface(descriptor);
          android.os.IVoldTaskListener _arg0;
          _arg0 = android.os.IVoldTaskListener.Stub.asInterface(data.readStrongBinder());
          this.abortIdleMaint(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_mountAppFuse:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          java.io.FileDescriptor _result = this.mountAppFuse(_arg0, _arg1);
          reply.writeNoException();
          reply.writeRawFileDescriptor(_result);
          return true;
        }
        case TRANSACTION_unmountAppFuse:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.unmountAppFuse(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fdeCheckPassword:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.fdeCheckPassword(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fdeRestart:
        {
          data.enforceInterface(descriptor);
          this.fdeRestart();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fdeComplete:
        {
          data.enforceInterface(descriptor);
          int _result = this.fdeComplete();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_fdeEnable:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.fdeEnable(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fdeChangePassword:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.fdeChangePassword(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fdeVerifyPassword:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.fdeVerifyPassword(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fdeGetField:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.fdeGetField(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_fdeSetField:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.fdeSetField(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fdeGetPasswordType:
        {
          data.enforceInterface(descriptor);
          int _result = this.fdeGetPasswordType();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_fdeGetPassword:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.fdeGetPassword();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_fdeClearPassword:
        {
          data.enforceInterface(descriptor);
          this.fdeClearPassword();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fbeEnable:
        {
          data.enforceInterface(descriptor);
          this.fbeEnable();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_mountDefaultEncrypted:
        {
          data.enforceInterface(descriptor);
          this.mountDefaultEncrypted();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_initUser0:
        {
          data.enforceInterface(descriptor);
          this.initUser0();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isConvertibleToFbe:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isConvertibleToFbe();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_mountFstab:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.mountFstab(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_encryptFstab:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.encryptFstab(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_createUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.createUserKey(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.destroyUserKey(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_addUserKeyAuth:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          this.addUserKeyAuth(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_clearUserKeyAuth:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          this.clearUserKeyAuth(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fixateNewestUserKeyAuth:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.fixateNewestUserKeyAuth(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unlockUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          this.unlockUserKey(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_lockUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.lockUserKey(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_prepareUserStorage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          this.prepareUserStorage(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyUserStorage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          this.destroyUserStorage(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_prepareSandboxForApp:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          int _arg3;
          _arg3 = data.readInt();
          this.prepareSandboxForApp(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroySandboxForApp:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.destroySandboxForApp(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_startCheckpoint:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.startCheckpoint(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_needsCheckpoint:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.needsCheckpoint();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_needsRollback:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.needsRollback();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_abortChanges:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.abortChanges(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_commitChanges:
        {
          data.enforceInterface(descriptor);
          this.commitChanges();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_prepareCheckpoint:
        {
          data.enforceInterface(descriptor);
          this.prepareCheckpoint();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_restoreCheckpoint:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.restoreCheckpoint(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_restoreCheckpointPart:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.restoreCheckpointPart(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_markBootAttempt:
        {
          data.enforceInterface(descriptor);
          this.markBootAttempt();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_supportsCheckpoint:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.supportsCheckpoint();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_supportsBlockCheckpoint:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.supportsBlockCheckpoint();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_supportsFileCheckpoint:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.supportsFileCheckpoint();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_createStubVolume:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          java.lang.String _arg4;
          _arg4 = data.readString();
          java.lang.String _result = this.createStubVolume(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_destroyStubVolume:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.destroyStubVolume(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_openAppFuseFile:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          java.io.FileDescriptor _result = this.openAppFuseFile(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeRawFileDescriptor(_result);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IVold
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void setListener(android.os.IVoldListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setListener, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setListener(listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void monitor() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_monitor, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().monitor();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void reset() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reset, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reset();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void shutdown() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_shutdown, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().shutdown();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void onUserAdded(int userId, int userSerial) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(userSerial);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onUserAdded, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onUserAdded(userId, userSerial);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void onUserRemoved(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onUserRemoved, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onUserRemoved(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void onUserStarted(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onUserStarted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onUserStarted(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void onUserStopped(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onUserStopped, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onUserStopped(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void addAppIds(java.lang.String[] packageNames, int[] appIds) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringArray(packageNames);
          _data.writeIntArray(appIds);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addAppIds, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().addAppIds(packageNames, appIds);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void addSandboxIds(int[] appIds, java.lang.String[] sandboxIds) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeIntArray(appIds);
          _data.writeStringArray(sandboxIds);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addSandboxIds, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().addSandboxIds(appIds, sandboxIds);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void onSecureKeyguardStateChanged(boolean isShowing) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((isShowing)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_onSecureKeyguardStateChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onSecureKeyguardStateChanged(isShowing);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void partition(java.lang.String diskId, int partitionType, int ratio) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          _data.writeInt(partitionType);
          _data.writeInt(ratio);
          boolean _status = mRemote.transact(Stub.TRANSACTION_partition, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().partition(diskId, partitionType, ratio);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void forgetPartition(java.lang.String partGuid, java.lang.String fsUuid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(partGuid);
          _data.writeString(fsUuid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_forgetPartition, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().forgetPartition(partGuid, fsUuid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void mount(java.lang.String volId, int mountFlags, int mountUserId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeInt(mountFlags);
          _data.writeInt(mountUserId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mount, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().mount(volId, mountFlags, mountUserId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void unmount(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unmount, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unmount(volId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void format(java.lang.String volId, java.lang.String fsType) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeString(fsType);
          boolean _status = mRemote.transact(Stub.TRANSACTION_format, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().format(volId, fsType);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void benchmark(java.lang.String volId, android.os.IVoldTaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_benchmark, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().benchmark(volId, listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void checkEncryption(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_checkEncryption, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().checkEncryption(volId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void moveStorage(java.lang.String fromVolId, java.lang.String toVolId, android.os.IVoldTaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(fromVolId);
          _data.writeString(toVolId);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_moveStorage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().moveStorage(fromVolId, toVolId, listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void remountUid(int uid, int remountMode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(remountMode);
          boolean _status = mRemote.transact(Stub.TRANSACTION_remountUid, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().remountUid(uid, remountMode);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void mkdirs(java.lang.String path) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(path);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mkdirs, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().mkdirs(path);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.String createObb(java.lang.String sourcePath, java.lang.String sourceKey, int ownerGid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(sourcePath);
          _data.writeString(sourceKey);
          _data.writeInt(ownerGid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createObb, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createObb(sourcePath, sourceKey, ownerGid);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void destroyObb(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyObb, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyObb(volId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fstrim(int fstrimFlags, android.os.IVoldTaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(fstrimFlags);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_fstrim, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fstrim(fstrimFlags, listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void runIdleMaint(android.os.IVoldTaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_runIdleMaint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().runIdleMaint(listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void abortIdleMaint(android.os.IVoldTaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_abortIdleMaint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().abortIdleMaint(listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.io.FileDescriptor mountAppFuse(int uid, int mountId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.io.FileDescriptor _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(mountId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mountAppFuse, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().mountAppFuse(uid, mountId);
          }
          _reply.readException();
          _result = _reply.readRawFileDescriptor();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void unmountAppFuse(int uid, int mountId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(mountId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unmountAppFuse, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unmountAppFuse(uid, mountId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fdeCheckPassword(java.lang.String password) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(password);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeCheckPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fdeCheckPassword(password);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fdeRestart() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeRestart, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fdeRestart();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int fdeComplete() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeComplete, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().fdeComplete();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void fdeEnable(int passwordType, java.lang.String password, int encryptionFlags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(passwordType);
          _data.writeString(password);
          _data.writeInt(encryptionFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeEnable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fdeEnable(passwordType, password, encryptionFlags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fdeChangePassword(int passwordType, java.lang.String currentPassword, java.lang.String password) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(passwordType);
          _data.writeString(currentPassword);
          _data.writeString(password);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeChangePassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fdeChangePassword(passwordType, currentPassword, password);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fdeVerifyPassword(java.lang.String password) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(password);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeVerifyPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fdeVerifyPassword(password);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.String fdeGetField(java.lang.String key) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(key);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeGetField, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().fdeGetField(key);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void fdeSetField(java.lang.String key, java.lang.String value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(key);
          _data.writeString(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeSetField, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fdeSetField(key, value);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int fdeGetPasswordType() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeGetPasswordType, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().fdeGetPasswordType();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String fdeGetPassword() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeGetPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().fdeGetPassword();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void fdeClearPassword() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fdeClearPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fdeClearPassword();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fbeEnable() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fbeEnable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fbeEnable();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void mountDefaultEncrypted() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mountDefaultEncrypted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().mountDefaultEncrypted();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void initUser0() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_initUser0, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().initUser0();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isConvertibleToFbe() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isConvertibleToFbe, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isConvertibleToFbe();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void mountFstab(java.lang.String blkDevice, java.lang.String mountPoint) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(blkDevice);
          _data.writeString(mountPoint);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mountFstab, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().mountFstab(blkDevice, mountPoint);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void encryptFstab(java.lang.String blkDevice, java.lang.String mountPoint) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(blkDevice);
          _data.writeString(mountPoint);
          boolean _status = mRemote.transact(Stub.TRANSACTION_encryptFstab, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().encryptFstab(blkDevice, mountPoint);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void createUserKey(int userId, int userSerial, boolean ephemeral) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(userSerial);
          _data.writeInt(((ephemeral)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_createUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().createUserKey(userId, userSerial, ephemeral);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyUserKey(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyUserKey(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void addUserKeyAuth(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(userSerial);
          _data.writeString(token);
          _data.writeString(secret);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addUserKeyAuth, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().addUserKeyAuth(userId, userSerial, token, secret);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void clearUserKeyAuth(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(userSerial);
          _data.writeString(token);
          _data.writeString(secret);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearUserKeyAuth, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearUserKeyAuth(userId, userSerial, token, secret);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fixateNewestUserKeyAuth(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fixateNewestUserKeyAuth, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fixateNewestUserKeyAuth(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void unlockUserKey(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(userSerial);
          _data.writeString(token);
          _data.writeString(secret);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unlockUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unlockUserKey(userId, userSerial, token, secret);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void lockUserKey(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_lockUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().lockUserKey(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void prepareUserStorage(java.lang.String uuid, int userId, int userSerial, int storageFlags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(userId);
          _data.writeInt(userSerial);
          _data.writeInt(storageFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_prepareUserStorage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().prepareUserStorage(uuid, userId, userSerial, storageFlags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyUserStorage(java.lang.String uuid, int userId, int storageFlags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(userId);
          _data.writeInt(storageFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyUserStorage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyUserStorage(uuid, userId, storageFlags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void prepareSandboxForApp(java.lang.String packageName, int appId, java.lang.String sandboxId, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(appId);
          _data.writeString(sandboxId);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_prepareSandboxForApp, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().prepareSandboxForApp(packageName, appId, sandboxId, userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroySandboxForApp(java.lang.String packageName, java.lang.String sandboxId, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeString(sandboxId);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroySandboxForApp, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroySandboxForApp(packageName, sandboxId, userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void startCheckpoint(int retry) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(retry);
          boolean _status = mRemote.transact(Stub.TRANSACTION_startCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().startCheckpoint(retry);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean needsCheckpoint() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_needsCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().needsCheckpoint();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean needsRollback() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_needsRollback, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().needsRollback();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void abortChanges(java.lang.String device, boolean retry) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(device);
          _data.writeInt(((retry)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_abortChanges, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().abortChanges(device, retry);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void commitChanges() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_commitChanges, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().commitChanges();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void prepareCheckpoint() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_prepareCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().prepareCheckpoint();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void restoreCheckpoint(java.lang.String device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(device);
          boolean _status = mRemote.transact(Stub.TRANSACTION_restoreCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().restoreCheckpoint(device);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void restoreCheckpointPart(java.lang.String device, int count) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(device);
          _data.writeInt(count);
          boolean _status = mRemote.transact(Stub.TRANSACTION_restoreCheckpointPart, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().restoreCheckpointPart(device, count);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void markBootAttempt() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_markBootAttempt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().markBootAttempt();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean supportsCheckpoint() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_supportsCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().supportsCheckpoint();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean supportsBlockCheckpoint() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_supportsBlockCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().supportsBlockCheckpoint();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean supportsFileCheckpoint() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_supportsFileCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().supportsFileCheckpoint();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String createStubVolume(java.lang.String sourcePath, java.lang.String mountPath, java.lang.String fsType, java.lang.String fsUuid, java.lang.String fsLabel) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(sourcePath);
          _data.writeString(mountPath);
          _data.writeString(fsType);
          _data.writeString(fsUuid);
          _data.writeString(fsLabel);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createStubVolume, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createStubVolume(sourcePath, mountPath, fsType, fsUuid, fsLabel);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void destroyStubVolume(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyStubVolume, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyStubVolume(volId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.io.FileDescriptor openAppFuseFile(int uid, int mountId, int fileId, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.io.FileDescriptor _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(mountId);
          _data.writeInt(fileId);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_openAppFuseFile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().openAppFuseFile(uid, mountId, fileId, flags);
          }
          _reply.readException();
          _result = _reply.readRawFileDescriptor();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.os.IVold sDefaultImpl;
    }
    static final int TRANSACTION_setListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_monitor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_reset = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_shutdown = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_onUserAdded = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_onUserRemoved = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_onUserStarted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_onUserStopped = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_addAppIds = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_addSandboxIds = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_onSecureKeyguardStateChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_partition = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_forgetPartition = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_mount = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_unmount = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_format = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_benchmark = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_checkEncryption = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_moveStorage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    static final int TRANSACTION_remountUid = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    static final int TRANSACTION_mkdirs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
    static final int TRANSACTION_createObb = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
    static final int TRANSACTION_destroyObb = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
    static final int TRANSACTION_fstrim = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
    static final int TRANSACTION_runIdleMaint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
    static final int TRANSACTION_abortIdleMaint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
    static final int TRANSACTION_mountAppFuse = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
    static final int TRANSACTION_unmountAppFuse = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
    static final int TRANSACTION_fdeCheckPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
    static final int TRANSACTION_fdeRestart = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
    static final int TRANSACTION_fdeComplete = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
    static final int TRANSACTION_fdeEnable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
    static final int TRANSACTION_fdeChangePassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
    static final int TRANSACTION_fdeVerifyPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
    static final int TRANSACTION_fdeGetField = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
    static final int TRANSACTION_fdeSetField = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
    static final int TRANSACTION_fdeGetPasswordType = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
    static final int TRANSACTION_fdeGetPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 37);
    static final int TRANSACTION_fdeClearPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 38);
    static final int TRANSACTION_fbeEnable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 39);
    static final int TRANSACTION_mountDefaultEncrypted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 40);
    static final int TRANSACTION_initUser0 = (android.os.IBinder.FIRST_CALL_TRANSACTION + 41);
    static final int TRANSACTION_isConvertibleToFbe = (android.os.IBinder.FIRST_CALL_TRANSACTION + 42);
    static final int TRANSACTION_mountFstab = (android.os.IBinder.FIRST_CALL_TRANSACTION + 43);
    static final int TRANSACTION_encryptFstab = (android.os.IBinder.FIRST_CALL_TRANSACTION + 44);
    static final int TRANSACTION_createUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 45);
    static final int TRANSACTION_destroyUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 46);
    static final int TRANSACTION_addUserKeyAuth = (android.os.IBinder.FIRST_CALL_TRANSACTION + 47);
    static final int TRANSACTION_clearUserKeyAuth = (android.os.IBinder.FIRST_CALL_TRANSACTION + 48);
    static final int TRANSACTION_fixateNewestUserKeyAuth = (android.os.IBinder.FIRST_CALL_TRANSACTION + 49);
    static final int TRANSACTION_unlockUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 50);
    static final int TRANSACTION_lockUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 51);
    static final int TRANSACTION_prepareUserStorage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 52);
    static final int TRANSACTION_destroyUserStorage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 53);
    static final int TRANSACTION_prepareSandboxForApp = (android.os.IBinder.FIRST_CALL_TRANSACTION + 54);
    static final int TRANSACTION_destroySandboxForApp = (android.os.IBinder.FIRST_CALL_TRANSACTION + 55);
    static final int TRANSACTION_startCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 56);
    static final int TRANSACTION_needsCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 57);
    static final int TRANSACTION_needsRollback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 58);
    static final int TRANSACTION_abortChanges = (android.os.IBinder.FIRST_CALL_TRANSACTION + 59);
    static final int TRANSACTION_commitChanges = (android.os.IBinder.FIRST_CALL_TRANSACTION + 60);
    static final int TRANSACTION_prepareCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 61);
    static final int TRANSACTION_restoreCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 62);
    static final int TRANSACTION_restoreCheckpointPart = (android.os.IBinder.FIRST_CALL_TRANSACTION + 63);
    static final int TRANSACTION_markBootAttempt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 64);
    static final int TRANSACTION_supportsCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 65);
    static final int TRANSACTION_supportsBlockCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 66);
    static final int TRANSACTION_supportsFileCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 67);
    static final int TRANSACTION_createStubVolume = (android.os.IBinder.FIRST_CALL_TRANSACTION + 68);
    static final int TRANSACTION_destroyStubVolume = (android.os.IBinder.FIRST_CALL_TRANSACTION + 69);
    static final int TRANSACTION_openAppFuseFile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 70);
    public static boolean setDefaultImpl(android.os.IVold impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IVold getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int ENCRYPTION_FLAG_NO_UI = 4;
  public static final int ENCRYPTION_STATE_NONE = 1;
  public static final int ENCRYPTION_STATE_OK = 0;
  public static final int ENCRYPTION_STATE_ERROR_UNKNOWN = -1;
  public static final int ENCRYPTION_STATE_ERROR_INCOMPLETE = -2;
  public static final int ENCRYPTION_STATE_ERROR_INCONSISTENT = -3;
  public static final int ENCRYPTION_STATE_ERROR_CORRUPT = -4;
  public static final int FSTRIM_FLAG_DEEP_TRIM = 1;
  public static final int MOUNT_FLAG_PRIMARY = 1;
  public static final int MOUNT_FLAG_VISIBLE = 2;
  public static final int PARTITION_TYPE_PUBLIC = 0;
  public static final int PARTITION_TYPE_PRIVATE = 1;
  public static final int PARTITION_TYPE_MIXED = 2;
  public static final int PASSWORD_TYPE_PASSWORD = 0;
  public static final int PASSWORD_TYPE_DEFAULT = 1;
  public static final int PASSWORD_TYPE_PATTERN = 2;
  public static final int PASSWORD_TYPE_PIN = 3;
  public static final int STORAGE_FLAG_DE = 1;
  public static final int STORAGE_FLAG_CE = 2;
  public static final int REMOUNT_MODE_NONE = 0;
  public static final int REMOUNT_MODE_DEFAULT = 1;
  public static final int REMOUNT_MODE_READ = 2;
  public static final int REMOUNT_MODE_WRITE = 3;
  public static final int REMOUNT_MODE_LEGACY = 4;
  public static final int REMOUNT_MODE_INSTALLER = 5;
  public static final int REMOUNT_MODE_FULL = 6;
  public static final int VOLUME_STATE_UNMOUNTED = 0;
  public static final int VOLUME_STATE_CHECKING = 1;
  public static final int VOLUME_STATE_MOUNTED = 2;
  public static final int VOLUME_STATE_MOUNTED_READ_ONLY = 3;
  public static final int VOLUME_STATE_FORMATTING = 4;
  public static final int VOLUME_STATE_EJECTING = 5;
  public static final int VOLUME_STATE_UNMOUNTABLE = 6;
  public static final int VOLUME_STATE_REMOVED = 7;
  public static final int VOLUME_STATE_BAD_REMOVAL = 8;
  public static final int VOLUME_TYPE_PUBLIC = 0;
  public static final int VOLUME_TYPE_PRIVATE = 1;
  public static final int VOLUME_TYPE_EMULATED = 2;
  public static final int VOLUME_TYPE_ASEC = 3;
  public static final int VOLUME_TYPE_OBB = 4;
  public static final int VOLUME_TYPE_STUB = 5;
  public void setListener(android.os.IVoldListener listener) throws android.os.RemoteException;
  public void monitor() throws android.os.RemoteException;
  public void reset() throws android.os.RemoteException;
  public void shutdown() throws android.os.RemoteException;
  public void onUserAdded(int userId, int userSerial) throws android.os.RemoteException;
  public void onUserRemoved(int userId) throws android.os.RemoteException;
  public void onUserStarted(int userId) throws android.os.RemoteException;
  public void onUserStopped(int userId) throws android.os.RemoteException;
  public void addAppIds(java.lang.String[] packageNames, int[] appIds) throws android.os.RemoteException;
  public void addSandboxIds(int[] appIds, java.lang.String[] sandboxIds) throws android.os.RemoteException;
  public void onSecureKeyguardStateChanged(boolean isShowing) throws android.os.RemoteException;
  public void partition(java.lang.String diskId, int partitionType, int ratio) throws android.os.RemoteException;
  public void forgetPartition(java.lang.String partGuid, java.lang.String fsUuid) throws android.os.RemoteException;
  public void mount(java.lang.String volId, int mountFlags, int mountUserId) throws android.os.RemoteException;
  public void unmount(java.lang.String volId) throws android.os.RemoteException;
  public void format(java.lang.String volId, java.lang.String fsType) throws android.os.RemoteException;
  public void benchmark(java.lang.String volId, android.os.IVoldTaskListener listener) throws android.os.RemoteException;
  public void checkEncryption(java.lang.String volId) throws android.os.RemoteException;
  public void moveStorage(java.lang.String fromVolId, java.lang.String toVolId, android.os.IVoldTaskListener listener) throws android.os.RemoteException;
  public void remountUid(int uid, int remountMode) throws android.os.RemoteException;
  public void mkdirs(java.lang.String path) throws android.os.RemoteException;
  public java.lang.String createObb(java.lang.String sourcePath, java.lang.String sourceKey, int ownerGid) throws android.os.RemoteException;
  public void destroyObb(java.lang.String volId) throws android.os.RemoteException;
  public void fstrim(int fstrimFlags, android.os.IVoldTaskListener listener) throws android.os.RemoteException;
  public void runIdleMaint(android.os.IVoldTaskListener listener) throws android.os.RemoteException;
  public void abortIdleMaint(android.os.IVoldTaskListener listener) throws android.os.RemoteException;
  public java.io.FileDescriptor mountAppFuse(int uid, int mountId) throws android.os.RemoteException;
  public void unmountAppFuse(int uid, int mountId) throws android.os.RemoteException;
  public void fdeCheckPassword(java.lang.String password) throws android.os.RemoteException;
  public void fdeRestart() throws android.os.RemoteException;
  public int fdeComplete() throws android.os.RemoteException;
  public void fdeEnable(int passwordType, java.lang.String password, int encryptionFlags) throws android.os.RemoteException;
  public void fdeChangePassword(int passwordType, java.lang.String currentPassword, java.lang.String password) throws android.os.RemoteException;
  public void fdeVerifyPassword(java.lang.String password) throws android.os.RemoteException;
  public java.lang.String fdeGetField(java.lang.String key) throws android.os.RemoteException;
  public void fdeSetField(java.lang.String key, java.lang.String value) throws android.os.RemoteException;
  public int fdeGetPasswordType() throws android.os.RemoteException;
  public java.lang.String fdeGetPassword() throws android.os.RemoteException;
  public void fdeClearPassword() throws android.os.RemoteException;
  public void fbeEnable() throws android.os.RemoteException;
  public void mountDefaultEncrypted() throws android.os.RemoteException;
  public void initUser0() throws android.os.RemoteException;
  public boolean isConvertibleToFbe() throws android.os.RemoteException;
  public void mountFstab(java.lang.String blkDevice, java.lang.String mountPoint) throws android.os.RemoteException;
  public void encryptFstab(java.lang.String blkDevice, java.lang.String mountPoint) throws android.os.RemoteException;
  public void createUserKey(int userId, int userSerial, boolean ephemeral) throws android.os.RemoteException;
  public void destroyUserKey(int userId) throws android.os.RemoteException;
  public void addUserKeyAuth(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException;
  public void clearUserKeyAuth(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException;
  public void fixateNewestUserKeyAuth(int userId) throws android.os.RemoteException;
  public void unlockUserKey(int userId, int userSerial, java.lang.String token, java.lang.String secret) throws android.os.RemoteException;
  public void lockUserKey(int userId) throws android.os.RemoteException;
  public void prepareUserStorage(java.lang.String uuid, int userId, int userSerial, int storageFlags) throws android.os.RemoteException;
  public void destroyUserStorage(java.lang.String uuid, int userId, int storageFlags) throws android.os.RemoteException;
  public void prepareSandboxForApp(java.lang.String packageName, int appId, java.lang.String sandboxId, int userId) throws android.os.RemoteException;
  public void destroySandboxForApp(java.lang.String packageName, java.lang.String sandboxId, int userId) throws android.os.RemoteException;
  public void startCheckpoint(int retry) throws android.os.RemoteException;
  public boolean needsCheckpoint() throws android.os.RemoteException;
  public boolean needsRollback() throws android.os.RemoteException;
  public void abortChanges(java.lang.String device, boolean retry) throws android.os.RemoteException;
  public void commitChanges() throws android.os.RemoteException;
  public void prepareCheckpoint() throws android.os.RemoteException;
  public void restoreCheckpoint(java.lang.String device) throws android.os.RemoteException;
  public void restoreCheckpointPart(java.lang.String device, int count) throws android.os.RemoteException;
  public void markBootAttempt() throws android.os.RemoteException;
  public boolean supportsCheckpoint() throws android.os.RemoteException;
  public boolean supportsBlockCheckpoint() throws android.os.RemoteException;
  public boolean supportsFileCheckpoint() throws android.os.RemoteException;
  public java.lang.String createStubVolume(java.lang.String sourcePath, java.lang.String mountPath, java.lang.String fsType, java.lang.String fsUuid, java.lang.String fsLabel) throws android.os.RemoteException;
  public void destroyStubVolume(java.lang.String volId) throws android.os.RemoteException;
  public java.io.FileDescriptor openAppFuseFile(int uid, int mountId, int fileId, int flags) throws android.os.RemoteException;
}
