/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.security.keystore;
/**
 * @hide
 */
public interface IKeystoreCertificateChainCallback extends android.os.IInterface
{
  /** Default implementation for IKeystoreCertificateChainCallback. */
  public static class Default implements android.security.keystore.IKeystoreCertificateChainCallback
  {
    @Override public void onFinished(android.security.keystore.KeystoreResponse response, android.security.keymaster.KeymasterCertificateChain chain) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.security.keystore.IKeystoreCertificateChainCallback
  {
    private static final java.lang.String DESCRIPTOR = "android.security.keystore.IKeystoreCertificateChainCallback";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.security.keystore.IKeystoreCertificateChainCallback interface,
     * generating a proxy if needed.
     */
    public static android.security.keystore.IKeystoreCertificateChainCallback asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.security.keystore.IKeystoreCertificateChainCallback))) {
        return ((android.security.keystore.IKeystoreCertificateChainCallback)iin);
      }
      return new android.security.keystore.IKeystoreCertificateChainCallback.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onFinished:
        {
          return "onFinished";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onFinished:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.KeystoreResponse _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.security.keystore.KeystoreResponse.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.security.keymaster.KeymasterCertificateChain _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.security.keymaster.KeymasterCertificateChain.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onFinished(_arg0, _arg1);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.security.keystore.IKeystoreCertificateChainCallback
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onFinished(android.security.keystore.KeystoreResponse response, android.security.keymaster.KeymasterCertificateChain chain) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((response!=null)) {
            _data.writeInt(1);
            response.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((chain!=null)) {
            _data.writeInt(1);
            chain.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onFinished, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onFinished(response, chain);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.security.keystore.IKeystoreCertificateChainCallback sDefaultImpl;
    }
    static final int TRANSACTION_onFinished = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    public static boolean setDefaultImpl(android.security.keystore.IKeystoreCertificateChainCallback impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.security.keystore.IKeystoreCertificateChainCallback getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onFinished(android.security.keystore.KeystoreResponse response, android.security.keymaster.KeymasterCertificateChain chain) throws android.os.RemoteException;
}
