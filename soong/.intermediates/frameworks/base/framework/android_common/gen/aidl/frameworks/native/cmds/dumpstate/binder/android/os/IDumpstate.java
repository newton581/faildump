/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/**
  * Binder interface for the currently running dumpstate process.
  * {@hide}
  */
public interface IDumpstate extends android.os.IInterface
{
  /** Default implementation for IDumpstate. */
  public static class Default implements android.os.IDumpstate
  {
    // TODO: remove method once startBugReport is used by Shell.
    /*
         * Sets the listener for this dumpstate progress.
         *
         * Returns a token used to monitor dumpstate death, or `nullptr` if the listener was already
         * set (the listener behaves like a Highlander: There Can be Only One).
         * Set {@code getSectionDetails} to true in order to receive callbacks with per section
         * progress details
         */
    @Override public android.os.IDumpstateToken setListener(java.lang.String name, android.os.IDumpstateListener listener, boolean getSectionDetails) throws android.os.RemoteException
    {
      return null;
    }
    /*
         * Starts a bugreport in the background.
         *
         *<p>Shows the user a dialog to get consent for sharing the bugreport with the calling
         * application. If they deny {@link IDumpstateListener#onError} will be called. If they
         * consent and bugreport generation is successful artifacts will be copied to the given fds and
         * {@link IDumpstateListener#onFinished} will be called. If there
         * are errors in bugreport generation {@link IDumpstateListener#onError} will be called.
         *
         * @param callingUid UID of the original application that requested the report.
         * @param callingPackage package of the original application that requested the report.
         * @param bugreportFd the file to which the zipped bugreport should be written
         * @param screenshotFd the file to which screenshot should be written; optional
         * @param bugreportMode the mode that specifies other run time options; must be one of above
         * @param listener callback for updates; optional
         */
    @Override public void startBugreport(int callingUid, java.lang.String callingPackage, java.io.FileDescriptor bugreportFd, java.io.FileDescriptor screenshotFd, int bugreportMode, android.os.IDumpstateListener listener) throws android.os.RemoteException
    {
    }
    /*
         * Cancels the bugreport currently in progress.
         */
    @Override public void cancelBugreport() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IDumpstate
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IDumpstate";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IDumpstate interface,
     * generating a proxy if needed.
     */
    public static android.os.IDumpstate asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IDumpstate))) {
        return ((android.os.IDumpstate)iin);
      }
      return new android.os.IDumpstate.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_setListener:
        {
          return "setListener";
        }
        case TRANSACTION_startBugreport:
        {
          return "startBugreport";
        }
        case TRANSACTION_cancelBugreport:
        {
          return "cancelBugreport";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_setListener:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.os.IDumpstateListener _arg1;
          _arg1 = android.os.IDumpstateListener.Stub.asInterface(data.readStrongBinder());
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          android.os.IDumpstateToken _result = this.setListener(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
          return true;
        }
        case TRANSACTION_startBugreport:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.io.FileDescriptor _arg2;
          _arg2 = data.readRawFileDescriptor();
          java.io.FileDescriptor _arg3;
          _arg3 = data.readRawFileDescriptor();
          int _arg4;
          _arg4 = data.readInt();
          android.os.IDumpstateListener _arg5;
          _arg5 = android.os.IDumpstateListener.Stub.asInterface(data.readStrongBinder());
          this.startBugreport(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_cancelBugreport:
        {
          data.enforceInterface(descriptor);
          this.cancelBugreport();
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IDumpstate
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      // TODO: remove method once startBugReport is used by Shell.
      /*
           * Sets the listener for this dumpstate progress.
           *
           * Returns a token used to monitor dumpstate death, or `nullptr` if the listener was already
           * set (the listener behaves like a Highlander: There Can be Only One).
           * Set {@code getSectionDetails} to true in order to receive callbacks with per section
           * progress details
           */
      @Override public android.os.IDumpstateToken setListener(java.lang.String name, android.os.IDumpstateListener listener, boolean getSectionDetails) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.IDumpstateToken _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          _data.writeInt(((getSectionDetails)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setListener, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setListener(name, listener, getSectionDetails);
          }
          _reply.readException();
          _result = android.os.IDumpstateToken.Stub.asInterface(_reply.readStrongBinder());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /*
           * Starts a bugreport in the background.
           *
           *<p>Shows the user a dialog to get consent for sharing the bugreport with the calling
           * application. If they deny {@link IDumpstateListener#onError} will be called. If they
           * consent and bugreport generation is successful artifacts will be copied to the given fds and
           * {@link IDumpstateListener#onFinished} will be called. If there
           * are errors in bugreport generation {@link IDumpstateListener#onError} will be called.
           *
           * @param callingUid UID of the original application that requested the report.
           * @param callingPackage package of the original application that requested the report.
           * @param bugreportFd the file to which the zipped bugreport should be written
           * @param screenshotFd the file to which screenshot should be written; optional
           * @param bugreportMode the mode that specifies other run time options; must be one of above
           * @param listener callback for updates; optional
           */
      @Override public void startBugreport(int callingUid, java.lang.String callingPackage, java.io.FileDescriptor bugreportFd, java.io.FileDescriptor screenshotFd, int bugreportMode, android.os.IDumpstateListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(callingUid);
          _data.writeString(callingPackage);
          _data.writeRawFileDescriptor(bugreportFd);
          _data.writeRawFileDescriptor(screenshotFd);
          _data.writeInt(bugreportMode);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_startBugreport, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().startBugreport(callingUid, callingPackage, bugreportFd, screenshotFd, bugreportMode, listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /*
           * Cancels the bugreport currently in progress.
           */
      @Override public void cancelBugreport() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_cancelBugreport, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().cancelBugreport();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.os.IDumpstate sDefaultImpl;
    }
    static final int TRANSACTION_setListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_startBugreport = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_cancelBugreport = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    public static boolean setDefaultImpl(android.os.IDumpstate impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IDumpstate getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int BUGREPORT_MODE_FULL = 0;
  public static final int BUGREPORT_MODE_INTERACTIVE = 1;
  public static final int BUGREPORT_MODE_REMOTE = 2;
  public static final int BUGREPORT_MODE_WEAR = 3;
  public static final int BUGREPORT_MODE_TELEPHONY = 4;
  public static final int BUGREPORT_MODE_WIFI = 5;
  public static final int BUGREPORT_MODE_DEFAULT = 6;
  // TODO: remove method once startBugReport is used by Shell.
  /*
       * Sets the listener for this dumpstate progress.
       *
       * Returns a token used to monitor dumpstate death, or `nullptr` if the listener was already
       * set (the listener behaves like a Highlander: There Can be Only One).
       * Set {@code getSectionDetails} to true in order to receive callbacks with per section
       * progress details
       */
  public android.os.IDumpstateToken setListener(java.lang.String name, android.os.IDumpstateListener listener, boolean getSectionDetails) throws android.os.RemoteException;
  /*
       * Starts a bugreport in the background.
       *
       *<p>Shows the user a dialog to get consent for sharing the bugreport with the calling
       * application. If they deny {@link IDumpstateListener#onError} will be called. If they
       * consent and bugreport generation is successful artifacts will be copied to the given fds and
       * {@link IDumpstateListener#onFinished} will be called. If there
       * are errors in bugreport generation {@link IDumpstateListener#onError} will be called.
       *
       * @param callingUid UID of the original application that requested the report.
       * @param callingPackage package of the original application that requested the report.
       * @param bugreportFd the file to which the zipped bugreport should be written
       * @param screenshotFd the file to which screenshot should be written; optional
       * @param bugreportMode the mode that specifies other run time options; must be one of above
       * @param listener callback for updates; optional
       */
  public void startBugreport(int callingUid, java.lang.String callingPackage, java.io.FileDescriptor bugreportFd, java.io.FileDescriptor screenshotFd, int bugreportMode, android.os.IDumpstateListener listener) throws android.os.RemoteException;
  /*
       * Cancels the bugreport currently in progress.
       */
  public void cancelBugreport() throws android.os.RemoteException;
}
