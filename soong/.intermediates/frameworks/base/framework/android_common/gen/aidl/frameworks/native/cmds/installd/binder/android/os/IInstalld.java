/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/** {@hide} */
public interface IInstalld extends android.os.IInterface
{
  /** Default implementation for IInstalld. */
  public static class Default implements android.os.IInstalld
  {
    @Override public void createUserData(java.lang.String uuid, int userId, int userSerial, int flags) throws android.os.RemoteException
    {
    }
    @Override public void destroyUserData(java.lang.String uuid, int userId, int flags) throws android.os.RemoteException
    {
    }
    @Override public long createAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, int appId, java.lang.String seInfo, int targetSdkVersion) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public void restoreconAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, int appId, java.lang.String seInfo) throws android.os.RemoteException
    {
    }
    @Override public void migrateAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags) throws android.os.RemoteException
    {
    }
    @Override public void clearAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, long ceDataInode) throws android.os.RemoteException
    {
    }
    @Override public void destroyAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, long ceDataInode) throws android.os.RemoteException
    {
    }
    @Override public void fixupAppData(java.lang.String uuid, int flags) throws android.os.RemoteException
    {
    }
    @Override public long[] getAppSize(java.lang.String uuid, java.lang.String[] packageNames, int userId, int flags, int appId, long[] ceDataInodes, java.lang.String[] codePaths) throws android.os.RemoteException
    {
      return null;
    }
    @Override public long[] getUserSize(java.lang.String uuid, int userId, int flags, int[] appIds) throws android.os.RemoteException
    {
      return null;
    }
    @Override public long[] getExternalSize(java.lang.String uuid, int userId, int flags, int[] appIds) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setAppQuota(java.lang.String uuid, int userId, int appId, long cacheQuota) throws android.os.RemoteException
    {
    }
    @Override public void moveCompleteApp(java.lang.String fromUuid, java.lang.String toUuid, java.lang.String packageName, java.lang.String dataAppName, int appId, java.lang.String seInfo, int targetSdkVersion) throws android.os.RemoteException
    {
    }
    @Override public void dexopt(java.lang.String apkPath, int uid, java.lang.String packageName, java.lang.String instructionSet, int dexoptNeeded, java.lang.String outputPath, int dexFlags, java.lang.String compilerFilter, java.lang.String uuid, java.lang.String sharedLibraries, java.lang.String seInfo, boolean downgrade, int targetSdkVersion, java.lang.String profileName, java.lang.String dexMetadataPath, java.lang.String compilationReason) throws android.os.RemoteException
    {
    }
    @Override public boolean compileLayouts(java.lang.String apkPath, java.lang.String packageName, java.lang.String outDexFile, int uid) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void rmdex(java.lang.String codePath, java.lang.String instructionSet) throws android.os.RemoteException
    {
    }
    @Override public boolean mergeProfiles(int uid, java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean dumpProfiles(int uid, java.lang.String packageName, java.lang.String profileName, java.lang.String codePath) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean copySystemProfile(java.lang.String systemProfile, int uid, java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void clearAppProfiles(java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
    {
    }
    @Override public void destroyAppProfiles(java.lang.String packageName) throws android.os.RemoteException
    {
    }
    @Override public boolean createProfileSnapshot(int appId, java.lang.String packageName, java.lang.String profileName, java.lang.String classpath) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void destroyProfileSnapshot(java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
    {
    }
    @Override public void idmap(java.lang.String targetApkPath, java.lang.String overlayApkPath, int uid) throws android.os.RemoteException
    {
    }
    @Override public void removeIdmap(java.lang.String overlayApkPath) throws android.os.RemoteException
    {
    }
    @Override public void rmPackageDir(java.lang.String packageDir) throws android.os.RemoteException
    {
    }
    @Override public void freeCache(java.lang.String uuid, long targetFreeBytes, long cacheReservedBytes, int flags) throws android.os.RemoteException
    {
    }
    @Override public void linkNativeLibraryDirectory(java.lang.String uuid, java.lang.String packageName, java.lang.String nativeLibPath32, int userId) throws android.os.RemoteException
    {
    }
    @Override public void createOatDir(java.lang.String oatDir, java.lang.String instructionSet) throws android.os.RemoteException
    {
    }
    @Override public void linkFile(java.lang.String relativePath, java.lang.String fromBase, java.lang.String toBase) throws android.os.RemoteException
    {
    }
    @Override public void moveAb(java.lang.String apkPath, java.lang.String instructionSet, java.lang.String outputPath) throws android.os.RemoteException
    {
    }
    @Override public void deleteOdex(java.lang.String apkPath, java.lang.String instructionSet, java.lang.String outputPath) throws android.os.RemoteException
    {
    }
    @Override public void installApkVerity(java.lang.String filePath, java.io.FileDescriptor verityInput, int contentSize) throws android.os.RemoteException
    {
    }
    @Override public void assertFsverityRootHashMatches(java.lang.String filePath, byte[] expectedHash) throws android.os.RemoteException
    {
    }
    @Override public boolean reconcileSecondaryDexFile(java.lang.String dexPath, java.lang.String pkgName, int uid, java.lang.String[] isas, java.lang.String volume_uuid, int storage_flag) throws android.os.RemoteException
    {
      return false;
    }
    @Override public byte[] hashSecondaryDexFile(java.lang.String dexPath, java.lang.String pkgName, int uid, java.lang.String volumeUuid, int storageFlag) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void invalidateMounts() throws android.os.RemoteException
    {
    }
    @Override public boolean isQuotaSupported(java.lang.String uuid) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean prepareAppProfile(java.lang.String packageName, int userId, int appId, java.lang.String profileName, java.lang.String codePath, java.lang.String dexMetadata) throws android.os.RemoteException
    {
      return false;
    }
    @Override public long snapshotAppData(java.lang.String uuid, java.lang.String packageName, int userId, int snapshotId, int storageFlags) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public void restoreAppDataSnapshot(java.lang.String uuid, java.lang.String packageName, int appId, java.lang.String seInfo, int user, int snapshotId, int storageflags) throws android.os.RemoteException
    {
    }
    @Override public void destroyAppDataSnapshot(java.lang.String uuid, java.lang.String packageName, int userId, long ceSnapshotInode, int snapshotId, int storageFlags) throws android.os.RemoteException
    {
    }
    @Override public void migrateLegacyObbData() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IInstalld
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IInstalld";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IInstalld interface,
     * generating a proxy if needed.
     */
    public static android.os.IInstalld asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IInstalld))) {
        return ((android.os.IInstalld)iin);
      }
      return new android.os.IInstalld.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_createUserData:
        {
          return "createUserData";
        }
        case TRANSACTION_destroyUserData:
        {
          return "destroyUserData";
        }
        case TRANSACTION_createAppData:
        {
          return "createAppData";
        }
        case TRANSACTION_restoreconAppData:
        {
          return "restoreconAppData";
        }
        case TRANSACTION_migrateAppData:
        {
          return "migrateAppData";
        }
        case TRANSACTION_clearAppData:
        {
          return "clearAppData";
        }
        case TRANSACTION_destroyAppData:
        {
          return "destroyAppData";
        }
        case TRANSACTION_fixupAppData:
        {
          return "fixupAppData";
        }
        case TRANSACTION_getAppSize:
        {
          return "getAppSize";
        }
        case TRANSACTION_getUserSize:
        {
          return "getUserSize";
        }
        case TRANSACTION_getExternalSize:
        {
          return "getExternalSize";
        }
        case TRANSACTION_setAppQuota:
        {
          return "setAppQuota";
        }
        case TRANSACTION_moveCompleteApp:
        {
          return "moveCompleteApp";
        }
        case TRANSACTION_dexopt:
        {
          return "dexopt";
        }
        case TRANSACTION_compileLayouts:
        {
          return "compileLayouts";
        }
        case TRANSACTION_rmdex:
        {
          return "rmdex";
        }
        case TRANSACTION_mergeProfiles:
        {
          return "mergeProfiles";
        }
        case TRANSACTION_dumpProfiles:
        {
          return "dumpProfiles";
        }
        case TRANSACTION_copySystemProfile:
        {
          return "copySystemProfile";
        }
        case TRANSACTION_clearAppProfiles:
        {
          return "clearAppProfiles";
        }
        case TRANSACTION_destroyAppProfiles:
        {
          return "destroyAppProfiles";
        }
        case TRANSACTION_createProfileSnapshot:
        {
          return "createProfileSnapshot";
        }
        case TRANSACTION_destroyProfileSnapshot:
        {
          return "destroyProfileSnapshot";
        }
        case TRANSACTION_idmap:
        {
          return "idmap";
        }
        case TRANSACTION_removeIdmap:
        {
          return "removeIdmap";
        }
        case TRANSACTION_rmPackageDir:
        {
          return "rmPackageDir";
        }
        case TRANSACTION_freeCache:
        {
          return "freeCache";
        }
        case TRANSACTION_linkNativeLibraryDirectory:
        {
          return "linkNativeLibraryDirectory";
        }
        case TRANSACTION_createOatDir:
        {
          return "createOatDir";
        }
        case TRANSACTION_linkFile:
        {
          return "linkFile";
        }
        case TRANSACTION_moveAb:
        {
          return "moveAb";
        }
        case TRANSACTION_deleteOdex:
        {
          return "deleteOdex";
        }
        case TRANSACTION_installApkVerity:
        {
          return "installApkVerity";
        }
        case TRANSACTION_assertFsverityRootHashMatches:
        {
          return "assertFsverityRootHashMatches";
        }
        case TRANSACTION_reconcileSecondaryDexFile:
        {
          return "reconcileSecondaryDexFile";
        }
        case TRANSACTION_hashSecondaryDexFile:
        {
          return "hashSecondaryDexFile";
        }
        case TRANSACTION_invalidateMounts:
        {
          return "invalidateMounts";
        }
        case TRANSACTION_isQuotaSupported:
        {
          return "isQuotaSupported";
        }
        case TRANSACTION_prepareAppProfile:
        {
          return "prepareAppProfile";
        }
        case TRANSACTION_snapshotAppData:
        {
          return "snapshotAppData";
        }
        case TRANSACTION_restoreAppDataSnapshot:
        {
          return "restoreAppDataSnapshot";
        }
        case TRANSACTION_destroyAppDataSnapshot:
        {
          return "destroyAppDataSnapshot";
        }
        case TRANSACTION_migrateLegacyObbData:
        {
          return "migrateLegacyObbData";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_createUserData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          this.createUserData(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyUserData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          this.destroyUserData(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_createAppData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          java.lang.String _arg5;
          _arg5 = data.readString();
          int _arg6;
          _arg6 = data.readInt();
          long _result = this.createAppData(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_restoreconAppData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          java.lang.String _arg5;
          _arg5 = data.readString();
          this.restoreconAppData(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_migrateAppData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          this.migrateAppData(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_clearAppData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          long _arg4;
          _arg4 = data.readLong();
          this.clearAppData(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyAppData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          long _arg4;
          _arg4 = data.readLong();
          this.destroyAppData(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fixupAppData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.fixupAppData(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getAppSize:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String[] _arg1;
          _arg1 = data.createStringArray();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          long[] _arg5;
          _arg5 = data.createLongArray();
          java.lang.String[] _arg6;
          _arg6 = data.createStringArray();
          long[] _result = this.getAppSize(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
          reply.writeNoException();
          reply.writeLongArray(_result);
          return true;
        }
        case TRANSACTION_getUserSize:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int[] _arg3;
          _arg3 = data.createIntArray();
          long[] _result = this.getUserSize(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeLongArray(_result);
          return true;
        }
        case TRANSACTION_getExternalSize:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int[] _arg3;
          _arg3 = data.createIntArray();
          long[] _result = this.getExternalSize(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeLongArray(_result);
          return true;
        }
        case TRANSACTION_setAppQuota:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          long _arg3;
          _arg3 = data.readLong();
          this.setAppQuota(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_moveCompleteApp:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          java.lang.String _arg5;
          _arg5 = data.readString();
          int _arg6;
          _arg6 = data.readInt();
          this.moveCompleteApp(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_dexopt:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          java.lang.String _arg5;
          _arg5 = data.readString();
          int _arg6;
          _arg6 = data.readInt();
          java.lang.String _arg7;
          _arg7 = data.readString();
          java.lang.String _arg8;
          _arg8 = data.readString();
          java.lang.String _arg9;
          _arg9 = data.readString();
          java.lang.String _arg10;
          _arg10 = data.readString();
          boolean _arg11;
          _arg11 = (0!=data.readInt());
          int _arg12;
          _arg12 = data.readInt();
          java.lang.String _arg13;
          _arg13 = data.readString();
          java.lang.String _arg14;
          _arg14 = data.readString();
          java.lang.String _arg15;
          _arg15 = data.readString();
          this.dexopt(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7, _arg8, _arg9, _arg10, _arg11, _arg12, _arg13, _arg14, _arg15);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_compileLayouts:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          int _arg3;
          _arg3 = data.readInt();
          boolean _result = this.compileLayouts(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_rmdex:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.rmdex(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_mergeProfiles:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _result = this.mergeProfiles(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_dumpProfiles:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          boolean _result = this.dumpProfiles(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_copySystemProfile:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          boolean _result = this.copySystemProfile(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_clearAppProfiles:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.clearAppProfiles(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyAppProfiles:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.destroyAppProfiles(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_createProfileSnapshot:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          boolean _result = this.createProfileSnapshot(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_destroyProfileSnapshot:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.destroyProfileSnapshot(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_idmap:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.idmap(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_removeIdmap:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.removeIdmap(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_rmPackageDir:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.rmPackageDir(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_freeCache:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          long _arg1;
          _arg1 = data.readLong();
          long _arg2;
          _arg2 = data.readLong();
          int _arg3;
          _arg3 = data.readInt();
          this.freeCache(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_linkNativeLibraryDirectory:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          int _arg3;
          _arg3 = data.readInt();
          this.linkNativeLibraryDirectory(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_createOatDir:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.createOatDir(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_linkFile:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.linkFile(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_moveAb:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.moveAb(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_deleteOdex:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.deleteOdex(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_installApkVerity:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.io.FileDescriptor _arg1;
          _arg1 = data.readRawFileDescriptor();
          int _arg2;
          _arg2 = data.readInt();
          this.installApkVerity(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_assertFsverityRootHashMatches:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          byte[] _arg1;
          _arg1 = data.createByteArray();
          this.assertFsverityRootHashMatches(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_reconcileSecondaryDexFile:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String[] _arg3;
          _arg3 = data.createStringArray();
          java.lang.String _arg4;
          _arg4 = data.readString();
          int _arg5;
          _arg5 = data.readInt();
          boolean _result = this.reconcileSecondaryDexFile(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_hashSecondaryDexFile:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          byte[] _result = this.hashSecondaryDexFile(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_invalidateMounts:
        {
          data.enforceInterface(descriptor);
          this.invalidateMounts();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isQuotaSupported:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.isQuotaSupported(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_prepareAppProfile:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _arg3;
          _arg3 = data.readString();
          java.lang.String _arg4;
          _arg4 = data.readString();
          java.lang.String _arg5;
          _arg5 = data.readString();
          boolean _result = this.prepareAppProfile(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_snapshotAppData:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          long _result = this.snapshotAppData(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_restoreAppDataSnapshot:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          int _arg5;
          _arg5 = data.readInt();
          int _arg6;
          _arg6 = data.readInt();
          this.restoreAppDataSnapshot(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyAppDataSnapshot:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          long _arg3;
          _arg3 = data.readLong();
          int _arg4;
          _arg4 = data.readInt();
          int _arg5;
          _arg5 = data.readInt();
          this.destroyAppDataSnapshot(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_migrateLegacyObbData:
        {
          data.enforceInterface(descriptor);
          this.migrateLegacyObbData();
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IInstalld
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void createUserData(java.lang.String uuid, int userId, int userSerial, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(userId);
          _data.writeInt(userSerial);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createUserData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().createUserData(uuid, userId, userSerial, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyUserData(java.lang.String uuid, int userId, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(userId);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyUserData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyUserData(uuid, userId, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public long createAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, int appId, java.lang.String seInfo, int targetSdkVersion) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeInt(flags);
          _data.writeInt(appId);
          _data.writeString(seInfo);
          _data.writeInt(targetSdkVersion);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createAppData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createAppData(uuid, packageName, userId, flags, appId, seInfo, targetSdkVersion);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void restoreconAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, int appId, java.lang.String seInfo) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeInt(flags);
          _data.writeInt(appId);
          _data.writeString(seInfo);
          boolean _status = mRemote.transact(Stub.TRANSACTION_restoreconAppData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().restoreconAppData(uuid, packageName, userId, flags, appId, seInfo);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void migrateAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_migrateAppData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().migrateAppData(uuid, packageName, userId, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void clearAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, long ceDataInode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeInt(flags);
          _data.writeLong(ceDataInode);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearAppData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearAppData(uuid, packageName, userId, flags, ceDataInode);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, long ceDataInode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeInt(flags);
          _data.writeLong(ceDataInode);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyAppData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyAppData(uuid, packageName, userId, flags, ceDataInode);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fixupAppData(java.lang.String uuid, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fixupAppData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fixupAppData(uuid, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public long[] getAppSize(java.lang.String uuid, java.lang.String[] packageNames, int userId, int flags, int appId, long[] ceDataInodes, java.lang.String[] codePaths) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeStringArray(packageNames);
          _data.writeInt(userId);
          _data.writeInt(flags);
          _data.writeInt(appId);
          _data.writeLongArray(ceDataInodes);
          _data.writeStringArray(codePaths);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAppSize, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAppSize(uuid, packageNames, userId, flags, appId, ceDataInodes, codePaths);
          }
          _reply.readException();
          _result = _reply.createLongArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long[] getUserSize(java.lang.String uuid, int userId, int flags, int[] appIds) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(userId);
          _data.writeInt(flags);
          _data.writeIntArray(appIds);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getUserSize, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getUserSize(uuid, userId, flags, appIds);
          }
          _reply.readException();
          _result = _reply.createLongArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long[] getExternalSize(java.lang.String uuid, int userId, int flags, int[] appIds) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(userId);
          _data.writeInt(flags);
          _data.writeIntArray(appIds);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getExternalSize, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getExternalSize(uuid, userId, flags, appIds);
          }
          _reply.readException();
          _result = _reply.createLongArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setAppQuota(java.lang.String uuid, int userId, int appId, long cacheQuota) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeInt(userId);
          _data.writeInt(appId);
          _data.writeLong(cacheQuota);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setAppQuota, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setAppQuota(uuid, userId, appId, cacheQuota);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void moveCompleteApp(java.lang.String fromUuid, java.lang.String toUuid, java.lang.String packageName, java.lang.String dataAppName, int appId, java.lang.String seInfo, int targetSdkVersion) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(fromUuid);
          _data.writeString(toUuid);
          _data.writeString(packageName);
          _data.writeString(dataAppName);
          _data.writeInt(appId);
          _data.writeString(seInfo);
          _data.writeInt(targetSdkVersion);
          boolean _status = mRemote.transact(Stub.TRANSACTION_moveCompleteApp, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().moveCompleteApp(fromUuid, toUuid, packageName, dataAppName, appId, seInfo, targetSdkVersion);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void dexopt(java.lang.String apkPath, int uid, java.lang.String packageName, java.lang.String instructionSet, int dexoptNeeded, java.lang.String outputPath, int dexFlags, java.lang.String compilerFilter, java.lang.String uuid, java.lang.String sharedLibraries, java.lang.String seInfo, boolean downgrade, int targetSdkVersion, java.lang.String profileName, java.lang.String dexMetadataPath, java.lang.String compilationReason) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(apkPath);
          _data.writeInt(uid);
          _data.writeString(packageName);
          _data.writeString(instructionSet);
          _data.writeInt(dexoptNeeded);
          _data.writeString(outputPath);
          _data.writeInt(dexFlags);
          _data.writeString(compilerFilter);
          _data.writeString(uuid);
          _data.writeString(sharedLibraries);
          _data.writeString(seInfo);
          _data.writeInt(((downgrade)?(1):(0)));
          _data.writeInt(targetSdkVersion);
          _data.writeString(profileName);
          _data.writeString(dexMetadataPath);
          _data.writeString(compilationReason);
          boolean _status = mRemote.transact(Stub.TRANSACTION_dexopt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().dexopt(apkPath, uid, packageName, instructionSet, dexoptNeeded, outputPath, dexFlags, compilerFilter, uuid, sharedLibraries, seInfo, downgrade, targetSdkVersion, profileName, dexMetadataPath, compilationReason);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean compileLayouts(java.lang.String apkPath, java.lang.String packageName, java.lang.String outDexFile, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(apkPath);
          _data.writeString(packageName);
          _data.writeString(outDexFile);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_compileLayouts, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().compileLayouts(apkPath, packageName, outDexFile, uid);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void rmdex(java.lang.String codePath, java.lang.String instructionSet) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(codePath);
          _data.writeString(instructionSet);
          boolean _status = mRemote.transact(Stub.TRANSACTION_rmdex, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().rmdex(codePath, instructionSet);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean mergeProfiles(int uid, java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeString(packageName);
          _data.writeString(profileName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mergeProfiles, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().mergeProfiles(uid, packageName, profileName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean dumpProfiles(int uid, java.lang.String packageName, java.lang.String profileName, java.lang.String codePath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeString(packageName);
          _data.writeString(profileName);
          _data.writeString(codePath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_dumpProfiles, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().dumpProfiles(uid, packageName, profileName, codePath);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean copySystemProfile(java.lang.String systemProfile, int uid, java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(systemProfile);
          _data.writeInt(uid);
          _data.writeString(packageName);
          _data.writeString(profileName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_copySystemProfile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().copySystemProfile(systemProfile, uid, packageName, profileName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void clearAppProfiles(java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeString(profileName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearAppProfiles, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearAppProfiles(packageName, profileName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyAppProfiles(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyAppProfiles, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyAppProfiles(packageName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean createProfileSnapshot(int appId, java.lang.String packageName, java.lang.String profileName, java.lang.String classpath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(appId);
          _data.writeString(packageName);
          _data.writeString(profileName);
          _data.writeString(classpath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createProfileSnapshot, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createProfileSnapshot(appId, packageName, profileName, classpath);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void destroyProfileSnapshot(java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeString(profileName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyProfileSnapshot, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyProfileSnapshot(packageName, profileName);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void idmap(java.lang.String targetApkPath, java.lang.String overlayApkPath, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(targetApkPath);
          _data.writeString(overlayApkPath);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_idmap, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().idmap(targetApkPath, overlayApkPath, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void removeIdmap(java.lang.String overlayApkPath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(overlayApkPath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeIdmap, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().removeIdmap(overlayApkPath);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void rmPackageDir(java.lang.String packageDir) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageDir);
          boolean _status = mRemote.transact(Stub.TRANSACTION_rmPackageDir, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().rmPackageDir(packageDir);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void freeCache(java.lang.String uuid, long targetFreeBytes, long cacheReservedBytes, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeLong(targetFreeBytes);
          _data.writeLong(cacheReservedBytes);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_freeCache, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().freeCache(uuid, targetFreeBytes, cacheReservedBytes, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void linkNativeLibraryDirectory(java.lang.String uuid, java.lang.String packageName, java.lang.String nativeLibPath32, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeString(nativeLibPath32);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_linkNativeLibraryDirectory, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().linkNativeLibraryDirectory(uuid, packageName, nativeLibPath32, userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void createOatDir(java.lang.String oatDir, java.lang.String instructionSet) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(oatDir);
          _data.writeString(instructionSet);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createOatDir, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().createOatDir(oatDir, instructionSet);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void linkFile(java.lang.String relativePath, java.lang.String fromBase, java.lang.String toBase) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(relativePath);
          _data.writeString(fromBase);
          _data.writeString(toBase);
          boolean _status = mRemote.transact(Stub.TRANSACTION_linkFile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().linkFile(relativePath, fromBase, toBase);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void moveAb(java.lang.String apkPath, java.lang.String instructionSet, java.lang.String outputPath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(apkPath);
          _data.writeString(instructionSet);
          _data.writeString(outputPath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_moveAb, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().moveAb(apkPath, instructionSet, outputPath);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void deleteOdex(java.lang.String apkPath, java.lang.String instructionSet, java.lang.String outputPath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(apkPath);
          _data.writeString(instructionSet);
          _data.writeString(outputPath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_deleteOdex, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().deleteOdex(apkPath, instructionSet, outputPath);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void installApkVerity(java.lang.String filePath, java.io.FileDescriptor verityInput, int contentSize) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(filePath);
          _data.writeRawFileDescriptor(verityInput);
          _data.writeInt(contentSize);
          boolean _status = mRemote.transact(Stub.TRANSACTION_installApkVerity, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().installApkVerity(filePath, verityInput, contentSize);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void assertFsverityRootHashMatches(java.lang.String filePath, byte[] expectedHash) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(filePath);
          _data.writeByteArray(expectedHash);
          boolean _status = mRemote.transact(Stub.TRANSACTION_assertFsverityRootHashMatches, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().assertFsverityRootHashMatches(filePath, expectedHash);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean reconcileSecondaryDexFile(java.lang.String dexPath, java.lang.String pkgName, int uid, java.lang.String[] isas, java.lang.String volume_uuid, int storage_flag) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(dexPath);
          _data.writeString(pkgName);
          _data.writeInt(uid);
          _data.writeStringArray(isas);
          _data.writeString(volume_uuid);
          _data.writeInt(storage_flag);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reconcileSecondaryDexFile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().reconcileSecondaryDexFile(dexPath, pkgName, uid, isas, volume_uuid, storage_flag);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public byte[] hashSecondaryDexFile(java.lang.String dexPath, java.lang.String pkgName, int uid, java.lang.String volumeUuid, int storageFlag) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(dexPath);
          _data.writeString(pkgName);
          _data.writeInt(uid);
          _data.writeString(volumeUuid);
          _data.writeInt(storageFlag);
          boolean _status = mRemote.transact(Stub.TRANSACTION_hashSecondaryDexFile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().hashSecondaryDexFile(dexPath, pkgName, uid, volumeUuid, storageFlag);
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void invalidateMounts() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_invalidateMounts, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().invalidateMounts();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isQuotaSupported(java.lang.String uuid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isQuotaSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isQuotaSupported(uuid);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean prepareAppProfile(java.lang.String packageName, int userId, int appId, java.lang.String profileName, java.lang.String codePath, java.lang.String dexMetadata) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeInt(appId);
          _data.writeString(profileName);
          _data.writeString(codePath);
          _data.writeString(dexMetadata);
          boolean _status = mRemote.transact(Stub.TRANSACTION_prepareAppProfile, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().prepareAppProfile(packageName, userId, appId, profileName, codePath, dexMetadata);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long snapshotAppData(java.lang.String uuid, java.lang.String packageName, int userId, int snapshotId, int storageFlags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeInt(snapshotId);
          _data.writeInt(storageFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_snapshotAppData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().snapshotAppData(uuid, packageName, userId, snapshotId, storageFlags);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void restoreAppDataSnapshot(java.lang.String uuid, java.lang.String packageName, int appId, java.lang.String seInfo, int user, int snapshotId, int storageflags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(appId);
          _data.writeString(seInfo);
          _data.writeInt(user);
          _data.writeInt(snapshotId);
          _data.writeInt(storageflags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_restoreAppDataSnapshot, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().restoreAppDataSnapshot(uuid, packageName, appId, seInfo, user, snapshotId, storageflags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyAppDataSnapshot(java.lang.String uuid, java.lang.String packageName, int userId, long ceSnapshotInode, int snapshotId, int storageFlags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(uuid);
          _data.writeString(packageName);
          _data.writeInt(userId);
          _data.writeLong(ceSnapshotInode);
          _data.writeInt(snapshotId);
          _data.writeInt(storageFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyAppDataSnapshot, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyAppDataSnapshot(uuid, packageName, userId, ceSnapshotInode, snapshotId, storageFlags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void migrateLegacyObbData() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_migrateLegacyObbData, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().migrateLegacyObbData();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.os.IInstalld sDefaultImpl;
    }
    static final int TRANSACTION_createUserData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_destroyUserData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_createAppData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_restoreconAppData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_migrateAppData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_clearAppData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_destroyAppData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_fixupAppData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_getAppSize = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_getUserSize = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_getExternalSize = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_setAppQuota = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_moveCompleteApp = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_dexopt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_compileLayouts = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_rmdex = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_mergeProfiles = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_dumpProfiles = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_copySystemProfile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    static final int TRANSACTION_clearAppProfiles = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    static final int TRANSACTION_destroyAppProfiles = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
    static final int TRANSACTION_createProfileSnapshot = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
    static final int TRANSACTION_destroyProfileSnapshot = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
    static final int TRANSACTION_idmap = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
    static final int TRANSACTION_removeIdmap = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
    static final int TRANSACTION_rmPackageDir = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
    static final int TRANSACTION_freeCache = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
    static final int TRANSACTION_linkNativeLibraryDirectory = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
    static final int TRANSACTION_createOatDir = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
    static final int TRANSACTION_linkFile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
    static final int TRANSACTION_moveAb = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
    static final int TRANSACTION_deleteOdex = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
    static final int TRANSACTION_installApkVerity = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
    static final int TRANSACTION_assertFsverityRootHashMatches = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
    static final int TRANSACTION_reconcileSecondaryDexFile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
    static final int TRANSACTION_hashSecondaryDexFile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
    static final int TRANSACTION_invalidateMounts = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
    static final int TRANSACTION_isQuotaSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 37);
    static final int TRANSACTION_prepareAppProfile = (android.os.IBinder.FIRST_CALL_TRANSACTION + 38);
    static final int TRANSACTION_snapshotAppData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 39);
    static final int TRANSACTION_restoreAppDataSnapshot = (android.os.IBinder.FIRST_CALL_TRANSACTION + 40);
    static final int TRANSACTION_destroyAppDataSnapshot = (android.os.IBinder.FIRST_CALL_TRANSACTION + 41);
    static final int TRANSACTION_migrateLegacyObbData = (android.os.IBinder.FIRST_CALL_TRANSACTION + 42);
    public static boolean setDefaultImpl(android.os.IInstalld impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IInstalld getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int FLAG_STORAGE_DE = 1;
  public static final int FLAG_STORAGE_CE = 2;
  public static final int FLAG_STORAGE_EXTERNAL = 4;
  public static final int FLAG_CLEAR_CACHE_ONLY = 16;
  public static final int FLAG_CLEAR_CODE_CACHE_ONLY = 32;
  public static final int FLAG_FREE_CACHE_V2 = 256;
  public static final int FLAG_FREE_CACHE_V2_DEFY_QUOTA = 512;
  public static final int FLAG_FREE_CACHE_NOOP = 1024;
  public static final int FLAG_USE_QUOTA = 4096;
  public static final int FLAG_FORCE = 8192;
  public static final int FLAG_CLEAR_APP_DATA_KEEP_ART_PROFILES = 131072;
  public void createUserData(java.lang.String uuid, int userId, int userSerial, int flags) throws android.os.RemoteException;
  public void destroyUserData(java.lang.String uuid, int userId, int flags) throws android.os.RemoteException;
  public long createAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, int appId, java.lang.String seInfo, int targetSdkVersion) throws android.os.RemoteException;
  public void restoreconAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, int appId, java.lang.String seInfo) throws android.os.RemoteException;
  public void migrateAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags) throws android.os.RemoteException;
  public void clearAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, long ceDataInode) throws android.os.RemoteException;
  public void destroyAppData(java.lang.String uuid, java.lang.String packageName, int userId, int flags, long ceDataInode) throws android.os.RemoteException;
  public void fixupAppData(java.lang.String uuid, int flags) throws android.os.RemoteException;
  public long[] getAppSize(java.lang.String uuid, java.lang.String[] packageNames, int userId, int flags, int appId, long[] ceDataInodes, java.lang.String[] codePaths) throws android.os.RemoteException;
  public long[] getUserSize(java.lang.String uuid, int userId, int flags, int[] appIds) throws android.os.RemoteException;
  public long[] getExternalSize(java.lang.String uuid, int userId, int flags, int[] appIds) throws android.os.RemoteException;
  public void setAppQuota(java.lang.String uuid, int userId, int appId, long cacheQuota) throws android.os.RemoteException;
  public void moveCompleteApp(java.lang.String fromUuid, java.lang.String toUuid, java.lang.String packageName, java.lang.String dataAppName, int appId, java.lang.String seInfo, int targetSdkVersion) throws android.os.RemoteException;
  public void dexopt(java.lang.String apkPath, int uid, java.lang.String packageName, java.lang.String instructionSet, int dexoptNeeded, java.lang.String outputPath, int dexFlags, java.lang.String compilerFilter, java.lang.String uuid, java.lang.String sharedLibraries, java.lang.String seInfo, boolean downgrade, int targetSdkVersion, java.lang.String profileName, java.lang.String dexMetadataPath, java.lang.String compilationReason) throws android.os.RemoteException;
  public boolean compileLayouts(java.lang.String apkPath, java.lang.String packageName, java.lang.String outDexFile, int uid) throws android.os.RemoteException;
  public void rmdex(java.lang.String codePath, java.lang.String instructionSet) throws android.os.RemoteException;
  public boolean mergeProfiles(int uid, java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException;
  public boolean dumpProfiles(int uid, java.lang.String packageName, java.lang.String profileName, java.lang.String codePath) throws android.os.RemoteException;
  public boolean copySystemProfile(java.lang.String systemProfile, int uid, java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException;
  public void clearAppProfiles(java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException;
  public void destroyAppProfiles(java.lang.String packageName) throws android.os.RemoteException;
  public boolean createProfileSnapshot(int appId, java.lang.String packageName, java.lang.String profileName, java.lang.String classpath) throws android.os.RemoteException;
  public void destroyProfileSnapshot(java.lang.String packageName, java.lang.String profileName) throws android.os.RemoteException;
  public void idmap(java.lang.String targetApkPath, java.lang.String overlayApkPath, int uid) throws android.os.RemoteException;
  public void removeIdmap(java.lang.String overlayApkPath) throws android.os.RemoteException;
  public void rmPackageDir(java.lang.String packageDir) throws android.os.RemoteException;
  public void freeCache(java.lang.String uuid, long targetFreeBytes, long cacheReservedBytes, int flags) throws android.os.RemoteException;
  public void linkNativeLibraryDirectory(java.lang.String uuid, java.lang.String packageName, java.lang.String nativeLibPath32, int userId) throws android.os.RemoteException;
  public void createOatDir(java.lang.String oatDir, java.lang.String instructionSet) throws android.os.RemoteException;
  public void linkFile(java.lang.String relativePath, java.lang.String fromBase, java.lang.String toBase) throws android.os.RemoteException;
  public void moveAb(java.lang.String apkPath, java.lang.String instructionSet, java.lang.String outputPath) throws android.os.RemoteException;
  public void deleteOdex(java.lang.String apkPath, java.lang.String instructionSet, java.lang.String outputPath) throws android.os.RemoteException;
  public void installApkVerity(java.lang.String filePath, java.io.FileDescriptor verityInput, int contentSize) throws android.os.RemoteException;
  public void assertFsverityRootHashMatches(java.lang.String filePath, byte[] expectedHash) throws android.os.RemoteException;
  public boolean reconcileSecondaryDexFile(java.lang.String dexPath, java.lang.String pkgName, int uid, java.lang.String[] isas, java.lang.String volume_uuid, int storage_flag) throws android.os.RemoteException;
  public byte[] hashSecondaryDexFile(java.lang.String dexPath, java.lang.String pkgName, int uid, java.lang.String volumeUuid, int storageFlag) throws android.os.RemoteException;
  public void invalidateMounts() throws android.os.RemoteException;
  public boolean isQuotaSupported(java.lang.String uuid) throws android.os.RemoteException;
  public boolean prepareAppProfile(java.lang.String packageName, int userId, int appId, java.lang.String profileName, java.lang.String codePath, java.lang.String dexMetadata) throws android.os.RemoteException;
  public long snapshotAppData(java.lang.String uuid, java.lang.String packageName, int userId, int snapshotId, int storageFlags) throws android.os.RemoteException;
  public void restoreAppDataSnapshot(java.lang.String uuid, java.lang.String packageName, int appId, java.lang.String seInfo, int user, int snapshotId, int storageflags) throws android.os.RemoteException;
  public void destroyAppDataSnapshot(java.lang.String uuid, java.lang.String packageName, int userId, long ceSnapshotInode, int snapshotId, int storageFlags) throws android.os.RemoteException;
  public void migrateLegacyObbData() throws android.os.RemoteException;
}
