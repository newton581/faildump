/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.hardware.hdmi;
/**
 * Listener used to get the status of the HDMI CEC volume control feature (enabled/disabled).
 * @hide
 */
public interface IHdmiCecVolumeControlFeatureListener extends android.os.IInterface
{
  /** Default implementation for IHdmiCecVolumeControlFeatureListener. */
  public static class Default implements android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener
  {
    /**
         * Called when the HDMI Control (CEC) volume control feature is enabled/disabled.
         *
         * @param enabled status of HDMI CEC volume control feature
         * @see {@link HdmiControlManager#setHdmiCecVolumeControlEnabled(boolean)} ()}
         **/
    @Override public void onHdmiCecVolumeControlFeature(boolean enabled) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener
  {
    private static final java.lang.String DESCRIPTOR = "android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener interface,
     * generating a proxy if needed.
     */
    public static android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener))) {
        return ((android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener)iin);
      }
      return new android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onHdmiCecVolumeControlFeature:
        {
          return "onHdmiCecVolumeControlFeature";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onHdmiCecVolumeControlFeature:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          this.onHdmiCecVolumeControlFeature(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Called when the HDMI Control (CEC) volume control feature is enabled/disabled.
           *
           * @param enabled status of HDMI CEC volume control feature
           * @see {@link HdmiControlManager#setHdmiCecVolumeControlEnabled(boolean)} ()}
           **/
      @Override public void onHdmiCecVolumeControlFeature(boolean enabled) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((enabled)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_onHdmiCecVolumeControlFeature, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onHdmiCecVolumeControlFeature(enabled);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener sDefaultImpl;
    }
    static final int TRANSACTION_onHdmiCecVolumeControlFeature = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    public static boolean setDefaultImpl(android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.hardware.hdmi.IHdmiCecVolumeControlFeatureListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * Called when the HDMI Control (CEC) volume control feature is enabled/disabled.
       *
       * @param enabled status of HDMI CEC volume control feature
       * @see {@link HdmiControlManager#setHdmiCecVolumeControlEnabled(boolean)} ()}
       **/
  public void onHdmiCecVolumeControlFeature(boolean enabled) throws android.os.RemoteException;
}
