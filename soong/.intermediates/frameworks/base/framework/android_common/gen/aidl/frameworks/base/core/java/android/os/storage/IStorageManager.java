/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os.storage;
/**
 * WARNING! Update IMountService.h and IMountService.cpp if you change this
 * file. In particular, the transaction ids below must match the
 * _TRANSACTION enum in IMountService.cpp
 *
 * @hide - Applications should use android.os.storage.StorageManager to access
 *       storage functions.
 */
public interface IStorageManager extends android.os.IInterface
{
  /** Default implementation for IStorageManager. */
  public static class Default implements android.os.storage.IStorageManager
  {
    /**
         * Registers an IStorageEventListener for receiving async notifications.
         */
    @Override public void registerListener(android.os.storage.IStorageEventListener listener) throws android.os.RemoteException
    {
    }
    /**
         * Unregisters an IStorageEventListener
         */
    @Override public void unregisterListener(android.os.storage.IStorageEventListener listener) throws android.os.RemoteException
    {
    }
    /**
         * Shuts down the StorageManagerService and gracefully unmounts all external media.
         * Invokes call back once the shutdown is complete.
         */
    @Override public void shutdown(android.os.storage.IStorageShutdownObserver observer) throws android.os.RemoteException
    {
    }
    /**
         * Mounts an Opaque Binary Blob (OBB) with the specified decryption key and
         * only allows the calling process's UID access to the contents.
         * StorageManagerService will call back to the supplied IObbActionListener to inform
         * it of the terminal state of the call.
         */
    @Override public void mountObb(java.lang.String rawPath, java.lang.String canonicalPath, java.lang.String key, android.os.storage.IObbActionListener token, int nonce, android.content.res.ObbInfo obbInfo) throws android.os.RemoteException
    {
    }
    /**
         * Unmounts an Opaque Binary Blob (OBB). When the force flag is specified,
         * any program using it will be forcibly killed to unmount the image.
         * StorageManagerService will call back to the supplied IObbActionListener to inform
         * it of the terminal state of the call.
         */
    @Override public void unmountObb(java.lang.String rawPath, boolean force, android.os.storage.IObbActionListener token, int nonce) throws android.os.RemoteException
    {
    }
    /**
         * Checks whether the specified Opaque Binary Blob (OBB) is mounted
         * somewhere.
         */
    @Override public boolean isObbMounted(java.lang.String rawPath) throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Gets the path to the mounted Opaque Binary Blob (OBB).
         */
    @Override public java.lang.String getMountedObbPath(java.lang.String rawPath) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Decrypts any encrypted volumes.
         */
    @Override public int decryptStorage(java.lang.String password) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Encrypts storage.
         */
    @Override public int encryptStorage(int type, java.lang.String password) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Changes the encryption password.
         */
    @Override public int changeEncryptionPassword(int type, java.lang.String password) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Returns list of all mountable volumes.
         */
    @Override public android.os.storage.StorageVolume[] getVolumeList(int uid, java.lang.String packageName, int flags) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Determines the encryption state of the volume.
         * @return a numerical value. See {@code ENCRYPTION_STATE_*} for possible
         * values.
         * Note that this has been replaced in most cases by the APIs in
         * StorageManager (see isEncryptable and below)
         * This is still useful to get the error state when encryption has failed
         * and CryptKeeper needs to throw up a screen advising the user what to do
         */
    @Override public int getEncryptionState() throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Verify the encryption password against the stored volume.  This method
         * may only be called by the system process.
         */
    @Override public int verifyEncryptionPassword(java.lang.String password) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Ensure that all directories along given path exist, creating parent
         * directories as needed. Validates that given path is absolute and that it
         * contains no relative "." or ".." paths or symlinks. Also ensures that
         * path belongs to a volume managed by vold, and that path is either
         * external storage data or OBB directory belonging to calling app.
         */
    @Override public void mkdirs(java.lang.String callingPkg, java.lang.String path) throws android.os.RemoteException
    {
    }
    /**
         * Determines the type of the encryption password
         * @return PasswordType
         */
    @Override public int getPasswordType() throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Get password from vold
         * @return password or empty string
         */
    @Override public java.lang.String getPassword() throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Securely clear password from vold
         */
    @Override public void clearPassword() throws android.os.RemoteException
    {
    }
    /**
         * Set a field in the crypto header.
         * @param field field to set
         * @param contents contents to set in field
         */
    @Override public void setField(java.lang.String field, java.lang.String contents) throws android.os.RemoteException
    {
    }
    /**
         * Gets a field from the crypto header.
         * @param field field to get
         * @return contents of field
         */
    @Override public java.lang.String getField(java.lang.String field) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Report the time of the last maintenance operation such as fstrim.
         * @return Timestamp of the last maintenance operation, in the
         *     System.currentTimeMillis() time base
         * @throws RemoteException
         */
    @Override public long lastMaintenance() throws android.os.RemoteException
    {
      return 0L;
    }
    /**
         * Kick off an immediate maintenance operation
         * @throws RemoteException
         */
    @Override public void runMaintenance() throws android.os.RemoteException
    {
    }
    @Override public android.os.storage.DiskInfo[] getDisks() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.storage.VolumeInfo[] getVolumes(int flags) throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.storage.VolumeRecord[] getVolumeRecords(int flags) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void mount(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override public void unmount(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override public void format(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override public void partitionPublic(java.lang.String diskId) throws android.os.RemoteException
    {
    }
    @Override public void partitionPrivate(java.lang.String diskId) throws android.os.RemoteException
    {
    }
    @Override public void partitionMixed(java.lang.String diskId, int ratio) throws android.os.RemoteException
    {
    }
    @Override public void setVolumeNickname(java.lang.String fsUuid, java.lang.String nickname) throws android.os.RemoteException
    {
    }
    @Override public void setVolumeUserFlags(java.lang.String fsUuid, int flags, int mask) throws android.os.RemoteException
    {
    }
    @Override public void forgetVolume(java.lang.String fsUuid) throws android.os.RemoteException
    {
    }
    @Override public void forgetAllVolumes() throws android.os.RemoteException
    {
    }
    @Override public java.lang.String getPrimaryStorageUuid() throws android.os.RemoteException
    {
      return null;
    }
    @Override public void setPrimaryStorageUuid(java.lang.String volumeUuid, android.content.pm.IPackageMoveObserver callback) throws android.os.RemoteException
    {
    }
    @Override public void benchmark(java.lang.String volId, android.os.IVoldTaskListener listener) throws android.os.RemoteException
    {
    }
    @Override public void setDebugFlags(int flags, int mask) throws android.os.RemoteException
    {
    }
    @Override public void createUserKey(int userId, int serialNumber, boolean ephemeral) throws android.os.RemoteException
    {
    }
    @Override public void destroyUserKey(int userId) throws android.os.RemoteException
    {
    }
    @Override public void unlockUserKey(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException
    {
    }
    @Override public void lockUserKey(int userId) throws android.os.RemoteException
    {
    }
    @Override public boolean isUserKeyUnlocked(int userId) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void prepareUserStorage(java.lang.String volumeUuid, int userId, int serialNumber, int flags) throws android.os.RemoteException
    {
    }
    @Override public void destroyUserStorage(java.lang.String volumeUuid, int userId, int flags) throws android.os.RemoteException
    {
    }
    @Override public boolean isConvertibleToFBE() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void addUserKeyAuth(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException
    {
    }
    @Override public void fixateNewestUserKeyAuth(int userId) throws android.os.RemoteException
    {
    }
    @Override public void fstrim(int flags, android.os.IVoldTaskListener listener) throws android.os.RemoteException
    {
    }
    @Override public com.android.internal.os.AppFuseMount mountProxyFileDescriptorBridge() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.ParcelFileDescriptor openProxyFileDescriptor(int mountPointId, int fileId, int mode) throws android.os.RemoteException
    {
      return null;
    }
    @Override public long getCacheQuotaBytes(java.lang.String volumeUuid, int uid) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public long getCacheSizeBytes(java.lang.String volumeUuid, int uid) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public long getAllocatableBytes(java.lang.String volumeUuid, int flags, java.lang.String callingPackage) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public void allocateBytes(java.lang.String volumeUuid, long bytes, int flags, java.lang.String callingPackage) throws android.os.RemoteException
    {
    }
    @Override public void runIdleMaintenance() throws android.os.RemoteException
    {
    }
    @Override public void abortIdleMaintenance() throws android.os.RemoteException
    {
    }
    @Override public void commitChanges() throws android.os.RemoteException
    {
    }
    @Override public boolean supportsCheckpoint() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void startCheckpoint(int numTries) throws android.os.RemoteException
    {
    }
    @Override public boolean needsCheckpoint() throws android.os.RemoteException
    {
      return false;
    }
    @Override public void abortChanges(java.lang.String message, boolean retry) throws android.os.RemoteException
    {
    }
    @Override public void clearUserKeyAuth(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.storage.IStorageManager
  {
    private static final java.lang.String DESCRIPTOR = "android.os.storage.IStorageManager";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.storage.IStorageManager interface,
     * generating a proxy if needed.
     */
    public static android.os.storage.IStorageManager asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.storage.IStorageManager))) {
        return ((android.os.storage.IStorageManager)iin);
      }
      return new android.os.storage.IStorageManager.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_registerListener:
        {
          return "registerListener";
        }
        case TRANSACTION_unregisterListener:
        {
          return "unregisterListener";
        }
        case TRANSACTION_shutdown:
        {
          return "shutdown";
        }
        case TRANSACTION_mountObb:
        {
          return "mountObb";
        }
        case TRANSACTION_unmountObb:
        {
          return "unmountObb";
        }
        case TRANSACTION_isObbMounted:
        {
          return "isObbMounted";
        }
        case TRANSACTION_getMountedObbPath:
        {
          return "getMountedObbPath";
        }
        case TRANSACTION_decryptStorage:
        {
          return "decryptStorage";
        }
        case TRANSACTION_encryptStorage:
        {
          return "encryptStorage";
        }
        case TRANSACTION_changeEncryptionPassword:
        {
          return "changeEncryptionPassword";
        }
        case TRANSACTION_getVolumeList:
        {
          return "getVolumeList";
        }
        case TRANSACTION_getEncryptionState:
        {
          return "getEncryptionState";
        }
        case TRANSACTION_verifyEncryptionPassword:
        {
          return "verifyEncryptionPassword";
        }
        case TRANSACTION_mkdirs:
        {
          return "mkdirs";
        }
        case TRANSACTION_getPasswordType:
        {
          return "getPasswordType";
        }
        case TRANSACTION_getPassword:
        {
          return "getPassword";
        }
        case TRANSACTION_clearPassword:
        {
          return "clearPassword";
        }
        case TRANSACTION_setField:
        {
          return "setField";
        }
        case TRANSACTION_getField:
        {
          return "getField";
        }
        case TRANSACTION_lastMaintenance:
        {
          return "lastMaintenance";
        }
        case TRANSACTION_runMaintenance:
        {
          return "runMaintenance";
        }
        case TRANSACTION_getDisks:
        {
          return "getDisks";
        }
        case TRANSACTION_getVolumes:
        {
          return "getVolumes";
        }
        case TRANSACTION_getVolumeRecords:
        {
          return "getVolumeRecords";
        }
        case TRANSACTION_mount:
        {
          return "mount";
        }
        case TRANSACTION_unmount:
        {
          return "unmount";
        }
        case TRANSACTION_format:
        {
          return "format";
        }
        case TRANSACTION_partitionPublic:
        {
          return "partitionPublic";
        }
        case TRANSACTION_partitionPrivate:
        {
          return "partitionPrivate";
        }
        case TRANSACTION_partitionMixed:
        {
          return "partitionMixed";
        }
        case TRANSACTION_setVolumeNickname:
        {
          return "setVolumeNickname";
        }
        case TRANSACTION_setVolumeUserFlags:
        {
          return "setVolumeUserFlags";
        }
        case TRANSACTION_forgetVolume:
        {
          return "forgetVolume";
        }
        case TRANSACTION_forgetAllVolumes:
        {
          return "forgetAllVolumes";
        }
        case TRANSACTION_getPrimaryStorageUuid:
        {
          return "getPrimaryStorageUuid";
        }
        case TRANSACTION_setPrimaryStorageUuid:
        {
          return "setPrimaryStorageUuid";
        }
        case TRANSACTION_benchmark:
        {
          return "benchmark";
        }
        case TRANSACTION_setDebugFlags:
        {
          return "setDebugFlags";
        }
        case TRANSACTION_createUserKey:
        {
          return "createUserKey";
        }
        case TRANSACTION_destroyUserKey:
        {
          return "destroyUserKey";
        }
        case TRANSACTION_unlockUserKey:
        {
          return "unlockUserKey";
        }
        case TRANSACTION_lockUserKey:
        {
          return "lockUserKey";
        }
        case TRANSACTION_isUserKeyUnlocked:
        {
          return "isUserKeyUnlocked";
        }
        case TRANSACTION_prepareUserStorage:
        {
          return "prepareUserStorage";
        }
        case TRANSACTION_destroyUserStorage:
        {
          return "destroyUserStorage";
        }
        case TRANSACTION_isConvertibleToFBE:
        {
          return "isConvertibleToFBE";
        }
        case TRANSACTION_addUserKeyAuth:
        {
          return "addUserKeyAuth";
        }
        case TRANSACTION_fixateNewestUserKeyAuth:
        {
          return "fixateNewestUserKeyAuth";
        }
        case TRANSACTION_fstrim:
        {
          return "fstrim";
        }
        case TRANSACTION_mountProxyFileDescriptorBridge:
        {
          return "mountProxyFileDescriptorBridge";
        }
        case TRANSACTION_openProxyFileDescriptor:
        {
          return "openProxyFileDescriptor";
        }
        case TRANSACTION_getCacheQuotaBytes:
        {
          return "getCacheQuotaBytes";
        }
        case TRANSACTION_getCacheSizeBytes:
        {
          return "getCacheSizeBytes";
        }
        case TRANSACTION_getAllocatableBytes:
        {
          return "getAllocatableBytes";
        }
        case TRANSACTION_allocateBytes:
        {
          return "allocateBytes";
        }
        case TRANSACTION_runIdleMaintenance:
        {
          return "runIdleMaintenance";
        }
        case TRANSACTION_abortIdleMaintenance:
        {
          return "abortIdleMaintenance";
        }
        case TRANSACTION_commitChanges:
        {
          return "commitChanges";
        }
        case TRANSACTION_supportsCheckpoint:
        {
          return "supportsCheckpoint";
        }
        case TRANSACTION_startCheckpoint:
        {
          return "startCheckpoint";
        }
        case TRANSACTION_needsCheckpoint:
        {
          return "needsCheckpoint";
        }
        case TRANSACTION_abortChanges:
        {
          return "abortChanges";
        }
        case TRANSACTION_clearUserKeyAuth:
        {
          return "clearUserKeyAuth";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_registerListener:
        {
          data.enforceInterface(descriptor);
          android.os.storage.IStorageEventListener _arg0;
          _arg0 = android.os.storage.IStorageEventListener.Stub.asInterface(data.readStrongBinder());
          this.registerListener(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unregisterListener:
        {
          data.enforceInterface(descriptor);
          android.os.storage.IStorageEventListener _arg0;
          _arg0 = android.os.storage.IStorageEventListener.Stub.asInterface(data.readStrongBinder());
          this.unregisterListener(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_shutdown:
        {
          data.enforceInterface(descriptor);
          android.os.storage.IStorageShutdownObserver _arg0;
          _arg0 = android.os.storage.IStorageShutdownObserver.Stub.asInterface(data.readStrongBinder());
          this.shutdown(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_mountObb:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          android.os.storage.IObbActionListener _arg3;
          _arg3 = android.os.storage.IObbActionListener.Stub.asInterface(data.readStrongBinder());
          int _arg4;
          _arg4 = data.readInt();
          android.content.res.ObbInfo _arg5;
          if ((0!=data.readInt())) {
            _arg5 = android.content.res.ObbInfo.CREATOR.createFromParcel(data);
          }
          else {
            _arg5 = null;
          }
          this.mountObb(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unmountObb:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          android.os.storage.IObbActionListener _arg2;
          _arg2 = android.os.storage.IObbActionListener.Stub.asInterface(data.readStrongBinder());
          int _arg3;
          _arg3 = data.readInt();
          this.unmountObb(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isObbMounted:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.isObbMounted(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getMountedObbPath:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.getMountedObbPath(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_decryptStorage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _result = this.decryptStorage(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_encryptStorage:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _result = this.encryptStorage(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_changeEncryptionPassword:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _result = this.changeEncryptionPassword(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getVolumeList:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          android.os.storage.StorageVolume[] _result = this.getVolumeList(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_getEncryptionState:
        {
          data.enforceInterface(descriptor);
          int _result = this.getEncryptionState();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_verifyEncryptionPassword:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _result = this.verifyEncryptionPassword(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_mkdirs:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.mkdirs(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPasswordType:
        {
          data.enforceInterface(descriptor);
          int _result = this.getPasswordType();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getPassword:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getPassword();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_clearPassword:
        {
          data.enforceInterface(descriptor);
          this.clearPassword();
          return true;
        }
        case TRANSACTION_setField:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.setField(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_getField:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.getField(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_lastMaintenance:
        {
          data.enforceInterface(descriptor);
          long _result = this.lastMaintenance();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_runMaintenance:
        {
          data.enforceInterface(descriptor);
          this.runMaintenance();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getDisks:
        {
          data.enforceInterface(descriptor);
          android.os.storage.DiskInfo[] _result = this.getDisks();
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_getVolumes:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.os.storage.VolumeInfo[] _result = this.getVolumes(_arg0);
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_getVolumeRecords:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.os.storage.VolumeRecord[] _result = this.getVolumeRecords(_arg0);
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_mount:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.mount(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unmount:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.unmount(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_format:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.format(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_partitionPublic:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.partitionPublic(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_partitionPrivate:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.partitionPrivate(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_partitionMixed:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.partitionMixed(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setVolumeNickname:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.setVolumeNickname(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setVolumeUserFlags:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          this.setVolumeUserFlags(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_forgetVolume:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.forgetVolume(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_forgetAllVolumes:
        {
          data.enforceInterface(descriptor);
          this.forgetAllVolumes();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getPrimaryStorageUuid:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getPrimaryStorageUuid();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_setPrimaryStorageUuid:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.content.pm.IPackageMoveObserver _arg1;
          _arg1 = android.content.pm.IPackageMoveObserver.Stub.asInterface(data.readStrongBinder());
          this.setPrimaryStorageUuid(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_benchmark:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.os.IVoldTaskListener _arg1;
          _arg1 = android.os.IVoldTaskListener.Stub.asInterface(data.readStrongBinder());
          this.benchmark(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setDebugFlags:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.setDebugFlags(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_createUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.createUserKey(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.destroyUserKey(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unlockUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          byte[] _arg3;
          _arg3 = data.createByteArray();
          this.unlockUserKey(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_lockUserKey:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.lockUserKey(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isUserKeyUnlocked:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.isUserKeyUnlocked(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_prepareUserStorage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          this.prepareUserStorage(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_destroyUserStorage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          this.destroyUserStorage(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isConvertibleToFBE:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isConvertibleToFBE();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_addUserKeyAuth:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          byte[] _arg3;
          _arg3 = data.createByteArray();
          this.addUserKeyAuth(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fixateNewestUserKeyAuth:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.fixateNewestUserKeyAuth(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_fstrim:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.os.IVoldTaskListener _arg1;
          _arg1 = android.os.IVoldTaskListener.Stub.asInterface(data.readStrongBinder());
          this.fstrim(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_mountProxyFileDescriptorBridge:
        {
          data.enforceInterface(descriptor);
          com.android.internal.os.AppFuseMount _result = this.mountProxyFileDescriptorBridge();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_openProxyFileDescriptor:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          android.os.ParcelFileDescriptor _result = this.openProxyFileDescriptor(_arg0, _arg1, _arg2);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getCacheQuotaBytes:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          long _result = this.getCacheQuotaBytes(_arg0, _arg1);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getCacheSizeBytes:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          long _result = this.getCacheSizeBytes(_arg0, _arg1);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getAllocatableBytes:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          long _result = this.getAllocatableBytes(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_allocateBytes:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          long _arg1;
          _arg1 = data.readLong();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _arg3;
          _arg3 = data.readString();
          this.allocateBytes(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_runIdleMaintenance:
        {
          data.enforceInterface(descriptor);
          this.runIdleMaintenance();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_abortIdleMaintenance:
        {
          data.enforceInterface(descriptor);
          this.abortIdleMaintenance();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_commitChanges:
        {
          data.enforceInterface(descriptor);
          this.commitChanges();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_supportsCheckpoint:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.supportsCheckpoint();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_startCheckpoint:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.startCheckpoint(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_needsCheckpoint:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.needsCheckpoint();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_abortChanges:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.abortChanges(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_clearUserKeyAuth:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          byte[] _arg3;
          _arg3 = data.createByteArray();
          this.clearUserKeyAuth(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.storage.IStorageManager
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Registers an IStorageEventListener for receiving async notifications.
           */
      @Override public void registerListener(android.os.storage.IStorageEventListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_registerListener, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().registerListener(listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Unregisters an IStorageEventListener
           */
      @Override public void unregisterListener(android.os.storage.IStorageEventListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_unregisterListener, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unregisterListener(listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Shuts down the StorageManagerService and gracefully unmounts all external media.
           * Invokes call back once the shutdown is complete.
           */
      @Override public void shutdown(android.os.storage.IStorageShutdownObserver observer) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((observer!=null))?(observer.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_shutdown, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().shutdown(observer);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Mounts an Opaque Binary Blob (OBB) with the specified decryption key and
           * only allows the calling process's UID access to the contents.
           * StorageManagerService will call back to the supplied IObbActionListener to inform
           * it of the terminal state of the call.
           */
      @Override public void mountObb(java.lang.String rawPath, java.lang.String canonicalPath, java.lang.String key, android.os.storage.IObbActionListener token, int nonce, android.content.res.ObbInfo obbInfo) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(rawPath);
          _data.writeString(canonicalPath);
          _data.writeString(key);
          _data.writeStrongBinder((((token!=null))?(token.asBinder()):(null)));
          _data.writeInt(nonce);
          if ((obbInfo!=null)) {
            _data.writeInt(1);
            obbInfo.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_mountObb, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().mountObb(rawPath, canonicalPath, key, token, nonce, obbInfo);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Unmounts an Opaque Binary Blob (OBB). When the force flag is specified,
           * any program using it will be forcibly killed to unmount the image.
           * StorageManagerService will call back to the supplied IObbActionListener to inform
           * it of the terminal state of the call.
           */
      @Override public void unmountObb(java.lang.String rawPath, boolean force, android.os.storage.IObbActionListener token, int nonce) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(rawPath);
          _data.writeInt(((force)?(1):(0)));
          _data.writeStrongBinder((((token!=null))?(token.asBinder()):(null)));
          _data.writeInt(nonce);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unmountObb, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unmountObb(rawPath, force, token, nonce);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Checks whether the specified Opaque Binary Blob (OBB) is mounted
           * somewhere.
           */
      @Override public boolean isObbMounted(java.lang.String rawPath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(rawPath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isObbMounted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isObbMounted(rawPath);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Gets the path to the mounted Opaque Binary Blob (OBB).
           */
      @Override public java.lang.String getMountedObbPath(java.lang.String rawPath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(rawPath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMountedObbPath, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMountedObbPath(rawPath);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Decrypts any encrypted volumes.
           */
      @Override public int decryptStorage(java.lang.String password) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(password);
          boolean _status = mRemote.transact(Stub.TRANSACTION_decryptStorage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().decryptStorage(password);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Encrypts storage.
           */
      @Override public int encryptStorage(int type, java.lang.String password) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(type);
          _data.writeString(password);
          boolean _status = mRemote.transact(Stub.TRANSACTION_encryptStorage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().encryptStorage(type, password);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Changes the encryption password.
           */
      @Override public int changeEncryptionPassword(int type, java.lang.String password) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(type);
          _data.writeString(password);
          boolean _status = mRemote.transact(Stub.TRANSACTION_changeEncryptionPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().changeEncryptionPassword(type, password);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns list of all mountable volumes.
           */
      @Override public android.os.storage.StorageVolume[] getVolumeList(int uid, java.lang.String packageName, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.storage.StorageVolume[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeString(packageName);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getVolumeList, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getVolumeList(uid, packageName, flags);
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.os.storage.StorageVolume.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Determines the encryption state of the volume.
           * @return a numerical value. See {@code ENCRYPTION_STATE_*} for possible
           * values.
           * Note that this has been replaced in most cases by the APIs in
           * StorageManager (see isEncryptable and below)
           * This is still useful to get the error state when encryption has failed
           * and CryptKeeper needs to throw up a screen advising the user what to do
           */
      @Override public int getEncryptionState() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getEncryptionState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getEncryptionState();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Verify the encryption password against the stored volume.  This method
           * may only be called by the system process.
           */
      @Override public int verifyEncryptionPassword(java.lang.String password) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(password);
          boolean _status = mRemote.transact(Stub.TRANSACTION_verifyEncryptionPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().verifyEncryptionPassword(password);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Ensure that all directories along given path exist, creating parent
           * directories as needed. Validates that given path is absolute and that it
           * contains no relative "." or ".." paths or symlinks. Also ensures that
           * path belongs to a volume managed by vold, and that path is either
           * external storage data or OBB directory belonging to calling app.
           */
      @Override public void mkdirs(java.lang.String callingPkg, java.lang.String path) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(callingPkg);
          _data.writeString(path);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mkdirs, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().mkdirs(callingPkg, path);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Determines the type of the encryption password
           * @return PasswordType
           */
      @Override public int getPasswordType() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPasswordType, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPasswordType();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Get password from vold
           * @return password or empty string
           */
      @Override public java.lang.String getPassword() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPassword, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPassword();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Securely clear password from vold
           */
      @Override public void clearPassword() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearPassword, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearPassword();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Set a field in the crypto header.
           * @param field field to set
           * @param contents contents to set in field
           */
      @Override public void setField(java.lang.String field, java.lang.String contents) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(field);
          _data.writeString(contents);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setField, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setField(field, contents);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Gets a field from the crypto header.
           * @param field field to get
           * @return contents of field
           */
      @Override public java.lang.String getField(java.lang.String field) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(field);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getField, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getField(field);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Report the time of the last maintenance operation such as fstrim.
           * @return Timestamp of the last maintenance operation, in the
           *     System.currentTimeMillis() time base
           * @throws RemoteException
           */
      @Override public long lastMaintenance() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_lastMaintenance, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().lastMaintenance();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Kick off an immediate maintenance operation
           * @throws RemoteException
           */
      @Override public void runMaintenance() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_runMaintenance, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().runMaintenance();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public android.os.storage.DiskInfo[] getDisks() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.storage.DiskInfo[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDisks, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDisks();
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.os.storage.DiskInfo.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.storage.VolumeInfo[] getVolumes(int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.storage.VolumeInfo[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getVolumes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getVolumes(flags);
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.os.storage.VolumeInfo.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.storage.VolumeRecord[] getVolumeRecords(int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.storage.VolumeRecord[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getVolumeRecords, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getVolumeRecords(flags);
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.os.storage.VolumeRecord.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void mount(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mount, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().mount(volId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void unmount(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unmount, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unmount(volId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void format(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_format, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().format(volId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void partitionPublic(java.lang.String diskId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_partitionPublic, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().partitionPublic(diskId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void partitionPrivate(java.lang.String diskId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_partitionPrivate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().partitionPrivate(diskId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void partitionMixed(java.lang.String diskId, int ratio) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          _data.writeInt(ratio);
          boolean _status = mRemote.transact(Stub.TRANSACTION_partitionMixed, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().partitionMixed(diskId, ratio);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setVolumeNickname(java.lang.String fsUuid, java.lang.String nickname) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(fsUuid);
          _data.writeString(nickname);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setVolumeNickname, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setVolumeNickname(fsUuid, nickname);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setVolumeUserFlags(java.lang.String fsUuid, int flags, int mask) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(fsUuid);
          _data.writeInt(flags);
          _data.writeInt(mask);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setVolumeUserFlags, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setVolumeUserFlags(fsUuid, flags, mask);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void forgetVolume(java.lang.String fsUuid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(fsUuid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_forgetVolume, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().forgetVolume(fsUuid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void forgetAllVolumes() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_forgetAllVolumes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().forgetAllVolumes();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.String getPrimaryStorageUuid() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPrimaryStorageUuid, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPrimaryStorageUuid();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setPrimaryStorageUuid(java.lang.String volumeUuid, android.content.pm.IPackageMoveObserver callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volumeUuid);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPrimaryStorageUuid, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setPrimaryStorageUuid(volumeUuid, callback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void benchmark(java.lang.String volId, android.os.IVoldTaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_benchmark, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().benchmark(volId, listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setDebugFlags(int flags, int mask) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(flags);
          _data.writeInt(mask);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setDebugFlags, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setDebugFlags(flags, mask);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void createUserKey(int userId, int serialNumber, boolean ephemeral) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(serialNumber);
          _data.writeInt(((ephemeral)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_createUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().createUserKey(userId, serialNumber, ephemeral);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyUserKey(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyUserKey(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void unlockUserKey(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(serialNumber);
          _data.writeByteArray(token);
          _data.writeByteArray(secret);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unlockUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unlockUserKey(userId, serialNumber, token, secret);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void lockUserKey(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_lockUserKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().lockUserKey(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isUserKeyUnlocked(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isUserKeyUnlocked, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isUserKeyUnlocked(userId);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void prepareUserStorage(java.lang.String volumeUuid, int userId, int serialNumber, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volumeUuid);
          _data.writeInt(userId);
          _data.writeInt(serialNumber);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_prepareUserStorage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().prepareUserStorage(volumeUuid, userId, serialNumber, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void destroyUserStorage(java.lang.String volumeUuid, int userId, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volumeUuid);
          _data.writeInt(userId);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_destroyUserStorage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().destroyUserStorage(volumeUuid, userId, flags);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isConvertibleToFBE() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isConvertibleToFBE, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isConvertibleToFBE();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void addUserKeyAuth(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(serialNumber);
          _data.writeByteArray(token);
          _data.writeByteArray(secret);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addUserKeyAuth, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().addUserKeyAuth(userId, serialNumber, token, secret);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fixateNewestUserKeyAuth(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_fixateNewestUserKeyAuth, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fixateNewestUserKeyAuth(userId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void fstrim(int flags, android.os.IVoldTaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(flags);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_fstrim, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().fstrim(flags, listener);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public com.android.internal.os.AppFuseMount mountProxyFileDescriptorBridge() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        com.android.internal.os.AppFuseMount _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_mountProxyFileDescriptorBridge, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().mountProxyFileDescriptorBridge();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = com.android.internal.os.AppFuseMount.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.ParcelFileDescriptor openProxyFileDescriptor(int mountPointId, int fileId, int mode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.ParcelFileDescriptor _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(mountPointId);
          _data.writeInt(fileId);
          _data.writeInt(mode);
          boolean _status = mRemote.transact(Stub.TRANSACTION_openProxyFileDescriptor, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().openProxyFileDescriptor(mountPointId, fileId, mode);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.ParcelFileDescriptor.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getCacheQuotaBytes(java.lang.String volumeUuid, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volumeUuid);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCacheQuotaBytes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCacheQuotaBytes(volumeUuid, uid);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getCacheSizeBytes(java.lang.String volumeUuid, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volumeUuid);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCacheSizeBytes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCacheSizeBytes(volumeUuid, uid);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getAllocatableBytes(java.lang.String volumeUuid, int flags, java.lang.String callingPackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volumeUuid);
          _data.writeInt(flags);
          _data.writeString(callingPackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAllocatableBytes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAllocatableBytes(volumeUuid, flags, callingPackage);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void allocateBytes(java.lang.String volumeUuid, long bytes, int flags, java.lang.String callingPackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volumeUuid);
          _data.writeLong(bytes);
          _data.writeInt(flags);
          _data.writeString(callingPackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_allocateBytes, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().allocateBytes(volumeUuid, bytes, flags, callingPackage);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void runIdleMaintenance() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_runIdleMaintenance, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().runIdleMaintenance();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void abortIdleMaintenance() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_abortIdleMaintenance, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().abortIdleMaintenance();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void commitChanges() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_commitChanges, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().commitChanges();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean supportsCheckpoint() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_supportsCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().supportsCheckpoint();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void startCheckpoint(int numTries) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(numTries);
          boolean _status = mRemote.transact(Stub.TRANSACTION_startCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().startCheckpoint(numTries);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean needsCheckpoint() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_needsCheckpoint, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().needsCheckpoint();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void abortChanges(java.lang.String message, boolean retry) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(message);
          _data.writeInt(((retry)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_abortChanges, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().abortChanges(message, retry);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void clearUserKeyAuth(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(serialNumber);
          _data.writeByteArray(token);
          _data.writeByteArray(secret);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clearUserKeyAuth, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().clearUserKeyAuth(userId, serialNumber, token, secret);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.os.storage.IStorageManager sDefaultImpl;
    }
    static final int TRANSACTION_registerListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_unregisterListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_shutdown = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    static final int TRANSACTION_mountObb = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
    static final int TRANSACTION_unmountObb = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
    static final int TRANSACTION_isObbMounted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
    static final int TRANSACTION_getMountedObbPath = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
    static final int TRANSACTION_decryptStorage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
    static final int TRANSACTION_encryptStorage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
    static final int TRANSACTION_changeEncryptionPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
    static final int TRANSACTION_getVolumeList = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
    static final int TRANSACTION_getEncryptionState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
    static final int TRANSACTION_verifyEncryptionPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
    static final int TRANSACTION_mkdirs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
    static final int TRANSACTION_getPasswordType = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
    static final int TRANSACTION_getPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
    static final int TRANSACTION_clearPassword = (android.os.IBinder.FIRST_CALL_TRANSACTION + 37);
    static final int TRANSACTION_setField = (android.os.IBinder.FIRST_CALL_TRANSACTION + 38);
    static final int TRANSACTION_getField = (android.os.IBinder.FIRST_CALL_TRANSACTION + 39);
    static final int TRANSACTION_lastMaintenance = (android.os.IBinder.FIRST_CALL_TRANSACTION + 41);
    static final int TRANSACTION_runMaintenance = (android.os.IBinder.FIRST_CALL_TRANSACTION + 42);
    static final int TRANSACTION_getDisks = (android.os.IBinder.FIRST_CALL_TRANSACTION + 44);
    static final int TRANSACTION_getVolumes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 45);
    static final int TRANSACTION_getVolumeRecords = (android.os.IBinder.FIRST_CALL_TRANSACTION + 46);
    static final int TRANSACTION_mount = (android.os.IBinder.FIRST_CALL_TRANSACTION + 47);
    static final int TRANSACTION_unmount = (android.os.IBinder.FIRST_CALL_TRANSACTION + 48);
    static final int TRANSACTION_format = (android.os.IBinder.FIRST_CALL_TRANSACTION + 49);
    static final int TRANSACTION_partitionPublic = (android.os.IBinder.FIRST_CALL_TRANSACTION + 50);
    static final int TRANSACTION_partitionPrivate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 51);
    static final int TRANSACTION_partitionMixed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 52);
    static final int TRANSACTION_setVolumeNickname = (android.os.IBinder.FIRST_CALL_TRANSACTION + 53);
    static final int TRANSACTION_setVolumeUserFlags = (android.os.IBinder.FIRST_CALL_TRANSACTION + 54);
    static final int TRANSACTION_forgetVolume = (android.os.IBinder.FIRST_CALL_TRANSACTION + 55);
    static final int TRANSACTION_forgetAllVolumes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 56);
    static final int TRANSACTION_getPrimaryStorageUuid = (android.os.IBinder.FIRST_CALL_TRANSACTION + 57);
    static final int TRANSACTION_setPrimaryStorageUuid = (android.os.IBinder.FIRST_CALL_TRANSACTION + 58);
    static final int TRANSACTION_benchmark = (android.os.IBinder.FIRST_CALL_TRANSACTION + 59);
    static final int TRANSACTION_setDebugFlags = (android.os.IBinder.FIRST_CALL_TRANSACTION + 60);
    static final int TRANSACTION_createUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 61);
    static final int TRANSACTION_destroyUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 62);
    static final int TRANSACTION_unlockUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 63);
    static final int TRANSACTION_lockUserKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 64);
    static final int TRANSACTION_isUserKeyUnlocked = (android.os.IBinder.FIRST_CALL_TRANSACTION + 65);
    static final int TRANSACTION_prepareUserStorage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 66);
    static final int TRANSACTION_destroyUserStorage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 67);
    static final int TRANSACTION_isConvertibleToFBE = (android.os.IBinder.FIRST_CALL_TRANSACTION + 68);
    static final int TRANSACTION_addUserKeyAuth = (android.os.IBinder.FIRST_CALL_TRANSACTION + 70);
    static final int TRANSACTION_fixateNewestUserKeyAuth = (android.os.IBinder.FIRST_CALL_TRANSACTION + 71);
    static final int TRANSACTION_fstrim = (android.os.IBinder.FIRST_CALL_TRANSACTION + 72);
    static final int TRANSACTION_mountProxyFileDescriptorBridge = (android.os.IBinder.FIRST_CALL_TRANSACTION + 73);
    static final int TRANSACTION_openProxyFileDescriptor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 74);
    static final int TRANSACTION_getCacheQuotaBytes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 75);
    static final int TRANSACTION_getCacheSizeBytes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 76);
    static final int TRANSACTION_getAllocatableBytes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 77);
    static final int TRANSACTION_allocateBytes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 78);
    static final int TRANSACTION_runIdleMaintenance = (android.os.IBinder.FIRST_CALL_TRANSACTION + 79);
    static final int TRANSACTION_abortIdleMaintenance = (android.os.IBinder.FIRST_CALL_TRANSACTION + 80);
    static final int TRANSACTION_commitChanges = (android.os.IBinder.FIRST_CALL_TRANSACTION + 83);
    static final int TRANSACTION_supportsCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 84);
    static final int TRANSACTION_startCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 85);
    static final int TRANSACTION_needsCheckpoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 86);
    static final int TRANSACTION_abortChanges = (android.os.IBinder.FIRST_CALL_TRANSACTION + 87);
    static final int TRANSACTION_clearUserKeyAuth = (android.os.IBinder.FIRST_CALL_TRANSACTION + 88);
    public static boolean setDefaultImpl(android.os.storage.IStorageManager impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.storage.IStorageManager getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * Registers an IStorageEventListener for receiving async notifications.
       */
  public void registerListener(android.os.storage.IStorageEventListener listener) throws android.os.RemoteException;
  /**
       * Unregisters an IStorageEventListener
       */
  public void unregisterListener(android.os.storage.IStorageEventListener listener) throws android.os.RemoteException;
  /**
       * Shuts down the StorageManagerService and gracefully unmounts all external media.
       * Invokes call back once the shutdown is complete.
       */
  public void shutdown(android.os.storage.IStorageShutdownObserver observer) throws android.os.RemoteException;
  /**
       * Mounts an Opaque Binary Blob (OBB) with the specified decryption key and
       * only allows the calling process's UID access to the contents.
       * StorageManagerService will call back to the supplied IObbActionListener to inform
       * it of the terminal state of the call.
       */
  public void mountObb(java.lang.String rawPath, java.lang.String canonicalPath, java.lang.String key, android.os.storage.IObbActionListener token, int nonce, android.content.res.ObbInfo obbInfo) throws android.os.RemoteException;
  /**
       * Unmounts an Opaque Binary Blob (OBB). When the force flag is specified,
       * any program using it will be forcibly killed to unmount the image.
       * StorageManagerService will call back to the supplied IObbActionListener to inform
       * it of the terminal state of the call.
       */
  public void unmountObb(java.lang.String rawPath, boolean force, android.os.storage.IObbActionListener token, int nonce) throws android.os.RemoteException;
  /**
       * Checks whether the specified Opaque Binary Blob (OBB) is mounted
       * somewhere.
       */
  public boolean isObbMounted(java.lang.String rawPath) throws android.os.RemoteException;
  /**
       * Gets the path to the mounted Opaque Binary Blob (OBB).
       */
  public java.lang.String getMountedObbPath(java.lang.String rawPath) throws android.os.RemoteException;
  /**
       * Decrypts any encrypted volumes.
       */
  public int decryptStorage(java.lang.String password) throws android.os.RemoteException;
  /**
       * Encrypts storage.
       */
  public int encryptStorage(int type, java.lang.String password) throws android.os.RemoteException;
  /**
       * Changes the encryption password.
       */
  public int changeEncryptionPassword(int type, java.lang.String password) throws android.os.RemoteException;
  /**
       * Returns list of all mountable volumes.
       */
  public android.os.storage.StorageVolume[] getVolumeList(int uid, java.lang.String packageName, int flags) throws android.os.RemoteException;
  /**
       * Determines the encryption state of the volume.
       * @return a numerical value. See {@code ENCRYPTION_STATE_*} for possible
       * values.
       * Note that this has been replaced in most cases by the APIs in
       * StorageManager (see isEncryptable and below)
       * This is still useful to get the error state when encryption has failed
       * and CryptKeeper needs to throw up a screen advising the user what to do
       */
  public int getEncryptionState() throws android.os.RemoteException;
  /**
       * Verify the encryption password against the stored volume.  This method
       * may only be called by the system process.
       */
  public int verifyEncryptionPassword(java.lang.String password) throws android.os.RemoteException;
  /**
       * Ensure that all directories along given path exist, creating parent
       * directories as needed. Validates that given path is absolute and that it
       * contains no relative "." or ".." paths or symlinks. Also ensures that
       * path belongs to a volume managed by vold, and that path is either
       * external storage data or OBB directory belonging to calling app.
       */
  public void mkdirs(java.lang.String callingPkg, java.lang.String path) throws android.os.RemoteException;
  /**
       * Determines the type of the encryption password
       * @return PasswordType
       */
  public int getPasswordType() throws android.os.RemoteException;
  /**
       * Get password from vold
       * @return password or empty string
       */
  public java.lang.String getPassword() throws android.os.RemoteException;
  /**
       * Securely clear password from vold
       */
  public void clearPassword() throws android.os.RemoteException;
  /**
       * Set a field in the crypto header.
       * @param field field to set
       * @param contents contents to set in field
       */
  public void setField(java.lang.String field, java.lang.String contents) throws android.os.RemoteException;
  /**
       * Gets a field from the crypto header.
       * @param field field to get
       * @return contents of field
       */
  public java.lang.String getField(java.lang.String field) throws android.os.RemoteException;
  /**
       * Report the time of the last maintenance operation such as fstrim.
       * @return Timestamp of the last maintenance operation, in the
       *     System.currentTimeMillis() time base
       * @throws RemoteException
       */
  public long lastMaintenance() throws android.os.RemoteException;
  /**
       * Kick off an immediate maintenance operation
       * @throws RemoteException
       */
  public void runMaintenance() throws android.os.RemoteException;
  public android.os.storage.DiskInfo[] getDisks() throws android.os.RemoteException;
  public android.os.storage.VolumeInfo[] getVolumes(int flags) throws android.os.RemoteException;
  public android.os.storage.VolumeRecord[] getVolumeRecords(int flags) throws android.os.RemoteException;
  public void mount(java.lang.String volId) throws android.os.RemoteException;
  public void unmount(java.lang.String volId) throws android.os.RemoteException;
  public void format(java.lang.String volId) throws android.os.RemoteException;
  public void partitionPublic(java.lang.String diskId) throws android.os.RemoteException;
  public void partitionPrivate(java.lang.String diskId) throws android.os.RemoteException;
  public void partitionMixed(java.lang.String diskId, int ratio) throws android.os.RemoteException;
  public void setVolumeNickname(java.lang.String fsUuid, java.lang.String nickname) throws android.os.RemoteException;
  public void setVolumeUserFlags(java.lang.String fsUuid, int flags, int mask) throws android.os.RemoteException;
  public void forgetVolume(java.lang.String fsUuid) throws android.os.RemoteException;
  public void forgetAllVolumes() throws android.os.RemoteException;
  public java.lang.String getPrimaryStorageUuid() throws android.os.RemoteException;
  public void setPrimaryStorageUuid(java.lang.String volumeUuid, android.content.pm.IPackageMoveObserver callback) throws android.os.RemoteException;
  public void benchmark(java.lang.String volId, android.os.IVoldTaskListener listener) throws android.os.RemoteException;
  public void setDebugFlags(int flags, int mask) throws android.os.RemoteException;
  public void createUserKey(int userId, int serialNumber, boolean ephemeral) throws android.os.RemoteException;
  public void destroyUserKey(int userId) throws android.os.RemoteException;
  public void unlockUserKey(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException;
  public void lockUserKey(int userId) throws android.os.RemoteException;
  public boolean isUserKeyUnlocked(int userId) throws android.os.RemoteException;
  public void prepareUserStorage(java.lang.String volumeUuid, int userId, int serialNumber, int flags) throws android.os.RemoteException;
  public void destroyUserStorage(java.lang.String volumeUuid, int userId, int flags) throws android.os.RemoteException;
  public boolean isConvertibleToFBE() throws android.os.RemoteException;
  public void addUserKeyAuth(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException;
  public void fixateNewestUserKeyAuth(int userId) throws android.os.RemoteException;
  public void fstrim(int flags, android.os.IVoldTaskListener listener) throws android.os.RemoteException;
  public com.android.internal.os.AppFuseMount mountProxyFileDescriptorBridge() throws android.os.RemoteException;
  public android.os.ParcelFileDescriptor openProxyFileDescriptor(int mountPointId, int fileId, int mode) throws android.os.RemoteException;
  public long getCacheQuotaBytes(java.lang.String volumeUuid, int uid) throws android.os.RemoteException;
  public long getCacheSizeBytes(java.lang.String volumeUuid, int uid) throws android.os.RemoteException;
  public long getAllocatableBytes(java.lang.String volumeUuid, int flags, java.lang.String callingPackage) throws android.os.RemoteException;
  public void allocateBytes(java.lang.String volumeUuid, long bytes, int flags, java.lang.String callingPackage) throws android.os.RemoteException;
  public void runIdleMaintenance() throws android.os.RemoteException;
  public void abortIdleMaintenance() throws android.os.RemoteException;
  public void commitChanges() throws android.os.RemoteException;
  public boolean supportsCheckpoint() throws android.os.RemoteException;
  public void startCheckpoint(int numTries) throws android.os.RemoteException;
  public boolean needsCheckpoint() throws android.os.RemoteException;
  public void abortChanges(java.lang.String message, boolean retry) throws android.os.RemoteException;
  public void clearUserKeyAuth(int userId, int serialNumber, byte[] token, byte[] secret) throws android.os.RemoteException;
}
