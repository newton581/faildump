/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/**
  * Binder interface to report system health incidents.
  * {@hide}
  */
public interface IIncidentManager extends android.os.IInterface
{
  /** Default implementation for IIncidentManager. */
  public static class Default implements android.os.IIncidentManager
  {
    /**
         * Takes a report with the given args, reporting status to the optional listener.
         *
         * When the report is completed, the system report listener will be notified.
         */
    @Override public void reportIncident(android.os.IncidentReportArgs args) throws android.os.RemoteException
    {
    }
    /**
         * Takes a report with the given args, reporting status to the optional listener.
         *
         * When the report is completed, the system report listener will be notified.
         */
    @Override public void reportIncidentToStream(android.os.IncidentReportArgs args, android.os.IIncidentReportStatusListener listener, java.io.FileDescriptor stream) throws android.os.RemoteException
    {
    }
    /**
         * Tell the incident daemon that the android system server is up and running.
         */
    @Override public void systemRunning() throws android.os.RemoteException
    {
    }
    /**
         * List the incident reports for the given ComponentName.  This is called
         * via IncidentCompanion, which validates that the package name matches
         * the caller.
         */
    @Override public java.util.List<java.lang.String> getIncidentReportList(java.lang.String pkg, java.lang.String cls) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Get the IncidentReport object.
         */
    @Override public android.os.IncidentManager.IncidentReport getIncidentReport(java.lang.String pkg, java.lang.String cls, java.lang.String id) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Reduce the refcount on this receiver. This is called
         * via IncidentCompanion, which validates that the package name matches
         * the caller.
         */
    @Override public void deleteIncidentReports(java.lang.String pkg, java.lang.String cls, java.lang.String id) throws android.os.RemoteException
    {
    }
    /**
         * Delete all incident reports for this package.
         */
    @Override public void deleteAllIncidentReports(java.lang.String pkg) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IIncidentManager
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IIncidentManager";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IIncidentManager interface,
     * generating a proxy if needed.
     */
    public static android.os.IIncidentManager asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IIncidentManager))) {
        return ((android.os.IIncidentManager)iin);
      }
      return new android.os.IIncidentManager.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_reportIncident:
        {
          return "reportIncident";
        }
        case TRANSACTION_reportIncidentToStream:
        {
          return "reportIncidentToStream";
        }
        case TRANSACTION_systemRunning:
        {
          return "systemRunning";
        }
        case TRANSACTION_getIncidentReportList:
        {
          return "getIncidentReportList";
        }
        case TRANSACTION_getIncidentReport:
        {
          return "getIncidentReport";
        }
        case TRANSACTION_deleteIncidentReports:
        {
          return "deleteIncidentReports";
        }
        case TRANSACTION_deleteAllIncidentReports:
        {
          return "deleteAllIncidentReports";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_reportIncident:
        {
          data.enforceInterface(descriptor);
          android.os.IncidentReportArgs _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.IncidentReportArgs.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.reportIncident(_arg0);
          return true;
        }
        case TRANSACTION_reportIncidentToStream:
        {
          data.enforceInterface(descriptor);
          android.os.IncidentReportArgs _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.IncidentReportArgs.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.IIncidentReportStatusListener _arg1;
          _arg1 = android.os.IIncidentReportStatusListener.Stub.asInterface(data.readStrongBinder());
          java.io.FileDescriptor _arg2;
          _arg2 = data.readRawFileDescriptor();
          this.reportIncidentToStream(_arg0, _arg1, _arg2);
          return true;
        }
        case TRANSACTION_systemRunning:
        {
          data.enforceInterface(descriptor);
          this.systemRunning();
          return true;
        }
        case TRANSACTION_getIncidentReportList:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.util.List<java.lang.String> _result = this.getIncidentReportList(_arg0, _arg1);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_getIncidentReport:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          android.os.IncidentManager.IncidentReport _result = this.getIncidentReport(_arg0, _arg1, _arg2);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_deleteIncidentReports:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          this.deleteIncidentReports(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_deleteAllIncidentReports:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.deleteAllIncidentReports(_arg0);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IIncidentManager
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Takes a report with the given args, reporting status to the optional listener.
           *
           * When the report is completed, the system report listener will be notified.
           */
      @Override public void reportIncident(android.os.IncidentReportArgs args) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((args!=null)) {
            _data.writeInt(1);
            args.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportIncident, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportIncident(args);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Takes a report with the given args, reporting status to the optional listener.
           *
           * When the report is completed, the system report listener will be notified.
           */
      @Override public void reportIncidentToStream(android.os.IncidentReportArgs args, android.os.IIncidentReportStatusListener listener, java.io.FileDescriptor stream) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((args!=null)) {
            _data.writeInt(1);
            args.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          _data.writeRawFileDescriptor(stream);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportIncidentToStream, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().reportIncidentToStream(args, listener, stream);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Tell the incident daemon that the android system server is up and running.
           */
      @Override public void systemRunning() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_systemRunning, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().systemRunning();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * List the incident reports for the given ComponentName.  This is called
           * via IncidentCompanion, which validates that the package name matches
           * the caller.
           */
      @Override public java.util.List<java.lang.String> getIncidentReportList(java.lang.String pkg, java.lang.String cls) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkg);
          _data.writeString(cls);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getIncidentReportList, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getIncidentReportList(pkg, cls);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Get the IncidentReport object.
           */
      @Override public android.os.IncidentManager.IncidentReport getIncidentReport(java.lang.String pkg, java.lang.String cls, java.lang.String id) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.IncidentManager.IncidentReport _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkg);
          _data.writeString(cls);
          _data.writeString(id);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getIncidentReport, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getIncidentReport(pkg, cls, id);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.IncidentManager.IncidentReport.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Reduce the refcount on this receiver. This is called
           * via IncidentCompanion, which validates that the package name matches
           * the caller.
           */
      @Override public void deleteIncidentReports(java.lang.String pkg, java.lang.String cls, java.lang.String id) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkg);
          _data.writeString(cls);
          _data.writeString(id);
          boolean _status = mRemote.transact(Stub.TRANSACTION_deleteIncidentReports, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().deleteIncidentReports(pkg, cls, id);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Delete all incident reports for this package.
           */
      @Override public void deleteAllIncidentReports(java.lang.String pkg) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(pkg);
          boolean _status = mRemote.transact(Stub.TRANSACTION_deleteAllIncidentReports, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().deleteAllIncidentReports(pkg);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.os.IIncidentManager sDefaultImpl;
    }
    static final int TRANSACTION_reportIncident = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_reportIncidentToStream = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_systemRunning = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_getIncidentReportList = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_getIncidentReport = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_deleteIncidentReports = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_deleteAllIncidentReports = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    public static boolean setDefaultImpl(android.os.IIncidentManager impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IIncidentManager getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * Takes a report with the given args, reporting status to the optional listener.
       *
       * When the report is completed, the system report listener will be notified.
       */
  public void reportIncident(android.os.IncidentReportArgs args) throws android.os.RemoteException;
  /**
       * Takes a report with the given args, reporting status to the optional listener.
       *
       * When the report is completed, the system report listener will be notified.
       */
  public void reportIncidentToStream(android.os.IncidentReportArgs args, android.os.IIncidentReportStatusListener listener, java.io.FileDescriptor stream) throws android.os.RemoteException;
  /**
       * Tell the incident daemon that the android system server is up and running.
       */
  public void systemRunning() throws android.os.RemoteException;
  /**
       * List the incident reports for the given ComponentName.  This is called
       * via IncidentCompanion, which validates that the package name matches
       * the caller.
       */
  public java.util.List<java.lang.String> getIncidentReportList(java.lang.String pkg, java.lang.String cls) throws android.os.RemoteException;
  /**
       * Get the IncidentReport object.
       */
  public android.os.IncidentManager.IncidentReport getIncidentReport(java.lang.String pkg, java.lang.String cls, java.lang.String id) throws android.os.RemoteException;
  /**
       * Reduce the refcount on this receiver. This is called
       * via IncidentCompanion, which validates that the package name matches
       * the caller.
       */
  public void deleteIncidentReports(java.lang.String pkg, java.lang.String cls, java.lang.String id) throws android.os.RemoteException;
  /**
       * Delete all incident reports for this package.
       */
  public void deleteAllIncidentReports(java.lang.String pkg) throws android.os.RemoteException;
}
