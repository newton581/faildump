/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.hardware.hdmi;
/**
 * Callback interface definition for HDMI client to get informed of
 * the CEC availability change event.
 *
 * @hide
 */
public interface IHdmiControlStatusChangeListener extends android.os.IInterface
{
  /** Default implementation for IHdmiControlStatusChangeListener. */
  public static class Default implements android.hardware.hdmi.IHdmiControlStatusChangeListener
  {
    /**
         * Called when HDMI Control (CEC) is enabled/disabled.
         *
         * @param isCecEnabled status of HDMI Control
         * {@link android.provider.Settings.Global#HDMI_CONTROL_ENABLED}: {@code true} if enabled.
         * @param isCecAvailable status of CEC support of the connected display (the TV).
         * {@code true} if supported.
         *
         * Note: Value of isCecAvailable is only valid when isCecEnabled is true.
         **/
    @Override public void onStatusChange(boolean isCecEnabled, boolean isCecAvailable) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.hardware.hdmi.IHdmiControlStatusChangeListener
  {
    private static final java.lang.String DESCRIPTOR = "android.hardware.hdmi.IHdmiControlStatusChangeListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.hardware.hdmi.IHdmiControlStatusChangeListener interface,
     * generating a proxy if needed.
     */
    public static android.hardware.hdmi.IHdmiControlStatusChangeListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.hardware.hdmi.IHdmiControlStatusChangeListener))) {
        return ((android.hardware.hdmi.IHdmiControlStatusChangeListener)iin);
      }
      return new android.hardware.hdmi.IHdmiControlStatusChangeListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onStatusChange:
        {
          return "onStatusChange";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onStatusChange:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.onStatusChange(_arg0, _arg1);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.hardware.hdmi.IHdmiControlStatusChangeListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Called when HDMI Control (CEC) is enabled/disabled.
           *
           * @param isCecEnabled status of HDMI Control
           * {@link android.provider.Settings.Global#HDMI_CONTROL_ENABLED}: {@code true} if enabled.
           * @param isCecAvailable status of CEC support of the connected display (the TV).
           * {@code true} if supported.
           *
           * Note: Value of isCecAvailable is only valid when isCecEnabled is true.
           **/
      @Override public void onStatusChange(boolean isCecEnabled, boolean isCecAvailable) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((isCecEnabled)?(1):(0)));
          _data.writeInt(((isCecAvailable)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_onStatusChange, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onStatusChange(isCecEnabled, isCecAvailable);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.hardware.hdmi.IHdmiControlStatusChangeListener sDefaultImpl;
    }
    static final int TRANSACTION_onStatusChange = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    public static boolean setDefaultImpl(android.hardware.hdmi.IHdmiControlStatusChangeListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.hardware.hdmi.IHdmiControlStatusChangeListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * Called when HDMI Control (CEC) is enabled/disabled.
       *
       * @param isCecEnabled status of HDMI Control
       * {@link android.provider.Settings.Global#HDMI_CONTROL_ENABLED}: {@code true} if enabled.
       * @param isCecAvailable status of CEC support of the connected display (the TV).
       * {@code true} if supported.
       *
       * Note: Value of isCecAvailable is only valid when isCecEnabled is true.
       **/
  public void onStatusChange(boolean isCecEnabled, boolean isCecAvailable) throws android.os.RemoteException;
}
