/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/** @hide */
public interface IUpdateEngineCallback extends android.os.IInterface
{
  /** Default implementation for IUpdateEngineCallback. */
  public static class Default implements android.os.IUpdateEngineCallback
  {
    /** @hide */
    @Override public void onStatusUpdate(int status_code, float percentage) throws android.os.RemoteException
    {
    }
    /** @hide */
    @Override public void onPayloadApplicationComplete(int error_code) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IUpdateEngineCallback
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IUpdateEngineCallback";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IUpdateEngineCallback interface,
     * generating a proxy if needed.
     */
    public static android.os.IUpdateEngineCallback asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IUpdateEngineCallback))) {
        return ((android.os.IUpdateEngineCallback)iin);
      }
      return new android.os.IUpdateEngineCallback.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onStatusUpdate:
        {
          return "onStatusUpdate";
        }
        case TRANSACTION_onPayloadApplicationComplete:
        {
          return "onPayloadApplicationComplete";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onStatusUpdate:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          float _arg1;
          _arg1 = data.readFloat();
          this.onStatusUpdate(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onPayloadApplicationComplete:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onPayloadApplicationComplete(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IUpdateEngineCallback
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /** @hide */
      @Override public void onStatusUpdate(int status_code, float percentage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(status_code);
          _data.writeFloat(percentage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onStatusUpdate, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onStatusUpdate(status_code, percentage);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /** @hide */
      @Override public void onPayloadApplicationComplete(int error_code) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(error_code);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onPayloadApplicationComplete, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onPayloadApplicationComplete(error_code);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.os.IUpdateEngineCallback sDefaultImpl;
    }
    static final int TRANSACTION_onStatusUpdate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onPayloadApplicationComplete = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(android.os.IUpdateEngineCallback impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IUpdateEngineCallback getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /** @hide */
  public void onStatusUpdate(int status_code, float percentage) throws android.os.RemoteException;
  /** @hide */
  public void onPayloadApplicationComplete(int error_code) throws android.os.RemoteException;
}
