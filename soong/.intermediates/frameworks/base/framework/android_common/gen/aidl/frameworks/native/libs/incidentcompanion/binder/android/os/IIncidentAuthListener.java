/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/**
 * Callback for IIncidentCompanion.
 *
 * @hide
 */
public interface IIncidentAuthListener extends android.os.IInterface
{
  /** Default implementation for IIncidentAuthListener. */
  public static class Default implements android.os.IIncidentAuthListener
  {
    /**
         * The user approved the incident or bug report to be sent.
         */
    @Override public void onReportApproved() throws android.os.RemoteException
    {
    }
    /**
         * The user did not approve the incident or bug report to be sent.
         */
    @Override public void onReportDenied() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IIncidentAuthListener
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IIncidentAuthListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IIncidentAuthListener interface,
     * generating a proxy if needed.
     */
    public static android.os.IIncidentAuthListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IIncidentAuthListener))) {
        return ((android.os.IIncidentAuthListener)iin);
      }
      return new android.os.IIncidentAuthListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onReportApproved:
        {
          return "onReportApproved";
        }
        case TRANSACTION_onReportDenied:
        {
          return "onReportDenied";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onReportApproved:
        {
          data.enforceInterface(descriptor);
          this.onReportApproved();
          return true;
        }
        case TRANSACTION_onReportDenied:
        {
          data.enforceInterface(descriptor);
          this.onReportDenied();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IIncidentAuthListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * The user approved the incident or bug report to be sent.
           */
      @Override public void onReportApproved() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onReportApproved, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onReportApproved();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * The user did not approve the incident or bug report to be sent.
           */
      @Override public void onReportDenied() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onReportDenied, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onReportDenied();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.os.IIncidentAuthListener sDefaultImpl;
    }
    static final int TRANSACTION_onReportApproved = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onReportDenied = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(android.os.IIncidentAuthListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IIncidentAuthListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * The user approved the incident or bug report to be sent.
       */
  public void onReportApproved() throws android.os.RemoteException;
  /**
       * The user did not approve the incident or bug report to be sent.
       */
  public void onReportDenied() throws android.os.RemoteException;
}
