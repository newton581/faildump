/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/**
  * Token used by the IDumpstateListener to watch for dumpstate death.
  * {@hide}
  */
public interface IDumpstateToken extends android.os.IInterface
{
  /** Default implementation for IDumpstateToken. */
  public static class Default implements android.os.IDumpstateToken
  {
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IDumpstateToken
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IDumpstateToken";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IDumpstateToken interface,
     * generating a proxy if needed.
     */
    public static android.os.IDumpstateToken asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IDumpstateToken))) {
        return ((android.os.IDumpstateToken)iin);
      }
      return new android.os.IDumpstateToken.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IDumpstateToken
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      public static android.os.IDumpstateToken sDefaultImpl;
    }
    public static boolean setDefaultImpl(android.os.IDumpstateToken impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IDumpstateToken getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
}
