/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.bluetooth;
/**
 * System private API for talking with the Bluetooth service.
 *
 * {@hide}
 */
public interface IBluetooth extends android.os.IInterface
{
  /** Default implementation for IBluetooth. */
  public static class Default implements android.bluetooth.IBluetooth
  {
    @Override public boolean isEnabled() throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getState() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean enable() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean enableNoAutoConnect() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean disable() throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String getAddress() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.ParcelUuid[] getUuids() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setName(java.lang.String name) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String getName() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.bluetooth.BluetoothClass getBluetoothClass() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setBluetoothClass(android.bluetooth.BluetoothClass bluetoothClass) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getIoCapability() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setIoCapability(int capability) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getLeIoCapability() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setLeIoCapability(int capability) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getScanMode() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setScanMode(int mode, int duration) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getDiscoverableTimeout() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setDiscoverableTimeout(int timeout) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean startDiscovery(java.lang.String callingPackage) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean cancelDiscovery() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isDiscovering() throws android.os.RemoteException
    {
      return false;
    }
    @Override public long getDiscoveryEndMillis() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public int getAdapterConnectionState() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int getProfileConnectionState(int profile) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public android.bluetooth.BluetoothDevice[] getBondedDevices() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean createBond(android.bluetooth.BluetoothDevice device, int transport) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean createBondOutOfBand(android.bluetooth.BluetoothDevice device, int transport, android.bluetooth.OobData oobData) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean cancelBondProcess(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean removeBond(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getBondState(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean isBondingInitiatedLocally(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setBondingInitiatedLocally(android.bluetooth.BluetoothDevice devicei, boolean localInitiated) throws android.os.RemoteException
    {
    }
    @Override public long getSupportedProfiles() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public int getConnectionState(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public java.lang.String getRemoteName(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return null;
    }
    @Override public int getRemoteType(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public java.lang.String getRemoteAlias(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setRemoteAlias(android.bluetooth.BluetoothDevice device, java.lang.String name) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getRemoteClass(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public android.os.ParcelUuid[] getRemoteUuids(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean fetchRemoteUuids(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean sdpSearch(android.bluetooth.BluetoothDevice device, android.os.ParcelUuid uuid) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getBatteryLevel(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int getMaxConnectedAudioDevices() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean isTwsPlusDevice(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public java.lang.String getTwsPlusPeerAddress(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean setPin(android.bluetooth.BluetoothDevice device, boolean accept, int len, byte[] pinCode) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setPasskey(android.bluetooth.BluetoothDevice device, boolean accept, int len, byte[] passkey) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setPairingConfirmation(android.bluetooth.BluetoothDevice device, boolean accept) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getPhonebookAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setSilenceMode(android.bluetooth.BluetoothDevice device, boolean silence) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean getSilenceMode(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setPhonebookAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getMessageAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setMessageAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getSimAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setSimAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void sendConnectionStateChange(android.bluetooth.BluetoothDevice device, int profile, int state, int prevState) throws android.os.RemoteException
    {
    }
    @Override public void registerCallback(android.bluetooth.IBluetoothCallback callback) throws android.os.RemoteException
    {
    }
    @Override public void unregisterCallback(android.bluetooth.IBluetoothCallback callback) throws android.os.RemoteException
    {
    }
    // For Socket

    @Override public android.bluetooth.IBluetoothSocketManager getSocketManager() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean factoryReset() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isMultiAdvertisementSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isOffloadedFilteringSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isOffloadedScanBatchingSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isActivityAndEnergyReportingSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isLe2MPhySupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isLeCodedPhySupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isLeExtendedAdvertisingSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isLePeriodicAdvertisingSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getLeMaximumAdvertisingDataLength() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public android.bluetooth.BluetoothActivityEnergyInfo reportActivityInfo() throws android.os.RemoteException
    {
      return null;
    }
    // For Metadata

    @Override public boolean registerMetadataListener(android.bluetooth.IBluetoothMetadataListener listener, android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean unregisterMetadataListener(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean setMetadata(android.bluetooth.BluetoothDevice device, int key, byte[] value) throws android.os.RemoteException
    {
      return false;
    }
    @Override public byte[] getMetadata(android.bluetooth.BluetoothDevice device, int key) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Requests the controller activity info asynchronously.
         * The implementor is expected to reply with the
         * {@link android.bluetooth.BluetoothActivityEnergyInfo} object placed into the Bundle with the
         * key {@link android.os.BatteryStats#RESULT_RECEIVER_CONTROLLER_KEY}.
         * The result code is ignored.
         */
    @Override public void requestActivityInfo(android.os.ResultReceiver result) throws android.os.RemoteException
    {
    }
    @Override public void onLeServiceUp() throws android.os.RemoteException
    {
    }
    @Override public void updateQuietModeStatus(boolean quietMode) throws android.os.RemoteException
    {
    }
    @Override public void onBrEdrDown() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.bluetooth.IBluetooth
  {
    private static final java.lang.String DESCRIPTOR = "android.bluetooth.IBluetooth";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.bluetooth.IBluetooth interface,
     * generating a proxy if needed.
     */
    public static android.bluetooth.IBluetooth asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.bluetooth.IBluetooth))) {
        return ((android.bluetooth.IBluetooth)iin);
      }
      return new android.bluetooth.IBluetooth.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_isEnabled:
        {
          return "isEnabled";
        }
        case TRANSACTION_getState:
        {
          return "getState";
        }
        case TRANSACTION_enable:
        {
          return "enable";
        }
        case TRANSACTION_enableNoAutoConnect:
        {
          return "enableNoAutoConnect";
        }
        case TRANSACTION_disable:
        {
          return "disable";
        }
        case TRANSACTION_getAddress:
        {
          return "getAddress";
        }
        case TRANSACTION_getUuids:
        {
          return "getUuids";
        }
        case TRANSACTION_setName:
        {
          return "setName";
        }
        case TRANSACTION_getName:
        {
          return "getName";
        }
        case TRANSACTION_getBluetoothClass:
        {
          return "getBluetoothClass";
        }
        case TRANSACTION_setBluetoothClass:
        {
          return "setBluetoothClass";
        }
        case TRANSACTION_getIoCapability:
        {
          return "getIoCapability";
        }
        case TRANSACTION_setIoCapability:
        {
          return "setIoCapability";
        }
        case TRANSACTION_getLeIoCapability:
        {
          return "getLeIoCapability";
        }
        case TRANSACTION_setLeIoCapability:
        {
          return "setLeIoCapability";
        }
        case TRANSACTION_getScanMode:
        {
          return "getScanMode";
        }
        case TRANSACTION_setScanMode:
        {
          return "setScanMode";
        }
        case TRANSACTION_getDiscoverableTimeout:
        {
          return "getDiscoverableTimeout";
        }
        case TRANSACTION_setDiscoverableTimeout:
        {
          return "setDiscoverableTimeout";
        }
        case TRANSACTION_startDiscovery:
        {
          return "startDiscovery";
        }
        case TRANSACTION_cancelDiscovery:
        {
          return "cancelDiscovery";
        }
        case TRANSACTION_isDiscovering:
        {
          return "isDiscovering";
        }
        case TRANSACTION_getDiscoveryEndMillis:
        {
          return "getDiscoveryEndMillis";
        }
        case TRANSACTION_getAdapterConnectionState:
        {
          return "getAdapterConnectionState";
        }
        case TRANSACTION_getProfileConnectionState:
        {
          return "getProfileConnectionState";
        }
        case TRANSACTION_getBondedDevices:
        {
          return "getBondedDevices";
        }
        case TRANSACTION_createBond:
        {
          return "createBond";
        }
        case TRANSACTION_createBondOutOfBand:
        {
          return "createBondOutOfBand";
        }
        case TRANSACTION_cancelBondProcess:
        {
          return "cancelBondProcess";
        }
        case TRANSACTION_removeBond:
        {
          return "removeBond";
        }
        case TRANSACTION_getBondState:
        {
          return "getBondState";
        }
        case TRANSACTION_isBondingInitiatedLocally:
        {
          return "isBondingInitiatedLocally";
        }
        case TRANSACTION_setBondingInitiatedLocally:
        {
          return "setBondingInitiatedLocally";
        }
        case TRANSACTION_getSupportedProfiles:
        {
          return "getSupportedProfiles";
        }
        case TRANSACTION_getConnectionState:
        {
          return "getConnectionState";
        }
        case TRANSACTION_getRemoteName:
        {
          return "getRemoteName";
        }
        case TRANSACTION_getRemoteType:
        {
          return "getRemoteType";
        }
        case TRANSACTION_getRemoteAlias:
        {
          return "getRemoteAlias";
        }
        case TRANSACTION_setRemoteAlias:
        {
          return "setRemoteAlias";
        }
        case TRANSACTION_getRemoteClass:
        {
          return "getRemoteClass";
        }
        case TRANSACTION_getRemoteUuids:
        {
          return "getRemoteUuids";
        }
        case TRANSACTION_fetchRemoteUuids:
        {
          return "fetchRemoteUuids";
        }
        case TRANSACTION_sdpSearch:
        {
          return "sdpSearch";
        }
        case TRANSACTION_getBatteryLevel:
        {
          return "getBatteryLevel";
        }
        case TRANSACTION_getMaxConnectedAudioDevices:
        {
          return "getMaxConnectedAudioDevices";
        }
        case TRANSACTION_isTwsPlusDevice:
        {
          return "isTwsPlusDevice";
        }
        case TRANSACTION_getTwsPlusPeerAddress:
        {
          return "getTwsPlusPeerAddress";
        }
        case TRANSACTION_setPin:
        {
          return "setPin";
        }
        case TRANSACTION_setPasskey:
        {
          return "setPasskey";
        }
        case TRANSACTION_setPairingConfirmation:
        {
          return "setPairingConfirmation";
        }
        case TRANSACTION_getPhonebookAccessPermission:
        {
          return "getPhonebookAccessPermission";
        }
        case TRANSACTION_setSilenceMode:
        {
          return "setSilenceMode";
        }
        case TRANSACTION_getSilenceMode:
        {
          return "getSilenceMode";
        }
        case TRANSACTION_setPhonebookAccessPermission:
        {
          return "setPhonebookAccessPermission";
        }
        case TRANSACTION_getMessageAccessPermission:
        {
          return "getMessageAccessPermission";
        }
        case TRANSACTION_setMessageAccessPermission:
        {
          return "setMessageAccessPermission";
        }
        case TRANSACTION_getSimAccessPermission:
        {
          return "getSimAccessPermission";
        }
        case TRANSACTION_setSimAccessPermission:
        {
          return "setSimAccessPermission";
        }
        case TRANSACTION_sendConnectionStateChange:
        {
          return "sendConnectionStateChange";
        }
        case TRANSACTION_registerCallback:
        {
          return "registerCallback";
        }
        case TRANSACTION_unregisterCallback:
        {
          return "unregisterCallback";
        }
        case TRANSACTION_getSocketManager:
        {
          return "getSocketManager";
        }
        case TRANSACTION_factoryReset:
        {
          return "factoryReset";
        }
        case TRANSACTION_isMultiAdvertisementSupported:
        {
          return "isMultiAdvertisementSupported";
        }
        case TRANSACTION_isOffloadedFilteringSupported:
        {
          return "isOffloadedFilteringSupported";
        }
        case TRANSACTION_isOffloadedScanBatchingSupported:
        {
          return "isOffloadedScanBatchingSupported";
        }
        case TRANSACTION_isActivityAndEnergyReportingSupported:
        {
          return "isActivityAndEnergyReportingSupported";
        }
        case TRANSACTION_isLe2MPhySupported:
        {
          return "isLe2MPhySupported";
        }
        case TRANSACTION_isLeCodedPhySupported:
        {
          return "isLeCodedPhySupported";
        }
        case TRANSACTION_isLeExtendedAdvertisingSupported:
        {
          return "isLeExtendedAdvertisingSupported";
        }
        case TRANSACTION_isLePeriodicAdvertisingSupported:
        {
          return "isLePeriodicAdvertisingSupported";
        }
        case TRANSACTION_getLeMaximumAdvertisingDataLength:
        {
          return "getLeMaximumAdvertisingDataLength";
        }
        case TRANSACTION_reportActivityInfo:
        {
          return "reportActivityInfo";
        }
        case TRANSACTION_registerMetadataListener:
        {
          return "registerMetadataListener";
        }
        case TRANSACTION_unregisterMetadataListener:
        {
          return "unregisterMetadataListener";
        }
        case TRANSACTION_setMetadata:
        {
          return "setMetadata";
        }
        case TRANSACTION_getMetadata:
        {
          return "getMetadata";
        }
        case TRANSACTION_requestActivityInfo:
        {
          return "requestActivityInfo";
        }
        case TRANSACTION_onLeServiceUp:
        {
          return "onLeServiceUp";
        }
        case TRANSACTION_updateQuietModeStatus:
        {
          return "updateQuietModeStatus";
        }
        case TRANSACTION_onBrEdrDown:
        {
          return "onBrEdrDown";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_isEnabled:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isEnabled();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getState:
        {
          data.enforceInterface(descriptor);
          int _result = this.getState();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_enable:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.enable();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_enableNoAutoConnect:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.enableNoAutoConnect();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_disable:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.disable();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getAddress:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getAddress();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getUuids:
        {
          data.enforceInterface(descriptor);
          android.os.ParcelUuid[] _result = this.getUuids();
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_setName:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.setName(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getName:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getName();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getBluetoothClass:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothClass _result = this.getBluetoothClass();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setBluetoothClass:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothClass _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothClass.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.setBluetoothClass(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getIoCapability:
        {
          data.enforceInterface(descriptor);
          int _result = this.getIoCapability();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setIoCapability:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.setIoCapability(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getLeIoCapability:
        {
          data.enforceInterface(descriptor);
          int _result = this.getLeIoCapability();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setLeIoCapability:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.setLeIoCapability(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getScanMode:
        {
          data.enforceInterface(descriptor);
          int _result = this.getScanMode();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setScanMode:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.setScanMode(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getDiscoverableTimeout:
        {
          data.enforceInterface(descriptor);
          int _result = this.getDiscoverableTimeout();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setDiscoverableTimeout:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.setDiscoverableTimeout(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_startDiscovery:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.startDiscovery(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_cancelDiscovery:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.cancelDiscovery();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isDiscovering:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isDiscovering();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getDiscoveryEndMillis:
        {
          data.enforceInterface(descriptor);
          long _result = this.getDiscoveryEndMillis();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getAdapterConnectionState:
        {
          data.enforceInterface(descriptor);
          int _result = this.getAdapterConnectionState();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getProfileConnectionState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _result = this.getProfileConnectionState(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getBondedDevices:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice[] _result = this.getBondedDevices();
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_createBond:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.createBond(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_createBondOutOfBand:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          android.bluetooth.OobData _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.bluetooth.OobData.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          boolean _result = this.createBondOutOfBand(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_cancelBondProcess:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.cancelBondProcess(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_removeBond:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.removeBond(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getBondState:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getBondState(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isBondingInitiatedLocally:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isBondingInitiatedLocally(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setBondingInitiatedLocally:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setBondingInitiatedLocally(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getSupportedProfiles:
        {
          data.enforceInterface(descriptor);
          long _result = this.getSupportedProfiles();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getConnectionState:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getConnectionState(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getRemoteName:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getRemoteName(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getRemoteType:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getRemoteType(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getRemoteAlias:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getRemoteAlias(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_setRemoteAlias:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.setRemoteAlias(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getRemoteClass:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getRemoteClass(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getRemoteUuids:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.ParcelUuid[] _result = this.getRemoteUuids(_arg0);
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_fetchRemoteUuids:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.fetchRemoteUuids(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_sdpSearch:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.ParcelUuid _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.ParcelUuid.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          boolean _result = this.sdpSearch(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getBatteryLevel:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getBatteryLevel(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getMaxConnectedAudioDevices:
        {
          data.enforceInterface(descriptor);
          int _result = this.getMaxConnectedAudioDevices();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isTwsPlusDevice:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.isTwsPlusDevice(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getTwsPlusPeerAddress:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          java.lang.String _result = this.getTwsPlusPeerAddress(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_setPin:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          int _arg2;
          _arg2 = data.readInt();
          byte[] _arg3;
          _arg3 = data.createByteArray();
          boolean _result = this.setPin(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setPasskey:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          int _arg2;
          _arg2 = data.readInt();
          byte[] _arg3;
          _arg3 = data.createByteArray();
          boolean _result = this.setPasskey(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setPairingConfirmation:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          boolean _result = this.setPairingConfirmation(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getPhonebookAccessPermission:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getPhonebookAccessPermission(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setSilenceMode:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          boolean _result = this.setSilenceMode(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getSilenceMode:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.getSilenceMode(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setPhonebookAccessPermission:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.setPhonebookAccessPermission(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getMessageAccessPermission:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getMessageAccessPermission(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setMessageAccessPermission:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.setMessageAccessPermission(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getSimAccessPermission:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _result = this.getSimAccessPermission(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_setSimAccessPermission:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          boolean _result = this.setSimAccessPermission(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_sendConnectionStateChange:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          this.sendConnectionStateChange(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_registerCallback:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothCallback _arg0;
          _arg0 = android.bluetooth.IBluetoothCallback.Stub.asInterface(data.readStrongBinder());
          this.registerCallback(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unregisterCallback:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothCallback _arg0;
          _arg0 = android.bluetooth.IBluetoothCallback.Stub.asInterface(data.readStrongBinder());
          this.unregisterCallback(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getSocketManager:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothSocketManager _result = this.getSocketManager();
          reply.writeNoException();
          reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
          return true;
        }
        case TRANSACTION_factoryReset:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.factoryReset();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isMultiAdvertisementSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isMultiAdvertisementSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isOffloadedFilteringSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isOffloadedFilteringSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isOffloadedScanBatchingSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isOffloadedScanBatchingSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isActivityAndEnergyReportingSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isActivityAndEnergyReportingSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isLe2MPhySupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isLe2MPhySupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isLeCodedPhySupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isLeCodedPhySupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isLeExtendedAdvertisingSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isLeExtendedAdvertisingSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isLePeriodicAdvertisingSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isLePeriodicAdvertisingSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getLeMaximumAdvertisingDataLength:
        {
          data.enforceInterface(descriptor);
          int _result = this.getLeMaximumAdvertisingDataLength();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_reportActivityInfo:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothActivityEnergyInfo _result = this.reportActivityInfo();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_registerMetadataListener:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothMetadataListener _arg0;
          _arg0 = android.bluetooth.IBluetoothMetadataListener.Stub.asInterface(data.readStrongBinder());
          android.bluetooth.BluetoothDevice _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          boolean _result = this.registerMetadataListener(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_unregisterMetadataListener:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _result = this.unregisterMetadataListener(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setMetadata:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          boolean _result = this.setMetadata(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getMetadata:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          byte[] _result = this.getMetadata(_arg0, _arg1);
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_requestActivityInfo:
        {
          data.enforceInterface(descriptor);
          android.os.ResultReceiver _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.ResultReceiver.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.requestActivityInfo(_arg0);
          return true;
        }
        case TRANSACTION_onLeServiceUp:
        {
          data.enforceInterface(descriptor);
          this.onLeServiceUp();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_updateQuietModeStatus:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          this.updateQuietModeStatus(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onBrEdrDown:
        {
          data.enforceInterface(descriptor);
          this.onBrEdrDown();
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.bluetooth.IBluetooth
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public boolean isEnabled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isEnabled();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getState() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getState();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean enable() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_enable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().enable();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean enableNoAutoConnect() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_enableNoAutoConnect, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().enableNoAutoConnect();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean disable() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_disable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().disable();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getAddress() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAddress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAddress();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.ParcelUuid[] getUuids() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.ParcelUuid[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getUuids, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getUuids();
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.os.ParcelUuid.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setName(java.lang.String name) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setName(name);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getName() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getName();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.bluetooth.BluetoothClass getBluetoothClass() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.bluetooth.BluetoothClass _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBluetoothClass, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBluetoothClass();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.bluetooth.BluetoothClass.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setBluetoothClass(android.bluetooth.BluetoothClass bluetoothClass) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((bluetoothClass!=null)) {
            _data.writeInt(1);
            bluetoothClass.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_setBluetoothClass, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setBluetoothClass(bluetoothClass);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getIoCapability() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getIoCapability, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getIoCapability();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setIoCapability(int capability) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(capability);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setIoCapability, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setIoCapability(capability);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getLeIoCapability() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLeIoCapability, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLeIoCapability();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setLeIoCapability(int capability) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(capability);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setLeIoCapability, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setLeIoCapability(capability);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getScanMode() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getScanMode, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getScanMode();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setScanMode(int mode, int duration) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(mode);
          _data.writeInt(duration);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setScanMode, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setScanMode(mode, duration);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getDiscoverableTimeout() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDiscoverableTimeout, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDiscoverableTimeout();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setDiscoverableTimeout(int timeout) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(timeout);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setDiscoverableTimeout, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setDiscoverableTimeout(timeout);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean startDiscovery(java.lang.String callingPackage) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(callingPackage);
          boolean _status = mRemote.transact(Stub.TRANSACTION_startDiscovery, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().startDiscovery(callingPackage);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean cancelDiscovery() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_cancelDiscovery, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().cancelDiscovery();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isDiscovering() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isDiscovering, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isDiscovering();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getDiscoveryEndMillis() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getDiscoveryEndMillis, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getDiscoveryEndMillis();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getAdapterConnectionState() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAdapterConnectionState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAdapterConnectionState();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getProfileConnectionState(int profile) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(profile);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getProfileConnectionState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getProfileConnectionState(profile);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.bluetooth.BluetoothDevice[] getBondedDevices() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.bluetooth.BluetoothDevice[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBondedDevices, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBondedDevices();
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.bluetooth.BluetoothDevice.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean createBond(android.bluetooth.BluetoothDevice device, int transport) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(transport);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createBond, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createBond(device, transport);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean createBondOutOfBand(android.bluetooth.BluetoothDevice device, int transport, android.bluetooth.OobData oobData) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(transport);
          if ((oobData!=null)) {
            _data.writeInt(1);
            oobData.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_createBondOutOfBand, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createBondOutOfBand(device, transport, oobData);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean cancelBondProcess(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_cancelBondProcess, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().cancelBondProcess(device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean removeBond(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeBond, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().removeBond(device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getBondState(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBondState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBondState(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isBondingInitiatedLocally(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isBondingInitiatedLocally, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isBondingInitiatedLocally(device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setBondingInitiatedLocally(android.bluetooth.BluetoothDevice devicei, boolean localInitiated) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((devicei!=null)) {
            _data.writeInt(1);
            devicei.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((localInitiated)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setBondingInitiatedLocally, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setBondingInitiatedLocally(devicei, localInitiated);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public long getSupportedProfiles() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSupportedProfiles, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSupportedProfiles();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getConnectionState(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getConnectionState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getConnectionState(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getRemoteName(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRemoteName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRemoteName(device);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getRemoteType(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRemoteType, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRemoteType(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getRemoteAlias(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRemoteAlias, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRemoteAlias(device);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setRemoteAlias(android.bluetooth.BluetoothDevice device, java.lang.String name) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeString(name);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setRemoteAlias, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setRemoteAlias(device, name);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getRemoteClass(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRemoteClass, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRemoteClass(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.ParcelUuid[] getRemoteUuids(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.ParcelUuid[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getRemoteUuids, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getRemoteUuids(device);
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.os.ParcelUuid.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean fetchRemoteUuids(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_fetchRemoteUuids, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().fetchRemoteUuids(device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean sdpSearch(android.bluetooth.BluetoothDevice device, android.os.ParcelUuid uuid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((uuid!=null)) {
            _data.writeInt(1);
            uuid.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_sdpSearch, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().sdpSearch(device, uuid);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getBatteryLevel(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBatteryLevel, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBatteryLevel(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getMaxConnectedAudioDevices() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMaxConnectedAudioDevices, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMaxConnectedAudioDevices();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isTwsPlusDevice(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_isTwsPlusDevice, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isTwsPlusDevice(device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getTwsPlusPeerAddress(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getTwsPlusPeerAddress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getTwsPlusPeerAddress(device);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setPin(android.bluetooth.BluetoothDevice device, boolean accept, int len, byte[] pinCode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((accept)?(1):(0)));
          _data.writeInt(len);
          _data.writeByteArray(pinCode);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPin(device, accept, len, pinCode);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setPasskey(android.bluetooth.BluetoothDevice device, boolean accept, int len, byte[] passkey) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((accept)?(1):(0)));
          _data.writeInt(len);
          _data.writeByteArray(passkey);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPasskey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPasskey(device, accept, len, passkey);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setPairingConfirmation(android.bluetooth.BluetoothDevice device, boolean accept) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((accept)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPairingConfirmation, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPairingConfirmation(device, accept);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getPhonebookAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPhonebookAccessPermission, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPhonebookAccessPermission(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setSilenceMode(android.bluetooth.BluetoothDevice device, boolean silence) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((silence)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setSilenceMode, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setSilenceMode(device, silence);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean getSilenceMode(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSilenceMode, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSilenceMode(device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setPhonebookAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setPhonebookAccessPermission, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setPhonebookAccessPermission(device, value);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getMessageAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMessageAccessPermission, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMessageAccessPermission(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setMessageAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setMessageAccessPermission, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setMessageAccessPermission(device, value);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getSimAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSimAccessPermission, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSimAccessPermission(device);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setSimAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setSimAccessPermission, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setSimAccessPermission(device, value);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void sendConnectionStateChange(android.bluetooth.BluetoothDevice device, int profile, int state, int prevState) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(profile);
          _data.writeInt(state);
          _data.writeInt(prevState);
          boolean _status = mRemote.transact(Stub.TRANSACTION_sendConnectionStateChange, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().sendConnectionStateChange(device, profile, state, prevState);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void registerCallback(android.bluetooth.IBluetoothCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_registerCallback, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().registerCallback(callback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void unregisterCallback(android.bluetooth.IBluetoothCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_unregisterCallback, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unregisterCallback(callback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      // For Socket

      @Override public android.bluetooth.IBluetoothSocketManager getSocketManager() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.bluetooth.IBluetoothSocketManager _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSocketManager, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSocketManager();
          }
          _reply.readException();
          _result = android.bluetooth.IBluetoothSocketManager.Stub.asInterface(_reply.readStrongBinder());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean factoryReset() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_factoryReset, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().factoryReset();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isMultiAdvertisementSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isMultiAdvertisementSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isMultiAdvertisementSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isOffloadedFilteringSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isOffloadedFilteringSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isOffloadedFilteringSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isOffloadedScanBatchingSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isOffloadedScanBatchingSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isOffloadedScanBatchingSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isActivityAndEnergyReportingSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isActivityAndEnergyReportingSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isActivityAndEnergyReportingSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isLe2MPhySupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isLe2MPhySupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isLe2MPhySupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isLeCodedPhySupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isLeCodedPhySupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isLeCodedPhySupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isLeExtendedAdvertisingSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isLeExtendedAdvertisingSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isLeExtendedAdvertisingSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isLePeriodicAdvertisingSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isLePeriodicAdvertisingSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isLePeriodicAdvertisingSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getLeMaximumAdvertisingDataLength() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLeMaximumAdvertisingDataLength, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLeMaximumAdvertisingDataLength();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.bluetooth.BluetoothActivityEnergyInfo reportActivityInfo() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.bluetooth.BluetoothActivityEnergyInfo _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reportActivityInfo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().reportActivityInfo();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.bluetooth.BluetoothActivityEnergyInfo.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // For Metadata

      @Override public boolean registerMetadataListener(android.bluetooth.IBluetoothMetadataListener listener, android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_registerMetadataListener, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().registerMetadataListener(listener, device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean unregisterMetadataListener(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_unregisterMetadataListener, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().unregisterMetadataListener(device);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setMetadata(android.bluetooth.BluetoothDevice device, int key, byte[] value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(key);
          _data.writeByteArray(value);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setMetadata, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setMetadata(device, key, value);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public byte[] getMetadata(android.bluetooth.BluetoothDevice device, int key) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(key);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMetadata, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMetadata(device, key);
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Requests the controller activity info asynchronously.
           * The implementor is expected to reply with the
           * {@link android.bluetooth.BluetoothActivityEnergyInfo} object placed into the Bundle with the
           * key {@link android.os.BatteryStats#RESULT_RECEIVER_CONTROLLER_KEY}.
           * The result code is ignored.
           */
      @Override public void requestActivityInfo(android.os.ResultReceiver result) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((result!=null)) {
            _data.writeInt(1);
            result.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_requestActivityInfo, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().requestActivityInfo(result);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onLeServiceUp() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onLeServiceUp, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onLeServiceUp();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void updateQuietModeStatus(boolean quietMode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((quietMode)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_updateQuietModeStatus, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().updateQuietModeStatus(quietMode);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void onBrEdrDown() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onBrEdrDown, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onBrEdrDown();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.bluetooth.IBluetooth sDefaultImpl;
    }
    static final int TRANSACTION_isEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_enable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_enableNoAutoConnect = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_disable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_getAddress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_getUuids = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_setName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_getName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_getBluetoothClass = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_setBluetoothClass = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_getIoCapability = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_setIoCapability = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_getLeIoCapability = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_setLeIoCapability = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_getScanMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_setScanMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_getDiscoverableTimeout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_setDiscoverableTimeout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    static final int TRANSACTION_startDiscovery = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    static final int TRANSACTION_cancelDiscovery = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
    static final int TRANSACTION_isDiscovering = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
    static final int TRANSACTION_getDiscoveryEndMillis = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
    static final int TRANSACTION_getAdapterConnectionState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
    static final int TRANSACTION_getProfileConnectionState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
    static final int TRANSACTION_getBondedDevices = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
    static final int TRANSACTION_createBond = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
    static final int TRANSACTION_createBondOutOfBand = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
    static final int TRANSACTION_cancelBondProcess = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
    static final int TRANSACTION_removeBond = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
    static final int TRANSACTION_getBondState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
    static final int TRANSACTION_isBondingInitiatedLocally = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
    static final int TRANSACTION_setBondingInitiatedLocally = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
    static final int TRANSACTION_getSupportedProfiles = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
    static final int TRANSACTION_getConnectionState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
    static final int TRANSACTION_getRemoteName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
    static final int TRANSACTION_getRemoteType = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
    static final int TRANSACTION_getRemoteAlias = (android.os.IBinder.FIRST_CALL_TRANSACTION + 37);
    static final int TRANSACTION_setRemoteAlias = (android.os.IBinder.FIRST_CALL_TRANSACTION + 38);
    static final int TRANSACTION_getRemoteClass = (android.os.IBinder.FIRST_CALL_TRANSACTION + 39);
    static final int TRANSACTION_getRemoteUuids = (android.os.IBinder.FIRST_CALL_TRANSACTION + 40);
    static final int TRANSACTION_fetchRemoteUuids = (android.os.IBinder.FIRST_CALL_TRANSACTION + 41);
    static final int TRANSACTION_sdpSearch = (android.os.IBinder.FIRST_CALL_TRANSACTION + 42);
    static final int TRANSACTION_getBatteryLevel = (android.os.IBinder.FIRST_CALL_TRANSACTION + 43);
    static final int TRANSACTION_getMaxConnectedAudioDevices = (android.os.IBinder.FIRST_CALL_TRANSACTION + 44);
    static final int TRANSACTION_isTwsPlusDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 45);
    static final int TRANSACTION_getTwsPlusPeerAddress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 46);
    static final int TRANSACTION_setPin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 47);
    static final int TRANSACTION_setPasskey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 48);
    static final int TRANSACTION_setPairingConfirmation = (android.os.IBinder.FIRST_CALL_TRANSACTION + 49);
    static final int TRANSACTION_getPhonebookAccessPermission = (android.os.IBinder.FIRST_CALL_TRANSACTION + 50);
    static final int TRANSACTION_setSilenceMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 51);
    static final int TRANSACTION_getSilenceMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 52);
    static final int TRANSACTION_setPhonebookAccessPermission = (android.os.IBinder.FIRST_CALL_TRANSACTION + 53);
    static final int TRANSACTION_getMessageAccessPermission = (android.os.IBinder.FIRST_CALL_TRANSACTION + 54);
    static final int TRANSACTION_setMessageAccessPermission = (android.os.IBinder.FIRST_CALL_TRANSACTION + 55);
    static final int TRANSACTION_getSimAccessPermission = (android.os.IBinder.FIRST_CALL_TRANSACTION + 56);
    static final int TRANSACTION_setSimAccessPermission = (android.os.IBinder.FIRST_CALL_TRANSACTION + 57);
    static final int TRANSACTION_sendConnectionStateChange = (android.os.IBinder.FIRST_CALL_TRANSACTION + 58);
    static final int TRANSACTION_registerCallback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 59);
    static final int TRANSACTION_unregisterCallback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 60);
    static final int TRANSACTION_getSocketManager = (android.os.IBinder.FIRST_CALL_TRANSACTION + 61);
    static final int TRANSACTION_factoryReset = (android.os.IBinder.FIRST_CALL_TRANSACTION + 62);
    static final int TRANSACTION_isMultiAdvertisementSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 63);
    static final int TRANSACTION_isOffloadedFilteringSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 64);
    static final int TRANSACTION_isOffloadedScanBatchingSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 65);
    static final int TRANSACTION_isActivityAndEnergyReportingSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 66);
    static final int TRANSACTION_isLe2MPhySupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 67);
    static final int TRANSACTION_isLeCodedPhySupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 68);
    static final int TRANSACTION_isLeExtendedAdvertisingSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 69);
    static final int TRANSACTION_isLePeriodicAdvertisingSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 70);
    static final int TRANSACTION_getLeMaximumAdvertisingDataLength = (android.os.IBinder.FIRST_CALL_TRANSACTION + 71);
    static final int TRANSACTION_reportActivityInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 72);
    static final int TRANSACTION_registerMetadataListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 73);
    static final int TRANSACTION_unregisterMetadataListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 74);
    static final int TRANSACTION_setMetadata = (android.os.IBinder.FIRST_CALL_TRANSACTION + 75);
    static final int TRANSACTION_getMetadata = (android.os.IBinder.FIRST_CALL_TRANSACTION + 76);
    static final int TRANSACTION_requestActivityInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 77);
    static final int TRANSACTION_onLeServiceUp = (android.os.IBinder.FIRST_CALL_TRANSACTION + 78);
    static final int TRANSACTION_updateQuietModeStatus = (android.os.IBinder.FIRST_CALL_TRANSACTION + 79);
    static final int TRANSACTION_onBrEdrDown = (android.os.IBinder.FIRST_CALL_TRANSACTION + 80);
    public static boolean setDefaultImpl(android.bluetooth.IBluetooth impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.bluetooth.IBluetooth getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public boolean isEnabled() throws android.os.RemoteException;
  public int getState() throws android.os.RemoteException;
  public boolean enable() throws android.os.RemoteException;
  public boolean enableNoAutoConnect() throws android.os.RemoteException;
  public boolean disable() throws android.os.RemoteException;
  public java.lang.String getAddress() throws android.os.RemoteException;
  public android.os.ParcelUuid[] getUuids() throws android.os.RemoteException;
  public boolean setName(java.lang.String name) throws android.os.RemoteException;
  public java.lang.String getName() throws android.os.RemoteException;
  public android.bluetooth.BluetoothClass getBluetoothClass() throws android.os.RemoteException;
  public boolean setBluetoothClass(android.bluetooth.BluetoothClass bluetoothClass) throws android.os.RemoteException;
  public int getIoCapability() throws android.os.RemoteException;
  public boolean setIoCapability(int capability) throws android.os.RemoteException;
  public int getLeIoCapability() throws android.os.RemoteException;
  public boolean setLeIoCapability(int capability) throws android.os.RemoteException;
  public int getScanMode() throws android.os.RemoteException;
  public boolean setScanMode(int mode, int duration) throws android.os.RemoteException;
  public int getDiscoverableTimeout() throws android.os.RemoteException;
  public boolean setDiscoverableTimeout(int timeout) throws android.os.RemoteException;
  public boolean startDiscovery(java.lang.String callingPackage) throws android.os.RemoteException;
  public boolean cancelDiscovery() throws android.os.RemoteException;
  public boolean isDiscovering() throws android.os.RemoteException;
  public long getDiscoveryEndMillis() throws android.os.RemoteException;
  public int getAdapterConnectionState() throws android.os.RemoteException;
  public int getProfileConnectionState(int profile) throws android.os.RemoteException;
  public android.bluetooth.BluetoothDevice[] getBondedDevices() throws android.os.RemoteException;
  public boolean createBond(android.bluetooth.BluetoothDevice device, int transport) throws android.os.RemoteException;
  public boolean createBondOutOfBand(android.bluetooth.BluetoothDevice device, int transport, android.bluetooth.OobData oobData) throws android.os.RemoteException;
  public boolean cancelBondProcess(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean removeBond(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public int getBondState(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean isBondingInitiatedLocally(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public void setBondingInitiatedLocally(android.bluetooth.BluetoothDevice devicei, boolean localInitiated) throws android.os.RemoteException;
  public long getSupportedProfiles() throws android.os.RemoteException;
  public int getConnectionState(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public java.lang.String getRemoteName(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public int getRemoteType(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public java.lang.String getRemoteAlias(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean setRemoteAlias(android.bluetooth.BluetoothDevice device, java.lang.String name) throws android.os.RemoteException;
  public int getRemoteClass(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public android.os.ParcelUuid[] getRemoteUuids(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean fetchRemoteUuids(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean sdpSearch(android.bluetooth.BluetoothDevice device, android.os.ParcelUuid uuid) throws android.os.RemoteException;
  public int getBatteryLevel(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public int getMaxConnectedAudioDevices() throws android.os.RemoteException;
  public boolean isTwsPlusDevice(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public java.lang.String getTwsPlusPeerAddress(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean setPin(android.bluetooth.BluetoothDevice device, boolean accept, int len, byte[] pinCode) throws android.os.RemoteException;
  public boolean setPasskey(android.bluetooth.BluetoothDevice device, boolean accept, int len, byte[] passkey) throws android.os.RemoteException;
  public boolean setPairingConfirmation(android.bluetooth.BluetoothDevice device, boolean accept) throws android.os.RemoteException;
  public int getPhonebookAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean setSilenceMode(android.bluetooth.BluetoothDevice device, boolean silence) throws android.os.RemoteException;
  public boolean getSilenceMode(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean setPhonebookAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException;
  public int getMessageAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean setMessageAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException;
  public int getSimAccessPermission(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean setSimAccessPermission(android.bluetooth.BluetoothDevice device, int value) throws android.os.RemoteException;
  public void sendConnectionStateChange(android.bluetooth.BluetoothDevice device, int profile, int state, int prevState) throws android.os.RemoteException;
  public void registerCallback(android.bluetooth.IBluetoothCallback callback) throws android.os.RemoteException;
  public void unregisterCallback(android.bluetooth.IBluetoothCallback callback) throws android.os.RemoteException;
  // For Socket

  public android.bluetooth.IBluetoothSocketManager getSocketManager() throws android.os.RemoteException;
  public boolean factoryReset() throws android.os.RemoteException;
  public boolean isMultiAdvertisementSupported() throws android.os.RemoteException;
  public boolean isOffloadedFilteringSupported() throws android.os.RemoteException;
  public boolean isOffloadedScanBatchingSupported() throws android.os.RemoteException;
  public boolean isActivityAndEnergyReportingSupported() throws android.os.RemoteException;
  public boolean isLe2MPhySupported() throws android.os.RemoteException;
  public boolean isLeCodedPhySupported() throws android.os.RemoteException;
  public boolean isLeExtendedAdvertisingSupported() throws android.os.RemoteException;
  public boolean isLePeriodicAdvertisingSupported() throws android.os.RemoteException;
  public int getLeMaximumAdvertisingDataLength() throws android.os.RemoteException;
  public android.bluetooth.BluetoothActivityEnergyInfo reportActivityInfo() throws android.os.RemoteException;
  // For Metadata

  public boolean registerMetadataListener(android.bluetooth.IBluetoothMetadataListener listener, android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean unregisterMetadataListener(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
  public boolean setMetadata(android.bluetooth.BluetoothDevice device, int key, byte[] value) throws android.os.RemoteException;
  public byte[] getMetadata(android.bluetooth.BluetoothDevice device, int key) throws android.os.RemoteException;
  /**
       * Requests the controller activity info asynchronously.
       * The implementor is expected to reply with the
       * {@link android.bluetooth.BluetoothActivityEnergyInfo} object placed into the Bundle with the
       * key {@link android.os.BatteryStats#RESULT_RECEIVER_CONTROLLER_KEY}.
       * The result code is ignored.
       */
  public void requestActivityInfo(android.os.ResultReceiver result) throws android.os.RemoteException;
  public void onLeServiceUp() throws android.os.RemoteException;
  public void updateQuietModeStatus(boolean quietMode) throws android.os.RemoteException;
  public void onBrEdrDown() throws android.os.RemoteException;
}
