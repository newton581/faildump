/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/**
  * Listener for dumpstate events.
  *
  * <p>When bugreport creation is complete one of {@code onError} or {@code onFinished} is called.
  *
  * <p>These methods are synchronous by design in order to make dumpstate's lifecycle simpler
  * to handle.
  *
  * {@hide}
  */
public interface IDumpstateListener extends android.os.IInterface
{
  /** Default implementation for IDumpstateListener. */
  public static class Default implements android.os.IDumpstateListener
  {
    /**
         * Called when there is a progress update.
         *
         * @param progress the progress in [0, 100]
         */
    @Override public void onProgress(int progress) throws android.os.RemoteException
    {
    }
    /**
         * Called on an error condition with one of the error codes listed above.
         */
    @Override public void onError(int errorCode) throws android.os.RemoteException
    {
    }
    /**
         * Called when taking bugreport finishes successfully.
         */
    @Override public void onFinished() throws android.os.RemoteException
    {
    }
    // TODO(b/111441001): Remove old methods when not used anymore.

    @Override public void onProgressUpdated(int progress) throws android.os.RemoteException
    {
    }
    @Override public void onMaxProgressUpdated(int maxProgress) throws android.os.RemoteException
    {
    }
    /**
         * Called after every section is complete.
         *
         * @param  name          section name
         * @param  status        values from status_t
         *                       {@code OK} section completed successfully
         *                       {@code TIMEOUT} dump timed out
         *                       {@code != OK} error
         * @param  size          size in bytes, may be invalid if status != OK
         * @param  durationMs    duration in ms
         */
    @Override public void onSectionComplete(java.lang.String name, int status, int size, int durationMs) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IDumpstateListener
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IDumpstateListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IDumpstateListener interface,
     * generating a proxy if needed.
     */
    public static android.os.IDumpstateListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IDumpstateListener))) {
        return ((android.os.IDumpstateListener)iin);
      }
      return new android.os.IDumpstateListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onProgress:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onProgress(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onError:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onError(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onFinished:
        {
          data.enforceInterface(descriptor);
          this.onFinished();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onProgressUpdated:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onProgressUpdated(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onMaxProgressUpdated:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onMaxProgressUpdated(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onSectionComplete:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          this.onSectionComplete(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IDumpstateListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Called when there is a progress update.
           *
           * @param progress the progress in [0, 100]
           */
      @Override public void onProgress(int progress) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(progress);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onProgress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onProgress(progress);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Called on an error condition with one of the error codes listed above.
           */
      @Override public void onError(int errorCode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(errorCode);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onError, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onError(errorCode);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Called when taking bugreport finishes successfully.
           */
      @Override public void onFinished() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onFinished, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onFinished();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      // TODO(b/111441001): Remove old methods when not used anymore.

      @Override public void onProgressUpdated(int progress) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(progress);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onProgressUpdated, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onProgressUpdated(progress);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void onMaxProgressUpdated(int maxProgress) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(maxProgress);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onMaxProgressUpdated, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onMaxProgressUpdated(maxProgress);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * Called after every section is complete.
           *
           * @param  name          section name
           * @param  status        values from status_t
           *                       {@code OK} section completed successfully
           *                       {@code TIMEOUT} dump timed out
           *                       {@code != OK} error
           * @param  size          size in bytes, may be invalid if status != OK
           * @param  durationMs    duration in ms
           */
      @Override public void onSectionComplete(java.lang.String name, int status, int size, int durationMs) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(status);
          _data.writeInt(size);
          _data.writeInt(durationMs);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onSectionComplete, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onSectionComplete(name, status, size, durationMs);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.os.IDumpstateListener sDefaultImpl;
    }
    static final int TRANSACTION_onProgress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onError = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onFinished = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_onProgressUpdated = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_onMaxProgressUpdated = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_onSectionComplete = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    public static boolean setDefaultImpl(android.os.IDumpstateListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IDumpstateListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int BUGREPORT_ERROR_INVALID_INPUT = 1;
  public static final int BUGREPORT_ERROR_RUNTIME_ERROR = 2;
  public static final int BUGREPORT_ERROR_USER_DENIED_CONSENT = 3;
  public static final int BUGREPORT_ERROR_USER_CONSENT_TIMED_OUT = 4;
  public static final int BUGREPORT_ERROR_ANOTHER_REPORT_IN_PROGRESS = 5;
  /**
       * Called when there is a progress update.
       *
       * @param progress the progress in [0, 100]
       */
  public void onProgress(int progress) throws android.os.RemoteException;
  /**
       * Called on an error condition with one of the error codes listed above.
       */
  public void onError(int errorCode) throws android.os.RemoteException;
  /**
       * Called when taking bugreport finishes successfully.
       */
  public void onFinished() throws android.os.RemoteException;
  // TODO(b/111441001): Remove old methods when not used anymore.

  public void onProgressUpdated(int progress) throws android.os.RemoteException;
  public void onMaxProgressUpdated(int maxProgress) throws android.os.RemoteException;
  /**
       * Called after every section is complete.
       *
       * @param  name          section name
       * @param  status        values from status_t
       *                       {@code OK} section completed successfully
       *                       {@code TIMEOUT} dump timed out
       *                       {@code != OK} error
       * @param  size          size in bytes, may be invalid if status != OK
       * @param  durationMs    duration in ms
       */
  public void onSectionComplete(java.lang.String name, int status, int size, int durationMs) throws android.os.RemoteException;
}
