<?xml version="1.0" encoding="utf-8"?>
<!--
    Copyright (C) 2015 The Android Open Source Project

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<manifest android:installLocation="internalOnly" package="com.android.messaging" xmlns:android="http://schemas.android.com/apk/res/android">

    <uses-sdk android:minSdkVersion="29" android:targetSdkVersion="29"/>

    <!-- Application holds CPU wakelock while working in background -->
    <uses-permission android:name="android.permission.WAKE_LOCK"/>
    <!-- Application needs SMS/MMS permissions -->
    <uses-permission android:name="android.permission.READ_SMS"/>
    <uses-permission android:name="android.permission.WRITE_SMS"/>
    <uses-permission android:name="android.permission.RECEIVE_SMS"/>
    <uses-permission android:name="android.permission.RECEIVE_MMS"/>
    <uses-permission android:name="android.permission.SEND_SMS"/>
    <!-- Application needs access to MMS network -->
    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
    <uses-permission android:name="android.permission.CHANGE_NETWORK_STATE"/>
    <!-- Application needs CONTACT permissions -->
    <uses-permission android:name="android.permission.READ_CONTACTS"/>
    <uses-permission android:name="android.permission.WRITE_CONTACTS"/>
    <!-- Application needs to read profiles for the user itself from CP2 -->
    <uses-permission android:name="android.permission.READ_PROFILE"/>
    <uses-permission android:name="android.permission.VIBRATE"/>
    <uses-permission android:name="android.permission.READ_PHONE_STATE"/>

    <uses-permission android:name="android.permission.CAMERA"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.RECORD_AUDIO"/>
    <uses-permission android:name="android.permission.CALL_PHONE"/>
    <uses-permission android:name="android.permission.DOWNLOAD_WITHOUT_NOTIFICATION"/>
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>

    <!--  Optional features -->
    <uses-feature android:name="android.hardware.camera" android:required="false"/>
    <uses-feature android:name="android.hardware.camera.front" android:required="false"/>
    <uses-feature android:name="android.hardware.camera.autofocus" android:required="false"/>
    <uses-feature android:name="android.hardware.microphone" android:required="false"/>
    <uses-feature android:name="android.hardware.screen.portrait" android:required="false"/>

    <application android:allowBackup="false" android:appCategory="social" android:extractNativeLibs="false" android:icon="@mipmap/ic_launcher" android:label="@string/app_name" android:name="com.android.messaging.BugleApplication" android:requestLegacyExternalStorage="true" android:supportsRtl="true" android:theme="@style/BugleTheme">

        <!-- Displays a list of conversations -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/app_name" android:name=".ui.conversationlist.ConversationListActivity" android:screenOrientation="user" android:theme="@style/LaunchTheme">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.LAUNCHER"/>
                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.APP_MESSAGING"/>
            </intent-filter>
        </activity>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:name=".ui.PermissionCheckActivity" android:screenOrientation="portrait"/>

        <!-- Launches a conversation (ensures correct app name shown in recents) -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:documentLaunchMode="always" android:name=".ui.conversation.LaunchConversationActivity" android:noHistory="true" android:screenOrientation="user" android:theme="@style/Invisible">
            <intent-filter>
                <action android:name="android.intent.action.VIEW"/>
                <action android:name="android.intent.action.SENDTO"/>
                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>
                <data android:scheme="sms"/>
                <data android:scheme="smsto"/>
                <data android:scheme="mms"/>
                <data android:scheme="mmsto"/>
            </intent-filter>
        </activity>

        <!-- Displays a list of archived conversations -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/archived_activity_title" android:name=".ui.conversationlist.ArchivedConversationListActivity" android:parentActivityName="com.android.messaging.ui.conversationlist.ConversationListActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.ArchivedConversationListActivity">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.conversationlist.ConversationListActivity"/>
        </activity>

        <!-- Displays the contents of a single conversation -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:name=".ui.conversation.ConversationActivity" android:parentActivityName="com.android.messaging.ui.conversationlist.ConversationListActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.ConversationActivity" android:windowSoftInputMode="stateHidden|adjustResize">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.conversationlist.ConversationListActivity"/>
        </activity>

        <!-- Blocked Participants -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/blocked_contacts_title" android:name=".ui.BlockedParticipantsActivity" android:parentActivityName="com.android.messaging.ui.conversationlist.ConversationListActivity" android:screenOrientation="user" android:theme="@style/BugleTheme">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.conversationlist.ConversationListActivity"/>
        </activity>

        <!-- Full-screen photo viewer -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/photo_view_activity_title" android:name=".ui.photoviewer.BuglePhotoViewActivity" android:screenOrientation="user" android:theme="@style/BuglePhotoViewTheme"/>

        <!-- Settings -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/settings_activity_title" android:name=".ui.appsettings.SettingsActivity" android:parentActivityName="com.android.messaging.ui.conversationlist.ConversationListActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.SettingsActivity">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.conversationlist.ConversationListActivity"/>
        </activity>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/advanced_settings_activity_title" android:name=".ui.appsettings.PerSubscriptionSettingsActivity" android:parentActivityName="com.android.messaging.ui.appsettings.SettingsActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.SettingsActivity">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.appsettings.SettingsActivity"/>
        </activity>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/general_settings_activity_title" android:name=".ui.appsettings.ApplicationSettingsActivity" android:parentActivityName="com.android.messaging.ui.appsettings.SettingsActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.SettingsActivity">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.appsettings.SettingsActivity"/>
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.NOTIFICATION_PREFERENCES"/>
            </intent-filter>
        </activity>

        <!-- Handles sharing intent -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:documentLaunchMode="always" android:excludeFromRecents="true" android:name=".ui.conversationlist.ShareIntentActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.DialogActivity">
            <intent-filter android:label="@string/share_intent_label">
                <action android:name="android.intent.action.SEND"/>
                <action android:name="android.intent.action.SEND_MULTIPLE"/>
                <category android:name="android.intent.category.DEFAULT"/>
                <data android:mimeType="text/plain"/>
                <data android:mimeType="text/x-vCard"/>
                <data android:mimeType="text/x-vcard"/>
                <data android:mimeType="image/*"/>
                <data android:mimeType="audio/*"/>
                <data android:mimeType="video/*"/>
                <data android:mimeType="application/ogg"/>
            </intent-filter>
        </activity>

        <!-- People & Options -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/people_and_options_activity_title" android:name=".ui.conversationsettings.PeopleAndOptionsActivity" android:parentActivityName="com.android.messaging.ui.conversation.ConversationActivity" android:screenOrientation="user" android:theme="@style/BugleTheme">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.conversation.ConversationActivity"/>
        </activity>

         <!-- License -->
        <activity android:exported="true" android:label="@string/menu_license" android:name=".ui.LicenseActivity" android:theme="@android:style/Theme.Holo.Light.Dialog">
        </activity>

        <!-- Message Forwarding -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/forward_message_activity_title" android:name=".ui.conversationlist.ForwardMessageActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.DialogActivity">
        </activity>

        <!-- Entry point for handling remote input/actions. Currently, this is only used by Android
             Wear to send voice replies. Since that uses PendingIntents, we don't need to export
             this activity. If we want other apps to be able to use this activity at will,
             we'll need to guard it with a signature-matching protected permission. We would also
             need to add an intent filter and remove the android:exported attribute. -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:exported="false" android:name=".ui.RemoteInputEntrypointActivity" android:screenOrientation="user" android:theme="@style/Invisible">
        </activity>

        <!-- VCard details -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/vcard_detail_activity_title" android:name=".ui.VCardDetailActivity" android:screenOrientation="user" android:theme="@style/BugleTheme">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value=".ui.conversation.ConversationActivity"/>
        </activity>

        <!-- Attachment chooser -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:label="@string/attachment_chooser_activity_title" android:name=".ui.attachmentchooser.AttachmentChooserActivity" android:parentActivityName="com.android.messaging.ui.conversation.ConversationActivity" android:screenOrientation="user" android:theme="@style/BugleTheme">
            <meta-data android:name="android.support.PARENT_ACTIVITY" android:value="com.android.messaging.ui.conversation.ConversationActivity"/>
        </activity>

        <!-- Test activity that we use to host fragments/views. Unfortunately, apparently necessary
             because Android framework test cases want activity to be in the instrumented package.
             See http://developer.android.com/reference/android/test/ActivityInstrumentationTestCase2.html
        -->
        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:name=".ui.TestActivity">
        </activity>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:exported="false" android:name=".ui.debug.DebugMmsConfigActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.DialogActivity">
        </activity>

        <provider android:authorities="com.android.messaging.datamodel.MessagingContentProvider" android:exported="false" android:label="@string/app_name" android:name=".datamodel.MessagingContentProvider">
        </provider>

        <provider android:authorities="com.android.messaging.datamodel.MmsFileProvider" android:exported="false" android:grantUriPermissions="true" android:name=".datamodel.MmsFileProvider"/>

        <provider android:authorities="com.android.messaging.datamodel.MediaScratchFileProvider" android:exported="false" android:grantUriPermissions="true" android:name=".datamodel.MediaScratchFileProvider"/>


        <!-- Action Services -->
        <service android:exported="true" android:name=".datamodel.action.ActionServiceImpl" android:permission="android.permission.BIND_JOB_SERVICE"/>
        <service android:exported="true" android:name=".datamodel.action.BackgroundWorkerService" android:permission="android.permission.BIND_JOB_SERVICE"/>

        <!-- Sms and Mms related items -->

        <!-- Intents for Notification and Pre-KLP Delivery -->
        <!-- Registered with the highest possible priority (max_int) -->
        <receiver android:enabled="false" android:name=".receiver.MmsWapPushReceiver" android:permission="android.permission.BROADCAST_WAP_PUSH">
            <intent-filter android:priority="2147483647">
                <action android:name="android.provider.Telephony.WAP_PUSH_RECEIVED"/>
                <data android:mimeType="application/vnd.wap.mms-message"/>
            </intent-filter>
        </receiver>
        <receiver android:enabled="false" android:name=".receiver.SmsReceiver" android:permission="android.permission.BROADCAST_SMS">
            <intent-filter android:priority="2147483647">
                <action android:name="android.provider.Telephony.SMS_RECEIVED"/>
            </intent-filter>
            <intent-filter android:priority="2147483647">
                <action android:name="android.provider.Telephony.MMS_DOWNLOADED"/>
            </intent-filter>
        </receiver>

        <!-- Intents for aborting SMS/MMS broadcasts pre-KLP -->
        <!-- Registered for a priority just ahead of inbox Messaging apps (2) -->
        <receiver android:enabled="false" android:name=".receiver.AbortMmsWapPushReceiver" android:permission="android.permission.BROADCAST_WAP_PUSH">
            <intent-filter android:priority="3">
                <action android:name="android.provider.Telephony.WAP_PUSH_RECEIVED"/>
                <data android:mimeType="application/vnd.wap.mms-message"/>
            </intent-filter>
        </receiver>
        <receiver android:enabled="false" android:name=".receiver.AbortSmsReceiver" android:permission="android.permission.BROADCAST_SMS">
            <intent-filter android:priority="3">
                <action android:name="android.provider.Telephony.SMS_RECEIVED"/>
            </intent-filter>
        </receiver>

        <!-- Intents for KLP+ Delivery -->
        <receiver android:name=".receiver.MmsWapPushDeliverReceiver" android:permission="android.permission.BROADCAST_WAP_PUSH">
            <intent-filter>
                <action android:name="android.provider.Telephony.WAP_PUSH_DELIVER"/>
                <data android:mimeType="application/vnd.wap.mms-message"/>
            </intent-filter>
        </receiver>
        <receiver android:name=".receiver.SmsDeliverReceiver" android:permission="android.permission.BROADCAST_SMS">
            <intent-filter>
                <action android:name="android.provider.Telephony.SMS_DELIVER"/>
            </intent-filter>
        </receiver>

        <receiver android:exported="false" android:name=".receiver.SendStatusReceiver">
            <intent-filter>
                <action android:name="com.android.messaging.receiver.SendStatusReceiver.MESSAGE_SENT"/>
                <data android:scheme="content"/>
            </intent-filter>
            <intent-filter>
                <action android:name="com.android.messaging.receiver.SendStatusReceiver.MESSAGE_DELIVERED"/>
            </intent-filter>
            <intent-filter>
                <action android:name="com.android.messaging.receiver.SendStatusReceiver.MMS_SENT"/>
                <data android:scheme="content"/>
            </intent-filter>
            <intent-filter>
                <action android:name="com.android.messaging.receiver.SendStatusReceiver.MMS_DOWNLOADED"/>
                <data android:scheme="content"/>
            </intent-filter>
        </receiver>

        <service android:exported="true" android:name=".datamodel.NoConfirmationSmsSendService" android:permission="android.permission.SEND_RESPOND_VIA_MESSAGE">
            <intent-filter>
                <action android:name="android.intent.action.RESPOND_VIA_MESSAGE"/>
                <category android:name="android.intent.category.DEFAULT"/>
                <data android:scheme="sms"/>
                <data android:scheme="smsto"/>
                <data android:scheme="mms"/>
                <data android:scheme="mmsto"/>
            </intent-filter>
        </service>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:excludeFromRecents="true" android:label="@string/class_0_message_activity" android:launchMode="singleTask" android:name=".ui.ClassZeroActivity" android:screenOrientation="user" android:theme="@style/BugleTheme.DialogActivity">
        </activity>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:name=".ui.SmsStorageLowWarningActivity" android:theme="@style/Translucent"/>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:name=".ui.appsettings.ApnSettingsActivity" android:parentActivityName="com.android.messaging.ui.appsettings.SettingsActivity" android:screenOrientation="user" android:theme="@style/BugleTheme"/>

        <activity android:configChanges="orientation|screenSize|keyboardHidden" android:name=".ui.appsettings.ApnEditorActivity" android:parentActivityName="com.android.messaging.ui.appsettings.ApnSettingsActivity" android:screenOrientation="user" android:theme="@style/BugleTheme"/>

        <receiver android:name=".receiver.StorageStatusReceiver">
            <intent-filter>
                <action android:name="android.intent.action.DEVICE_STORAGE_LOW"/>
            </intent-filter>
            <intent-filter>
                <action android:name="android.intent.action.DEVICE_STORAGE_OK"/>
            </intent-filter>
        </receiver>

        <receiver android:name=".receiver.BootAndPackageReplacedReceiver">
            <intent-filter>
                <action android:name="android.intent.action.BOOT_COMPLETED"/>
                <action android:name="android.intent.action.MY_PACKAGE_REPLACED"/>
                </intent-filter>
        </receiver>

        <!-- Broadcast receiver that will be notified to reset notifications -->
        <receiver android:exported="false" android:name=".receiver.NotificationReceiver">
        </receiver>

        <!-- Broadcast receiver that will be notified for ActionService alarms. -->
        <receiver android:exported="false" android:name=".datamodel.action.ActionServiceImpl$PendingActionReceiver">
            <intent-filter>
                <action android:name="com.android.messaging.datamodel.PENDING_ACTION"/>
            </intent-filter>
        </receiver>

        <receiver android:name=".receiver.DefaultSmsSubscriptionChangeReceiver">
            <intent-filter>
                <action android:name="android.telephony.action.DEFAULT_SMS_SUBSCRIPTION_CHANGED"/>
            </intent-filter>
        </receiver>

        <!-- Widget that displays the conversation list -->
        <receiver android:label="@string/widget_conversation_name" android:name=".widget.BugleWidgetProvider">
            <intent-filter>
                <action android:name="android.appwidget.action.APPWIDGET_UPDATE"/>
            </intent-filter>
            <meta-data android:name="android.appwidget.provider" android:resource="@xml/widget_conversation_list"/>
        </receiver>

        <!-- Widget that displays the messages of a single conversation -->
        <receiver android:label="@string/widget_conversation_name" android:name=".widget.WidgetConversationProvider">
            <intent-filter>
                <action android:name="android.appwidget.action.APPWIDGET_UPDATE"/>
            </intent-filter>
            <meta-data android:name="android.appwidget.provider" android:resource="@xml/widget_conversation"/>
        </receiver>

        <service android:exported="false" android:name=".widget.WidgetConversationListService" android:permission="android.permission.BIND_REMOTEVIEWS"/>

        <service android:exported="false" android:name=".widget.WidgetConversationService" android:permission="android.permission.BIND_REMOTEVIEWS"/>

        <activity android:label="@string/app_name" android:name=".ui.WidgetPickConversationActivity" android:theme="@style/BugleTheme">
            <intent-filter>
                <action android:name="android.appwidget.action.APPWIDGET_CONFIGURE"/>
            </intent-filter>
        </activity>

        <service android:name="androidx.appcompat.mms.MmsService"/>
    </application>

</manifest>
