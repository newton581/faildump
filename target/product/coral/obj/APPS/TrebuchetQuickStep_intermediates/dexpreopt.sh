#!/bin/bash

err() {
  errno=$?
  echo "error: $0:$1 exited with status $errno" >&2
  echo "error in command:" >&2
  sed -n -e "$1p" $0 >&2
  if [ "$errno" -ne 0 ]; then
    exit $errno
  else
    exit 1
  fi
}

trap 'err $LINENO' ERR

mkdir -p out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/oat/arm64

rm -f out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/oat/arm64/package.odex

class_loader_context_arg=--class-loader-context=\&

stored_class_loader_context_arg=""

ANDROID_LOG_TAGS="*:e" out/soong/host/linux-x86/bin/dex2oatd --avoid-storing-invocation --write-invocation-to=out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/oat/arm64/package.invocation --runtime-arg -Xms64m --runtime-arg -Xmx512m --runtime-arg -Xbootclasspath:out/soong/coral/dex_bootjars_input/core-oj.jar:out/soong/coral/dex_bootjars_input/core-libart.jar:out/soong/coral/dex_bootjars_input/okhttp.jar:out/soong/coral/dex_bootjars_input/bouncycastle.jar:out/soong/coral/dex_bootjars_input/apache-xml.jar:out/soong/coral/dex_bootjars_input/framework.jar:out/soong/coral/dex_bootjars_input/ext.jar:out/soong/coral/dex_bootjars_input/telephony-common.jar:out/soong/coral/dex_bootjars_input/voip-common.jar:out/soong/coral/dex_bootjars_input/ims-common.jar:out/soong/coral/dex_bootjars_input/android.test.base.jar --runtime-arg -Xbootclasspath-locations:/apex/com.android.runtime/javalib/core-oj.jar:/apex/com.android.runtime/javalib/core-libart.jar:/apex/com.android.runtime/javalib/okhttp.jar:/apex/com.android.runtime/javalib/bouncycastle.jar:/apex/com.android.runtime/javalib/apache-xml.jar:/system/framework/framework.jar:/system/framework/ext.jar:/system/framework/telephony-common.jar:/system/framework/voip-common.jar:/system/framework/ims-common.jar:/system/framework/android.test.base.jar ${class_loader_context_arg} ${stored_class_loader_context_arg} --boot-image=out/soong/coral/dex_bootjars/system/framework/boot.art --dex-file=$1 --dex-location=/product/priv-app/TrebuchetQuickStep/TrebuchetQuickStep.apk --oat-file=out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/oat/arm64/package.odex --android-root=out/empty --instruction-set=arm64 --instruction-set-variant=generic --instruction-set-features=default --no-generate-debug-info --generate-build-id --abort-on-hard-verifier-error --force-determinism --no-inline-from=core-oj.jar --copy-dex-files=false --compiler-filter=speed --generate-mini-debug-info --compilation-reason=prebuilt

rm -rf out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install

mkdir -p out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install

mkdir -p out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install/product/priv-app/TrebuchetQuickStep/oat/arm64

cp -f out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/oat/arm64/package.odex out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install/product/priv-app/TrebuchetQuickStep/oat/arm64/TrebuchetQuickStep.odex

mkdir -p out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install/product/priv-app/TrebuchetQuickStep/oat/arm64

cp -f out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/oat/arm64/package.vdex out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install/product/priv-app/TrebuchetQuickStep/oat/arm64/TrebuchetQuickStep.vdex

out/soong/host/linux-x86/bin/soong_zip -o $2 -C out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install -D out/target/product/coral/obj/APPS/TrebuchetQuickStep_intermediates/dexpreopt_install

rm -f $2.d
echo -n $2 > $2.d
cat >> $2.d <<'EOF'
: \
    out/soong/host/linux-x86/bin/dex2oatd \
    out/soong/host/linux-x86/bin/soong_zip \
    out/soong/coral/dex_bootjars/system/framework/arm64/boot.art \
    out/soong/coral/dex_bootjars_input/android.test.base.jar \
    out/soong/coral/dex_bootjars_input/apache-xml.jar \
    out/soong/coral/dex_bootjars_input/bouncycastle.jar \
    out/soong/coral/dex_bootjars_input/core-libart.jar \
    out/soong/coral/dex_bootjars_input/core-oj.jar \
    out/soong/coral/dex_bootjars_input/ext.jar \
    out/soong/coral/dex_bootjars_input/framework.jar \
    out/soong/coral/dex_bootjars_input/ims-common.jar \
    out/soong/coral/dex_bootjars_input/okhttp.jar \
    out/soong/coral/dex_bootjars_input/telephony-common.jar \
    out/soong/coral/dex_bootjars_input/voip-common.jar \

EOF
